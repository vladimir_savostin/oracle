package analyst

import (
	"github.com/astaxie/beego/logs"
	"github.com/go-pg/pg"
	"oracle/config"
	"oracle/database"
	"time"
)

//Analyst - сущность, которая проводит проверку сформированных сигналов (сбылись ли они или нет)
type Analyst struct {
	DB *pg.DB //БД

	workPeriod time.Duration
}

//timeFramesToTime - отображение таймфреймов в нотации оракула во время time.Time
var timeFramesToTime = map[string]time.Duration{
	"15": 15 * time.Minute,
	"30": 30 * time.Minute,
	"1H": 1 * time.Hour,
	"5H": 5 * time.Hour,
	"1D": 24 * time.Hour,
	"1W": 7 * 24 * time.Hour,
	"1M": 31 * 24 * time.Hour,
}

//bullTrends - список значений параметра Description, которые говорят о предстоящем росте цены
//используется при анализе цены после формирования сигнала
var bullTrends = []string{
	"Бычий разворот",
	"Продолжение бычьего тренда",
}

//bearTrends - список значений параметра Description, которые говорят о предстоящем падении цены
//используется при анализе цены после формирования сигнала
var bearTrends = []string{
	"Медвежий разворот",
	"Продолжение медвежьего тренда",
}

func NewAnalyst(settings *config.Settings) (a *Analyst, err error) {
	a = new(Analyst)
	a.DB, err = database.NewFor(settings, "ANALYST")
	if err != nil {
		return nil, err
	}
	a.workPeriod = time.Second * time.Duration(settings.Analyst.WorkPeriod)
	logs.Warn("Initialized the new collector")
	return
}

func (analyst *Analyst) CloseDB() {
	err := analyst.DB.Close()
	if err != nil {
		logs.Error("CloseDB: %s", err)
	}
	logs.Warn("Closed the database %s", analyst.DB.Options().Database)
}
