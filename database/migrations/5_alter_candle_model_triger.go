package main

import (
	"github.com/astaxie/beego/logs"
	"github.com/go-pg/migrations"
)

func init() {
	migrations.Register(func(db migrations.DB) error {
		logs.Warn("alter candle_model_trigger script...")
		_, err := db.Exec(alter_trigger_candle_model)
		return err
	}, func(db migrations.DB) error {
		return nil
	})
}

//добавление в функцию отправки уведомления полезных данных
const alter_trigger_candle_model = `
CREATE OR REPLACE FUNCTION notify_strategist() RETURNS trigger AS $$
	BEGIN
		--отправляем сообщение только если получившаяся запись является необработанной (is_done!=true)
		IF NEW.is_done IS NOT TRUE THEN
			PERFORM pg_notify('candle_models_chan', format('<- check: id:%s; instrument:%s; candle_model:%s; date:%s', NEW.id, NEW.instrument, NEW.model_name, NEW.date));
			RETURN NULL;
		END IF;
		RETURN NULL;
	END;
$$ LANGUAGE plpgsql;
`
