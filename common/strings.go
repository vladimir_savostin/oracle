package common

import "strings"

//FormatJSON форматирует JSON сообщение в более удобочитаемый вид для отправки в телеграм канал
//Производится базовое форматирование, убираются спецсиволы {}"
func FormatJSON(source string) (result string) {
	result = strings.Replace(source, "{\n", "", -1)
	result = strings.Replace(result, "\n}", "", -1)
	result = strings.Replace(result, "\"", "", -1)
	return
}
