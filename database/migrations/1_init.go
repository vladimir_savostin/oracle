package main

import (
	"github.com/astaxie/beego/logs"
	"github.com/go-pg/migrations"
)

func init() {
	migrations.Register(func(db migrations.DB) error {
		logs.Warn("create script...")
		_, err := db.Exec(create)
		return err
	}, func(db migrations.DB) error {
		logs.Warn("drop script...")
		_, err := db.Exec(drop)
		return err
	})
}

const create = `
CREATE TABLE "candle_models" (
	"id" serial NOT NULL,
	"instrument" varchar NOT NULL,
	"time_frame" varchar NOT NULL,
	"reliability" integer NOT NULL,
	"model_name" varchar NOT NULL,
	"candle_number" varchar NOT NULL,
	"description" varchar NOT NULL,
	"date" varchar NOT NULL,
	"is_done" boolean,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	CONSTRAINT candle_models_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE "indicators" (
	"instrument" varchar NOT NULL,
	"minute15" varchar,
	"minute30" varchar,
	"hour1" varchar,
	"hour5" varchar,
	"day" varchar,
	"week" varchar,
	"month" varchar,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	CONSTRAINT indicators_pk PRIMARY KEY ("instrument")
) WITH (
  OIDS=FALSE
);

CREATE TABLE "moving_averages" (
	"instrument" varchar NOT NULL,
	"minute15" varchar,
	"minute30" varchar,
	"hour1" varchar,
	"hour5" varchar,
	"day" varchar,
	"week" varchar,
	"month" varchar,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	CONSTRAINT moving_averages_pk PRIMARY KEY ("instrument")
) WITH (
  OIDS=FALSE
);

CREATE TABLE "indicators_summary" (
	"instrument" varchar NOT NULL,
	"minute15" varchar,
	"minute30" varchar,
	"hour1" varchar,
	"hour5" varchar,
	"day" varchar,
	"week" varchar,
	"month" varchar,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	CONSTRAINT indicators_summary_pk PRIMARY KEY ("instrument")
) WITH (
  OIDS=FALSE
);

CREATE TABLE "prices" (
	"instrument" varchar NOT NULL,
	"price" varchar NOT NULL,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	CONSTRAINT prices_pk PRIMARY KEY ("instrument")
) WITH (
  OIDS=FALSE
);
`

const drop = `
DROP TABLE IF EXISTS "candle_models";
DROP TABLE IF EXISTS "indicators";
DROP TABLE IF EXISTS "moving_averages";
DROP TABLE IF EXISTS "indicators_summary";
DROP TABLE IF EXISTS "prices";
`
