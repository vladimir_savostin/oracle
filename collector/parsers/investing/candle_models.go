package investing

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"oracle/collector/models/investing"
	"oracle/common"
	"strings"
)

const (
	//нумерация колонок таблицы свечных моделей
	NAME          = 1 //имя инсрумента
	TIME_FRAME    = 2 //временной промежуток свечи
	RELIABILITY   = 3 //надежность сигнала
	MODEL         = 4 //свечная модель
	CANDLE_NUMBER = 5 //номер свечи
	ICON          = 6 //иконка, отображающая тренд
)

// ParseCandleModels - парсит html страницу свечных моделей, находя нужную информацию
func (p *Parser) ParseCandleModels() (models []investing.CandleModel, err error) {
	headers := make(map[string]string)
	headers["Cookie"] = fmt.Sprintf("ses_id=%s", p.MainAccount.Cookie["ses_id"])
	resp, err := common.SendHTTPRequest("GET", URL_CANDLESTICK_PATERNS, headers, nil)
	if err != nil {
		return nil, err
	}
	document, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return nil, err
	}
	document.Find("tr").Each(func(i int, s *goquery.Selection) { //находим все элементы, являющиеся строками таблицы и пробегаем по каждой
		if strings.TrimSpace(s.Text()) == "Завершенные модели" { //если строка содержит необходимый текст, значит нашли то что нужно и ищем следующие(на том же уровне иерархии) строки
			p.getCandleModelsOfSameType(s, &models)
		}
	})
	return models, nil
}

// getCandleModelsOfSameType - Находит все сигналы одного типа (имеется ввиду новизна) начиная с текущей строки s таблицы
// и до строки, обозначающей следующий тип сигналов
// Существует два типа сигналов ("Новые модели" и "Завершенные модели")
func (p *Parser) getCandleModelsOfSameType(s *goquery.Selection, models *[]investing.CandleModel) {
	s.NextAll().EachWithBreak(func(i int, s *goquery.Selection) bool { //берем все следующие строки на этом же уровне иерархии и работаем с каждой
		if _, ok := s.Children().Attr("colspan"); ok {
			return false //если натыкаемся на строку, у которой потомок имеет параметр colspan (объединение ячеек), значит дошли до следующего типа и вынуждены закончить цикл
		}
		var model investing.CandleModel
		model.Date, _ = s.Attr("data-time")                   //у каждой строки в списке завершенных моделей есть параметр содержащий дату формирования, берем его; у новых он  тоже есть, но пустой
		s.Children().Each(func(i int, s *goquery.Selection) { //работая с каждой отдельной строкой берем ее потомков (н-р для <tr> потомком будет <td>) и работаем с каждым потомком
			switch i { //параметр i определяет в данном случае, с каким столбцом работаем
			case NAME:
				model.Instrument = s.Text()
			case TIME_FRAME:
				model.TimeFrame = s.Text()
			case RELIABILITY:
				model.Reliability = p.countReliability(s)
			case MODEL:
				model.ModelName = s.Text()
			case CANDLE_NUMBER:
				model.CandleNumber = s.Text()
			case ICON:
				model.Description, _ = s.Attr("title")
				//т.к столбец ICON является последним, то на данном шаге итерации упакованный row сохраняем в models
				//и очищаем значения name и row на всякий случай
				*models = append(*models, model)
				model.ClearData()
			}
		})
		return true
	})
}

//countReliability - подсчитывает надежность по количеству темных звезд в ячейке столбца "надежность"
func (p *Parser) countReliability(s *goquery.Selection) int {
	count := 0
	s.Children().Each(func(_ int, s *goquery.Selection) {
		if class, _ := s.Attr("class"); class == "reliabilityStarDarkIcon" {
			count++
		}
	})
	return count
}
