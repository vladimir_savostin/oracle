package database

import (
	"fmt"
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"time"
)

const ( //отображение значений таймфреймов в нотации оракула на названия столбцов в базе
	Minute15 = "15"
	Minute30 = "30"
	Hour1    = "1H"
	Hour5    = "5H"
	Day      = "1D"
	Week     = "1W"
	Month    = "1M"
)

//Indicator - таблица базы данных, хранящая актуальные сигналы индикаторов по всем таймфреймам для конкретных интсрументов
type Indicator struct {
	Instrument string
	Minute15   string
	Minute30   string
	Hour1      string
	Hour5      string
	Day        string
	Week       string
	Month      string
	CreatedAt  time.Time `sql:"created_at"`
	UpdatedAt  time.Time `sql:"updated_at"`
}

//GetAtTimeFrame - возвращает значение индикатора для определенного таймфрейма
func (i *Indicator) GetAtTimeFrame(timeFrame string) string {
	switch timeFrame {
	case Minute15:
		return i.Minute15
	case Minute30:
		return i.Minute30
	case Hour1:
		return i.Hour1
	case Hour5:
		return i.Hour5
	case Day:
		return i.Day
	case Week:
		return i.Week
	case Month:
		return i.Month
	}
	return fmt.Sprintf("invalid timeFrame - %s", timeFrame)
}

//SaveOrUpdate - сохраняет в базу, если же по данному иструменту уже есть запись, то обновляет ее
func (i *Indicator) SaveOrUpdate(db *pg.DB) error {
	_, err := db.Model(i).
		OnConflict("(instrument) DO UPDATE").
		Set("minute15=?minute15").
		Set("minute30=?minute30").
		Set("hour1=?hour1").
		Set("hour5=?hour5").
		Set("day=?day").
		Set("week=?week").
		Set("month=?month").
		Set("updated_at=?", time.Now()).
		Insert()
	if err != nil {
		return err
	}
	return nil
}

//FindWhere - находит необходимую сущность в базе с применением различных условий
func (i *Indicator) FindWhere(db *pg.DB, fn func(query *orm.Query) (*orm.Query, error)) error {
	return db.Model(i).
		Apply(fn).
		Select()
}

//BeforeInsert - действия, проводимые перед сохранением в базу
func (i *Indicator) BeforeInsert(db orm.DB) error {
	if i.CreatedAt.IsZero() {
		i.CreatedAt = time.Now()
	}
	return nil
}

//MovingAverage - таблица базы данных, хранящая актуальные сигналы скользящих средних по всем таймфреймам для конкретных интсрументов
type MovingAverage struct {
	Instrument string
	Minute15   string
	Minute30   string
	Hour1      string
	Hour5      string
	Day        string
	Week       string
	Month      string
	CreatedAt  time.Time
	UpdatedAt  time.Time
}

//GetAtTimeFrame - возвращает значение индикатора для определенного таймфрейма
func (ma *MovingAverage) GetAtTimeFrame(timeFrame string) string {
	switch timeFrame {
	case Minute15:
		return ma.Minute15
	case Minute30:
		return ma.Minute30
	case Hour1:
		return ma.Hour1
	case Hour5:
		return ma.Hour5
	case Day:
		return ma.Day
	case Week:
		return ma.Week
	case Month:
		return ma.Month
	}
	return fmt.Sprintf("invalid timeFrame - %s", timeFrame)
}

//SaveOrUpdate - сохраняет в базу, если же по данному иструменту уже есть запись, то обновляет ее
func (ma *MovingAverage) SaveOrUpdate(db *pg.DB) error {
	_, err := db.Model(ma).
		OnConflict("(instrument) DO UPDATE").
		Set("minute15=?minute15").
		Set("minute30=?minute30").
		Set("hour1=?hour1").
		Set("hour5=?hour5").
		Set("day=?day").
		Set("week=?week").
		Set("month=?month").
		Set("updated_at=?", time.Now()).
		Insert()
	if err != nil {
		return err
	}
	return nil
}

//FindWhere - находит необходимую сущность в базе с применением различных условий
func (ma *MovingAverage) FindWhere(db *pg.DB, fn func(query *orm.Query) (*orm.Query, error)) error {
	return db.Model(ma).
		Apply(fn).
		Select()
}

//BeforeInsert - действия, проводимые перед сохранением в базу
func (ma *MovingAverage) BeforeInsert(db orm.DB) error {
	if ma.CreatedAt.IsZero() {
		ma.CreatedAt = time.Now()
	}
	return nil
}

//MovingAverage - таблица базы данных, резюмирующая значения сигналов по индикаторам и по скользящим средним
//по всем таймфреймам для конкретных интсрументов
type IndicatorSummary struct {
	tableName  struct{} `sql:"indicators_summary"`
	Instrument string
	Minute15   string
	Minute30   string
	Hour1      string
	Hour5      string
	Day        string
	Week       string
	Month      string
	CreatedAt  time.Time
	UpdatedAt  time.Time
}

//GetAtTimeFrame - возвращает значение индикатора для определенного таймфрейма
func (sum *IndicatorSummary) GetAtTimeFrame(timeFrame string) string {
	switch timeFrame {
	case Minute15:
		return sum.Minute15
	case Minute30:
		return sum.Minute30
	case Hour1:
		return sum.Hour1
	case Hour5:
		return sum.Hour5
	case Day:
		return sum.Day
	case Week:
		return sum.Week
	case Month:
		return sum.Month
	}
	return fmt.Sprintf("invalid timeFrame - %s", timeFrame)
}

//SaveOrUpdate - сохраняет в базу, если же по данному иструменту уже есть запись, то обновляет ее
func (sum *IndicatorSummary) SaveOrUpdate(db *pg.DB) error {
	_, err := db.Model(sum).
		OnConflict("(instrument) DO UPDATE").
		Set("minute15=?minute15").
		Set("minute30=?minute30").
		Set("hour1=?hour1").
		Set("hour5=?hour5").
		Set("day=?day").
		Set("week=?week").
		Set("month=?month").
		Set("updated_at=?", time.Now()).
		Insert()
	if err != nil {
		return err
	}
	return nil
}

//FindWhere - находит необходимую сущность в базе с применением различных условий
func (sum *IndicatorSummary) FindWhere(db *pg.DB, fn func(query *orm.Query) (*orm.Query, error)) error {
	return db.Model(sum).
		Apply(fn).
		Select()
}

//BeforeInsert - действия, проводимые перед сохранением в базу
func (sum *IndicatorSummary) BeforeInsert(db orm.DB) error {
	if sum.CreatedAt.IsZero() {
		sum.CreatedAt = time.Now()
	}
	return nil
}
