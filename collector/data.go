package collector

import (
	"github.com/astaxie/beego/logs"
	"github.com/go-pg/pg"
	"oracle/collector/parsers/investing"
	"oracle/config"
	"oracle/database"
	"sync"
	"time"
)

type Collector struct {
	DB      *pg.DB //БД
	Parsers []*investing.Parser
	Ready   chan bool //канал, через который коллектор уведомляет стратега о том что он обновил всю информацию в БД

	workPeriod time.Duration   //период работы коллектора
	wg         *sync.WaitGroup //wait group для управления горутинами поиска иформации
}

func NewCollector(settings *config.Settings) (c *Collector, err error) {
	c = new(Collector)
	c.DB, err = database.NewFor(settings, "COLLECTOR")
	if err != nil {
		return nil, err
	}
	for _, credentials := range settings.Collector.Parsers {
		parser, err := investing.NewParser(credentials)
		if err != nil {
			return nil, err
		}
		c.Parsers = append(c.Parsers, parser)
	}
	c.Ready = make(chan bool)
	c.workPeriod = time.Second * time.Duration(settings.Collector.WorkPeriod)
	logs.Warn("Initialized the new collector")
	return
}

func (collector *Collector) checkError(err error) {
	if err != nil {
		logs.Error(err)
		return
	}
}

func (collector *Collector) CloseDB() {
	err := collector.DB.Close()
	if err != nil {
		logs.Error("CloseDB: %s", err)
	}
	logs.Warn("Closed the database %s", collector.DB.Options().Database)
}
