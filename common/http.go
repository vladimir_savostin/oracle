package common

import (
	"encoding/json"
	"errors"
	"github.com/astaxie/beego/logs"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
)

func SendHTTPRequest(method, path string, headers map[string]string, body io.Reader) (http.Response, error) {
	result := strings.ToUpper(method)
	if result != "POST" && result != "GET" && result != "DELETE" {
		return http.Response{}, errors.New("Invalid HTTP method specified.")
	}
	req, err := http.NewRequest(method, path, body)
	if err != nil {
		return http.Response{}, err
	}
	for k, v := range headers {
		req.Header.Add(k, v)
	}
	logs.Debug(req)
	//request, err := ioutil.ReadAll(req.Body)
	//fmt.Println(string(request))
	httpClient := &http.Client{}
	resp, err := httpClient.Do(req)
	if err != nil {
		return http.Response{}, err
	}
	//logs.Debug(resp)
	//contents, err := ioutil.ReadAll(resp.Body)
	//defer resp.Body.Close()
	//logs.Debug(string(contents))
	if err != nil {
		return http.Response{}, err
	}
	return *resp, nil
}

func GetResponseBody(resp http.Response, payload interface{}) error {
	buf, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	err = json.Unmarshal(buf, &payload)
	if err != nil {
		return err
	}
	return nil
}
