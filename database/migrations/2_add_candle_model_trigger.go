package main

import (
	"github.com/astaxie/beego/logs"
	"github.com/go-pg/migrations"
)

func init() {
	migrations.Register(func(db migrations.DB) error {
		logs.Warn("candle_model_trigger script...")
		_, err := db.Exec(create_trigger_candle_model)
		return err
	}, func(db migrations.DB) error {
		logs.Warn("drop candle_model_trigger script...")
		_, err := db.Exec(drop_trigger_candle_model)
		return err
	})
}

const create_trigger_candle_model = `
CREATE OR REPLACE FUNCTION notify_strategist() RETURNS trigger AS $$
	BEGIN
		--отправляем сообщение только если получившаяся запись является необработанной (is_done!=true)
		IF NEW.is_done IS NOT TRUE THEN
			PERFORM pg_notify('candle_models_chan', 'check');
			RETURN NULL;
		END IF;
		RETURN NULL;
	END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER check_candle_model 
AFTER INSERT OR UPDATE ON candle_models
FOR EACH ROW 
EXECUTE PROCEDURE notify_strategist();
`
const drop_trigger_candle_model = `
DROP TRIGGER check_candle_model ON candle_models;
DROP FUNCTION notify_strategist();
`
