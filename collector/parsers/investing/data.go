package investing

const (
	//paths:
	URL_AUTH                = "https://ru.investing.com/members-admin/auth/signInByEmail/"
	URL_CANDLESTICK_PATERNS = "https://ru.investing.com/technical/candlestick-patterns"
	URL_INDICATORS          = "https://ru.investing.com/technical/personalized-quotes-technical-summary"
)

//Parser - занимается парсингом страниц
type Parser struct {
	MainAccount  *Account //основной аккаунт для получения сигналов
	ExtraAccount *Account //дополнительный аккаунт для поддержки основного
}

//Account - хранит данные по конкретному аккаунту
type Account struct {
	Credentials
	//Cookie - отображение, которое хранит все cookie параметры для конкретного экземпляра парсера,
	//эти параметры Parser получет при авторизации Authorize()
	//ключ=название заголовка cookie
	Cookie map[string]string
}

//Credentials - хранит данные для авторизации
type Credentials struct {
	Email    string
	Password string
}

// authResponse - хранит тело ответа при авторизации
type authResponse struct {
	Status  string `json:"response"`
	Message string `json:"msg"`
}
