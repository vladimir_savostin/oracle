package investing

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/astaxie/beego/logs"
	"net/http"
	"net/url"
	"oracle/common"
	"oracle/config"
	"strings"
)

func NewParser(credentials *config.Parser) (*Parser, error) {
	parser := new(Parser)
	err := parser.Init(credentials)
	if err != nil {
		return nil, err
	}
	err = parser.Authorize()
	if err != nil {
		return nil, err
	}
	logs.Warn("Initialized the new parser: main_account=%s, extra_account=%s",
		parser.MainAccount.Email, parser.ExtraAccount.Email)
	return parser, nil
}

//Init - инициализация парсера
func (p *Parser) Init(credentials *config.Parser) error {
	p.MainAccount = new(Account)
	p.ExtraAccount = new(Account)
	p.MainAccount.Email = credentials.MainEmail
	p.MainAccount.Password = credentials.MainPassword
	p.MainAccount.Cookie = make(map[string]string)
	p.ExtraAccount.Email = credentials.ExtraEmail
	p.ExtraAccount.Password = credentials.ExtraPassword
	p.ExtraAccount.Cookie = make(map[string]string)
	return nil
}

// Авторизация парсера, при авторизации парсер получит cookie для основного и дополнительного аккаунта
func (p *Parser) Authorize() error {
	err := p.authorizeAccount(p.MainAccount)
	if err != nil {
		return err
	}
	return p.authorizeAccount(p.ExtraAccount)
}

//authorizeAccount - авторизация конкретного аккаунта
func (p *Parser) authorizeAccount(account *Account) error {
	values := url.Values{}
	values.Set("email", account.Email)
	values.Set("password", account.Password)
	resp, err := http.PostForm(URL_AUTH, values)
	if err != nil {
		return err
	}
	var body authResponse
	err = common.GetResponseBody(*resp, &body)
	if err != nil {
		return err
	}
	if body.Status != "OK" {
		return fmt.Errorf("authorize %s: status: %s; msg: %s", account.Email, body.Status, body.Message)
	}
	cookies := resp.Header["Set-Cookie"]
	sesID, err := p.findCookie(cookies, "ses_id", "deleted")
	if err != nil {
		return err
	}
	account.Cookie["ses_id"] = sesID
	return nil
}

//sendRequest - посылает авторизованный запрос на страницу, котороую будем парсить и возвращает  html-документ в формате goquery.Document
func (p *Parser) sendRequest(path string, account *Account) (*goquery.Document, error) {
	headers := make(map[string]string)
	sesID, ok := account.Cookie["ses_id"]
	if !ok {
		return nil, fmt.Errorf("no cookie found for %s", account.Email)
	}
	headers["Cookie"] = fmt.Sprintf("ses_id=%s", sesID)
	resp, err := common.SendHTTPRequest("GET", URL_INDICATORS, headers, nil)
	if err != nil {
		return nil, err
	}
	return goquery.NewDocumentFromReader(resp.Body)
}

// findCookie находит в заголовках cookies нужный параметр parameter и возвращает его значение
// параметр defaultValue определяет значение искомого параметра по умолчанию, которое также может содержаться в cookies,
// если defaultValue присутствует в cookies, то оно должно игнорироваться
func (p *Parser) findCookie(cookies []string, paramName, defaultValue string) (string, error) {
	for _, v := range cookies { //пробегаем по параметрам cookies
		params := strings.Split(v, ";") //каждый элемент среза cookies может содержать несколько параметров, поэтому дополнительно делим их на отдельные параметры
		for _, param := range params {  //теперь работаем с каждым параметром отдельно
			param := strings.TrimSpace(param) //уберем пробелы, котрые возможно остались после дополнительонго разделения на отдельные параметры
			if strings.HasPrefix(param, paramName) {
				if paramValue := strings.TrimPrefix(param, paramName+"="); paramValue != defaultValue {
					return paramValue, nil //возвращаем искомое значение, если оно не равно defaultValue
				}
			}
		}
	}
	return "", fmt.Errorf("findCookie: required param not found, paramName=%s", paramName)
}
