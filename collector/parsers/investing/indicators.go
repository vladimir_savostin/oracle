package investing

import (
	"github.com/PuerkitoBio/goquery"
	"oracle/collector/models/investing"
)

//отображение времени в документе html на время используемое оракулом
var dataPeriods = map[string]string{
	"900":   "15",
	"1800":  "30",
	"3600":  "1H",
	"18000": "5H",
	"86400": "1D",
	"week":  "1W",
	"month": "1M",
}

//ParseIndicators - извлекает с html-страницы все индикаторы (индикаторы, скользящие средние и резюмэ) и кладет их в investing.Indicators
func (p *Parser) ParseIndicators() (investing.Indicators, error) {
	indicatorsMap := make(investing.Indicators)
	err := p.parseIndicatorsWithAccount(p.MainAccount, indicatorsMap)
	if err != nil {
		return nil, err
	}
	err = p.parseIndicatorsWithAccount(p.ExtraAccount, indicatorsMap)
	if err != nil {
		return nil, err
	}
	return indicatorsMap, nil
}

//parseIndicatorsWithAccount - парсит индикаторы используя конкретный аккаунт
func (p *Parser) parseIndicatorsWithAccount(account *Account, indicatorsMap investing.Indicators) error {
	document, err := p.sendRequest(URL_INDICATORS, account)
	if err != nil {
		return err
	}
	timeFrames := p.getTimeFrames(document)
	document.Find("tr").Each(func(trNumber int, tr *goquery.Selection) { //находим все строки и работаем с каждой
		if atrr, _ := tr.Attr("data-row-type"); atrr == "movingAverages" { //если это строка скользящих средних, то:
			instrument, _ := p.getInstrumentAndPrice(tr) //получаем из нее имя инструмента и текущую цену
			movingAverages := p.getMovingAverages(tr)    //получаем и скользащие средние
			indicators := p.getIndicators(tr)            //находим индикаторы
			summary := p.getSummary(tr)                  //находим резюмэ
			//сохраняем найденные значения
			innerMap := make(map[string]investing.Indicator)
			for i, timeFrame := range timeFrames {
				innerMap[timeFrame] = investing.Indicator{
					MovingAverages: movingAverages[i],
					Indicators:     indicators[i],
					Summary:        summary[i],
				}
				indicatorsMap.ADD(instrument, innerMap)
			}
		}

	})
	return nil
}

//getTimeFrames - возвращает срез временных интервалов, представленных в таблице с индикаторами
//данные в срезе представлены слева направо как и на исходной странице
func (p *Parser) getTimeFrames(document *goquery.Document) []string {
	timeFrames := make([]string, 0, 4)
	document.Find("th").Each(func(i int, s *goquery.Selection) { //ищем все заголовки
		if t, ok := s.Attr("data-period"); ok { //у которых есть аттрибут data-period
			timeFrames = append(timeFrames, dataPeriods[t]) //добавляем время в нотации оракула
		}
	})
	return timeFrames
}

//getInstrumentAndPrice - возвращает имя иструмента и текущую цену, находя их в строке tr формата html
func (p *Parser) getInstrumentAndPrice(tr *goquery.Selection) (instrument, price string) {
	//заходим в первого потомка (<td>) и далее в его потомков (<a> и <p>)
	tr.Children().Children().Each(func(i int, s *goquery.Selection) {
		switch i {
		case 0: //<a>
			instrument = s.Text()
		case 1: //<p>
			price = s.Text()
		}
	})
	return
}

//getMovingAverages - получает срез скользящих средних по всем представленным временным периодам
//этот метод принимает в качетсве аргумента текущую строку html и работает с ней
//возвращает скользящие средние в том порядке, в каком они представлены на старнице (аналогично getTimeFrames)
func (p *Parser) getMovingAverages(tr *goquery.Selection) (indicators []string) {
	tr.Children().Each(func(tdNumber int, td *goquery.Selection) { //работаем с каждым столбцом строки
		switch tdNumber {
		case 0: //пропуск
		case 1: //пропуск
		default: //остальные ячейки содержат значения - добавляем их в срез
			indicators = append(indicators, td.Text())
		}
	})
	return indicators
}

//getIndicators - получает срез индикаторов по всем представленным временным периодам
//этот метод принимает в качетсве аргумента текущую строку html, но работает со следующей строкой
//возвращает индикаторы в том порядке, в каком они представлены на странице (аналогично getTimeFrames)
func (p *Parser) getIndicators(tr *goquery.Selection) (indicators []string) {
	if atrr, _ := tr.Next().Attr("data-row-type"); atrr != "indicators" {
		return //если следующая строка не содержит тип "индикаторы", то выходим
	} //иначе продолжаем работу со следующей строкой
	tr.Next().Children().Each(func(tdNumber int, td *goquery.Selection) { //работаем с каждым столбцом след строки
		switch tdNumber {
		case 0: //пропуск
		default: //остальные ячейки содержат значения - добавляем их в срез
			indicators = append(indicators, td.Text())
		}
	})
	return indicators
}

//getIndicators - получает срез резюмирующих значений индикаторов по всем представленным временным периодам
//этот метод принимает в качетсве аргумента текущую строку html, но работает со следующей через одну строкой
//возвращает итоговые значения в том порядке, в каком они представлены на странице (аналогично getTimeFrames)
func (p *Parser) getSummary(tr *goquery.Selection) (indicators []string) {
	if atrr, _ := tr.Next().Next().Attr("data-row-type"); atrr != "summary" {
		return //если следующая через одну строка не содержит тип "резюмэ", то выходим
	} //иначе продолжаем работу со следующей через одну строкой
	tr.Next().Next().Children().Each(func(tdNumber int, td *goquery.Selection) {
		switch tdNumber {
		case 0: //пропуск
		default: //остальные ячейки содержат значения - добавляем их в срез
			indicators = append(indicators, td.Text())
		}
	})
	return indicators
}
