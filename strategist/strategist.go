package strategist

import (
	"github.com/astaxie/beego/logs"
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/jinzhu/copier"
	"oracle/collector/models"
	"oracle/common"
	"oracle/config"
	"oracle/database"
)

//Strategist - сущность которая проверяет различные стратегии на свечных моделях
type Strategist struct {
	DB            *pg.DB                  //База данных
	Notifications <-chan *pg.Notification //Канал, в который БД шлет уведомления о новых свечных моделях
}

func NewStrategist(settings *config.Settings) (s *Strategist, err error) {
	s = new(Strategist)
	s.DB, err = database.NewFor(settings, "STRATEGIST")
	s.Notifications = s.DB.Listen(database.NOTIFY_CANDLE_MODEL_CHAN).Channel()
	return
}

//Work - основной цикл работы стратега
func (s *Strategist) Work(signal chan models.Signal, ready chan bool, shutdown chan bool) {
	go func() { //запуск основного цикла работы в отдельной горутине
		for range ready { //работа в цикле по сигналам от коллектора, что он завершил обновление БД
			s.implementStrategy(strategy_1, signal)
		}
	}()
	<-shutdown //ожидание сигнала на остановку сервиса
}

//implementStrategy - применяет определенную стартегию, котрая передается в качестве аргумента-функции
//формирует сигналы, сохраняя их и отправляя телеграмм боту
//если стратегия для модели не подходит, то бракует эту модель (ничего не делает)
func (s *Strategist) implementStrategy(strategy func(db *pg.DB) (models.Signal, int64, error), signalChanel chan models.Signal) {
	var signal database.Signal
	for { //работа в цикле пока не закончатся уведомления от БД
		select {
		case notify := <-s.Notifications: //если есть сигнал от тригера БД о появлении новых свечных моделей то:
			if notify.Channel == database.NOTIFY_CANDLE_MODEL_CHAN { //если пришло нужное сообщение, значит в базе появилась или обновилась модель, проверяем ее
				logs.Warn("STRATEGIST: new notification from db: ", notify)
				data, candleModelID, err := strategy(s.DB) //применяемая стратегия вернет сигнал, id свечной модели и ошибку
				common.CheckError(err, data.GetInfo())
				//отправим сигнал в канал телеграм-боту
				signalChanel <- data
				//пометим  свечную модель как отработанную
				s.setCandleModelAsDone(candleModelID)
				//сохраняем сигнал в таблицу сигналов
				err = copier.Copy(&signal, &data)
				common.CheckError(err)
				err = signal.Save(s.DB)
				common.CheckError(err, data.GetInfo())
				//сохраняем этот же сигнал в таблицу анализируемых сигналов
				var analysedSignal database.AnalysedSignal
				err = copier.Copy(&analysedSignal, &signal)
				common.CheckError(err)
				err = analysedSignal.Save(s.DB)
				common.CheckError(err, analysedSignal.GetInfo())
				//обнулим все поля сигнала
				signal.Clear()
			}
		default: //если сигналов от тригера БД нет, выходим из цикла
			return
		}
	}

}

//setCandleModelAsDone - отмечает свечную модель с определенным id как отработанную
func (s *Strategist) setCandleModelAsDone(id int64) error {
	var m database.CandleModel
	m.ID = id
	return m.SetIsDone(s.DB, true)
}

//strategy_1 - самая простая стратегия, вытаскивает модель и при любом раскладе формирует из нее сигнал
func strategy_1(db *pg.DB) (signal models.Signal, id int64, err error) {
	var candleModel database.CandleModel
	//вытасикваем из базы первую попавшуюся необработанную модель
	err = candleModel.FindFirstWhere(db, func(q *orm.Query) (*orm.Query, error) {
		return q.Where("is_done IS NOT ?", true), nil
	})
	if err != nil {
		return
	}
	id = candleModel.ID                      //сразу инициализируем id, чтобы в случае неуспеха мы все равно передали его в setCandleModelAsDone(id int64)
	err = copier.Copy(&signal, &candleModel) //сохраняем модель в сигнал
	if err != nil {
		return
	}
	//достаем индикатор
	var indicator database.Indicator
	err = indicator.FindWhere(db, func(q *orm.Query) (*orm.Query, error) {
		return q.Where("instrument=?", signal.Instrument), nil
	})
	if err != nil {
		return
	}
	signal.Indicators = indicator.GetAtTimeFrame(signal.TimeFrame) //сохраняем в сигнал
	//достаем скользящие средние
	var indicatorMA database.MovingAverage
	err = indicatorMA.FindWhere(db, func(q *orm.Query) (*orm.Query, error) {
		return q.Where("instrument=?", signal.Instrument), nil
	})
	if err != nil {
		return
	}
	signal.MovingAverages = indicatorMA.GetAtTimeFrame(signal.TimeFrame) //сохраняем в сигнал
	//достаем результирующее по индикаторам и скользящим средним
	var indicatorSummary database.IndicatorSummary
	err = indicatorSummary.FindWhere(db, func(q *orm.Query) (*orm.Query, error) {
		return q.Where("instrument=?", signal.Instrument), nil
	})
	if err != nil {
		return
	}
	signal.IndicatorsSummary = indicatorSummary.GetAtTimeFrame(signal.TimeFrame) //сохраняем в сигнал
	//достаем текущую цену
	var price database.Price
	err = price.FindWhere(db, func(q *orm.Query) (*orm.Query, error) {
		return q.Where("instrument=?", signal.Instrument), nil
	})
	if err != nil {
		return
	}
	signal.Price = price.Price
	//возвращаем сигнал и id свечной модели
	signal.Strategy = "strategy_1" //todo может лучше сформировать сущность стратегии с именем и функцией, или добавить эти поля самому стратегу
	return signal, candleModel.ID, err
}
