package main

import (
	"github.com/astaxie/beego/logs"
	"oracle/analyst"
	"oracle/collector"
	"oracle/collector/models"
	"oracle/config"
	"oracle/server"
	"oracle/strategist"
	"oracle/telegram"
	"sync"
)

func main() {
	//загрузка настроек
	settings, err := config.GetSettings("settings.json")
	if err != nil {
		panic(err)
	}
	wg := sync.WaitGroup{}
	logs.Warn("***********Oracle is started***************************")
	logs.Warn("current settings:\n%+v", settings)
	defer logs.Warn("***********Oracle is stopped***************************")
	signalChanel := make(chan models.Signal)
	shutdownChanel := make(chan bool)
	//запуск телеграм бота
	bot, err := telegram.NewBot(settings)
	if err != nil {
		panic(err)
	}
	wg.Add(1)
	go func() {
		bot.Work(signalChanel, shutdownChanel)
		wg.Done()
	}()
	//запуск коллектора
	c, err := collector.NewCollector(settings)
	if err != nil {
		panic(err)
	}

	wg.Add(1)
	go func() {
		c.Work(shutdownChanel)
		wg.Done()
	}()
	//запуск стратега
	s, err := strategist.NewStrategist(settings)
	if err != nil {
		panic(err)
	}
	wg.Add(1)
	go func() {
		s.Work(signalChanel, c.Ready, shutdownChanel)
		wg.Done()
	}()
	//запуск аналитика
	a, err := analyst.NewAnalyst(settings)
	if err != nil {
		panic(err)
	}
	wg.Add(1)
	go func() {
		a.Work(shutdownChanel)
		wg.Done()
	}()
	//запуск сервера
	errChanel := server.Run()
	select {
	case err := <-errChanel:
		logs.Error(err)
	}
	wg.Wait()
}
