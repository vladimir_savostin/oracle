package database

import (
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"time"
)

//Price - таблица базы данных, хранящая актуальные цены по конкретным инструментам
type Price struct {
	Instrument string
	Price      string
	CreatedAt  time.Time `sql:"created_at"`
	UpdatedAt  time.Time `sql:"updated_at"`
}

//SaveOrUpdate - сохраняет в базу, если же по данному иструменту уже есть запись, то обновляет ее
func (p *Price) SaveOrUpdate(db *pg.DB) error {
	_, err := db.Model(p).
		OnConflict("(instrument) DO UPDATE").
		Set("price=?price").
		Set("updated_at=?", time.Now()).
		Insert()
	if err != nil {
		return err
	}
	return nil
}

//FindWhere - находит необходимую сущность в базе с применением различных условий
func (p *Price) FindWhere(db *pg.DB, fn func(query *orm.Query) (*orm.Query, error)) error {
	return db.Model(p).
		Apply(fn).
		Select()
}

//BeforeInsert - действия, проводимые перед сохранением в базу
func (p *Price) BeforeInsert(db orm.DB) error {
	if p.CreatedAt.IsZero() {
		p.CreatedAt = time.Now()
	}
	return nil
}
