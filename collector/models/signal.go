package models

import (
	"fmt"
	"oracle/common"
)

//Срез всех сигналов
type Signals []Signal

//Signal содержит всю сводную информацию по конкретному сигналу для конкретного инструмента
type Signal struct {
	Instrument        string //имя биржевого инструмента (н-р "RUB/USD")
	Price             string //текущая цена
	TimeFrame         string //Временной интервал свечи (н-р 1H, 1D и т.д.)
	Reliability       int    //Надежность сигнала (от 1 до 3)
	ModelName         string //Имя свечной модели (н-р "Shooting star")
	CandleNumber      string //Номер свечи
	Description       string //Словесное описание иконки тренда (н-р "Медвежий разворот")
	Date              string //дата формирования завершенной модели
	Indicators        string //Индикаторы
	MovingAverages    string //Скользящие средние
	IndicatorsSummary string //Резюмэ по индикаторам и скользящим средним
	Strategy          string //Стратегия, сформировавшая данный сигнал
}

//FormatSignalString - форматирует строку source в более удобочитаемый вид для отправки в телеграм
func (s Signal) FormatSignalString(source string) (result string) {
	result = common.FormatJSON(source)
	return
}

func (s Signal) GetInfo() string {
	return fmt.Sprintf("signal: %s, timeFrame: %s, model: %s, date: %s", s.Instrument, s.TimeFrame, s.ModelName, s.Date)
}
