package main

import (
	"fmt"
	"github.com/slotix/pageres-go-wrapper"
	"os"
	"testing"
)

//В этом файле отдельно тестятся различные функции парсера

func TestParse(t *testing.T) {
	//parser, err := investing.NewParser()
	//if err != nil {
	//	t.Fatal(err)
	//}
	//parser.ParseCandleModels()
	shotsDir := "shots"
	os.Mkdir(shotsDir, 0777)
	params := sshot.Parameters{
		Command:   "pageres",
		Sizes:     "2000x4000",
		Crop:      "--crop",
		Scale:     "--scale 1",
		Timeout:   "--timeout 30",
		Filename:  fmt.Sprintf("test"),
		UserAgent: "",
	}
	urls := []string{
		"https://ru.investing.com/currencies/usd-kzt-chart",
	}
	sshot.GetShots(urls, params)
	sshot.DeleteZeroLengthFiles(shotsDir)
}
