package investing

//Price - структура хранящая текущую цену по конкретному инструменту
type Price struct {
	Instrument string
	Price      string
}
