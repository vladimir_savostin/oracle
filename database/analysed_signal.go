package database

import (
	"fmt"
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"time"
)

//Signals - срез сформированных сигналов
type AnalysedSignals []AnalysedSignal

//Signal таблица сформированных сигналов
type AnalysedSignal struct {
	ID                int64
	Instrument        string     //имя биржевого инструмента (н-р "RUB/USD")
	Price             string     //текущая цена
	TimeFrame         string     //Временной интервал свечи (н-р 1H, 1D и т.д.)
	Reliability       int        //Надежность сигнала (от 1 до 3)
	ModelName         string     //Имя свечной модели (н-р "Shooting star")
	CandleNumber      string     //Номер свечи
	Description       string     //Словесное описание иконки тренда (н-р "Медвежий разворот"), используется аналитиком при анализе ценового движения после сигнала
	Date              string     //дата формирования завершенной модели
	Indicators        string     //Индикаторы
	MovingAverages    string     //Скользящие средние
	IndicatorsSummary string     //Резюмэ по индикаторам и скользящим средним
	Strategy          string     //стратегия которая сформировала сигнал
	CurrentCandle     int        //номер текущей свечи после формирования сигнала (в момент формирования CurrentCandle=0)
	Candle1           CandleData //результаты после 1 свечи
	Candle2           CandleData //результаты после 2 свеч
	Candle3           CandleData //результаты после 3 свеч
	Candle4           CandleData //результаты после 4 свеч
	Candle5           CandleData //результаты после 5 свеч
	Candle6           CandleData //результаты после 6 свеч
	Candle7           CandleData //результаты после 7 свеч
	Candle8           CandleData //результаты после 8 свеч
	Candle9           CandleData //результаты после 9 свеч
	Candle10          CandleData //результаты после 10 свеч
	CreatedAt         time.Time
	UpdatedAt         time.Time //Время обновления (при сохранении тоже используется - UpdatedAt=time.Now())

	isDone bool //true - если анализ уже закончен
}

//CandleData - данные по инструменту через определенное количество свеч после формирования сигнала
type CandleData struct {
	Price     string //Цена на определенной свече после сигнала
	Success   bool   //Был ли успешным этот сигнал спустя определенное количество свеч
	CreatedAt time.Time
}

func (s *AnalysedSignal) GetInfo() string {
	return fmt.Sprintf("analysed signal: %s, timeFrame: %s, model: %s, date: %s", s.Instrument, s.TimeFrame, s.ModelName, s.Date)
}

//Save - сохраняет модель в базу
func (s *AnalysedSignal) Save(db *pg.DB) error {
	_, err := db.Model(s).Insert()
	if err != nil {
		return err
	}
	return nil
}

//UpdateCandle - обновляет данные CandleData на свече за номером number, прошедшей после формирования сигнала
func (s *AnalysedSignal) UpdateCandle(db *pg.DB, number int, data CandleData) error {
	query := fmt.Sprintf("candle%v=?", number)
	_, err := db.Model(s).
		WherePK().
		Set(query, data).
		Set("current_candle=?", number).
		Set("updated_at=?", time.Now()).
		Update()
	return err
}

//FindAllWhere - возвращает срез всех сигналов из таблицы БД c применением дополнительных параметров поиска
func (s *AnalysedSignals) FindAllWhere(db *pg.DB, fn func(query *orm.Query) (*orm.Query, error)) error {
	return db.Model(s).
		Apply(fn).
		Select()
}

//SetIsDone - устанавливает is_done в true/false
func (s *AnalysedSignal) SetIsDone(db *pg.DB, isDone bool) error {
	_, err := db.Model(s).
		Set("is_done=?", isDone).
		Set("updated_at=?", time.Now()).
		WherePK().
		Update()
	return err
}

//Delete - удаляет модель из базы
func (s *AnalysedSignal) Delete(db *pg.DB) error {
	_, err := db.Model(s).WherePK().Delete()
	if err != nil {
		return err
	}
	return nil
}

//BeforeInsert - действия, проводимые с s перед сохранением в базу
func (s *AnalysedSignal) BeforeInsert(db orm.DB) error {
	if s.CreatedAt.IsZero() {
		s.CreatedAt = time.Now()
	}
	if s.UpdatedAt.IsZero() {
		s.UpdatedAt = time.Now()
	}
	return nil
}
