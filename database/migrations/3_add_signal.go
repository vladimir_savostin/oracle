package main

import (
	"github.com/astaxie/beego/logs"
	"github.com/go-pg/migrations"
)

func init() {
	migrations.Register(func(db migrations.DB) error {
		logs.Warn("create table signal...")
		_, err := db.Exec(create_table_signals)
		return err
	}, func(db migrations.DB) error {
		logs.Warn("drop table signal...")
		_, err := db.Exec(drop_table_signals)
		return err
	})
}

const create_table_signals = `
CREATE TABLE "signals" (
	"id" serial NOT NULL,
	"instrument" varchar NOT NULL,
	"price" varchar NOT NULL,
	"time_frame" varchar NOT NULL,
	"reliability" integer NOT NULL,
	"model_name" varchar NOT NULL,
	"candle_number" varchar NOT NULL,
	"description" varchar NOT NULL,
	"date" varchar NOT NULL,
	"indicators" varchar NOT NULL,
	"moving_averages" varchar NOT NULL,
	"indicators_summary" varchar NOT NULL,
	"strategy" varchar NOT NULL,
	"created_at" TIMESTAMP,
	CONSTRAINT signals_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);
`

const drop_table_signals = `
DROP TABLE IF EXISTS "signals";
`
