package main

import (
	"flag"
	"fmt"
	"github.com/astaxie/beego/logs"
	"oracle/config"
	"oracle/database"
	"os"

	"github.com/go-pg/migrations"
)

const usageText = `This program runs command on the db. Supported commands are:
  - up [target] - runs all available migrations by default or up to target one if argument is provided.
  - down - reverts last migration.
  - reset - reverts all migrations.
  - version - prints current db version.
  - set_version [version] - sets db version without running migrations.
Usage:
  go run *.go <command> [args]
`

func main() {
	//загрузка настроек
	settings, err := config.GetSettings("settings.json")
	if err != nil {
		panic(err)
	}
	flag.Usage = usage
	flag.Parse()
	logs.Warn("current settings:\n%+v", settings)
	db, err := database.NewFor(settings, "MIGRATION")
	defer db.Close()
	if err != nil {
		panic(err) // если не смогли подключиться то и смысла дальше идти нет
	}
	oldVersion, newVersion, err := migrations.Run(db, flag.Args()...)
	if err != nil {
		exitf(err.Error())
	}
	if newVersion != oldVersion {
		logs.Warn("migrated from version %d to %d\n", oldVersion, newVersion)
	} else {
		logs.Warn("version is %d\n", oldVersion)
	}
}

func usage() {
	fmt.Printf(usageText)
	flag.PrintDefaults()
	os.Exit(2)
}

func errorf(s string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, s+"\n", args...)
}

func exitf(s string, args ...interface{}) {
	errorf(s, args...)
	os.Exit(1)
}
