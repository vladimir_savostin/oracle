package analyst

import (
	"fmt"
	"github.com/go-pg/pg/orm"
	"oracle/common"
	"oracle/database"
	"strconv"
	"strings"
	"time"
)

//Work - основной цикл работы аналитика
//работа производится с таблицей analysed_signals
func (analyst *Analyst) Work(shutdown chan bool) {
	defer analyst.CloseDB()
	go func() {
		for {
			//Вытаскиваем все сигналы, анализ по которым еще не закончен
			//Cигналы для анализа в базу (в таблицу analysed_signals)кладет стратег, а аналитик их только вытаскивает и анализирует
			var signals database.AnalysedSignals
			err := signals.FindAllWhere(analyst.DB, func(q *orm.Query) (*orm.Query, error) {
				return q.Where("is_done IS NOT ?", true), nil
			})
			common.CheckError(err)
			//далее работаем с каждым сигналом отдельно
			for _, signal := range signals {
				err = analyst.proceed(signal)
				common.CheckError(err)
			}
			time.Sleep(analyst.workPeriod)
		}

	}()
	<-shutdown
}

//proceed - проводит необходимый анализ и сохранение результатов анализа сигнала
func (analyst *Analyst) proceed(signal database.AnalysedSignal) error {
	timeFrame := timeFramesToTime[signal.TimeFrame]
	if time.Since(signal.UpdatedAt) >= timeFrame { //если с момента первого сохранения или обновления миновала одна свеча, то
		//увеличиваем номер текущей после сигнала свечи (при первом сохранении он равен нулю)
		signal.CurrentCandle++
		//вытаскиваем текущую цену инструмента, по которому был сигнал
		var currentPrice database.Price
		err := currentPrice.FindWhere(analyst.DB, func(q *orm.Query) (*orm.Query, error) {
			return q.Where("instrument=?", signal.Instrument), nil
		})
		if err != nil {
			return err
		}
		//проверим успешность сигнала
		success, err := checkPrice(signal.Price, currentPrice.Price, signal.Description)
		if err != nil {
			return err
		}
		//обновим аналитику по текущей свече
		//!!!После UpdateCandle параметр signal.UpdatedAt=time.Now(),
		//поэтому следующее обновление произойдет через timeFrame (т.е по завершении следующей свечи, см. самый первый if)
		data := database.CandleData{
			Price:     currentPrice.Price,
			Success:   success,
			CreatedAt: time.Now(),
		}
		err = signal.UpdateCandle(analyst.DB, signal.CurrentCandle, data)
		if err != nil {
			return err
		}
		//если текущая свеча последняя, по которой проводится анализ, то делаем пометку что анализ закончен
		if signal.CurrentCandle == 10 { //todo provide max number from config
			err = signal.SetIsDone(analyst.DB, true)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

//checkPrice - возвращает true, если ценовое движение подтвердило сигнал
func checkPrice(initialPrice, currentPrice string, trend string) (success bool, err error) {
	initial, err := strconv.ParseFloat(formatPrice(initialPrice), 64)
	if err != nil {
		return
	}
	current, err := strconv.ParseFloat(formatPrice(currentPrice), 64)
	if err != nil {
		return
	}
	if isIn(trend, bullTrends) { //если тренд бычий, то цена должна была вырасти
		return initial < current, nil
	}
	if isIn(trend, bearTrends) { //если тренд медвежий, то цена должна была упасть
		return initial > current, nil
	}
	//если trend и не бычий, и не медвежий - вернем ошибку:
	return false, fmt.Errorf("unknown trend description: %s", trend)
}

//isIn - возвращает true, если trend представлен в срезе trends
func isIn(trend string, trends []string) bool {
	for _, t := range trends {
		if trend == t {
			return true
		}
	}
	return false
}

//formatPrice - форматирует цену в соответствии с синтаксисом
func formatPrice(price string) string {
	s := strings.Replace(price, ".", "", -1)
	s = strings.Replace(s, ",", ".", -1)
	return s
}
