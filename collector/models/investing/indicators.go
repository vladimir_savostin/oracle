package investing

//Indicators - хранит все полученные индикаторы
//1-й ключ=название инструмента; 2-й ключ=time_frame; значение=indicator
type Indicators map[string]map[string]Indicator

//Indicator - содержит все индикаторы для конкретного инструмента по конкретному таймфрейму
type Indicator struct {
	MovingAverages string //Скользящие средние
	Indicators     string //Индикаторы
	Summary        string //Резюмэ
}

//ADD - добавляет к отображениею новые индикаторы по таймфреймам
func (m Indicators) ADD(instrument string, innerMap map[string]Indicator) {
	if _, ok := m[instrument]; !ok {
		m[instrument] = innerMap
		return
	}
	buf := m[instrument]
	for key, val := range innerMap {
		buf[key] = val
	}
	return
}
