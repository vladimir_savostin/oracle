package database

import (
	"github.com/astaxie/beego/logs"
	"github.com/go-pg/pg"
	"oracle/config"
)

type Database struct {
	DB *pg.DB
}

//NewFor - создает новое подключение к БД для конкретной сущности entity
func NewFor(settings *config.Settings, entity string) (*pg.DB, error) {
	db := pg.Connect(&pg.Options{
		User:     settings.DB.User,
		Password: settings.DB.Password,
		Database: settings.DB.Database,
	})
	db.OnQueryProcessed(func(event *pg.QueryProcessedEvent) { //настройка логирования базы
		query, err := event.FormattedQuery()
		if err != nil {
			panic(err)
		}
		logs.Info("DB:%s:\t%s:\t%s", entity, event.Func, query)
	})
	_, err := db.Exec("SELECT 'SIMPLE CHECK CONNECTION STRING'")
	if err != nil {
		return nil, err
	}

	logs.Warn("%s connected to the database %s", entity, db.Options().Database)
	return db, nil
}
