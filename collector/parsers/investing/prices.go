package investing

import (
	"github.com/PuerkitoBio/goquery"
	"oracle/collector/models/investing"
)

//ParsePrices - парсит html-страницу, извлекая цены по представленным инструментам
func (p *Parser) ParsePrices() (prices []investing.Price, err error) {
	document, err := p.sendRequest(URL_INDICATORS, p.MainAccount)
	if err != nil {
		return nil, err
	}
	document.Find("tr").Each(func(trNumber int, tr *goquery.Selection) { //находим все строки и работаем с каждой
		if atrr, _ := tr.Attr("data-row-type"); atrr == "movingAverages" { //если это строка скользящих средних, то:
			instrument, price := p.getInstrumentAndPrice(tr) //получаем из нее имя инструмента и текущую цену
			prices = append(prices, investing.Price{
				Instrument: instrument,
				Price:      price})
		}
	})
	return
}
