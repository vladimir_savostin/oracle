package database

import (
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"time"
)

type CandleModels []CandleModel

//CandleModel - таблица базы данных, хранящая актуальные завершенные свечные модели
type CandleModel struct {
	ID           int64
	Instrument   string    //Имя инструмента (н-р USD/EUR)
	TimeFrame    string    //Временной интервал свечи (н-р 1H, 1D и т.д.)
	Reliability  int       //Надежность сигнала (от 1 до 3)
	ModelName    string    //Имя свечной модели (н-р "Shooting star")
	CandleNumber string    //Номер свечи
	Description  string    //Словесное описание иконки тренда (н-р "Медвежий разворот")
	Date         string    //Время формирования завершенного сигнала
	CreatedAt    time.Time //Время занесения в базу
	UpdatedAt    time.Time

	isDone bool //Обработан/не обработан
}

//GetCandleNumber - возвращает номер свечи
func (m *CandleModel) GetCandleNumber() string {
	return m.CandleNumber
}

//Save - сохраняет модель в базу
func (m *CandleModel) Save(db *pg.DB) error {
	_, err := db.Model(m).Insert()
	if err != nil {
		return err
	}
	return nil
}

//Find - ищет есть ли модель m в базе данных,
//если есть, то она извлекается и кладется в m, возвращая true
func (m *CandleModel) Find(db *pg.DB) (bool, error) {
	err := db.Model(m).
		Where("instrument=?", m.Instrument).
		Where("time_frame=?", m.TimeFrame).
		Where("reliability=?", m.Reliability).
		Where("model_name=?", m.ModelName).
		Where("description=?", m.Description).
		Where("date=?", m.Date).
		Select()
	if err != nil {
		return false, err
	}
	if m.ID != 0 {
		return true, nil
	}
	return false, nil
}

//FindAll - возвращает срез всех сигналов из таблицы БД
func (m *CandleModels) FindAll(db *pg.DB) error {
	return db.Model(m).Select()
}

//FindFirstWhere - находит первую попавшуюся сущность, применяя к поиску функцию fn
func (m *CandleModel) FindFirstWhere(db *pg.DB, fn func(query *orm.Query) (*orm.Query, error)) error {
	return db.Model(m).
		Apply(fn).
		Limit(1).
		Select()
}

//SetCandleNumber - устанавливает номер свечи у завершенного сигнала
func (m *CandleModel) SetCandleNumber(db *pg.DB, candleNumber string) error {
	if m.CandleNumber == candleNumber {
		return nil //ничего не надо делать, если в базе лежит уже актуальный номер
	}
	_, err := db.Model(m).
		Set("candle_number=?", candleNumber).
		Set("updated_at=?", time.Now()).
		Where("id=?", m.ID).
		Update()
	return err
}

//SetIsDone - устанавливает is_done в true/false
func (m *CandleModel) SetIsDone(db *pg.DB, isDone bool) error {
	_, err := db.Model(m).
		Set("is_done=?", isDone).
		Set("updated_at=?", time.Now()).
		WherePK().
		Update()
	return err
}

//Delete - удаляет модель из базы
func (m *CandleModel) Delete(db *pg.DB) error {
	_, err := db.Model(m).WherePK().Delete()
	if err != nil {
		return err
	}
	return nil
}

//BeforeInsert - действия, проводимые с s перед сохранением в базу
func (m *CandleModel) BeforeInsert(db orm.DB) error {
	if m.CreatedAt.IsZero() {
		m.CreatedAt = time.Now()
	}
	return nil
}
