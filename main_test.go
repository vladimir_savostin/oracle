package main

import "testing"

func TestMain(m *testing.M) {
	m.Run()
}

//candle_models_HTML содержит код страницы, если распарсить которые - получим следующие данные:
//Instrument: XRP/USD,
//TimeFrame: 1W,
//Reliability: 2,
//ModelName: Doji Star Bearish,
//CandleNumber: 1,
//Description: Медвежий разворот,
//Date: 23.09.2018,
//
//Instrument: XRP/USD,
//TimeFrame: 1W,
//Reliability: 3,
//ModelName: Three Inside Up,
//CandleNumber: 2,
//Description: Бычий разворот,
//Date: 16.09.2018,
//
//Instrument: USD/RUB,
//TimeFrame: 1W,
//Reliability: 3,
//ModelName: Three Black Crows,
//CandleNumber: Текущая,
//Description: Медвежий разворот,
//
//Instrument: USD/RUB,
//TimeFrame: 1D,
//Reliability: 2,
//ModelName: Bullish doji Star,
//CandleNumber: 3,
//Description: Бычий разворот,
//Date: 25.09.2018,
//
//Instrument: ETH/USD,
//TimeFrame: 1D,
//Reliability: 2,
//ModelName: Engulfing Bearish,
//CandleNumber: Текущая,
//Description: Медвежий разворот,
//
//Instrument: BCH/USD,
//TimeFrame: 1W,
//Reliability: 3,
//ModelName: Three Outside Up,
//CandleNumber: 1,
//Description: Бычий разворот,
//Date: 23.09.2018,
//
//Instrument: BCH/USD,
//TimeFrame: 1W,
//Reliability: 2,
//ModelName: Bullish Engulfing,
//CandleNumber: 2,
//Description: Бычий разворот,
//Date: 16.09.2018,
const candle_models_HTML = `
<!DOCTYPE HTML>
<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" xmlns:schema="http://schema.org/" class="ru" lang="ru" geo="KZ">
<head>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    </script>

<link rel="dns-prefetch" href="https://i-invdn-com.akamaized.net" />
<link rel="dns-prefetch" href="https://a-invdn-com.akamaized.net" />

<title>Свечные модели — Investing.com</title>

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<link rel="shortcut icon" href="https://i-invdn-com.akamaized.net/logos/favicon.ico">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta http-equiv="content-language" content="ru">
<meta name="description" content="Японские свечные модели по инструментам на ведущих финансовых рынках в реальном времени: акции, индексы, фьючерсы, сырьевые товары, Форекс и CFD.">
<meta http-equiv="pragma" content="no-cache">
<meta name="wmail-verification" content="1e519b53c38953db">
	<meta name="twitter:url" content="https://ru.investing.com/technical/candlestick-patterns"/>
<meta name="twitter:site" content="@investingru">
<meta name="twitter:title" content="Свечные модели — Investing.com">
<meta name="twitter:description" content="Японские свечные модели по инструментам на ведущих финансовых рынках в реальном времени: акции, индексы, фьючерсы, сырьевые товары, Форекс и CFD.">
<meta name="twitter:card" content="summary">
<meta property="og:title" content="Свечные модели — Investing.com"/>
	<meta property="og:description" content="Японские свечные модели по инструментам на ведущих финансовых рынках в реальном времени: акции, индексы, фьючерсы, сырьевые товары, Форекс и CFD."/>
	<meta property="og:type" content="website"/>
	<meta property="og:url" content="https://ru.investing.com/technical/candlestick-patterns"/>
	<meta property="og:site_name" content="investing.com Россия"/>
	<meta property="og:locale" content="ru_RU"/>
	<meta property="og:image" content="https://i-invdn-com.akamaized.net/investing_300X300.png"/>
	    <meta name="google-site-verification" content="DTRxWXB3vjNUsTCPICWo9yzZmIllylXYkRevEXo7szg">
	



<!-- css -->
	<link rel="stylesheet" href="https://i-invdn-com.akamaized.net/css/video-js_v3.css" type="text/css">
            <link rel="stylesheet" href="https://i-invdn-com.akamaized.net/css/mainOldMin_v3b.css" type="text/css">
          				<link rel="stylesheet" href="https://i-invdn-com.akamaized.net/css/newMainCssMin_v57g.css" type="text/css">
			    

    <link href="https://i-invdn-com.akamaized.net/css/printContent_v10.css" media="print" rel="stylesheet" type="text/css" />


<!-- css -->

<!-- js -->
<script src="https://i-invdn-com.akamaized.net/js/jquery-6.4.9.04.min.js"></script>
<script type="text/javascript">
var domainId = "7";
window.CDN_URL = "https://i-invdn-com.akamaized.net";
</script>
<script>
(function() {
	var po = document.createElement('script');
	po.type = 'text/javascript';
	po.async = true;
	po.src = 'https://apis.google.com/js/client:plusone.js?onload=loadAfterGApiReady';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(po, s);
})();

</script>

<script type="text/javascript" src="https://i-invdn-com.akamaized.net/js/n/utils-0.16.min.js"></script>
<script type="text/javascript" src="https://i-invdn-com.akamaized.net/js/main-1.17.229.min.js"></script>
<script type="text/javascript" src="https://i-invdn-com.akamaized.net/js/instruments-1.0.16.min.js"></script>
<!-- Forces IE6,IE7 to understand PSEUDO CLASSES :after and :before (add before: " "; after: " "; to CSS) -->
<!--[if (gte IE 6)&(lte IE 7)]>
<script src="https://i-invdn-com.akamaized.net/js/jquery.pseudo-1.1.min.js"></script>
<![endif]-->
<!--Global variables for JavaScript-->
<script type="text/javascript">
    window.login_url = '/members-admin/login';
        window.close_word = 'Закрыть';
    window.lightbox_image = 'Картинка';
    window.lightbox_from = 'из';
        var FP = {
        global  : {
            _defaultDomain  : 'ru.investing.com',
            _textAlign      : 'left'
        }
    };
    var userType;
    var userSupportedType ;
</script>
<!--Gloval variables for JavaScript-->
<!-- js -->


<script type="text/javascript" src="http://vk.com/js/api/openapi.js?34"></script>
		   	<script type="text/javascript"> VK.init({apiId: 3564591 , onlyWidgets: true});</script><script type="text/javascript">
    /* Modernizr 2.0.6 (Custom Build) | MIT & BSD
     * Build: http://www.modernizr.com/download/#-csstransitions-iepp-cssclasses-testprop-testallprops-domprefixes
     */
    window.Modernizr=function(a,b,c){function A(a,b){var c=a.charAt(0).toUpperCase()+a.substr(1),d=(a+" "+n.join(c+" ")+c).split(" ");return z(d,b)}function z(a,b){for(var d in a)if(k[a[d]]!==c)return b=="pfx"?a[d]:!0;return!1}function y(a,b){return!!~(""+a).indexOf(b)}function x(a,b){return typeof a===b}function w(a,b){return v(prefixes.join(a+";")+(b||""))}function v(a){k.cssText=a}var d="2.0.6",e={},f=!0,g=b.documentElement,h=b.head||b.getElementsByTagName("head")[0],i="modernizr",j=b.createElement(i),k=j.style,l,m=Object.prototype.toString,n="Webkit Moz O ms Khtml".split(" "),o={},p={},q={},r=[],s,t={}.hasOwnProperty,u;!x(t,c)&&!x(t.call,c)?u=function(a,b){return t.call(a,b)}:u=function(a,b){return b in a&&x(a.constructor.prototype[b],c)},o.csstransitions=function(){return A("transitionProperty")};for(var B in o)u(o,B)&&(s=B.toLowerCase(),e[s]=o[B](),r.push((e[s]?"":"no-")+s));v(""),j=l=null,a.attachEvent&&function(){var a=b.createElement("div");a.innerHTML="<elem></elem>";return a.childNodes.length!==1}()&&function(a,b){function s(a){var b=-1;while(++b<g)a.createElement(f[b])}a.iepp=a.iepp||{};var d=a.iepp,e=d.html5elements||"abbr|article|aside|audio|canvas|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",f=e.split("|"),g=f.length,h=new RegExp("(^|\\s)("+e+")","gi"),i=new RegExp("<(/*)("+e+")","gi"),j=/^\s*[\{\}]\s*$/,k=new RegExp("(^|[^\\n]*?\\s)("+e+")([^\\n]*)({[\\n\\w\\W]*?})","gi"),l=b.createDocumentFragment(),m=b.documentElement,n=m.firstChild,o=b.createElement("body"),p=b.createElement("style"),q=/print|all/,r;d.getCSS=function(a,b){if(a+""===c)return"";var e=-1,f=a.length,g,h=[];while(++e<f){g=a[e];if(g.disabled)continue;b=g.media||b,q.test(b)&&h.push(d.getCSS(g.imports,b),g.cssText),b="all"}return h.join("")},d.parseCSS=function(a){var b=[],c;while((c=k.exec(a))!=null)b.push(((j.exec(c[1])?"\n":c[1])+c[2]+c[3]).replace(h,"$1.iepp_$2")+c[4]);return b.join("\n")},d.writeHTML=function(){var a=-1;r=r||b.body;while(++a<g){var c=b.getElementsByTagName(f[a]),d=c.length,e=-1;while(++e<d)c[e].className.indexOf("iepp_")<0&&(c[e].className+=" iepp_"+f[a])}l.appendChild(r),m.appendChild(o),o.className=r.className,o.id=r.id,o.innerHTML=r.innerHTML.replace(i,"<$1font")},d._beforePrint=function(){p.styleSheet.cssText=d.parseCSS(d.getCSS(b.styleSheets,"all")),d.writeHTML()},d.restoreHTML=function(){o.innerHTML="",m.removeChild(o),m.appendChild(r)},d._afterPrint=function(){d.restoreHTML(),p.styleSheet.cssText=""},s(b),s(l);d.disablePP||(n.insertBefore(p,n.firstChild),p.media="print",p.className="iepp-printshim",a.attachEvent("onbeforeprint",d._beforePrint),a.attachEvent("onafterprint",d._afterPrint))}(a,b),e._version=d,e._domPrefixes=n,e.testProp=function(a){return z([a])},e.testAllProps=A,g.className=g.className.replace(/\bno-js\b/,"")+(f?" js "+r.join(" "):"");return e}(this,this.document);
    window.ie = (function(){var undef,v = 3,div = document.createElement('div'),all = div.getElementsByTagName('i');while (div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',all[0]);return v > 4 ? v : undef;}());
</script>

<!-- TEMP -->

<style>
	.googleContentRec {display:inline-block;width:650px;height:300px;}

</style>

<style>
        /* INPUTS OVERWRITES */
    .combineSearchBox {border:1px solid #737373;} /* overwrite main css */
    .combineSearchBox.newInput.inputTextBox, .portfolioSearch .newInput.inputTextBox  {padding:0;}
</style>

<!--[if (IE 8)|(IE 9)]>
<link rel="stylesheet" type="text/css" href="https://i-invdn-com.akamaized.net/css/ie8-9main_v2b.css">
<![endif]-->
<!--[if IE 8]>
<link rel="stylesheet" type="text/css" href="https://i-invdn-com.akamaized.net/css/ie8main_v2a.css">
<![endif]-->
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="https://i-invdn-com.akamaized.net/css/ie7main_v1i.css">
<![endif]-->


<style>
    .MainTableDirectory .HeadTitlesRow TH {text-align:center; font-weight:bold;}
</style>

<style>
    .openTbl TH.icon {text-align:center;}     .openTbl TH.icon SPAN {position:relative; top:2px;}
</style>
<script>
    var oldIE = false, explorerEight = false ,explorerNine = false, sponsoredArticle;
</script>
<!--[if lte IE 7]>
<script>
    oldIE = true;
</script>
<![endif]-->
<!--[if (IE 8)]>
<script>
    explorerEight = true;
</script>
<![endif]-->
<!--[if (IE 9)]>
<script>
    explorerNine = true;
</script>
<![endif]-->
<script type='text/javascript'>
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
    (function() {
        var gads = document.createElement('script');
        gads.async = true;
        gads.type = 'text/javascript';
        var useSSL = 'https:' == document.location.protocol;
        gads.src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
        var node = document.getElementsByTagName('script')[0];
        node.parentNode.insertBefore(gads, node);
    })();
</script>
<script>var fd188ca3df8a80efc4ca429dd="d1dde445dc5e74a66ecbb98bdd4b7e46";</script><script src="https://cdn.optimizely.com/js/4229603524.js"></script><script>window.newsAndAnalysisAlertSettings = JSON.parse('null');</script>
</head>
<body class="takeover dfpTakeovers" >
    <div id="sideNotificationZone" class="sideNotificationZone">
		<div class="floatingAlertWrapper alertWrapper js-notification-item displayNone">
			<div class="alertNotifIcon"><span class="js-icon"></span></div>
			<div class="alertNotifData">
				<a class="alertDataTitle js-title"></a>
				<span class="alertDataDetails js-small-text"></span>
			</div>
			<a data-tooltip="Настройка уведомлений" class="js-manage-alerts gearIconSmall genToolTip oneliner reverseToolTip"></a>
			<span class="alertCloseIconSmall js-close"></span>
		</div>
    </div>

	<div class="breakingNews">
		<div class="floatingAlertWrapper">
			<span class="breakingNewsTitle bold">Главные новости</span>
			<span class="breakingNewsText js-breaking-news-content"></span>
		</div>
		<span class="closeIcon js-close"></span>
	</div>

<div class="generalOverlay js-general-overlay displayNone"></div>

<script>
    $('.js-general-overlay').on('click', $.stopProp);
</script>

		 <div class="earAdv left">
	        <div id='div-gpt-ad-1478019486943-0'>
			</div>
	    </div>


	<div class="wrapper">
			    <header>
    <!-- topBar -->
<div class="topBar">
	<div class="topBarInnerWrapper">
					<a href="/" class="topBarLogo">
									<img src="https://i-invdn-com.akamaized.net/logos/investing-com-logo.png" alt="Investing.com - Ведущий Финансовый Портал" class="investingLogo">
							</a>
			



		<div id="topBarPopup" load="topBarSpinner"></div>

		<div class="js-search-overlay topSearchOverlay displayNone"></div>
<div class="searchDiv newSearchDiv js-main-search-wrapper">
	<div class="searchBoxContainer topBarSearch topBarInputSelected" >
	    <input autocomplete='off' type="text" class="searchText arial_12 lightgrayFont js-main-search-bar" value="" placeholder="Поиск на сайте...">
		<label class="searchGlassIcon js-magnifying-glass-icon">&nbsp;</label>
		<i class="cssSpinner"></i>
	</div>

    <div class="js-results-dialog displayNone">
        <div class="js-query-results newSearch_topBar newSearch_topBarResults displayNone">
	        <div class="searchMain">
		        <div class="js-query-quotes-header textBox groupHeader searchPopup_results">
			        <a class="js-quote-header-link">Котировки</a>
			        <div class="js-quote-types-dropdown newSearchSelect selectWrap">
				        <a class="newBtnDropdown noHover">
                            <span class="js-dropdown-text-input inputDropDown">Все типы инструментов</span>
					        <i class="bottunImageDoubleArrow buttonWhiteImageDownArrow"></i>
				        </a>

			            <ul class="dropdownBtnList displayNone js-quote-types-dropdown-ul">
			            <li data-value='all'>Все типы инструментов</li><li data-value='indice'>Индексы</li><li data-value='equities'>Акции</li><li data-value='etf'>ETF</li><li data-value='fund'>Фонды</li><li data-value='commodity'>Товары</li><li data-value='currency'>Валюты</li><li data-value='crypto'>Крипто</li><li data-value='bond'>Облигации</li>			            </ul>
			        </div>
		        </div>

		        <div class="js-scrollable-results-wrapper newResultsContainer">
			        <div class="js-table-results tableWrapper"></div>
		        </div>

	            <!-- NO RESULTS -->
	            <div class="js-query-no-results noResults displayNone">
	                <i class="searchNoResultsNew"></i>
	                <p class="lighterGrayFont">Ваш поиск не дал результатов</p>
	            </div>

	            <div class="searchResultsFooter">
		            <i class="blueSearchGlassIcon middle"></i>
		            <a class="js-footer-link" href="/">Найти на сайте:&nbsp;<span class="js-footer-link-text bold"></span></a>
	            </div>
	        </div>
	        <div class="js-right-side-results searchAside"></div>
        </div>

        <div class="js-default-search-results newSearch_topBar">
	        <div class="searchMain js-main-group-results"></div>
	        <div class="searchAside">
		        <div class="searchAside_item">

			        <a class="asideTitle" href="/news/most-popular-news">
				        <div class="js-search-ga-header groupHeader" ga-label="Popular News">Популярные новости</div>
				        <span class="js-search-ga-more" ga-label="Popular News - More">Еще</span>
			        </a>
			        <div class="js-search-ga-items articles mediumTitle1" ga-label="Popular News - Article"><article class="articleItem  ">
	<a href="/news/economy-news/article-576305" class="img" ><img src="https://i-invdn-com.akamaized.net/news/indicatornews_2_150x108_S_1416306967.jpg" alt="Усманов хочет помочь своему другу - владельцу "Эвертона"" onerror="javascript:this.src='https://i-invdn-com.akamaized.net/news/news-paper_108x81.png';" ></a>	<div class="textDiv">
							<a href="/news/economy-news/article-576305" title="Усманов хочет помочь своему другу - владельцу "Эвертона"" class="title" >Усманов хочет помочь своему другу - владельцу "Эвертона"</a>
	                			</div>
	<div class="clear"></div>
</article><article class="articleItem  ">
	<a href="/news/economy-news/article-576297" class="img" ><img src="https://i-invdn-com.akamaized.net/news/LYNXMPED951YE_S.jpg" alt="Илон Маск заплатит штраф и выйдет из совета директоров Tesla" onerror="javascript:this.src='https://i-invdn-com.akamaized.net/news/news-paper_108x81.png';" ></a>	<div class="textDiv">
							<a href="/news/economy-news/article-576297" title="Илон Маск заплатит штраф и выйдет из совета директоров Tesla" class="title" >Илон Маск заплатит штраф и выйдет из совета директоров Tesla</a>
	                			</div>
	<div class="clear"></div>
</article><article class="articleItem  ">
	<a href="/news/economy-news/article-576303" class="img" ><img src="https://i-invdn-com.akamaized.net/news/indicatornews_5_150x108_S_1416303181.jpg" alt="Воробьев: Подмосковью "нужны новые рабочие места на новых заводах", а "лишнее жилье не нужно"" onerror="javascript:this.src='https://i-invdn-com.akamaized.net/news/news-paper_108x81.png';" ></a>	<div class="textDiv">
							<a href="/news/economy-news/article-576303" title="Воробьев: Подмосковью "нужны новые рабочие места на новых заводах", а "лишнее жилье не нужно"" class="title" >Воробьев: Подмосковью "нужны новые рабочие места на новых заводах", а "лишнее жилье не нужно"</a>
	                			</div>
	<div class="clear"></div>
</article></div>
		        </div>
						        <div class="searchAside_item js-popular-analysis-wrapper">

			        <a class="asideTitle" href="/analysis/most-popular-analysis">
				        <div class="js-search-ga-header groupHeader" ga-label="Popular Analysis">Популярная аналитика</div>
				        <span class="js-search-ga-more" ga-label="Popular Analysis - More">Еще</span>
			        </a>
			        <div class="js-search-ga-items articles smallTitle1 analysisImg js-popular-analysis-items" ga-label="Popular Analysis - Article"><article class="articleItem  ">
	<a href="/analysis/article-200241863" class="img" ><img src="https://d1-invdn-com.akamaized.net/company_logo/8a041e87308c804e01b487506cdf1197.jpg"  onerror="javascript:this.src='https://i-invdn-com.akamaized.net/anonym.gif';" ></a>	<div class="textDiv">
							<a href="/analysis/article-200241863" title="Акции JD.com потеряли 40%, и это неспроста" class="title" >Акции JD.com потеряли 40%, и это неспроста</a>
	                			</div>
	<div class="clear"></div>
</article><article class="articleItem  ">
	<a href="/analysis/article-200241809" class="img" ><img src="https://d7-invdn-com.akamaized.net/company_logo/200244477_1441640812.png"  onerror="javascript:this.src='https://i-invdn-com.akamaized.net/anonym.gif';" ></a>	<div class="textDiv">
							<a href="/analysis/article-200241809" title="Долларовые "быки" вернули себе контроль над рынком" class="title" >Долларовые "быки" вернули себе контроль над рынком</a>
	                			</div>
	<div class="clear"></div>
</article><article class="articleItem  ">
	<a href="/analysis/article-200241890" class="img" ><img src="https://d7-invdn-com.akamaized.net/company_logo/43fb9bbc0f27aba60f7dd6bb210541aa.jpg"  onerror="javascript:this.src='https://i-invdn-com.akamaized.net/anonym.gif';" ></a>	<div class="textDiv">
							<a href="/analysis/article-200241890" title="Прогноз форекс на неделю: 01.10.2018 - 05.10.2018" class="title" >Прогноз форекс на неделю: 01.10.2018 - 05.10.2018 <span class="middle videoIconNew">&nbsp;</span> </a>
	                			</div>
	<div class="clear"></div>
</article></div>
		        </div>
					        </div>

        </div>



    </div>

    <!--Templates-->

    <!--
        Group Templates
    -->
    <!--Quote group template-->
    <div class="js-group-template textBox displayNone">
        <div class="js-group-title groupHeader"></div>
        <div class="js-group-results newResultsContainer">
        </div>
    </div>

    <!--News items group template-->
    <div class="js-news-items-group-template searchAside_item displayNone">
        <a class="asideTitle js-group-more-link">
            <div class="js-group-title groupHeader"></div>
            <span>Еще</span>
        </a>
        <div class="js-group-results articles smallTitle1 analysisImg"></div>
    </div>

    <!--
        Row Templates
    -->
    <!--Quote row template-->
    <a class="row js-quote-row-template js-quote-item displayNone">
        <span class="first flag"><i class="ceFlags middle js-quote-item-flag"></i></span>
        <span class="second js-quote-item-symbol symbolName"></span>
        <span class="third js-quote-item-name"></span>
        <span class="fourth typeExchange js-quote-item-type"></span>
    </a>

    <!--News row template-->
    <article class="js-news-item-template articleItem displayNone">
        <a href="/" class="js-news-item-link">
            <img class="js-news-item-img" src="https://d1-invdn-com.akamaized.net/company_logo/d01b563261bcf223986f7ac222680343.jpg">
            <div class="js-news-item-name textDiv"></div>
        </a>
    </article>

    <div class="js-tool-item-template displayNone">
        <a class="js-tool-item-link eventsAndTools">
            <span class="js-tool-item-name"></span>
        </a>
    </div>
</div>

<script type="text/javascript">
	window.topBarSearchData = {
		texts: {
			recentSearchText: 'История поиска',
			popularSearchText: 'Популярные запросы',
            newsSearchText: 'Новости',
            analysisSearchText: 'Аналитика',
            economicEventsSearchText: 'Экономические события',
            toolsSearchText: 'Инструменты и разделы',
			authorsSearchText: 'Авторы',
			webinarsSearchText: 'Вебинары'
		},
		popularSearches: [{"pairId":"13994","link":"\/equities\/tesla-motors","symbol":"TSLA","name":"Tesla Inc","flag":"USA","type":"\u0410\u043a\u0446\u0438\u0438 - NASDAQ","ci":"5"},{"pairId":"8833","link":"\/commodities\/brent-oil","symbol":"LCO","name":"\u0424\u044c\u044e\u0447\u0435\u0440\u0441 \u043d\u0430 \u043d\u0435\u0444\u0442\u044c Brent","flag":"UK","type":"\u0422\u043e\u0432\u0430\u0440 - ICE","ci":"4"},{"pairId":"13711","link":"\/equities\/sberbank_rts","symbol":"SBER","name":"\u0421\u0431\u0435\u0440\u0431\u0430\u043d\u043a \u041f\u0410\u041e","flag":"Russian_Federation","type":"\u0410\u043a\u0446\u0438\u0438 - \u041c\u043e\u0441\u043a\u0432\u0430","ci":"56"},{"pairId":"2186","link":"\/currencies\/usd-rub","symbol":"USD\/RUB","name":"\u0414\u043e\u043b\u043b\u0430\u0440 \u0421\u0428\u0410 \u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u0438\u0439 \u0440\u0443\u0431\u043b\u044c","flag":"Russian_Federation","type":"\u0412\u0430\u043b\u044e\u0442\u0430","ci":"56"},{"pairId":"6374","link":"\/equities\/research-in-motion-ltd","symbol":"BB","name":"BlackBerry Ltd","flag":"USA","type":"\u0410\u043a\u0446\u0438\u0438 - \u041d\u044c\u044e-\u0419\u043e\u0440\u043a","ci":"5"}]	};
</script>
		<div class="topBarTools">
			<span id="userAccount" class="topBarUserAvatar js-open-auth-trigger-inside" data-page-type="topBar">
					<div class="loggedIn inlineblock"><span onmouseover="_myAccountDropDown.render();" onmouseout="_myAccountDropDown.render();" class="topBarText bold pointer name"><img src="https://i-invdn-com.akamaized.net/defaultUserMaleTmp.png" class="userAvatar middle inlineblock" id="userImg" ><span class="myAccount topBarText">Владимир</span></span></div><div id="myAccountHeaderPop" class="tooltipPopup bigArrowTopbar noHeader displayNone myAccountSelPopup" onmouseover="_myAccountDropDown.render();" onmouseout="_myAccountDropDown.render();"><div class="content"><ul class="bold "><li style="width:px;"><span class="smallGrayArrow middle"></span><a href="/portfolio" class="arial_12 bold">Инвестпортфель</a></li><li style="width:px;"><span class="smallGrayArrow middle"></span><a href="/members-admin/alert-center" class="arial_12 bold">Мои уведомления</a></li><li style="width:px;"><span class="smallGrayArrow middle"></span><a href="/members-admin/alerts-feed" class="arial_12 bold">Лента уведомлений</a></li><li style="width:px;"><span class="smallGrayArrow middle"></span><a href="/members-admin/saved-items" class="arial_12 bold">Закладки</a></li><li style="width:px;"><span class="smallGrayArrow middle"></span><a href="/members-admin/content-analysis-list" class="arial_12 bold">Публикации</a></li><li style="width:px;"><span class="smallGrayArrow middle"></span><a href="/members-admin/content-analysis-add" class="arial_12 bold">Добавить статью</a></li></ul><div class="dottedBorder">&nbsp;</div><ul class="noBold "><li style="width:px;"><span class="smallGrayEmptyArrow middle"></span><a href="/members-admin/sentiments" class="arial_12 bold">Мой прогноз</a></li><li style="width:px;"><span class="smallGrayEmptyArrow middle"></span><a href="/members-admin/trading-accounts-list" class="arial_12 bold">Счета</a></li></ul><div class="dottedBorder">&nbsp;</div><ul class="noBold "><li style="width:px;"><span class="smallGrayEmptyArrow"></span><a href="/members-admin/ad-free" class="arial_12 bold">Версия без рекламы</a></li><li style="width:px;"><span class="smallGrayEmptyArrow"></span><a href="/members-admin/config-edit-user-details" class="arial_12 bold">Настройки</a></li><li style="width:px;"><span class="smallGrayEmptyArrow"></span><a href="/members-admin/logout?logoutToken=f74c3e7963bb4d856fcad4bd54129a90" class="arial_12 bold">Выйти</a></li></ul></div><!-- /subMenuOneColumn --></div><!-- /myAccountHeaderPop --><div id="settingsDropDown" class="subMenuOneColumnWrap hoverboxWithShadow settingsPopup" onmouseover="settingsDropDown.render();" onmouseout="settingsDropDown.render();"><div class="subMenuOneColumn"><ul class="bold "></ul><div class="dottedBorder">&nbsp;</div><ul class="noBold "></ul></div><!-- /subMenuOneColumn --><span class="arrow">&nbsp;</span></div><!-- /myAccountHeaderPop -->			</span>
				                <span id="topBarAlertCenterBtn" class="topBarIconWrap">
		                <i class="topBarAlertsIcon"></i><i class="topBarAlertBadge js-badge arial_10 displayNone">0</i></span>
				                <span id="portfolioTopBarBtn" class="topBarIconWrap">
		                <i class="topBarPortfolioIcon"></i></span>
				                <span id="topBarMarketBtn" class="topBarIconWrap">
		                <i class="topBarWorldMarketsIcon"></i></span>
			
			<div class="langSelect inlineblock">
				<div id="langSelect" class="inlineblock pointer" onmouseover="$('#editionContainer').fadeOut('fast'); flagsDropDown.render();" onmouseout="flagsDropDown.render();"><span class="ceFlags Russian_Federation middle inlineblock"></span></div>
				<div id="TopFlagsContainer" class="tooltipPopup countrySelPopup bigArrowTopbar noHeader displayNone" onmouseover="flagsDropDown.render();" onmouseout="flagsDropDown.render();">
					<div class="content">
						<ul><li><a href="https://www.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to www.investing.com'); setTimeout('document.location = \'https://www.investing.com\'', 500);return false;"><span class="ceFlags USA"></span>English (USA)</a></li><li><a href="https://tr.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to tr.investing.com'); setTimeout('document.location = \'https://tr.investing.com\'', 500);return false;"><span class="ceFlags Turkey"></span>Türkçe</a></li><li><a href="https://uk.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to uk.investing.com'); setTimeout('document.location = \'https://uk.investing.com\'', 500);return false;"><span class="ceFlags UK"></span>English (UK)</a></li><li><a href="https://sa.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to sa.investing.com'); setTimeout('document.location = \'https://sa.investing.com\'', 500);return false;"><span class="ceFlags Saudi_Arabia"></span>‏العربية‏</a></li><li><a href="https://in.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to in.investing.com'); setTimeout('document.location = \'https://in.investing.com\'', 500);return false;"><span class="ceFlags India"></span>English (India)</a></li><li><a href="https://gr.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to gr.investing.com'); setTimeout('document.location = \'https://gr.investing.com\'', 500);return false;"><span class="ceFlags Greece"></span>Ελληνικά</a></li><li><a href="https://ca.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to ca.investing.com'); setTimeout('document.location = \'https://ca.investing.com\'', 500);return false;"><span class="ceFlags Canada"></span>English (Canada)</a></li><li><a href="https://se.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to se.investing.com'); setTimeout('document.location = \'https://se.investing.com\'', 500);return false;"><span class="ceFlags Sweden"></span>Svenska</a></li><li><a href="https://au.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to au.investing.com'); setTimeout('document.location = \'https://au.investing.com\'', 500);return false;"><span class="ceFlags Australia"></span>English (Australia)</a></li><li><a href="https://fi.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to fi.investing.com'); setTimeout('document.location = \'https://fi.investing.com\'', 500);return false;"><span class="ceFlags Finland"></span>Suomi</a></li><li><a href="https://za.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to za.investing.com'); setTimeout('document.location = \'https://za.investing.com\'', 500);return false;"><span class="ceFlags South_Africa"></span>English (South Africa)</a></li><li><a href="https://il.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to il.investing.com'); setTimeout('document.location = \'https://il.investing.com\'', 500);return false;"><span class="ceFlags Israel"></span>עברית</a></li><li><a href="https://de.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to de.investing.com'); setTimeout('document.location = \'https://de.investing.com\'', 500);return false;"><span class="ceFlags Germany"></span>Deutsch</a></li><li><a href="https://jp.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to jp.investing.com'); setTimeout('document.location = \'https://jp.investing.com\'', 500);return false;"><span class="ceFlags Japan"></span>日本語</a></li><li><a href="https://es.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to es.investing.com'); setTimeout('document.location = \'https://es.investing.com\'', 500);return false;"><span class="ceFlags Spain"></span>Español (España)</a></li><li><a href="https://kr.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to kr.investing.com'); setTimeout('document.location = \'https://kr.investing.com\'', 500);return false;"><span class="ceFlags South_Korea"></span>한국어</a></li><li><a href="https://mx.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to mx.investing.com'); setTimeout('document.location = \'https://mx.investing.com\'', 500);return false;"><span class="ceFlags Mexico"></span>Español (México)</a></li><li><a href="https://cn.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to cn.investing.com'); setTimeout('document.location = \'https://cn.investing.com\'', 500);return false;"><span class="ceFlags China"></span>中文</a></li><li><a href="https://fr.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to fr.investing.com'); setTimeout('document.location = \'https://fr.investing.com\'', 500);return false;"><span class="ceFlags France"></span>Français</a></li><li><a href="https://hk.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to hk.investing.com'); setTimeout('document.location = \'https://hk.investing.com\'', 500);return false;"><span class="ceFlags Hong_Kong"></span>香港</a></li><li><a href="https://it.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to it.investing.com'); setTimeout('document.location = \'https://it.investing.com\'', 500);return false;"><span class="ceFlags Italy"></span>Italiano</a></li><li><a href="https://id.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to id.investing.com'); setTimeout('document.location = \'https://id.investing.com\'', 500);return false;"><span class="ceFlags Indonesia"></span>Bahasa Indonesia</a></li><li><a href="https://nl.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to nl.investing.com'); setTimeout('document.location = \'https://nl.investing.com\'', 500);return false;"><span class="ceFlags Netherlands"></span>Nederlands</a></li><li><a href="https://ms.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to ms.investing.com'); setTimeout('document.location = \'https://ms.investing.com\'', 500);return false;"><span class="ceFlags Malaysia"></span>Bahasa Melayu</a></li><li><a href="https://pt.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to pt.investing.com'); setTimeout('document.location = \'https://pt.investing.com\'', 500);return false;"><span class="ceFlags Portugal"></span>Português (Portugal)</a></li><li><a href="https://th.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to th.investing.com'); setTimeout('document.location = \'https://th.investing.com\'', 500);return false;"><span class="ceFlags Thailand"></span>ไทย</a></li><li><a href="https://pl.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to pl.investing.com'); setTimeout('document.location = \'https://pl.investing.com\'', 500);return false;"><span class="ceFlags Poland"></span>Polski</a></li><li><a href="https://vn.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to vn.investing.com'); setTimeout('document.location = \'https://vn.investing.com\'', 500);return false;"><span class="ceFlags Vietnam"></span>Tiếng Việt</a></li><li><a href="https://br.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to br.investing.com'); setTimeout('document.location = \'https://br.investing.com\'', 500);return false;"><span class="ceFlags Brazil"></span>Português (Brasil)</a></li></ul>					</div>
				</div>
			</div>
			<!-- GEO POPUP -->
						<!-- /GEO POPUP -->
		</div>
	</div>
	<!-- /topBarInnerWrapper -->
</div><!-- /topBar -->

<script type="text/javascript">


	loader([{
		type: 'component', value: 'Translate'
	}]).ready(function(Translate) {
		Translate
			.setDictionary('TopBarAlertsSignInPopup', {
				'popupHeader': 'Последние уведомления',
				'notLoggedInText': "\u0417\u0430\u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0438\u0440\u0443\u0439\u0442\u0435\u0441\u044c, \u0447\u0442\u043e\u0431\u044b \u0441\u043e\u0437\u0434\u0430\u0432\u0430\u0442\u044c \u0443\u0432\u0435\u0434\u043e\u043c\u043b\u0435\u043d\u0438\u044f \u043f\u043e \u0438\u043d\u0441\u0442\u0440\u0443\u043c\u0435\u043d\u0442\u0430\u043c, \n\u044d\u043a\u043e\u043d\u043e\u043c\u0438\u0447\u0435\u0441\u043a\u0438\u043c \u0441\u043e\u0431\u044b\u0442\u0438\u044f\u043c \u0438 \u0430\u043d\u0430\u043b\u0438\u0442\u0438\u043a\u0435.",
				'iconClasses': 'signinIcon',
				'popupClasses': 'topBarAlertsSignInPopup'
			})
			.setDictionary('TopBarPortfolioSignInPopup', {
				'popupHeader': 'Портфель',
				'notLoggedInText': "\u0417\u0430\u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0438\u0440\u0443\u0439\u0442\u0435\u0441\u044c, \u0447\u0442\u043e\u0431\u044b \u0441\u043e\u0437\u0434\u0430\u0442\u044c \u0441\u043e\u0431\u0441\u0442\u0432\u0435\u043d\u043d\u044b\u0439 \u0438\u043d\u0432\u0435\u0441\u0442\u0438\u0446\u0438\u043e\u043d\u043d\u044b\u0439 \u043f\u043e\u0440\u0442\u0444\u0435\u043b\u044c \u0438 \u0441\u043f\u0438\u0441\u043e\u043a \u043d\u0430\u0431\u043b\u044e\u0434\u0435\u043d\u0438\u044f",
				'iconClasses': 'signinIconPortfolio',
				'popupClasses': 'topBarPortfolioBox'
			});
	})
</script>
<div id="navBar" class="navBar">
    <nav id="navMenu" class="navMenuWrapper">
        <ul class="navMenuUL">
            <li>
                <a href="/markets/" class="nav">Котировки</a>
                <ul class="subMenuNav">
                    <li class="row"><!--firstRow-->
            <a href="/currencies/">Форекс</a>
            <div class="navBarDropDown"  >
    <ul class="main"  >
        
                                        <li><a href="/currencies/single-currency-crosses">Кросс-курсы валют</a></li>
                    
                                        <li><a href="/currencies/streaming-forex-rates-majors">Основные валюты</a></li>
                    
                                        <li><a href="/currencies/live-currency-cross-rates">Валютные кроссы</a></li>
                    
                                        <li><a href="/currencies/exchange-rates-table">Таблица кросс-курсов</a></li>
                    
                                        <li><a href="/quotes/us-dollar-index">Индекс USD</a></li>
                    
                                        <li><a href="/currencies/fx-futures">Фьючерсы Форекс</a></li>
                    
                                        <li><a href="/currencies/forex-options">Форекс-опционы</a></li>
                        </ul>
    <ul class="popular"  >
                                                <li><a href="/currencies/eur-usd">EUR/USD</a></li>
                                                            <li><a href="/currencies/gbp-usd">GBP/USD</a></li>
                                                            <li><a href="/currencies/usd-rub">USD/RUB</a></li>
                                                            <li><a href="/currencies/eur-rub">EUR/RUB</a></li>
                                                            <li><a href="/currencies/usd-jpy">USD/JPY</a></li>
                                                            <li><a href="/currencies/usd-chf">USD/CHF</a></li>
                                                            <li><a href="/crypto/bitcoin/btc-usd">BTC/USD</a></li>
                        </ul>
</div>        </li>
                            <li class="row"><!--firstRow-->
            <a href="/commodities/">Товары</a>
            <div class="navBarDropDown"  >
    <ul class="main"  >
        
                                        <li><a href="/commodities/real-time-futures">Биржевые товары</a></li>
                    
                                        <li><a href="/commodities/metals">Металлы</a></li>
                    
                                        <li><a href="/commodities/softs">Товары softs</a></li>
                    
                                        <li><a href="/commodities/meats">Мясо</a></li>
                    
                                        <li><a href="/commodities/energies">Энергоносители</a></li>
                    
                                        <li><a href="/commodities/grains">Зерно</a></li>
                    
                                        <li><a href="/indices/commodities-indices">Сырьевые индексы</a></li>
                        </ul>
    <ul class="popular"  >
                                                <li><a href="/commodities/gold">Золото</a></li>
                                                            <li><a href="/commodities/brent-oil">Нефть Brent</a></li>
                                                            <li><a href="/commodities/crude-oil">Нефть WTI</a></li>
                                                            <li><a href="/commodities/silver">Серебро</a></li>
                                                            <li><a href="/commodities/natural-gas">Природный газ</a></li>
                                                            <li><a href="/commodities/copper">Медь</a></li>
                        </ul>
</div>        </li>
                            <li class="row"><!--firstRow-->
            <a href="/indices/">Индексы</a>
            <div class="navBarDropDown"  >
    <ul class="main"  >
        
                                        <li><a href="/indices/russia-indices">Индексы РФ</a></li>
                    
                                        <li><a href="/indices/major-indices">Основные индексы</a></li>
                    
                                        <li><a href="/indices/world-indices">Мировые индексы</a></li>
                    
                                        <li><a href="/indices/global-indices">Глобальные индексы</a></li>
                    
                                        <li><a href="/indices/indices-futures">Фьючерсы</a></li>
                    
                                        <li><a href="/indices/indices-cfds">CFD на индексы</a></li>
                        </ul>
    <ul class="popular"  >
                                                <li><a href="/indices/rtsi">РТС</a></li>
                                                            <li><a href="/indices/mcx">Индекс МосБиржи</a></li>
                                                            <li><a href="/indices/us-30">Dow 30</a></li>
                                                            <li><a href="/indices/us-spx-500">S&P 500</a></li>
                                                            <li><a href="/indices/germany-30">DAX</a></li>
                                                            <li><a href="/indices/nasdaq-composite">Nasdaq</a></li>
                        </ul>
</div>        </li>
                            <li class="row"><!--firstRow-->
            <a href="/equities/">Акции</a>
            <div class="navBarDropDown"  >
    <ul class="main"  >
        
                                        <li><a href="/stock-screener/">Фильтр акций</a></li>
                    
                                        <li><a href="/equities/trending-stocks">Трендовые акции</a></li>
                    
                                        <li><a href="/equities/russia">Россия</a></li>
                    
                                        <li><a href="/equities/pre-market">Премаркет США</a></li>
                    
                                        <li><a href="/equities/europe">Европа</a></li>
                    
                                        <li><a href="/equities/americas">Америка</a></li>
                    
                                        <li><a href="/equities/52-week-high">52-нед. максимум</a></li>
                    
                                        <li><a href="/equities/52-week-low">52-нед. минимум</a></li>
                    
                                        <li><a href="/equities/most-active-stocks">Активные</a></li>
                    
                                        <li><a href="/equities/top-stock-gainers">Лидеры роста</a></li>
                    
                                        <li><a href="/equities/top-stock-losers">Лидеры падения</a></li>
                    
                                        <li><a href="/equities/world-adrs">Мировые АДР</a></li>
                    
                                        <li><a href="/equities/russia-adrs">Россия - АДР</a></li>
                        </ul>
    <ul class="popular"  >
                                                <li><a href="/equities/sberbank_rts">Сбербанк</a></li>
                                                            <li><a href="/equities/severstal_rts">Северсталь</a></li>
                                                            <li><a href="/equities/gazprom_rts">Газпром</a></li>
                                                            <li><a href="/equities/rosneft_rts">Роснефть</a></li>
                                                            <li><a href="/equities/lukoil_rts">ЛУКОЙЛ</a></li>
                                                            <li><a href="/equities/rostelecom">Ростелеком</a></li>
                        </ul>
</div>        </li>
                            <li class="row"><!--firstRow-->
            <a href="/etfs/">ETF</a>
            <div class="navBarDropDown"  >
    <ul class="main"  >
        
                                        <li><a href="/etfs/major-etfs">Основные ETF</a></li>
                    
                                        <li><a href="/etfs/world-etfs">Мировые ETF</a></li>
                    
                                        <li><a href="/etfs/usa-etfs">США - ETF</a></li>
                        </ul>
    <ul class="popular"  >
                                                <li><a href="/etfs/spdr-s-p-500">SPDR S&P 500</a></li>
                                                            <li><a href="/etfs/ishares-msci-emg-markets">iShares MSCI Emerging Markets</a></li>
                                                            <li><a href="/etfs/spdr-gold-trust">SPDR Gold Shares</a></li>
                                                            <li><a href="/etfs/powershares-qqqq">Invesco QQQ Trust Series 1</a></li>
                        </ul>
</div>        </li>
                            <li class="row"><!--firstRow-->
            <a href="/funds/">Фонды</a>
            <div class="navBarDropDown"  >
    <ul class="main"  >
        
                                        <li><a href="/funds/world-funds">Мировые инвестфонды</a></li>
                    
                                        <li><a href="/funds/major-funds">Основные инвестфонды</a></li>
                        </ul>
    <ul class="popular"  >
                                                <li><a href="/funds/vanguard-total-stock-mkt-idx-instl">Vanguard Total Stock Mkt Idx Instl Sel</a></li>
                                                            <li><a href="/funds/pimco-commodity-real-ret-strat-c">PIMCO Commodity Real Ret Strat C</a></li>
                                                            <li><a href="/funds/vanguard-total-bond-market-index-ad">Vanguard Total Bond Market Index Adm</a></li>
                                                            <li><a href="/funds/vanguard-500-index-inv">Vanguard 500 Index Investor</a></li>
                        </ul>
</div>        </li>
                            <li class="row"><!--firstRow-->
            <a href="/rates-bonds/">Облигации</a>
            <div class="navBarDropDown"  >
    <ul class="main"  >
        
                                        <li><a href="/rates-bonds/financial-futures">Финансовые фьючерсы</a></li>
                    
                                        <li><a href="/rates-bonds/world-government-bonds">Гособлигации</a></li>
                    
                                        <li><a href="/rates-bonds/government-bond-spreads">Спреды доходности</a></li>
                    
                                        <li><a href="/rates-bonds/forward-rates">Форвардные курсы</a></li>
                    
                                        <li><a href="/indices/bond-indices">Облигационные индексы</a></li>
                        </ul>
    <ul class="popular"  >
                                                <li><a href="/rates-bonds/us-2-yr-t-note">US 2 YR T-Note</a></li>
                                                            <li><a href="/rates-bonds/us-10-yr-t-note">US 10 YR T-Note</a></li>
                                                            <li><a href="/rates-bonds/us-30-yr-t-bond">US 30Y T-Bond</a></li>
                                                            <li><a href="/rates-bonds/uk-gilt">UK Gilt</a></li>
                                                            <li><a href="/rates-bonds/euro-bund">Euro Bund</a></li>
                                                            <li><a href="/rates-bonds/euro-schatz">Euro SCHATZ</a></li>
                        </ul>
</div>        </li>
                            <li class="row"><!--firstRow-->
            <a href="/crypto/">Криптовалюты</a>
            <div class="navBarDropDown"  >
    <ul class="main"  >
        
                                        <li><a href="/crypto/currency-pairs">Пары с криптовалютами</a></li>
                    
                                        <li><a href="/crypto/currencies">Все криптовалюты</a></li>
                    
                                        <li><a href="/crypto/bitcoin">Bitcoin</a></li>
                    
                                        <li><a href="/crypto/ethereum">Ethereum</a></li>
                    
                                        <li><a href="/currency-converter/?tag=Cryptocurrency">Конвертер валют</a></li>
                        </ul>
    <ul class="popular"  >
                                                <li><a href="/crypto/bitcoin/btc-usd">BTC/USD</a></li>
                                                            <li><a href="/crypto/ethereum/eth-usd">ETH/USD</a></li>
                                                            <li><a href="/crypto/litecoin/ltc-usd">LTC/USD</a></li>
                                                            <li><a href="/crypto/ethereum-classic/etc-usd">ETC/USD</a></li>
                                                            <li><a href="/crypto/ethereum/eth-btc">ETH/BTC</a></li>
                        </ul>
</div>        </li>
            </ul>            </li>
                            <li >
					                    <a class="nav" href="//ru.investing.com/crypto/">Крипто</a>
                    <div class="navBarDropDown" style="width: 387px" >
    <ul class="main" style="width: 200px" >
        
                            <li class="navTitle">Криптовалюты</li>
                                
                                        <li><a href="/crypto/currencies">Все криптовалюты</a></li>
                    
                                        <li><a href="/crypto/currency-pairs">Пары с криптовалютами</a></li>
                    
                                        <li><a href="/crypto/ico-calendar">Календарь ICO</a></li>
                    
                                        <li><a href="/brokers/cryptocurrency-brokers">Брокеры криптовалют</a></li>
                    
                                        <li><a href="/crypto/bitcoin">Bitcoin</a></li>
                    
                                        <li><a href="/crypto/ethereum">Ethereum</a></li>
                    
                                        <li><a href="/crypto/ripple">Ripple</a></li>
                    
                                        <li><a href="/crypto/litecoin">Litecoin</a></li>
                    
                                        <li><a href="/currency-converter/?tag=Cryptocurrency">Конвертер валют</a></li>
                        </ul>
    <ul class="popular" style="width: 187px" >
                                    <li class="navTitle">Другие криптовалюты</li>
                                                                        <li><a href="/crypto/bitcoin/btc-usd">BTC/USD</a></li>
                                                            <li><a href="/crypto/ethereum/eth-usd">ETH/USD</a></li>
                                                            <li><a href="/crypto/litecoin/ltc-usd">LTC/USD</a></li>
                                                            <li><a href="/crypto/ethereum-classic/etc-usd">ETC/USD</a></li>
                                                            <li><a href="/crypto/ethereum/eth-btc">ETH/BTC</a></li>
                                                            <li><a href="/crypto/iota/iota-usd">IOTA/USD</a></li>
                                                            <li><a href="/crypto/ripple/xrp-usd">XRP/USD</a></li>
                                                            <li><a href="/crypto/bitcoin/bitcoin-futures">Фьючерс CME на Bitcoin</a></li>
                                                            <li><a href="/crypto/bitcoin/cboe-bitcoin-futures">Фьючерс CBOE на Bitcoin</a></li>
                        </ul>
</div>                </li>
                            <li >
					                    <a class="nav" href="//ru.investing.com/news/">Новости</a>
                    <div class="navBarDropDown" style="width: 367px" >
    <ul class="main" style="width: 180px" >
        
                            <li class="navTitle">Новости рынка</li>
                                
                                        <li><a href="/news/forex-news">Форекс</a></li>
                    
                                        <li><a href="/news/commodities-news">Сырьевые товары</a></li>
                    
                                        <li><a href="/news/stock-market-news">Фондовый рынок</a></li>
                    
                                        <li><a href="/news/economic-indicators">Экономпоказатели</a></li>
                    
                                        <li><a href="/news/economy-news">Экономика</a></li>
                    
                                        <li><a href="/news/cryptocurrency-news">Криптовалюты</a></li>
                        </ul>
    <ul class="popular" style="width: 187px" >
                                    <li class="navTitle">Дополнительно</li>
                                                                        <li><a href="/news/most-popular-news">Самое популярное</a></li>
                                                            <li><a href="/news/politics-news">Политика</a></li>
                                                            <li><a href="/economic-calendar/">Экономический календарь</a></li>
                        </ul>
</div>                </li>
                            <li >
					                    <a class="nav" href="//ru.investing.com/analysis/">Аналитика</a>
                    <div class="navBarDropDown" style="width: 346px" >
    <ul class="main" style="width: 159px" >
        
                            <li class="navTitle">Мнения</li>
                                
                                        <li><a href="/analysis/market-overview">Обзор рынка</a></li>
                    
                                        <li><a href="/analysis/forex">Форекс</a></li>
                    
                                        <li><a href="/analysis/stock-markets">Рынки акций</a></li>
                    
                                        <li><a href="/analysis/commodities">Сырьевые товары</a></li>
                    
                                        <li><a href="/analysis/bonds">Облигации</a></li>
                    
                                        <li><a href="/analysis/cryptocurrency">Криптовалюты</a></li>
                        </ul>
    <ul class="popular" style="width: 187px" >
                                    <li class="navTitle">Дополнительно</li>
                                                                        <li><a href="/analysis/most-popular-analysis">Самое популярное</a></li>
                                                            <li><a href="/analysis/editors-picks">Выбор редакции</a></li>
                                                            <li><a href="/analysis/comics">Карикатуры</a></li>
                        </ul>
</div>                </li>
                            <li class="selected">
					                    <a class="nav" href="//ru.investing.com/technical/">Теханализ</a>
                    <div class="navBarDropDown" style="width: 439px" >
    <ul class="main" style="width: 219px" >
        
                            <li class="navTitle">Инструменты</li>
                                
                                        <li><a href="/technical/personalized-quotes-technical-summary">Технический обзор</a></li>
                    
                                        <li><a href="/technical/technical-analysis">Технический анализ</a></li>
                    
                                        <li><a href="/technical/pivot-points">Точки разворота</a></li>
                    
                                        <li><a href="/technical/moving-averages">Скользящие средние</a></li>
                    
                                        <li><a href="/technical/indicators">Индикаторы</a></li>
                    
                                        <li><a href="/technical/candlestick-patterns">Свечные модели</a></li>
                        </ul>
    <ul class="popular" style="width: 220px" >
                                    <li class="navTitle">Дополнительно</li>
                                                                        <li><a href="/technical/candlestick-patterns">Свечные модели</a></li>
                                                            <li><a href="/tools/fibonacci-calculator">Калькулятор Фибоначчи</a></li>
                                                            <li><a href="/tools/pivot-point-calculator">Калькулятор точки разворота</a></li>
                        </ul>
</div>                </li>
                            <li >
					                    <a class="nav" href="//ru.investing.com/charts/">Графики</a>
                    <div class="navBarDropDown" style="width: 430px" >
    <ul class="main" style="width: 210px" >
        
                            <li class="navTitle">Графики в реальном времени</li>
                                
                                        <li><a href="/charts/live-charts">Интерактивные графики</a></li>
                    
                                        <li><a href="/charts/forex-charts">Форекс</a></li>
                    
                                        <li><a href="/charts/futures-charts">Фьючерсы</a></li>
                    
                                        <li><a href="/charts/stocks-charts">Акции</a></li>
                    
                                        <li><a href="/charts/indices-charts">Индексы</a></li>
                    
                                        <li><a href="/charts/cryptocurrency-charts">Графики криптовалют</a></li>
                        </ul>
    <ul class="popular" style="width: 220px" >
                                                <li><a href="/charts/real-time-forex-charts">Интерактивный график Форекс</a></li>
                                                            <li><a href="/charts/real-time-futures-charts">График фьючерсов</a></li>
                                                            <li><a href="/charts/real-time-indices-charts">Интерактивный график индексов</a></li>
                                                            <li><a href="/charts/real-time-stocks-charts">Интерактивный график акций</a></li>
                                                            <li><a href="/charts/multiple-forex-streaming-charts">Потоковые графики Форекс</a></li>
                                                            <li><a href="/charts/multiple-indices-streaming-charts">Потоковые графики индексов</a></li>
                        </ul>
</div>                </li>
                            <li >
					                    <a class="nav" href="//ru.investing.com/brokers/">Брокеры</a>
                    <div class="navBarDropDown" style="width: 360px" >
    <ul class="main" style="width: 180px" >
        
                            <li class="navTitle">Брокеры</li>
                                
                                        <li><a href="/brokers/forex-brokers">Форекс</a></li>
                    
                                        <li><a href="/brokers/cryptocurrency-brokers">Брокеры криптовалют</a></li>
                    
                                        <li><a href="/brokers/cfd-brokers">CFD</a></li>
                    
                                        <li><a href="/brokers/stock-brokers">Акции</a></li>
                    
                                        <li><a href="/brokers/promotions">Промоакции</a></li>
                    
                                        <li><a href="/brokers/compare-spreads-eur-usd">Сравнить спреды</a></li>
                    
                                        <li><a href="/brokers/compare-quotes-eur-usd">Сравнить котировки</a></li>
                        </ul>
    <ul class="popular" style="width: 180px" >
                                    <li class="navTitle">Дополнительно</li>
                                                                        <li><a href="/brokers/forex-demo-accounts">Демо-счета Форекс</a></li>
                                                            <li><a href="/brokers/forex-live-accounts">Счета Forex Live</a></li>
                                                            <li><a href="/brokers/press-releases">Пресс-релизы</a></li>
                                                            <li><a href="/brokers/interviews">Интервью</a></li>
                                                            <li><a href="/brokers/options-brokers">Опционы</a></li>
                                                            <li><a href="/brokers/spread-betting-brokers">Спред-беттинг</a></li>
                                                            <li><a href="/brokers/regulation">Регуляторы</a></li>
                        </ul>
</div>                </li>
                            <li >
					                    <a class="nav" href="//ru.investing.com/tools/">Инструменты</a>
                    <div class="navBarDropDown" style="width: 360px" >
    <ul class="main" style="width: 180px" >
        
                            <li class="navTitle">Календари</li>
                                
                                        <li><a href="/economic-calendar/">Экономический календарь</a></li>
                    
                                        <li><a href="/holiday-calendar/">Календарь праздников</a></li>
                    
                                        <li><a href="/earnings-calendar/">Календарь отчетности</a></li>
                    
                                        <li><a href="/dividends-calendar/">Календарь дивидендов</a></li>
                    
                                        <li><a href="/stock-split-calendar/">Календарь сплитов</a></li>
                    
                                        <li><a href="/ipo-calendar/">Календарь IPO</a></li>
                    
                                        <li><a href="/futures-expiration-calendar/">Календарь экспирации фьючерсов</a></li>
                    
                            <li class="navTitle">Инструменты</li>
                                
                                        <li><a href="/stock-screener/">Фильтр акций</a></li>
                    
                                        <li><a href="/central-banks/fed-rate-monitor">Прогноз по ставке ФРС</a></li>
                    
                                        <li><a href="/currency-converter/">Конвертер валют</a></li>
                    
                                        <li><a href="/tools/fibonacci-calculator">Калькулятор Фибоначчи</a></li>
                        </ul>
    <ul class="popular" style="width: 180px" >
                                    <li class="navTitle">Дополнительно</li>
                                                                        <li><a href="/tools/correlation-calculator">Корреляция Форекс</a></li>
                                                            <li><a href="/tools/pivot-point-calculator">Калькулятор точки разворота</a></li>
                                                            <li><a href="/tools/profit-calculator">Калькулятор доходности</a></li>
                                                            <li><a href="/tools/margin-calculator">Калькулятор маржи</a></li>
                                                            <li><a href="/tools/currency-heatmap">Тепловая карта валют</a></li>
                                                            <li><a href="/tools/forex-volatility-calculator">Калькулятор волатильности</a></li>
                                                            <li><a href="/tools/forward-rates-calculator">Калькулятор форвардных курсов</a></li>
                                                            <li><a href="/tools/mortgage-calculator">Ипотечный калькулятор</a></li>
                        </ul>
</div>                </li>
                            <li >
					                    <a class="nav" href="//ru.investing.com/portfolio/">Инвестпортфель</a>
                                    </li>
                            <li class="last">
					                    <a class="nav pointer" >Еще</a>
                    <div class="navBarDropDown" style="width: 327px" >
    <ul class="main" style="width: 159px" >
        
                                        <li><a href="/directory/traders/">Рейтинг торговых счетов</a></li>
                    
                                        <li><a href="/education/">Обучение</a></li>
                    
                                        <li><a href="/education/webinars">Вебинары</a></li>
                    
                                        <li><a href="/education/forex-trading-kit">Руководство трейдера</a></li>
                    
                                        <li><a href="/central-banks/">Банки</a></li>
                    
                                        <li><a href="/webmaster-tools/">Информеры</a></li>
                    
                                        <li><a href="/broker-blacklist/">Черный список</a></li>
                    
                                        <li><a href="/members-admin/ad-free">Версия без рекламы</a></li>
                        </ul>
    <ul class="popular" style="width: 168px" >
                                    <li class="navTitle">Программы</li>
                                                                        <li><a href="/directory/Торговые-Платформы">Торговые платформы</a></li>
                                                            <li><a href="/directory/Приложения-для-Графиков">Приложения для графиков</a></li>
                                                            <li><a href="/directory/Сигналы-Системы">Сигналы/Системы</a></li>
                        </ul>
</div>                </li>
                    </ul>
    </nav>

    <!-- /grey sub menu -->
    <nav id="subNav" class="subMenuWrapper">
        <ul class="subNavUL">
                        <li  >
			            <a href="/technical/personalized-quotes-technical-summary" >                Технический обзор            </a>            </li>
                    <li  >
			            <a href="/technical/technical-analysis" >                Технический анализ            </a>            </li>
                    <li  >
			            <a href="/technical/pivot-points" >                Точки разворота            </a>            </li>
                    <li  >
			            <a href="/technical/moving-averages" >                Скользящие средние            </a>            </li>
                    <li  >
			            <a href="/technical/indicators" >                Индикаторы            </a>            </li>
                    <li class="last selected" >
			            <a href="/technical/candlestick-patterns" >                Свечные модели            </a>            </li>
                </ul>
    </nav>
</div>

</header>

<script type='text/javascript'>
window.Adomik = window.Adomik || {};
    Adomik.randomAdGroup = function() {
    var rand = Math.random();
    switch (false) {
    case !(rand < 0.09): return "ad_ex" + (Math.floor(100 * rand));
    case !(rand < 0.10): return "ad_bc";
    default: return "ad_opt";
    }
};
</script> 

<div class="midHeader">
        <div id="sln-hbanner" class="wideTopBanner"><div id='div-gpt-ad-1441189641022-23' style='height:250px; display: none;'></div><div id='div-gpt-ad-1370187203350-head1' style='height:90px;'></div></div>
</div>


<div id="theBigX" class="largeBannerCloser" onClick="javascript:onBigX();"></div>

<script type='text/javascript'>

	function onBigX() {
		var slotLeaderboard;
		$('#theBigX').fadeOut('fast');
		$('.midHeader').animate({
			height: '90px'
		},200 );
		googletag.cmd.push(function() {
			slotLeaderboard = googletag.defineSlot('/6938/FP_RU_site/FP_RU_Leaderboard_Default', [[728, 90], [970, 90]], 'div-gpt-ad-1370187203350-head1').setTargeting('ad_group', Adomik.randomAdGroup()).addService(googletag.pubads());
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1370187203350-head1'); });
		});
				$('#div-gpt-ad-1441189641022-23').hide();
		$('#div-gpt-ad-1370187203350-head1').show();
	}

	function getMaxLeaderBoard() {
		return (getCookie("geoC") || '').toLowerCase() === 'us' ? 5 : 3;
	}

	function checkBigAd() {
		var counter = +getCookie("billboardCounter_7") || 0;
		if( true && counter === 0 && true) {
			$('.midHeader').height('250px');
			$('#div-gpt-ad-1441189641022-23').show();
			$('#div-gpt-ad-1370187203350-head1').hide();
			$('#theBigX').show();
			googletag.cmd.push(function() {
				googletag.defineSlot('/6938/FP_RU_site/FP_RU_Billboard_Default',  [970, 250] , 'div-gpt-ad-1441189641022-23').setTargeting('ad_group', Adomik.randomAdGroup()).addService(googletag.pubads());
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1441189641022-23'); });
			});
		} else {
			googletag.cmd.push(function() {
				googletag.defineSlot('/6938/FP_RU_site/FP_RU_Leaderboard_Default', [[728, 90], [970, 90]], 'div-gpt-ad-1370187203350-head1').setTargeting('ad_group', Adomik.randomAdGroup()).addService(googletag.pubads());
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1370187203350-head1'); });
			});
		}

		counter++;
		if(counter === getMaxLeaderBoard()) {
			counter = 0;
		}
		setCookie("billboardCounter_7", counter);


	    
		
		
	}

</script>



          		<!-- #2 columns template -->
  		<section id="leftColumn" >
        

<h1 class="float_lang_base_1 relativeAttr"
	dir="ltr" >Свечные модели	</h1>



	<div class="headBtnWrapper float_lang_base_2">
		<a class="newBtn infoBox LightGray noText" onmouseover="_infoBox.render();" onmouseout="_infoBox.render();">&nbsp;
			<div id="infoBoxToolTipWrap" class="tooltipPopup noTriangle">
				<div id="infoBoxToolTip">
					<span class="noBold arial_12">Комплексный инструмент отслеживания японских свечных моделей в реальном времени. Включает данные со всех крупнейших финансовых рынков: акции, индексы, фьючерсы и сырьевые товары, Форекс и CFD. Японские свечные модели — это распространенный метод технического анализа для прогноза направления цены.</span>
				</div>
			</div>
		</a>
	</div>
	<div class="headBtnWrapper float_lang_base_2">
			</div>






<div class="clear"></div>
<div class="innerHeaderSeperatorBottom"></div>
		<!-- Page Content -->
            <script type="text/javascript">
                window.siteData = {
                    htmlDirection: 'ltr',
                    decimalPoint: ',' || '.',
                    thousandSep: '.' || ',',
                    isEu : true,
                    userLoggedIn: true,
                    userHasPhoneRegistered: false,
                    currencyPosition: 'right',
                    datepicker: {
                        applyButton: 'Применить',
                        format: 'd/m/Y',
                        formatShort: 'd/m/y',
                        formatLong: 'd/m/Y',
                        formatSend: 'yy-mm-dd',
                        firstDay: '1',
                        dayNames: ["Вс","Пн","Вт","Ср","Чт","Пт","Сб"],
                        monthNamesShort: ["Янв.", "Фев.", "Март", "Апр.", "Май", "Июнь", "Июль", "Авг.", "Сент.", "Окт.", "Нояб.", "Дек."],
                        monthNames: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                        translations: {
                            custom: 'Выбрать даты',
                            start: 'Начало',
                            end: 'До окончания'
                        }
                    },
                    techTranslations: {"_Currencies_Strong_Buy":"\u0410\u043a\u0442\u0438\u0432\u043d\u043e \u043f\u043e\u043a\u0443\u043f\u0430\u0442\u044c","_Currencies_Strong_Sell":"\u0410\u043a\u0442\u0438\u0432\u043d\u043e \u043f\u0440\u043e\u0434\u0430\u0432\u0430\u0442\u044c","_specialBuy":"\u041f\u043e\u043a\u0443\u043f\u0430\u0442\u044c","_specialSell":"\u041f\u0440\u043e\u0434\u0430\u0432\u0430\u0442\u044c","_moving_avarge_tool_sell":"\u041f\u0440\u043e\u0434\u0430\u0432\u0430\u0442\u044c","_moving_avarge_tool_buy":"\u041f\u043e\u043a\u0443\u043f\u0430\u0442\u044c","_Currencies_Neutral":"\u041d\u0435\u0439\u0442\u0440\u0430\u043b\u044c\u043d\u043e","_not_available":"\u0414\u0430\u043d\u043d\u044b\u0435 \u043e\u0442\u0441\u0443\u0442\u0441\u0442\u0432\u0443\u044e\u0442","_Currencies_Less_Volatility":"\u041c\u0435\u043d\u0435\u0435 \u0432\u043e\u043b\u0430\u0442\u0438\u043b\u0435\u043d","_Currencies_More_Volatility":"\u0412\u044b\u0441\u043e\u043a\u0430\u044f \u0432\u043e\u043b\u0430\u0442-\u0442\u044c","_Currencies_Overbought":"\u041f\u0435\u0440\u0435\u043a\u0443\u043f\u043b\u0435\u043d\u043d\u043e\u0441\u0442\u044c","_Currencies_Oversold":"\u041f\u0435\u0440\u0435\u043f\u0440\u043e\u0434\u0430\u043d\u043d\u043e\u0441\u0442\u044c"},
                    edition: $('html').attr('lang')
                };
                                    window.siteData.blocked_users = [];
                
                (function (pattern) {
                    window.siteData.elapsedTimePattern = pattern.is('false') ? null : $.extend(JSON.parse(pattern), { justNow: 'Сейчас' });
                })('{"minutes":{"X \u043c\u0438\u043d\u0443\u0442\u0443 \u043d\u0430\u0437\u0430\u0434":[1,21,31,41,51],"X \u043c\u0438\u043d\u0443\u0442\u044b \u043d\u0430\u0437\u0430\u0434":["2-4","22-24","32-34","42-44","52-54"],"X \u043c\u0438\u043d\u0443\u0442 \u043d\u0430\u0437\u0430\u0434":["5-20","25-30","35-40","45-50","55-59"]},"hours":{"X \u0447\u0430\u0441 \u043d\u0430\u0437\u0430\u0434":[1,21],"X \u0447\u0430\u0441\u0430 \u043d\u0430\u0437\u0430\u0434":["2-4","22-24"],"X \u0447\u0430\u0441\u043e\u0432 \u043d\u0430\u0437\u0430\u0434":["5-20"]}}');

                                window.siteData.smlID = 10102;
                                                window.siteData.mmID = 25;
                
                window.truncatedCommentShowMoreDefine = 'Показать еще';
            </script>
            

<div class="newSocialButtons small">
    <a href="http://vkontakte.ru/share.php?url=https%3A%2F%2Fru.investing.com%2Ftechnical%2Fcandlestick-patterns" class="vk button genToolTip oneliner" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=615,width=600');return false;"  data-tooltip="Поделиться в ВКонтакте"><i class="vkLogo"></i></a><a href="https://www.facebook.com/sharer/sharer.php?app_id=179222828858436&u=https%3A%2F%2Fru.investing.com%2Ftechnical%2Fcandlestick-patterns&display=popup&ref=plugin" class="facebook button genToolTip oneliner" target="_blank" onclick="javascript:window.open(this.href, '', 'height=320,width=725');return false;" data-tooltip="Поделиться в Facebook"><i class="facebookLogo"></i></a><a href="javascript:void(0);" class="twitter button genToolTip oneliner" data-tooltip="Поделиться в Twitter"
                    onclick="openTwitPopup('/technical/candlestick-patterns', 10102,'commonPagesBySML','%D0%A1%D0%B2%D0%B5%D1%87%D0%BD%D1%8B%D0%B5+%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D0%B8','twitter',false);">
                    <i class="twitterLogo"></i></a><a href="javascript:void(0);" class="shareWFriend button genToolTip oneliner" data-tooltip="Отправить по электронной почте"
                    onclick="openSharePopup('/technical/candlestick-patterns', 10102,'commonPagesBySML', '%D0%A1%D0%B2%D0%B5%D1%87%D0%BD%D1%8B%D0%B5%20%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D0%B8%0A%0A','%D0%A1%D0%B2%D0%B5%D1%87%D0%BD%D1%8B%D0%B5%20%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D0%B8','shareLink',false,'1');">
                    <i class="newEnvelopeIcon"></i><span class="actionName"></span></a></div>
<script>
    $(document).ready(function () {
        $('.js-scrollto-comment').unbind('click').click(function (e) {
            e.preventDefault();
            var href = $(this).attr('href');
            if(href !=='#comments'){
                window.location = href;
                return false;
            }
            $.scrollTo($('#addAComment').offset().top-135,1000);
            setTimeout(function (e) {
                $('#addAComment textarea').focus();
            }, 1000);
        });
    });
</script><a href="javascript:void(0);" id="filterStateAnchor" class="newBtn filter candleStickOpenBtn LightGray float_lang_base_2">Настроить</a>
<div class="clear"></div>
<div class="clear"></div>
<div id="candleStick_filters" class="candleStickWrap displayNone" >
	<div class="ecoFilterBox inlineTitle js-category">
		<span>Временной период</span>
		<ul>
							<li><label><input  type="checkbox" name="time_frames[]" value="900"> 15 </label></li>
							<li><label><input  type="checkbox" name="time_frames[]" value="1800"> 30 </label></li>
							<li><label><input  type="checkbox" name="time_frames[]" value="3600"> 1H </label></li>
							<li><label><input  type="checkbox" name="time_frames[]" value="18000"> 5H </label></li>
							<li><label><input checked="checked" type="checkbox" name="time_frames[]" value="86400"> 1D </label></li>
							<li><label><input checked="checked" type="checkbox" name="time_frames[]" value="week"> 1W </label></li>
							<li><label><input  type="checkbox" name="time_frames[]" value="month"> 1M </label></li>
					</ul>
	</div>
	<div class="ecoFilterBox inlineTitle js-category">
		<span>Направление</span>
		<ul>
			<li><label><input type="checkbox" checked="checked" name="patterns_type[]" value="completed"> Завершенные модели</label></li>
			<li><label><input type="checkbox" checked="checked" name="patterns_type[]" value="emerging"> Новые модели</label></li>
		</ul>
	</div>
	<div class="ecoFilterBox inlineTitle js-category">
		<span>Тип</span>
		<ul>
			<li><label><input type="checkbox" checked="checked" name="directions[]" value="Bullish"> Бычий</label></li>
			<li><label><input type="checkbox" checked="checked" name="directions[]" value="Bearish"> Медвежий</label></li>
		</ul>
	</div>
	<div class="ecoFilterBox inlineTitle js-category">
		<span>Надежность</span>
		<div class="importanceFilter">
			<label><input type="checkbox"  name="importance[]" value="low"> <i class="reliabilityStarDarkIcon"></i><i class="reliabilityStarLightIcon"></i><i class="reliabilityStarLightIcon"></i></label>
			<label><input type="checkbox" checked="checked" name="importance[]" value="moderate"> <i class="reliabilityStarDarkIcon"></i><i class="reliabilityStarDarkIcon"></i><i class="reliabilityStarLightIcon"></i></label>
			<label><input type="checkbox" checked="checked" name="importance[]" value="high"> <i class="reliabilityStarDarkIcon"></i><i class="reliabilityStarDarkIcon"></i><i class="reliabilityStarDarkIcon"></i></label>
		</div>
	</div>
			<div id="pairs_table">
			<div class="ecoFilterBox customTechSummary">
	<div id="techSelectBox">
		<p class="arial_14 bold">Найти дополнительные активы для отображения</p>
		<!--  widgetsearch FROM   -->
    <div class="searchDiv newInput inputTextBox float_lang_base_1" id="searchDiv_techSearch">
        <div class="inlineblock searchBoxContainer topBarSearch " id="searchTextTopContainer_techSearch">
            <form onsubmit="return search_techSearch.allowdForm;" id="combineSearchForm_techSearch" action="/" method="post">
                <div class="inlineblock" id="searchBox_techSearch">
                <input type="text" autocomplete="off" class="searchText arial_12 lightgrayFont middle " id="searchText_techSearch" name="quotes_search_text"
                placeholder="USD/RUB или Газпром" tabindex="1"></div><label for="searchText_techSearch" class="searchGlassIcon">&nbsp;</label>
				<i class="cssSpinner"></i>
            </form>
        </div>
        <div class="clear"></div>

        <div class="searchPopupResults displayNone" id="subContainer_techSearch">
			            <div class="searchTabs" id="search_tabs_techSearch">
                <div id="searchBoxTabsTopTabsMenu">
                    <ul id="searchBoxTabsTopTabs_techSearch" class="tabsForBox">

                    </ul>
                </div>
            </div>
			            <div  id="countries_wrapper_techSearch" class="displayNone">
                <div class="newSiteIconsSprite smallTriangleArrowUp"></div>
                <div class="countriesDiv">
                    <table id="countries_table_techSearch">
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <div identifier="searchThing" class="textBox" id="searchTopResults_techSearch"></div>
            <!-- searchPopupResults -->
        </div>
    </div>

    <div id="search_loading_techSearch" style="display:none">
        <span class="inlineblock loading">&nbsp;</span>
    </div>
<div class="clear"></div>
<!--  END SECTION -->
<script>
    window.searchParams = window.searchParams || {};
    window.searches = window.searches || {};

    window.searchParams['techSearch'] = {
    	$newSearchWidget : true,
        $prefix:'techSearch',
        $AllCountries:'Все страны',
        $tabsinfo:'[{"id":"search_box_top_techSearchAll","name":"\u0412\u0441\u0435","select":true},{"id":"search_box_top_techSearchIndices","name":"\u0418\u043d\u0434\u0435\u043a\u0441\u044b","select":false},{"id":"search_box_top_techSearchStocks","name":"\u0410\u043a\u0446\u0438\u0438","select":false},{"id":"search_box_top_techSearchETFs","name":"ETF","select":false},{"id":"search_box_top_techSearchFunds","name":"\u0424\u043e\u043d\u0434\u044b","select":false},{"id":"search_box_top_techSearchCommodities","name":"\u0422\u043e\u0432\u0430\u0440\u044b","select":false},{"id":"search_box_top_techSearchForex","name":"\u0412\u0430\u043b\u044e\u0442\u044b","select":false},{"id":"search_box_top_techSearchBonds","name":"\u041e\u0431\u043b\u0438\u0433\u0430\u0446\u0438\u0438","select":false},{"id":"search_box_top_techSearchCountries","name":"\u0412\u0441\u0435 \u0441\u0442\u0440\u0430\u043d\u044b","select":false}]',
        $searchtype:'',
        $resultType:'userQuotes',
        $functionName:'addInstrumentToBox',
        $paramList:null,
		$url:'/instruments/Service/Search/',
        $no_search_results:'Ваш поиск не дал результатов'
    };

    window.searches['techSearch'] = new FXautoComplete(window.searchParams['techSearch']);

    $( document ).ready(function() {
        $('#searchText_techSearch').attr('tabindex', 1);
    });
</script>	</div>
	<div class="innerContainer" data-min="1" data-max="24">
		<span>Выбрано:
			<span class="js-techInstrumentCount">0</span> из <span class="js-techInstrumentCountTotal">0</span>
		</span>
		<ul id="techInstrumentsContainer">
			<li class="favItem js-techInstrumentLabel"><span></span><i class="bugCloseIcon"></i></li>
		</ul>
	</div>
</div>

<script>
	function addInstrumentToBox(pairObject) {
		Technical.addTechSearchBlock(pairObject);
	}
</script>		</div>
		<div class="ecoFilterBox submitBox align_center">
		<button class="js-candlestick-filters-submit newBtn Arrow LightGray " name="apply_filter" value="1" >Обновить</button>
	</div>
</div><table class="genTbl closedTbl patternTable js-csp-table">
    <thead>
    <tr>
		<th class="flag"></th>
        <th class="left " >Название</th>
                <th class="left">Временной период</th>
        <th class="left">Надежность</th>
        <th class="left">Модель</th>
        <th class="left genToolTip oneliner" data-tooltip="Х свечей назад">Свеча №</th>
        		<th class="icon"></th>
    </tr>
    </thead>
    <tbody id="patternTableBody" >
            <tr class="noHover">
            <td colspan="7" class="theDay">Новые модели</td>
        </tr>
                    <tr id="row0" onclick='Patterns.openDesc(0.)' data-time="">
				<td class="flag"><span class="middle ceFlags Russian_Federation"></span></td>
				<td class="left bold noWrap"><a class="js-stop-propagation "  title="USD/RUB - Доллар США Российский рубль" href="https://ru.investing.com/currencies/usd-rub-candlestick">USD/RUB</a></td>
					            <td class="left">1W</td>
	            <td class="left" title="Макс."><i class="reliabilityStarDarkIcon"></i><i class="reliabilityStarDarkIcon"></i><i class="reliabilityStarDarkIcon"></i></td>
				<td class="left bold">Three Black Crows</td>
				<td class="left">Текущая</td>
								<td class="icon" title="Медвежий разворот"><span class="bearRevIcon middle"></span></td>
            </tr>
                        <tr id="row1" onclick='Patterns.openDesc(1.)' data-time="">
				<td class="flag"><span class="middle ceFlags ethereum"></span></td>
				<td class="left bold noWrap"><a class="js-stop-propagation "  title="ETH/USD - Эфириум Доллар США" href="https://ru.investing.com/crypto/ethereum/eth-usd-candlestick">ETH/USD</a></td>
					            <td class="left">1D</td>
	            <td class="left" title="Средняя"><i class="reliabilityStarDarkIcon"></i><i class="reliabilityStarDarkIcon"></i><i class="reliabilityStarLightIcon"></i></td>
				<td class="left bold">Engulfing Bearish</td>
				<td class="left">Текущая</td>
								<td class="icon" title="Медвежий разворот"><span class="bearRevIcon middle"></span></td>
            </tr>
                            <tr class="noHover">
            <td colspan="7" class="theDay">Завершенные модели</td>
        </tr>
        
            <tr id="row2" onclick='Patterns.openDesc(2.)' data-time="23.09.2018">
				<td class="flag"><span class="middle ceFlags bitcoinCash"></span></td>
				<td class="left bold noWrap"><span class="aqPopupWrapper js-hover-me-wrapper"><a class="js-stop-propagation aqlink js-hover-me" hoverme="markets" data-pairid="1031333"  title="BCH/USD - Bitcoin Cash Доллар США" href="https://ru.investing.com/crypto/bitcoin-cash/bch-usd-candlestick">BCH/USD</a></span></td>
								<td class="left">1W</td>
				<td class="left" title="Макс."><i class="reliabilityStarDarkIcon"></i><i class="reliabilityStarDarkIcon"></i><i class="reliabilityStarDarkIcon"></i></td>
				<td class="left bold">Three Outside Up</td>
				<td class="left">1</td>
								<td class="icon" title="Бычий разворот"><span class="bullRevIcon"></span></td>
            </tr>
            
            <tr id="row3" onclick='Patterns.openDesc(3.)' data-time="23.09.2018">
				<td class="flag"><span class="middle ceFlags ripple"></span></td>
				<td class="left bold noWrap"><span class="aqPopupWrapper js-hover-me-wrapper"><a class="js-stop-propagation aqlink js-hover-me" hoverme="markets" data-pairid="1010782"  title="XRP/USD - Рипл Доллар США" href="https://ru.investing.com/crypto/ripple/xrp-usd-candlestick">XRP/USD</a></span></td>
								<td class="left">1W</td>
				<td class="left" title="Средняя"><i class="reliabilityStarDarkIcon"></i><i class="reliabilityStarDarkIcon"></i><i class="reliabilityStarLightIcon"></i></td>
				<td class="left bold">Doji Star Bearish</td>
				<td class="left">1</td>
								<td class="icon" title="Медвежий разворот"><span class="bearRevIcon"></span></td>
            </tr>
            
            <tr id="row4" onclick='Patterns.openDesc(4.)' data-time="16.09.2018">
				<td class="flag"><span class="middle ceFlags bitcoinCash"></span></td>
				<td class="left bold noWrap"><span class="aqPopupWrapper js-hover-me-wrapper"><a class="js-stop-propagation aqlink js-hover-me" hoverme="markets" data-pairid="1031333"  title="BCH/USD - Bitcoin Cash Доллар США" href="https://ru.investing.com/crypto/bitcoin-cash/bch-usd-candlestick">BCH/USD</a></span></td>
								<td class="left">1W</td>
				<td class="left" title="Средняя"><i class="reliabilityStarDarkIcon"></i><i class="reliabilityStarDarkIcon"></i><i class="reliabilityStarLightIcon"></i></td>
				<td class="left bold">Bullish Engulfing</td>
				<td class="left">2</td>
								<td class="icon" title="Бычий разворот"><span class="bullRevIcon"></span></td>
            </tr>
            
            <tr id="row5" onclick='Patterns.openDesc(5.)' data-time="16.09.2018">
				<td class="flag"><span class="middle ceFlags ripple"></span></td>
				<td class="left bold noWrap"><span class="aqPopupWrapper js-hover-me-wrapper"><a class="js-stop-propagation aqlink js-hover-me" hoverme="markets" data-pairid="1010782"  title="XRP/USD - Рипл Доллар США" href="https://ru.investing.com/crypto/ripple/xrp-usd-candlestick">XRP/USD</a></span></td>
								<td class="left">1W</td>
				<td class="left" title="Макс."><i class="reliabilityStarDarkIcon"></i><i class="reliabilityStarDarkIcon"></i><i class="reliabilityStarDarkIcon"></i></td>
				<td class="left bold">Three Inside Up</td>
				<td class="left">2</td>
								<td class="icon" title="Бычий разворот"><span class="bullRevIcon"></span></td>
            </tr>
            
            <tr id="row6" onclick='Patterns.openDesc(6.)' data-time="25.09.2018">
				<td class="flag"><span class="middle ceFlags Russian_Federation"></span></td>
				<td class="left bold noWrap"><span class="aqPopupWrapper js-hover-me-wrapper"><a class="js-stop-propagation aqlink js-hover-me" hoverme="markets" data-pairid="2186"  title="USD/RUB - Доллар США Российский рубль" href="https://ru.investing.com/currencies/usd-rub-candlestick">USD/RUB</a></span></td>
								<td class="left">1D</td>
				<td class="left" title="Средняя"><i class="reliabilityStarDarkIcon"></i><i class="reliabilityStarDarkIcon"></i><i class="reliabilityStarLightIcon"></i></td>
				<td class="left bold">Bullish doji Star</td>
				<td class="left">3</td>
								<td class="icon" title="Бычий разворот"><span class="bullRevIcon"></span></td>
            </tr>
                </tbody>
    <template class="displayNone js-csp-no-results">
        <table class="displayNone js-csp-no-results">
	        <tbody>
	            <tr class="noHover">
	                <td colspan="7" class="noResults center">Нет узнаваемых моделей</td>
	            </tr>
	        </tbody>
        </table>
    </template>
</table>

<script>
	$(document).ready(function(){
        Patterns.reliabilityIcons = {"Bullish\/reversal":"bullRevIcon","Bearish\/reversal":"bearRevIcon","Bullish\/continuation":"bullContIcon","Bearish\/continuation":"bearContIcon","rel\/low":"\u003Ci class=\"reliabilityStarDarkIcon\"\u003E\u003C\/i\u003E\u003Ci class=\"reliabilityStarLightIcon\"\u003E\u003C\/i\u003E\u003Ci class=\"reliabilityStarLightIcon\"\u003E\u003C\/i\u003E","rel\/medium":"\u003Ci class=\"reliabilityStarDarkIcon\"\u003E\u003C\/i\u003E\u003Ci class=\"reliabilityStarDarkIcon\"\u003E\u003C\/i\u003E\u003Ci class=\"reliabilityStarLightIcon\"\u003E\u003C\/i\u003E","rel\/moderate":"\u003Ci class=\"reliabilityStarDarkIcon\"\u003E\u003C\/i\u003E\u003Ci class=\"reliabilityStarDarkIcon\"\u003E\u003C\/i\u003E\u003Ci class=\"reliabilityStarLightIcon\"\u003E\u003C\/i\u003E","rel\/high":"\u003Ci class=\"reliabilityStarDarkIcon\"\u003E\u003C\/i\u003E\u003Ci class=\"reliabilityStarDarkIcon\"\u003E\u003C\/i\u003E\u003Ci class=\"reliabilityStarDarkIcon\"\u003E\u003C\/i\u003E"};
        Patterns.bindFilters();
    });

			window.techSummaryPairs = [{"pair_ID":2186,"name":"USD\/RUB","trans_name":"USD\/RUB"},{"pair_ID":945629,"name":"BTC\/USD","trans_name":"BTC\/USD"},{"pair_ID":997650,"name":"ETH\/USD","trans_name":"ETH\/USD"},{"pair_ID":1010782,"name":"XRP\/USD","trans_name":"XRP\/USD"},{"pair_ID":1031333,"name":"BCH\/USD","trans_name":"BCH\/USD"},{"pair_ID":1024821,"name":"EOS\/USD","trans_name":"EOS\/USD"},{"pair_ID":2148,"name":"USD\/KZT","trans_name":"USD\/KZT"},{"pair_ID":1656,"name":"EUR\/KZT","trans_name":"EUR\/KZT"}];
	</script>

<script type="text/javascript">
var texts = {
        _candlestick_patterns_bullish_reversal : 'Бычий разворот',
        _candlestick_patterns_bearish_reversal : 'Медвежий разворот',
        _candlestick_patterns_bullish_continuation : 'Продолжение бычьего тренда',
        _candlestick_patterns_bearish_continuation : 'Продолжение медвежьего тренда',
        _candlestick_patterns_Low : 'Мин.',
        _candlestick_patterns_medium : 'Средняя',
        _candlestick_patterns_High : 'Макс.',
        _candlestick_patterns_indication : 'Направление',
        _candlestick_patterns_reliability : 'Надежность',
        _candlestick_patterns_description : 'Описание',
        _candlestick_patterns_emerging_patterns : 'Новые модели',
        _candlestick_patterns_completed_patterns : 'Завершенные модели',
        _Candlestick_No_Patterns_Recognized : 'Нет узнаваемых моделей',
        _candlestick_patterns_current : 'Текущая',
        _candlestick_patterns_loading_msg : 'Загрузка...',
        _candlestick_patterns_candle_time : 'Время',
        _candlestick_patterns_candles_ago : 'Х свечей назад'
    }


    $(document).ready(function(){
        Patterns.patternsData = {};                    Patterns.patternsData[0] = {
                        desc: {
                            img: 'https://i-invdn-com.akamaized.net/tvc_patterns_images/35.gif',
                            type: 'Медвежий разворот',
                            reliability: 'Макс.',
                            desc: 'During an uptrend, three long black candles occur with consecutively lower closes. This pattern suggests that the market has been at a high price for too long, and investors are beginning to compensate for it.<br/>More significant if it appears after a mature advance.',
                            time: '',
                            candle: 0,
                            category: 'emerging'
                        }
                    };
                                        Patterns.patternsData[1] = {
                        desc: {
                            img: 'https://i-invdn-com.akamaized.net/tvc_patterns_images/9.gif',
                            type: 'Медвежий разворот',
                            reliability: 'Средняя',
                            desc: 'Occurring during an uptrend, this pattern characterized by a large black real body, which engulfs a white real body (it doesn’t need to engulf the shadows). This signifies that the uptrend has been hurt and the bears may be gaining strength. The Engulfing indicator is also the first two candles of the Three Outside patterns.<br/>It is a major reversal signal. <br/>Factors that are increasing this signal’s reliability: <BR/><br/>1) The first candlestick has a very small real body and the second candlestick a very large real body.<BR/><br/>2) The pattern appears after a protracted or very fast move.<BR/><br/>3) Heavy volume on the second black candlestick.<BR/><br/>4) The second candlestick engulfs more than one real body.',
                            time: '',
                            candle: 0,
                            category: 'emerging'
                        }
                    };
                                        Patterns.patternsData[2] = {
                        desc: {
                            img: 'https://i-invdn-com.akamaized.net/tvc_patterns_images/58.gif',
                            type: 'Бычий разворот',
                            reliability: 'Макс.',
                            desc: 'This pattern is a more reliable addition to the standard Engulfing pattern. <br/>A bullish Engulfing pattern occurs in the first two candles. <br/>The third candlestick is confirmation of the bullish trend reversal.',
                            time: '23.09.2018',
                            candle: 1,
                            category: 'completed'
                        }
                    };
                                        Patterns.patternsData[3] = {
                        desc: {
                            img: 'https://i-invdn-com.akamaized.net/tvc_patterns_images/8.gif',
                            type: 'Медвежий разворот',
                            reliability: 'Средняя',
                            desc: 'During an uptrend, the market builds strength on a long white candlestick and gaps up on the second candlestick. However, the second candlestick trades within a small range and closes at or near its open. This scenario generally shows erosion of confidence in the current trend. Confirmation of a trend reversal would be a lower open on the next candle.',
                            time: '23.09.2018',
                            candle: 1,
                            category: 'completed'
                        }
                    };
                                        Patterns.patternsData[4] = {
                        desc: {
                            img: 'https://i-invdn-com.akamaized.net/tvc_patterns_images/44.gif',
                            type: 'Бычий разворот',
                            reliability: 'Средняя',
                            desc: 'During a downtrend, the Bullish Engulfing depicts an opening at a new low and closes at or above the previous candle\'s open. This signifies that the downtrend has lost momentum and the bulls may be gaining strength.<br/>Factors increasing the pattern\'s effectiveness are:<BR/><br/>1) The first candlestick has a small real body and the second has a large real body.<BR/><br/>2) Pattern appears after protracted or very fast move.<BR/><br/>3) Heavy volume on second real body.<BR/><br/>4) The second candlestick engulfs more than one real body.<BR/>',
                            time: '16.09.2018',
                            candle: 2,
                            category: 'completed'
                        }
                    };
                                        Patterns.patternsData[5] = {
                        desc: {
                            img: 'https://i-invdn-com.akamaized.net/tvc_patterns_images/57.gif',
                            type: 'Бычий разворот',
                            reliability: 'Макс.',
                            desc: 'This pattern is a more reliable addition to the standard Harami pattern. <br/>A bullish Harami pattern occurs in the first two candles.  <br/>The third candle is a white candle with a higher close than the second candle and the confirmation of the bullish trend reversal.',
                            time: '16.09.2018',
                            candle: 2,
                            category: 'completed'
                        }
                    };
                                        Patterns.patternsData[6] = {
                        desc: {
                            img: 'https://i-invdn-com.akamaized.net/tvc_patterns_images/43.gif',
                            type: 'Бычий разворот',
                            reliability: 'Средняя',
                            desc: 'During a downtrend, the market strengthens the bears with a long black candlestick and gaps open on the second one. However, the second candlestick trades within a small range and closes at or near its open. This scenario generally shows the potential for a rally, as many positions have been changed. Confirmation of the trend reversal would be a higher open on the next candlestick.',
                            time: '25.09.2018',
                            candle: 3,
                            category: 'completed'
                        }
                    };
                    
        Patterns.texts = {
            _candlestick_patterns_bullish_reversal : 'Бычий разворот',
            _candlestick_patterns_bearish_reversal : 'Медвежий разворот',
            _candlestick_patterns_bullish_continuation : 'Продолжение бычьего тренда',
            _candlestick_patterns_bearish_continuation : 'Продолжение медвежьего тренда',
            _candlestick_patterns_Low : 'Мин.',
            _candlestick_patterns_medium : 'Средняя',
            _candlestick_patterns_High : 'Макс.',
            _candlestick_patterns_indication : 'Направление',
            _candlestick_patterns_reliability : 'Надежность',
            _candlestick_patterns_description : 'Описание',
            _candlestick_patterns_emerging_patterns : 'Новые модели',
            _candlestick_patterns_completed_patterns : 'Завершенные модели',
            _Candlestick_No_Patterns_Recognized : 'Нет узнаваемых моделей',
            _candlestick_patterns_current : 'Текущая',
            _candlestick_patterns_loading_msg : 'Загрузка...',
            _candlestick_patterns_candle_time : 'Время',
            _candlestick_patterns_candles_ago : 'Х свечей назад'
        }

        			Patterns.tableView = true;
			    });
</script>    <div class="clear"></div><div id="theDisclaimer" class="disclaimerBox"><span>Предупреждение:</span> <b>Fusion Media</b> would like to remind you that the data  contained in this website is not necessarily real-time nor accurate.  All CFDs (stocks, indexes, futures) and Forex prices are not provided by exchanges but rather by market makers, and so prices may not be accurate and may differ from the actual market price, meaning prices are indicative and not appropriate for trading purposes.  Therefore Fusion Media doesn't bear any responsibility for any trading losses you might incur as a result of using this data.<br /><br />
<b>Fusion Media</b> or anyone involved with Fusion Media will not accept any liability for loss or damage as a result of reliance on  the information including data, quotes, charts and buy/sell signals contained within this website. Please be fully informed regarding the risks and costs associated with trading the financial markets, it is one of the riskiest investment forms possible. </div>        <!-- /Page Content -->


</section>  <!-- /leftColumn -->
<aside id="rightColumn">
<div class="tradeNowRightColumn"></div><script type="text/javascript">
$(function() {
// Initialize chart block
var chart = Object.create(FP.Modules.js_sideblock_chart());
// Set containers
chart.set_wrapper('quotesBoxChartWrp');
chart.set_chart_container('quotesBoxChart');
chart.set_loading_container('quotesBoxChartLoading');
chart.nav();
// Trigger chart's rendering
$('#quotesBoxChart').trigger('chartTabChanged');

});
</script> <div class="clear  firstSideBox js-quotes rightBoxes" id="Quotes"><div class="TabContainer noOverflow js-tab-container">
<span class="newSiteIconsSprite arrow"></span>
<ul class="tabsForBox oneLvlTabsWidget js-one-level-tabs " id="quotesBoxWithTabsTop">

<li id=QBS_3 class="first selected" ><a href="javascript:void(0);">Товары</a></li>

<li id=QBS_1 ><a href="javascript:void(0);">Форекс</a></li>

<li id=QBS_2 ><a href="javascript:void(0);">Индексы</a></li>

<li id=QBS_7 class="last" ><a href="javascript:void(0);">Крипто</a></li>

</ul>
<span id="lastDummyLI" data-more-cat="Другие категории" class="moreTabOneLvlDummy displayNone"><a href="javascript:void();">Другие категории</a><span class="showMoreDownArrow"></span></span>
</div>

<script>
$(document).ready(function(){
tabsWidgetOnelevel('', 'storedSiteBlockQuotesBoxTabObject', 'quotesBoxWithTabsTop', +'');
});
</script><div class="quotesBox quotesBoxWithTabsBottom"><span class="threeDotsIconSmall genToolTip oneliner js-three-dots" data-tooltip="Настройка вкладок"></span><div class="tooltipPopup quotesTabsSelection displayNone js-popup-wrapper"><div class="header">Выбор вкладок</div><div class="content ui-sortable rowsWrap js-sideblock-tab-selection"><label class="addRow js-ts-label" data-tab-id="3"><span class="checkers"></span><input type="checkbox" class="checkbox"checked >Товары</label><label class="addRow js-ts-label" data-tab-id="1"><span class="checkers"></span><input type="checkbox" class="checkbox"checked >Форекс</label><label class="addRow js-ts-label" data-tab-id="2"><span class="checkers"></span><input type="checkbox" class="checkbox"checked >Индексы</label><label class="addRow js-ts-label" data-tab-id="7"><span class="checkers"></span><input type="checkbox" class="checkbox"checked >Крипто</label><label class="addRow js-ts-label" data-tab-id="4"><span class="checkers"></span><input type="checkbox" class="checkbox">Облигации</label><label class="addRow js-ts-label" data-tab-id="6"><span class="checkers"></span><input type="checkbox" class="checkbox">ETF</label><label class="addRow js-ts-label" data-tab-id="5"><span class="checkers"></span><input type="checkbox" class="checkbox">Акции</label></div><div class="footer"><span class="js-tab-selection-counter">Выбрано: %COUNT%/4 </span><a href="javascript:void(0);" class="newBtn Orange2 noIcon js-tab-selection-apply">Обновить</a></div></div><div class="tabsBoxBottom" id="quotesBoxChartWrp" data-chart_type="area" data-config_fontSize="9px" data-config_last_value_line="1" data-config_last_close_value_line="1" data-config_decimal_point="," data-config_thousands_sep="."><div class="chartFrame"><div id="quotesBoxChartTimeFrames" class="timePeriods"><span data-time-frame="1day">1д</span><span data-time-frame="1week">1н</span><span data-time-frame="1month">1м</span><span data-time-frame="6months">6м</span><span data-time-frame="1year">1г</span><span data-time-frame="5years">5л</span><span data-time-frame="max">Макс.</span><a href="#" class="js-chart-link quotesboxLinkIcon"></a></div><div id="quotesBoxChart" class="quotesChartWrapper"></div><div id="quotesBoxChartLoading" class="loading-responsive medium-circle"></div></div><table id="QBS_3_inner" data-gae='sb_commodities'  class=" genTbl openTbl quotesSideBlockTbl collapsedTbl"><tbody><tr class="LeftLiContainer Selected" pair="8833" data-pair_id="8833" data-external="sbcharts.investing.com" data-pair_url="/commodities/brent-oil-streaming-chart" id="sb_pair_8833" chart_link="/commodities/brent-oil-streaming-chart" data-chart-hash="ecab723bd0c0050290d8fcb3b6ecc974"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/commodities/brent-oil" title="Фьючерс на нефть Brent (F) (CFD)"  data-gae='sb_commodities-brent-oil' >Нефть Brent</a></td><td class="lastNum pid-8833-last" id="sb_last_8833">83,31</td><td class="chg greenFont pid-8833-pc" id="sb_change_8833">+0,58</td><td class="chgPer greenFont pid-8833-pcp" id="sb_changepc_8833">+0,70%</td><td class="icon"><span class="greenClockIcon isOpenPair-8833" id="sb_clock_8833"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="8849" data-pair_id="8849" data-external="sbcharts.investing.com" data-pair_url="/commodities/crude-oil-streaming-chart" id="sb_pair_8849" chart_link="/commodities/crude-oil-streaming-chart" data-chart-hash="c4caa3c525726eb96b6ee67ddfa57896"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/commodities/crude-oil" title="Фьючерсы на нефть WTI (F) (CFD)"  data-gae='sb_commodities-crude-oil' >Нефть WTI</a></td><td class="lastNum pid-8849-last" id="sb_last_8849">73,58</td><td class="chg greenFont pid-8849-pc" id="sb_change_8849">+0,33</td><td class="chgPer greenFont pid-8849-pcp" id="sb_changepc_8849">+0,45%</td><td class="icon"><span class="greenClockIcon isOpenPair-8849" id="sb_clock_8849"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="8830" data-pair_id="8830" data-external="sbcharts.investing.com" data-pair_url="/commodities/gold-streaming-chart" id="sb_pair_8830" chart_link="/commodities/gold-streaming-chart" data-chart-hash="d565a7612f2d571d32033540114babb3"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/commodities/gold" title="Фьючерс на золото (F) (CFD)"  data-gae='sb_commodities-gold' >Золото</a></td><td class="lastNum pid-8830-last" id="sb_last_8830">1.192,90</td><td class="chg redFont pid-8830-pc" id="sb_change_8830">-3,30</td><td class="chgPer redFont pid-8830-pcp" id="sb_changepc_8830">-0,28%</td><td class="icon"><span class="greenClockIcon isOpenPair-8830" id="sb_clock_8830"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="8836" data-pair_id="8836" data-external="sbcharts.investing.com" data-pair_url="/commodities/silver-streaming-chart" id="sb_pair_8836" chart_link="/commodities/silver-streaming-chart" data-chart-hash="c4651fb6e12fa8166f3d0af875aede53"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/commodities/silver" title="Фьючерс на серебро (F) (CFD)"  data-gae='sb_commodities-silver' >Серебро</a></td><td class="lastNum pid-8836-last" id="sb_last_8836">14,655</td><td class="chg redFont pid-8836-pc" id="sb_change_8836">-0,057</td><td class="chgPer redFont pid-8836-pcp" id="sb_changepc_8836">-0,39%</td><td class="icon"><span class="greenClockIcon isOpenPair-8836" id="sb_clock_8836"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="8910" data-pair_id="8910" data-external="sbcharts.investing.com" data-pair_url="/commodities/platinum-streaming-chart" id="sb_pair_8910" chart_link="/commodities/platinum-streaming-chart" data-chart-hash="ecc18f69640cd8c23af34a7a8bdda156"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/commodities/platinum" title="Фьючерс на платину (F) (CFD)"  data-gae='sb_commodities-platinum' >Платина</a></td><td class="lastNum pid-8910-last" id="sb_last_8910">817,20</td><td class="chg redFont pid-8910-pc" id="sb_change_8910">-1,30</td><td class="chgPer redFont pid-8910-pcp" id="sb_changepc_8910">-0,16%</td><td class="icon"><span class="greenClockIcon isOpenPair-8910" id="sb_clock_8910"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="8883" data-pair_id="8883" data-external="sbcharts.investing.com" data-pair_url="/commodities/palladium-streaming-chart" id="sb_pair_8883" chart_link="/commodities/palladium-streaming-chart" data-chart-hash="720874c49751ddb4fb54bb546174ec63"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/commodities/palladium" title="Фьючерс на палладий (F) (CFD)"  data-gae='sb_commodities-palladium' >Палладий</a></td><td class="lastNum pid-8883-last" id="sb_last_8883">1.057,10</td><td class="chg redFont pid-8883-pc" id="sb_change_8883">-7,50</td><td class="chgPer redFont pid-8883-pcp" id="sb_changepc_8883">-0,70%</td><td class="icon"><span class="greenClockIcon isOpenPair-8883" id="sb_clock_8883"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="8862" data-pair_id="8862" data-external="sbcharts.investing.com" data-pair_url="/commodities/natural-gas-streaming-chart" id="sb_pair_8862" chart_link="/commodities/natural-gas-streaming-chart" data-chart-hash="2862b1367008cea06e03f8cbaf7f40ed"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/commodities/natural-gas" title="Фьючерс на природный газ (F) (CFD)"  data-gae='sb_commodities-natural-gas' >Природный газ</a></td><td class="lastNum pid-8862-last" id="sb_last_8862">3,022</td><td class="chg greenFont pid-8862-pc" id="sb_change_8862">+0,020</td><td class="chgPer greenFont pid-8862-pcp" id="sb_changepc_8862">+0,67%</td><td class="icon"><span class="greenClockIcon isOpenPair-8862" id="sb_clock_8862"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer last" pair="49768" data-pair_id="49768" data-external="sbcharts.investing.com" data-pair_url="/commodities/aluminum-streaming-chart" id="sb_pair_49768" chart_link="/commodities/aluminum-streaming-chart" data-chart-hash="badc0afca3b1bf2883f1eb94670663a2"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/commodities/aluminum" title="Фьючерс на алюминий (F) (CFD)"  data-gae='sb_commodities-aluminum' >Алюминий</a></td><td class="lastNum pid-49768-last" id="sb_last_49768">2.052,50</td><td class="chg redFont pid-49768-pc" id="sb_change_49768">-0,50</td><td class="chgPer redFont pid-49768-pcp" id="sb_changepc_49768">-0,02%</td><td class="icon"><span class="greenClockIcon isOpenPair-49768" id="sb_clock_49768"></span></td><td class="sideColumn">&nbsp;</td></tr></tbody></table><table id="QBS_1_inner" data-gae='sb_forex'  class="displayNone genTbl openTbl quotesSideBlockTbl collapsedTbl"><tbody><tr class="LeftLiContainer Selected" pair="2186" data-pair_id="2186" data-external="sbcharts.investing.com" data-pair_url="/currencies/usd-rub-chart" id="sb_pair_2186" chart_link="/currencies/usd-rub-chart" data-chart-hash="1706480cd1261085a67b0e31f93d7e89"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/currencies/usd-rub" title="USD/RUB - Доллар США Российский рубль"  data-gae='sb_forex-usd-rub' >USD/RUB</a></td><td class="lastNum pid-2186-last" id="sb_last_2186">65,5503</td><td class="chg redFont pid-2186-pc" id="sb_change_2186">0,0000</td><td class="chgPer redFont pid-2186-pcp" id="sb_changepc_2186">0,00%</td><td class="icon"><span class="redClockIcon isOpenPair-2186" id="sb_clock_2186"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="1" data-pair_id="1" data-external="sbcharts.investing.com" data-pair_url="/currencies/eur-usd-chart" id="sb_pair_1" chart_link="/currencies/eur-usd-chart" data-chart-hash="429d74b767b97c5148ee1b2fd4151ba2"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/currencies/eur-usd" title="EUR/USD - Евро Доллар США"  data-gae='sb_forex-eur-usd' >EUR/USD</a></td><td class="lastNum pid-1-last" id="sb_last_1">1,1593</td><td class="chg redFont pid-1-pc" id="sb_change_1">-0,0011</td><td class="chgPer redFont pid-1-pcp" id="sb_changepc_1">-0,09%</td><td class="icon"><span class="greenClockIcon isOpenExch-1002" id="sb_clock_1"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="2" data-pair_id="2" data-external="sbcharts.investing.com" data-pair_url="/currencies/gbp-usd-chart" id="sb_pair_2" chart_link="/currencies/gbp-usd-chart" data-chart-hash="d8560659b81ddf1ba317896bd144332a"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/currencies/gbp-usd" title="GBP/USD - Британский фунт Доллар США"  data-gae='sb_forex-gbp-usd' >GBP/USD</a></td><td class="lastNum pid-2-last" id="sb_last_2">1,3036</td><td class="chg greenFont pid-2-pc" id="sb_change_2">+0,0005</td><td class="chgPer greenFont pid-2-pcp" id="sb_changepc_2">+0,04%</td><td class="icon"><span class="greenClockIcon isOpenExch-1002" id="sb_clock_2"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="7" data-pair_id="7" data-external="sbcharts.investing.com" data-pair_url="/currencies/usd-cad-chart" id="sb_pair_7" chart_link="/currencies/usd-cad-chart" data-chart-hash="68ca4930a11af002d4d0a2eff6864866"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/currencies/usd-cad" title="USD/CAD - Доллар США Канадский доллар"  data-gae='sb_forex-usd-cad' >USD/CAD</a></td><td class="lastNum pid-7-last" id="sb_last_7">1,2820</td><td class="chg redFont pid-7-pc" id="sb_change_7">-0,0090</td><td class="chgPer redFont pid-7-pcp" id="sb_changepc_7">-0,70%</td><td class="icon"><span class="greenClockIcon isOpenExch-1002" id="sb_clock_7"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="3" data-pair_id="3" data-external="sbcharts.investing.com" data-pair_url="/currencies/usd-jpy-chart" id="sb_pair_3" chart_link="/currencies/usd-jpy-chart" data-chart-hash="0fdacd3571a3a6ef8960f7e765071282"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/currencies/usd-jpy" title="USD/JPY - Доллар США Японская иена"  data-gae='sb_forex-usd-jpy' >USD/JPY</a></td><td class="lastNum pid-3-last" id="sb_last_3">113,94</td><td class="chg greenFont pid-3-pc" id="sb_change_3">+0,24</td><td class="chgPer greenFont pid-3-pcp" id="sb_changepc_3">+0,21%</td><td class="icon"><span class="greenClockIcon isOpenExch-1002" id="sb_clock_3"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="5" data-pair_id="5" data-external="sbcharts.investing.com" data-pair_url="/currencies/aud-usd-chart" id="sb_pair_5" chart_link="/currencies/aud-usd-chart" data-chart-hash="64644f156f6ac47c39d47a9f2ceb89dc"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/currencies/aud-usd" title="AUD/USD - Австралийский доллар Доллар США"  data-gae='sb_forex-aud-usd' >AUD/USD</a></td><td class="lastNum pid-5-last" id="sb_last_5">0,7213</td><td class="chg redFont pid-5-pc" id="sb_change_5">-0,0009</td><td class="chgPer redFont pid-5-pcp" id="sb_changepc_5">-0,12%</td><td class="icon"><span class="greenClockIcon isOpenExch-1002" id="sb_clock_5"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="11" data-pair_id="11" data-external="sbcharts.investing.com" data-pair_url="/currencies/gbp-jpy-chart" id="sb_pair_11" chart_link="/currencies/gbp-jpy-chart" data-chart-hash="1824abedd1ffc8fc66f66cf1089224c6"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/currencies/gbp-jpy" title="GBP/JPY - Британский фунт Японская иена"  data-gae='sb_forex-gbp-jpy' >GBP/JPY</a></td><td class="lastNum pid-11-last" id="sb_last_11">148,52</td><td class="chg greenFont pid-11-pc" id="sb_change_11">+0,35</td><td class="chgPer greenFont pid-11-pcp" id="sb_changepc_11">+0,24%</td><td class="icon"><span class="greenClockIcon isOpenExch-1002" id="sb_clock_11"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer last" pair="1691" data-pair_id="1691" data-external="sbcharts.investing.com" data-pair_url="/currencies/eur-rub-chart" id="sb_pair_1691" chart_link="/currencies/eur-rub-chart" data-chart-hash="f5d340c78a9833562bce290f9f21c36a"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/currencies/eur-rub" title="EUR/RUB - Евро Российский рубль"  data-gae='sb_forex-eur-rub' >EUR/RUB</a></td><td class="lastNum pid-1691-last" id="sb_last_1691">76,0865</td><td class="chg redFont pid-1691-pc" id="sb_change_1691">0,0000</td><td class="chgPer redFont pid-1691-pcp" id="sb_changepc_1691">0,00%</td><td class="icon"><span class="redClockIcon isOpenPair-1691" id="sb_clock_1691"></span></td><td class="sideColumn">&nbsp;</td></tr></tbody></table><table id="QBS_2_inner" data-gae='sb_indices'  class="displayNone genTbl openTbl quotesSideBlockTbl collapsedTbl"><tbody><tr class="LeftLiContainer Selected" pair="13665" data-pair_id="13665" data-external="sbcharts.investing.com" data-pair_url="/indices/rtsi-chart" id="sb_pair_13665" chart_link="/indices/rtsi-chart" data-chart-hash="c351250a76c38a7f8c64430699c9f172"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/indices/rtsi" title="РТС"  data-gae='sb_indices-rtsi' >РТС</a></td><td class="lastNum pid-13665-last" id="sb_last_13665">1.192,04</td><td class="chg greenFont pid-13665-pc" id="sb_change_13665">+4,16</td><td class="chgPer greenFont pid-13665-pcp" id="sb_changepc_13665">+0,35%</td><td class="icon"><span class="redClockIcon isOpenExch-40" id="sb_clock_13665"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="13666" data-pair_id="13666" data-external="sbcharts.investing.com" data-pair_url="/indices/mcx-chart" id="sb_pair_13666" chart_link="/indices/mcx-chart" data-chart-hash="771bf1dd1d32e90d63d06c49583f68b4"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/indices/mcx" title="Индекс МосБиржи"  data-gae='sb_indices-mcx' >Индекс МосБиржи</a></td><td class="lastNum pid-13666-last" id="sb_last_13666">2.475,36</td><td class="chg greenFont pid-13666-pc" id="sb_change_13666">+0,79</td><td class="chgPer greenFont pid-13666-pcp" id="sb_changepc_13666">+0,03%</td><td class="icon"><span class="redClockIcon isOpenExch-40" id="sb_clock_13666"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="8839" data-pair_id="8839" data-external="sbcharts.investing.com" data-pair_url="/indices/us-spx-500-futures-streaming-chart" id="sb_pair_8839" chart_link="/indices/us-spx-500-futures-streaming-chart" data-chart-hash="1a5f95b308e1d982eec66b97f9895bac"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/indices/us-spx-500-futures" title="Фьючерс на S&P 500 (F) (CFD)"  data-gae='sb_indices-us-spx-500-futures' >S&P 500 (F)</a></td><td class="lastNum pid-8839-last" id="sb_last_8839">2.934,75</td><td class="chg greenFont pid-8839-pc" id="sb_change_8839">+15,75</td><td class="chgPer greenFont pid-8839-pcp" id="sb_changepc_8839">+0,54%</td><td class="icon"><span class="greenClockIcon isOpenPair-8839" id="sb_clock_8839"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="169" data-pair_id="169" data-external="sbcharts.investing.com" data-pair_url="/indices/us-30-chart" id="sb_pair_169" chart_link="/indices/us-30-chart" data-chart-hash="d5f35207ae3dfc721a517a541dbba7d8"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/indices/us-30" title="Dow Jones Industrial Average"  data-gae='sb_indices-us-30' >Dow 30</a></td><td class="lastNum pid-169-last" id="sb_last_169">26.458,31</td><td class="chg greenFont pid-169-pc" id="sb_change_169">+18,38</td><td class="chgPer greenFont pid-169-pcp" id="sb_changepc_169">+0,07%</td><td class="icon"><span class="redClockIcon isOpenExch-1" id="sb_clock_169"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="172" data-pair_id="172" data-external="sbcharts.investing.com" data-pair_url="/indices/germany-30-chart" id="sb_pair_172" chart_link="/indices/germany-30-chart" data-chart-hash="20b3a6a97fa3088df002fbc8bbcbecbd"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/indices/germany-30" title="DAX (CFD)"  data-gae='sb_indices-germany-30' >DAX</a></td><td class="lastNum pid-172-last" id="sb_last_172">12.246,73</td><td class="chg redFont pid-172-pc" id="sb_change_172">-188,86</td><td class="chgPer redFont pid-172-pcp" id="sb_changepc_172">-1,52%</td><td class="icon"><span class="redClockIcon isOpenExch-4" id="sb_clock_172"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="27" data-pair_id="27" data-external="sbcharts.investing.com" data-pair_url="/indices/uk-100-chart" id="sb_pair_27" chart_link="/indices/uk-100-chart" data-chart-hash="1d2984fb05d560efab6535929b9749b6"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/indices/uk-100" title="FTSE 100 (CFD)"  data-gae='sb_indices-uk-100' >FTSE 100</a></td><td class="lastNum pid-27-last" id="sb_last_27">7.510,20</td><td class="chg redFont pid-27-pc" id="sb_change_27">-35,24</td><td class="chgPer redFont pid-27-pcp" id="sb_changepc_27">-0,47%</td><td class="icon"><span class="redClockIcon isOpenExch-3" id="sb_clock_27"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="8827" data-pair_id="8827" data-external="sbcharts.investing.com" data-pair_url="/quotes/us-dollar-index-streaming-chart" id="sb_pair_8827" chart_link="/quotes/us-dollar-index-streaming-chart" data-chart-hash="06620ff894868f010dafb51493836e71"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/quotes/us-dollar-index" title="Фьючерс на индекс USD (F) (CFD)"  data-gae='sb_indices-us-dollar-index' >Индекс USD</a></td><td class="lastNum pid-8827-last" id="sb_last_8827">94,82</td><td class="chg greenFont pid-8827-pc" id="sb_change_8827">+0,02</td><td class="chgPer greenFont pid-8827-pcp" id="sb_changepc_8827">+0,02%</td><td class="icon"><span class="greenClockIcon isOpenPair-8827" id="sb_clock_8827"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer last" pair="1055949" data-pair_id="1055949" data-external="sbcharts.investing.com" data-pair_url="/crypto/bitcoin/bitcoin-futures-streaming-chart" id="sb_pair_1055949" chart_link="/crypto/bitcoin/bitcoin-futures-streaming-chart" data-chart-hash="725b028dd90d739e762616e8d32a06ba"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/crypto/bitcoin/bitcoin-futures" title="Фьючерс CME на Bitcoin (F) (CFD)"  data-gae='sb_indices-bitcoin-futures' >Фьючерс на Bitcoin</a></td><td class="lastNum pid-1055949-last" id="sb_last_1055949">6.570,0</td><td class="chg redFont pid-1055949-pc" id="sb_change_1055949">-45,0</td><td class="chgPer redFont pid-1055949-pcp" id="sb_changepc_1055949">-0,68%</td><td class="icon"><span class="greenClockIcon isOpenExch-1004" id="sb_clock_1055949"></span></td><td class="sideColumn">&nbsp;</td></tr></tbody></table><table id="QBS_7_inner" class="displayNone genTbl openTbl quotesSideBlockTbl collapsedTbl"><tbody><tr class="LeftLiContainer Selected" pair="945629" data-pair_id="945629" data-external="sbcharts.investing.com" data-pair_url="/crypto/bitcoin/btc-usd-chart" id="sb_pair_945629" chart_link="/crypto/bitcoin/btc-usd-chart" data-chart-hash="980ef075ac6b87dd9f940f398b9bcbf8"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/crypto/bitcoin/btc-usd" title="BTC/USD - Биткойн Доллар США"  data-gae='-btc-usd' >BTC/USD</a></td><td class="lastNum pid-945629-last" id="sb_last_945629">6.612,1</td><td class="chg greenFont pid-945629-pc" id="sb_change_945629">+36,0</td><td class="chgPer greenFont pid-945629-pcp" id="sb_changepc_945629">+0,55%</td><td class="icon"><span class="greenClockIcon isOpenExch-1014" id="sb_clock_945629"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="997650" data-pair_id="997650" data-external="sbcharts.investing.com" data-pair_url="/crypto/ethereum/eth-usd-chart" id="sb_pair_997650" chart_link="/crypto/ethereum/eth-usd-chart" data-chart-hash="f1d1f100714c475a9df5f6ee74338791"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/crypto/ethereum/eth-usd" title="ETH/USD - Эфириум Доллар США"  data-gae='-eth-usd' >ETH/USD</a></td><td class="lastNum pid-997650-last" id="sb_last_997650">229,68</td><td class="chg redFont pid-997650-pc" id="sb_change_997650">-2,53</td><td class="chgPer redFont pid-997650-pcp" id="sb_changepc_997650">-1,09%</td><td class="icon"><span class="greenClockIcon isOpenExch-1014" id="sb_clock_997650"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="1031333" data-pair_id="1031333" data-external="sbcharts.investing.com" data-pair_url="/crypto/bitcoin-cash/bch-usd-chart" id="sb_pair_1031333" chart_link="/crypto/bitcoin-cash/bch-usd-chart" data-chart-hash="9b37b549c305bd9c6240e4fc8f477f5d"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/crypto/bitcoin-cash/bch-usd" title="BCH/USD - Bitcoin Cash Доллар США"  data-gae='-bch-usd' >BCH/USD</a></td><td class="lastNum pid-1031333-last" id="sb_last_1031333">534,19</td><td class="chg redFont pid-1031333-pc" id="sb_change_1031333">-1,71</td><td class="chgPer redFont pid-1031333-pcp" id="sb_changepc_1031333">-0,32%</td><td class="icon"><span class="greenClockIcon isOpenExch-1014" id="sb_clock_1031333"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="1010798" data-pair_id="1010798" data-external="sbcharts.investing.com" data-pair_url="/crypto/litecoin/ltc-usd-chart" id="sb_pair_1010798" chart_link="/crypto/litecoin/ltc-usd-chart" data-chart-hash="898e0191a596d14e3ac23c545ba0aa90"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/crypto/litecoin/ltc-usd" title="LTC/USD - Лайткоин Доллар США"  data-gae='-ltc-usd' >LTC/USD</a></td><td class="lastNum pid-1010798-last" id="sb_last_1010798">61,040</td><td class="chg greenFont pid-1010798-pc" id="sb_change_1010798">+0,380</td><td class="chgPer greenFont pid-1010798-pcp" id="sb_changepc_1010798">+0,63%</td><td class="icon"><span class="greenClockIcon isOpenExch-1016" id="sb_clock_1010798"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="1031068" data-pair_id="1031068" data-external="sbcharts.investing.com" data-pair_url="/crypto/iota/iota-usd-chart" id="sb_pair_1031068" chart_link="/crypto/iota/iota-usd-chart" data-chart-hash="547fd5173f147c0a19966be83152910c"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/crypto/iota/iota-usd" title="IOTA/USD - IOTA Доллар США"  data-gae='-iota-usd' >IOT/USD</a></td><td class="lastNum pid-1031068-last" id="sb_last_1031068">0,56830</td><td class="chg greenFont pid-1031068-pc" id="sb_change_1031068">+0,00453</td><td class="chgPer greenFont pid-1031068-pcp" id="sb_changepc_1031068">+0,80%</td><td class="icon"><span class="greenClockIcon isOpenExch-1014" id="sb_clock_1031068"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="1031678" data-pair_id="1031678" data-external="sbcharts.investing.com" data-pair_url="/crypto/bitcoin/btc-rub-chart" id="sb_pair_1031678" chart_link="/crypto/bitcoin/btc-rub-chart" data-chart-hash="8acda036134b4c8e0c6dc4b4afcb5021"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/crypto/bitcoin/btc-rub" title="BTC/RUB - Биткойн Российский рубль"  data-gae='-btc-rub' >BTC/RUB</a></td><td class="lastNum pid-1031678-last" id="sb_last_1031678">441.535</td><td class="chg greenFont pid-1031678-pc" id="sb_change_1031678">+4.075</td><td class="chgPer greenFont pid-1031678-pcp" id="sb_changepc_1031678">+0,93%</td><td class="icon"><span class="greenClockIcon isOpenExch-1029" id="sb_clock_1031678"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="1010782" data-pair_id="1010782" data-external="sbcharts.investing.com" data-pair_url="/crypto/ripple/xrp-usd-chart" id="sb_pair_1010782" chart_link="/crypto/ripple/xrp-usd-chart" data-chart-hash="3a1c1287203a98d5ff2e5514d3b2126a"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/crypto/ripple/xrp-usd" title="XRP/USD - Рипл Доллар США"  data-gae='-xrp-usd' >XRP/USD</a></td><td class="lastNum pid-1010782-last" id="sb_last_1010782">0,57800</td><td class="chg greenFont pid-1010782-pc" id="sb_change_1010782">+0,00510</td><td class="chgPer greenFont pid-1010782-pcp" id="sb_changepc_1010782">+0,89%</td><td class="icon"><span class="greenClockIcon isOpenExch-1015" id="sb_clock_1010782"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer last" pair="1010785" data-pair_id="1010785" data-external="sbcharts.investing.com" data-pair_url="/crypto/dash/dash-usd-chart" id="sb_pair_1010785" chart_link="/crypto/dash/dash-usd-chart" data-chart-hash="ea3c96e6853fe63956763735b8f09dcd"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/crypto/dash/dash-usd" title="DASH/USD - Дэш Доллар США"  data-gae='-dash-usd' >DASH/USD</a></td><td class="lastNum pid-1010785-last" id="sb_last_1010785">187,43</td><td class="chg greenFont pid-1010785-pc" id="sb_change_1010785">+1,22</td><td class="chgPer greenFont pid-1010785-pcp" id="sb_changepc_1010785">+0,66%</td><td class="icon"><span class="greenClockIcon isOpenExch-1015" id="sb_clock_1010785"></span></td><td class="sideColumn">&nbsp;</td></tr></tbody></table></div></div></div> <!-- /sideBanner --><div class="tradeNowRightColumn"><a href="javascript:void(0);" data-link="/jp.php?v2=YiJjPTViZD0yZ2xoYzNhZDdiN2k2Mjc9YnVlNzowZykwdGRjYTM1ZWZvb2xuIDYuMnBhbDNnNmZga2ZvNDI0ZmJxY3M1bmQ5MmRsYWMwYWE3cDcsNjw=" data-section="sideblock_bottom" data-tradenowid="228537" title="&#1053;&#1072;&#1095;&#1085;&#1080;&#1090;&#1077; &#1090;&#1086;&#1088;&#1075;&#1086;&#1074;&#1072;&#1090;&#1100; &#1089; Forex Club" data-color="Orange2" data-text="&#1058;&#1086;&#1088;&#1075;&#1086;&#1074;&#1072;&#1090;&#1100; &#1089;&#1077;&#1081;&#1095;&#1072;&#1089;" class="newBtn noIcon tradenowBtn js-tradenow-btn Orange2">Торговать сейчас</a>    <div class="clear"></div>
</div><div class="clear " id="ad1"><div id='div-gpt-ad-1370187203350-sb1' style='margin-top:18px;' ></div></div> <!-- /sideBanner -->	<div class="toolIframeBox rightBoxes techSummaryTool js-techsum-main-wrapper" id="TechSummary">
<div id="TSB_tabsMenu">
<div class="TabContainer noOverflow js-tab-container">
<span class="newSiteIconsSprite arrow"></span>
<ul class="tabsForBox oneLvlTabsWidget js-one-level-tabs tabsForBox js-tabs-container" id="TSB_topTabs">

<li id=TSB_1 class="first selected" data-tab-id=1><a href="javascript:void(0);">Форекс</a></li>

<li id=TSB_2 data-tab-id=2><a href="javascript:void(0);">Товары</a></li>

<li id=TSB_3 data-tab-id=3><a href="javascript:void(0);">Индексы</a></li>

<li id=TSB_4 class="last" data-tab-id=4><a href="javascript:void(0);">Крипто</a></li>

</ul>
<span id="lastDummyLI" data-more-cat="Другие категории" class="moreTabOneLvlDummy displayNone"><a href="javascript:void();">Другие категории</a><span class="showMoreDownArrow"></span></span>
</div>

<script>
$(document).ready(function(){
tabsWidgetOnelevel('', 'storedSiteBlockTechnicalBoxTabObject', 'TSB_topTabs', +'');
});
</script>		</div>
<div id="TSB_ChartWrp" rel="" data-chart_type="area" data-config_fontsize="9px" data-config_ymaxmultiplier="0" class="tabsBoxBottom techSummaryWrap js-box-content">
<span class="pointer threeDotsIconSmall genToolTip oneliner js-three-dots" data-tooltip="Настройка вкладок"></span>
<div id="techsum" class="tooltipPopup quotesTabsSelection js-popup-wrapper displayNone">
<div class="header"> Выбор вкладок </div>
<div class="content ui-sortable rowsWrap js-sideblock-tab-selection">
<label class="addRow js-ts-label" data-tab-id="1"><span class="checkers"></span><input type="checkbox" class="checkbox"checked >Форекс</label><label class="addRow js-ts-label" data-tab-id="2"><span class="checkers"></span><input type="checkbox" class="checkbox"checked >Товары</label><label class="addRow js-ts-label" data-tab-id="3"><span class="checkers"></span><input type="checkbox" class="checkbox"checked >Индексы</label><label class="addRow js-ts-label" data-tab-id="4"><span class="checkers"></span><input type="checkbox" class="checkbox"checked >Крипто</label><label class="addRow js-ts-label" data-tab-id="5"><span class="checkers"></span><input type="checkbox" class="checkbox">Акции</label><label class="addRow js-ts-label" data-tab-id="6"><span class="checkers"></span><input type="checkbox" class="checkbox">Облигации</label><label class="addRow js-ts-label" data-tab-id="7"><span class="checkers"></span><input type="checkbox" class="checkbox">ETF</label>				</div>
<div class="footer"><span class="js-tab-selection-counter"> Выбрано: %COUNT%/4</span><a href="javascript:void(0);" id="applyTechSumOrder" class="newBtn Orange2 noIcon js-tab-selection-apply"> Обновить</a></div>
</div>
<div class="dataBox timeframe js-timeframe">
<div class="float_lang_base_1" >Интервалы                    <select id="TSB_timeframe">


<option  value="60">1 мин</option>

<option selected="selected" value="300">5 мин</option>

<option  value="900">15 мин</option>

<option  value="1800">30 мин</option>

<option  value="3600">1 час</option>

<option  value="18000">5 часов</option>

<option  value="86400">1 день</option>

<option  value="week">1 неделя</option>

</select>
</div>
<div class="float_lang_base_2 dateFont" id="TSB_updateTime"></div>
<div class="clear"></div>
</div>
<div class="topBox">
<div class="loading-responsive medium-circle displayNone" id="TSB__loader"></div>
<div id="TSB__mainSummary">
<div id="TSB_arrowBox" class="arrowBox">
<span id="TSB__arrowBoxSpan" class="newSiteIconsSprite downArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name" class="arial_14 bold" title="EUR/USD - Евро Доллар США"><a href="/currencies/eur-usd" class="js-item-name">EUR/USD</a></p>
<p id="TSB__summary_last" class="arial_24 bold inlineblock js-item-last pid--last_nColor">1,1593</p>
<p id="TSB__summary_change" class="change js-item-change redFont">
<span id="TSB__summary_change_value" class="js-change-value pid--pc">-0,0011</span> <span id="TSB__summary_change_percent" class="js-change-percent pid--pcp parentheses">-0,09%</span>
</p>
</div>

<div id="TSB__data_summary" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval sell arial_12 bold float_lang_base_2" id="TSB__technical_summary">Продавать</span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy" data-counter-type="buy" class="parentheses">4</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell" data-counter-type="sell" class="parentheses">8</span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy" data-counter-type="buy" class="parentheses">3</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell" data-counter-type="sell" class="parentheses">5</span></p>
<div class="clear"></div>
</div>                </div>
</div>
<div id="TSB_main_summary_1" data-pairid="1" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_1" class="arrowBox">
<span id="TSB__arrowBoxSpan_1" class="newSiteIconsSprite downArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_1" class="arial_14 bold" title="EUR/USD - Евро Доллар США"><a href="/currencies/eur-usd" class="js-item-name">EUR/USD</a></p>
<p id="TSB__summary_last_1" class="arial_24 bold inlineblock js-item-last pid-1-last_nColor">1,1593</p>
<p id="TSB__summary_change_1" class="change js-item-change redFont">
<span id="TSB__summary_change_value_1" class="js-change-value pid-1-pc">-0,0011</span> <span id="TSB__summary_change_percent_1" class="js-change-percent pid-1-pcp parentheses">-0,09%</span>
</p>
</div>

<div id="TSB__data_summary_1" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval sell arial_12 bold float_lang_base_2" id="TSB__technical_summary_1">Продавать</span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_1" data-counter-type="buy" class="parentheses">4</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_1" data-counter-type="sell" class="parentheses">8</span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_1" data-counter-type="buy" class="parentheses">3</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_1" data-counter-type="sell" class="parentheses">5</span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_1691" data-pairid="1691" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_1691" class="arrowBox">
<span id="TSB__arrowBoxSpan_1691" class="newSiteIconsSprite downArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_1691" class="arial_14 bold" title="EUR/RUB - Евро Российский рубль"><a href="/currencies/eur-rub" class="js-item-name">EUR/RUB</a></p>
<p id="TSB__summary_last_1691" class="arial_24 bold inlineblock js-item-last pid-1691-last_nColor">76,0865</p>
<p id="TSB__summary_change_1691" class="change js-item-change redFont">
<span id="TSB__summary_change_value_1691" class="js-change-value pid-1691-pc">0,0000</span> <span id="TSB__summary_change_percent_1691" class="js-change-percent pid-1691-pcp parentheses">0,00%</span>
</p>
</div>

<div id="TSB__data_summary_1691" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval buy arial_12 bold float_lang_base_2" id="TSB__technical_summary_1691">Активно покупать</span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_1691" data-counter-type="buy" class="parentheses">10</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_1691" data-counter-type="sell" class="parentheses">2</span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_1691" data-counter-type="buy" class="parentheses">7</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_1691" data-counter-type="sell" class="parentheses">1</span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_2186" data-pairid="2186" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_2186" class="arrowBox">
<span id="TSB__arrowBoxSpan_2186" class="newSiteIconsSprite downArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_2186" class="arial_14 bold" title="USD/RUB - Доллар США Российский рубль"><a href="/currencies/usd-rub" class="js-item-name">USD/RUB</a></p>
<p id="TSB__summary_last_2186" class="arial_24 bold inlineblock js-item-last pid-2186-last_nColor">65,5503</p>
<p id="TSB__summary_change_2186" class="change js-item-change redFont">
<span id="TSB__summary_change_value_2186" class="js-change-value pid-2186-pc">0,0000</span> <span id="TSB__summary_change_percent_2186" class="js-change-percent pid-2186-pcp parentheses">0,00%</span>
</p>
</div>

<div id="TSB__data_summary_2186" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval buy arial_12 bold float_lang_base_2" id="TSB__technical_summary_2186">Активно покупать</span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_2186" data-counter-type="buy" class="parentheses">9</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_2186" data-counter-type="sell" class="parentheses">3</span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_2186" data-counter-type="buy" class="parentheses">9</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_2186" data-counter-type="sell" class="parentheses">0</span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_2" data-pairid="2" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_2" class="arrowBox">
<span id="TSB__arrowBoxSpan_2" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_2" class="arial_14 bold" title="GBP/USD - Британский фунт Доллар США"><a href="/currencies/gbp-usd" class="js-item-name">GBP/USD</a></p>
<p id="TSB__summary_last_2" class="arial_24 bold inlineblock js-item-last pid-2-last_nColor">1,3036</p>
<p id="TSB__summary_change_2" class="change js-item-change greenFont">
<span id="TSB__summary_change_value_2" class="js-change-value pid-2-pc">+0,0005</span> <span id="TSB__summary_change_percent_2" class="js-change-percent pid-2-pcp parentheses">+0,04%</span>
</p>
</div>

<div id="TSB__data_summary_2" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval buy arial_12 bold float_lang_base_2" id="TSB__technical_summary_2">Активно покупать</span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_2" data-counter-type="buy" class="parentheses">11</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_2" data-counter-type="sell" class="parentheses">1</span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_2" data-counter-type="buy" class="parentheses">9</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_2" data-counter-type="sell" class="parentheses">0</span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_3" data-pairid="3" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_3" class="arrowBox">
<span id="TSB__arrowBoxSpan_3" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_3" class="arial_14 bold" title="USD/JPY - Доллар США Японская иена"><a href="/currencies/usd-jpy" class="js-item-name">USD/JPY</a></p>
<p id="TSB__summary_last_3" class="arial_24 bold inlineblock js-item-last pid-3-last_nColor">113,94</p>
<p id="TSB__summary_change_3" class="change js-item-change greenFont">
<span id="TSB__summary_change_value_3" class="js-change-value pid-3-pc">+0,24</span> <span id="TSB__summary_change_percent_3" class="js-change-percent pid-3-pcp parentheses">+0,21%</span>
</p>
</div>

<div id="TSB__data_summary_3" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval buy arial_12 bold float_lang_base_2" id="TSB__technical_summary_3">Активно покупать</span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_3" data-counter-type="buy" class="parentheses">12</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_3" data-counter-type="sell" class="parentheses">0</span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_3" data-counter-type="buy" class="parentheses">6</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_3" data-counter-type="sell" class="parentheses">0</span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_9" data-pairid="9" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_9" class="arrowBox">
<span id="TSB__arrowBoxSpan_9" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_9" class="arial_14 bold" title="EUR/JPY - Евро Японская иена"><a href="/currencies/eur-jpy" class="js-item-name">EUR/JPY</a></p>
<p id="TSB__summary_last_9" class="arial_24 bold inlineblock js-item-last pid-9-last_nColor">132,09</p>
<p id="TSB__summary_change_9" class="change js-item-change greenFont">
<span id="TSB__summary_change_value_9" class="js-change-value pid-9-pc">+0,15</span> <span id="TSB__summary_change_percent_9" class="js-change-percent pid-9-pcp parentheses">+0,11%</span>
</p>
</div>

<div id="TSB__data_summary_9" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_9">Нейтрально</span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_9" data-counter-type="buy" class="parentheses">9</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_9" data-counter-type="sell" class="parentheses">3</span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_9" data-counter-type="buy" class="parentheses">3</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_9" data-counter-type="sell" class="parentheses">3</span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_5" data-pairid="5" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_5" class="arrowBox">
<span id="TSB__arrowBoxSpan_5" class="newSiteIconsSprite downArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_5" class="arial_14 bold" title="AUD/USD - Австралийский доллар Доллар США"><a href="/currencies/aud-usd" class="js-item-name">AUD/USD</a></p>
<p id="TSB__summary_last_5" class="arial_24 bold inlineblock js-item-last pid-5-last_nColor">0,7213</p>
<p id="TSB__summary_change_5" class="change js-item-change redFont">
<span id="TSB__summary_change_value_5" class="js-change-value pid-5-pc">-0,0009</span> <span id="TSB__summary_change_percent_5" class="js-change-percent pid-5-pcp parentheses">-0,12%</span>
</p>
</div>

<div id="TSB__data_summary_5" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_5">Нейтрально</span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_5" data-counter-type="buy" class="parentheses">6</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_5" data-counter-type="sell" class="parentheses">6</span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_5" data-counter-type="buy" class="parentheses">5</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_5" data-counter-type="sell" class="parentheses">2</span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8830" data-pairid="8830" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8830" class="arrowBox">
<span id="TSB__arrowBoxSpan_8830" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8830" class="arial_14 bold" title="Фьючерс на золото"><a href="/commodities/gold" class="js-item-name">Золото</a></p>
<p id="TSB__summary_last_8830" class="arial_24 bold inlineblock js-item-last pid-8830-last_nColor"></p>
<p id="TSB__summary_change_8830" class="change js-item-change Font">
<span id="TSB__summary_change_value_8830" class="js-change-value pid-8830-pc"></span> <span id="TSB__summary_change_percent_8830" class="js-change-percent pid-8830-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8830" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8830"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8830" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8830" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8830" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8830" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8836" data-pairid="8836" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8836" class="arrowBox">
<span id="TSB__arrowBoxSpan_8836" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8836" class="arial_14 bold" title="Фьючерс на серебро"><a href="/commodities/silver" class="js-item-name">Серебро</a></p>
<p id="TSB__summary_last_8836" class="arial_24 bold inlineblock js-item-last pid-8836-last_nColor"></p>
<p id="TSB__summary_change_8836" class="change js-item-change Font">
<span id="TSB__summary_change_value_8836" class="js-change-value pid-8836-pc"></span> <span id="TSB__summary_change_percent_8836" class="js-change-percent pid-8836-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8836" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8836"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8836" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8836" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8836" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8836" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8833" data-pairid="8833" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8833" class="arrowBox">
<span id="TSB__arrowBoxSpan_8833" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8833" class="arial_14 bold" title="Фьючерс на нефть Brent"><a href="/commodities/brent-oil" class="js-item-name">Нефть Brent</a></p>
<p id="TSB__summary_last_8833" class="arial_24 bold inlineblock js-item-last pid-8833-last_nColor"></p>
<p id="TSB__summary_change_8833" class="change js-item-change Font">
<span id="TSB__summary_change_value_8833" class="js-change-value pid-8833-pc"></span> <span id="TSB__summary_change_percent_8833" class="js-change-percent pid-8833-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8833" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8833"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8833" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8833" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8833" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8833" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8849" data-pairid="8849" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8849" class="arrowBox">
<span id="TSB__arrowBoxSpan_8849" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8849" class="arial_14 bold" title="Фьючерсы на нефть WTI"><a href="/commodities/crude-oil" class="js-item-name">Нефть WTI</a></p>
<p id="TSB__summary_last_8849" class="arial_24 bold inlineblock js-item-last pid-8849-last_nColor"></p>
<p id="TSB__summary_change_8849" class="change js-item-change Font">
<span id="TSB__summary_change_value_8849" class="js-change-value pid-8849-pc"></span> <span id="TSB__summary_change_percent_8849" class="js-change-percent pid-8849-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8849" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8849"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8849" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8849" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8849" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8849" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8883" data-pairid="8883" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8883" class="arrowBox">
<span id="TSB__arrowBoxSpan_8883" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8883" class="arial_14 bold" title="Фьючерс на палладий"><a href="/commodities/palladium" class="js-item-name">Палладий</a></p>
<p id="TSB__summary_last_8883" class="arial_24 bold inlineblock js-item-last pid-8883-last_nColor"></p>
<p id="TSB__summary_change_8883" class="change js-item-change Font">
<span id="TSB__summary_change_value_8883" class="js-change-value pid-8883-pc"></span> <span id="TSB__summary_change_percent_8883" class="js-change-percent pid-8883-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8883" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8883"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8883" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8883" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8883" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8883" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8831" data-pairid="8831" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8831" class="arrowBox">
<span id="TSB__arrowBoxSpan_8831" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8831" class="arial_14 bold" title="Фьючерс на медь"><a href="/commodities/copper" class="js-item-name">Медь</a></p>
<p id="TSB__summary_last_8831" class="arial_24 bold inlineblock js-item-last pid-8831-last_nColor"></p>
<p id="TSB__summary_change_8831" class="change js-item-change Font">
<span id="TSB__summary_change_value_8831" class="js-change-value pid-8831-pc"></span> <span id="TSB__summary_change_percent_8831" class="js-change-percent pid-8831-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8831" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8831"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8831" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8831" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8831" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8831" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8910" data-pairid="8910" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8910" class="arrowBox">
<span id="TSB__arrowBoxSpan_8910" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8910" class="arial_14 bold" title="Фьючерс на платину"><a href="/commodities/platinum" class="js-item-name">Платина</a></p>
<p id="TSB__summary_last_8910" class="arial_24 bold inlineblock js-item-last pid-8910-last_nColor"></p>
<p id="TSB__summary_change_8910" class="change js-item-change Font">
<span id="TSB__summary_change_value_8910" class="js-change-value pid-8910-pc"></span> <span id="TSB__summary_change_percent_8910" class="js-change-percent pid-8910-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8910" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8910"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8910" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8910" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8910" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8910" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_13665" data-pairid="13665" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_13665" class="arrowBox">
<span id="TSB__arrowBoxSpan_13665" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_13665" class="arial_14 bold" title="РТС (Москва)"><a href="/indices/rtsi" class="js-item-name">РТС</a></p>
<p id="TSB__summary_last_13665" class="arial_24 bold inlineblock js-item-last pid-13665-last_nColor"></p>
<p id="TSB__summary_change_13665" class="change js-item-change Font">
<span id="TSB__summary_change_value_13665" class="js-change-value pid-13665-pc"></span> <span id="TSB__summary_change_percent_13665" class="js-change-percent pid-13665-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_13665" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_13665"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_13665" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_13665" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_13665" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_13665" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_13666" data-pairid="13666" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_13666" class="arrowBox">
<span id="TSB__arrowBoxSpan_13666" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_13666" class="arial_14 bold" title="Индекс МосБиржи (Москва)"><a href="/indices/mcx" class="js-item-name">Индекс МосБиржи</a></p>
<p id="TSB__summary_last_13666" class="arial_24 bold inlineblock js-item-last pid-13666-last_nColor"></p>
<p id="TSB__summary_change_13666" class="change js-item-change Font">
<span id="TSB__summary_change_value_13666" class="js-change-value pid-13666-pc"></span> <span id="TSB__summary_change_percent_13666" class="js-change-percent pid-13666-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_13666" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_13666"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_13666" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_13666" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_13666" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_13666" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_172" data-pairid="172" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_172" class="arrowBox">
<span id="TSB__arrowBoxSpan_172" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_172" class="arial_14 bold" title="DAX (Xetra)"><a href="/indices/germany-30" class="js-item-name">DAX</a></p>
<p id="TSB__summary_last_172" class="arial_24 bold inlineblock js-item-last pid-172-last_nColor"></p>
<p id="TSB__summary_change_172" class="change js-item-change Font">
<span id="TSB__summary_change_value_172" class="js-change-value pid-172-pc"></span> <span id="TSB__summary_change_percent_172" class="js-change-percent pid-172-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_172" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_172"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_172" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_172" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_172" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_172" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_27" data-pairid="27" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_27" class="arrowBox">
<span id="TSB__arrowBoxSpan_27" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_27" class="arial_14 bold" title="FTSE 100 (Лондон)"><a href="/indices/uk-100" class="js-item-name">FTSE 100</a></p>
<p id="TSB__summary_last_27" class="arial_24 bold inlineblock js-item-last pid-27-last_nColor"></p>
<p id="TSB__summary_change_27" class="change js-item-change Font">
<span id="TSB__summary_change_value_27" class="js-change-value pid-27-pc"></span> <span id="TSB__summary_change_percent_27" class="js-change-percent pid-27-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_27" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_27"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_27" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_27" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_27" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_27" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_166" data-pairid="166" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_166" class="arrowBox">
<span id="TSB__arrowBoxSpan_166" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_166" class="arial_14 bold" title="S&P 500 (Нью-Йорк)"><a href="/indices/us-spx-500" class="js-item-name">S&P 500</a></p>
<p id="TSB__summary_last_166" class="arial_24 bold inlineblock js-item-last pid-166-last_nColor"></p>
<p id="TSB__summary_change_166" class="change js-item-change Font">
<span id="TSB__summary_change_value_166" class="js-change-value pid-166-pc"></span> <span id="TSB__summary_change_percent_166" class="js-change-percent pid-166-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_166" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_166"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_166" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_166" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_166" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_166" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_169" data-pairid="169" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_169" class="arrowBox">
<span id="TSB__arrowBoxSpan_169" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_169" class="arial_14 bold" title="Dow Jones Industrial Average (Нью-Йорк)"><a href="/indices/us-30" class="js-item-name">Dow 30</a></p>
<p id="TSB__summary_last_169" class="arial_24 bold inlineblock js-item-last pid-169-last_nColor"></p>
<p id="TSB__summary_change_169" class="change js-item-change Font">
<span id="TSB__summary_change_value_169" class="js-change-value pid-169-pc"></span> <span id="TSB__summary_change_percent_169" class="js-change-percent pid-169-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_169" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_169"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_169" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_169" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_169" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_169" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_178" data-pairid="178" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_178" class="arrowBox">
<span id="TSB__arrowBoxSpan_178" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_178" class="arial_14 bold" title="Nikkei 225 (Токио)"><a href="/indices/japan-ni225" class="js-item-name">Nikkei 225</a></p>
<p id="TSB__summary_last_178" class="arial_24 bold inlineblock js-item-last pid-178-last_nColor"></p>
<p id="TSB__summary_change_178" class="change js-item-change Font">
<span id="TSB__summary_change_value_178" class="js-change-value pid-178-pc"></span> <span id="TSB__summary_change_percent_178" class="js-change-percent pid-178-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_178" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_178"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_178" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_178" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_178" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_178" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_945629" data-pairid="945629" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_945629" class="arrowBox">
<span id="TSB__arrowBoxSpan_945629" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_945629" class="arial_14 bold" title="BTC/USD - Биткойн Доллар США"><a href="/crypto/bitcoin/btc-usd" class="js-item-name">BTC/USD</a></p>
<p id="TSB__summary_last_945629" class="arial_24 bold inlineblock js-item-last pid-945629-last_nColor"></p>
<p id="TSB__summary_change_945629" class="change js-item-change Font">
<span id="TSB__summary_change_value_945629" class="js-change-value pid-945629-pc"></span> <span id="TSB__summary_change_percent_945629" class="js-change-percent pid-945629-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_945629" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_945629"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_945629" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_945629" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_945629" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_945629" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_997650" data-pairid="997650" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_997650" class="arrowBox">
<span id="TSB__arrowBoxSpan_997650" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_997650" class="arial_14 bold" title="ETH/USD - Эфириум Доллар США"><a href="/crypto/ethereum/eth-usd" class="js-item-name">ETH/USD</a></p>
<p id="TSB__summary_last_997650" class="arial_24 bold inlineblock js-item-last pid-997650-last_nColor"></p>
<p id="TSB__summary_change_997650" class="change js-item-change Font">
<span id="TSB__summary_change_value_997650" class="js-change-value pid-997650-pc"></span> <span id="TSB__summary_change_percent_997650" class="js-change-percent pid-997650-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_997650" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_997650"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_997650" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_997650" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_997650" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_997650" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_1031333" data-pairid="1031333" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_1031333" class="arrowBox">
<span id="TSB__arrowBoxSpan_1031333" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_1031333" class="arial_14 bold" title="BCH/USD - Bitcoin Cash Доллар США"><a href="/crypto/bitcoin-cash/bch-usd" class="js-item-name">BCH/USD</a></p>
<p id="TSB__summary_last_1031333" class="arial_24 bold inlineblock js-item-last pid-1031333-last_nColor"></p>
<p id="TSB__summary_change_1031333" class="change js-item-change Font">
<span id="TSB__summary_change_value_1031333" class="js-change-value pid-1031333-pc"></span> <span id="TSB__summary_change_percent_1031333" class="js-change-percent pid-1031333-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_1031333" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_1031333"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_1031333" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_1031333" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_1031333" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_1031333" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_1010798" data-pairid="1010798" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_1010798" class="arrowBox">
<span id="TSB__arrowBoxSpan_1010798" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_1010798" class="arial_14 bold" title="LTC/USD - Лайткоин Доллар США"><a href="/crypto/litecoin/ltc-usd" class="js-item-name">LTC/USD</a></p>
<p id="TSB__summary_last_1010798" class="arial_24 bold inlineblock js-item-last pid-1010798-last_nColor"></p>
<p id="TSB__summary_change_1010798" class="change js-item-change Font">
<span id="TSB__summary_change_value_1010798" class="js-change-value pid-1010798-pc"></span> <span id="TSB__summary_change_percent_1010798" class="js-change-percent pid-1010798-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_1010798" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_1010798"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_1010798" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_1010798" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_1010798" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_1010798" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_1031068" data-pairid="1031068" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_1031068" class="arrowBox">
<span id="TSB__arrowBoxSpan_1031068" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_1031068" class="arial_14 bold" title="IOTA/USD - IOTA Доллар США"><a href="/crypto/iota/iota-usd" class="js-item-name">IOTA/USD</a></p>
<p id="TSB__summary_last_1031068" class="arial_24 bold inlineblock js-item-last pid-1031068-last_nColor"></p>
<p id="TSB__summary_change_1031068" class="change js-item-change Font">
<span id="TSB__summary_change_value_1031068" class="js-change-value pid-1031068-pc"></span> <span id="TSB__summary_change_percent_1031068" class="js-change-percent pid-1031068-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_1031068" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_1031068"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_1031068" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_1031068" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_1031068" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_1031068" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_1031678" data-pairid="1031678" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_1031678" class="arrowBox">
<span id="TSB__arrowBoxSpan_1031678" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_1031678" class="arial_14 bold" title="BTC/RUB - Биткойн Российский рубль"><a href="/crypto/bitcoin/btc-rub" class="js-item-name">BTC/RUB</a></p>
<p id="TSB__summary_last_1031678" class="arial_24 bold inlineblock js-item-last pid-1031678-last_nColor"></p>
<p id="TSB__summary_change_1031678" class="change js-item-change Font">
<span id="TSB__summary_change_value_1031678" class="js-change-value pid-1031678-pc"></span> <span id="TSB__summary_change_percent_1031678" class="js-change-percent pid-1031678-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_1031678" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_1031678"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_1031678" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_1031678" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_1031678" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_1031678" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_1010782" data-pairid="1010782" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_1010782" class="arrowBox">
<span id="TSB__arrowBoxSpan_1010782" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_1010782" class="arial_14 bold" title="XRP/USD - Рипл Доллар США"><a href="/crypto/ripple/xrp-usd" class="js-item-name">XRP/USD</a></p>
<p id="TSB__summary_last_1010782" class="arial_24 bold inlineblock js-item-last pid-1010782-last_nColor"></p>
<p id="TSB__summary_change_1010782" class="change js-item-change Font">
<span id="TSB__summary_change_value_1010782" class="js-change-value pid-1010782-pc"></span> <span id="TSB__summary_change_percent_1010782" class="js-change-percent pid-1010782-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_1010782" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_1010782"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_1010782" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_1010782" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_1010782" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_1010782" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_1010785" data-pairid="1010785" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_1010785" class="arrowBox">
<span id="TSB__arrowBoxSpan_1010785" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_1010785" class="arial_14 bold" title="DASH/USD - Дэш Доллар США"><a href="/crypto/dash/dash-usd" class="js-item-name">DASH/USD</a></p>
<p id="TSB__summary_last_1010785" class="arial_24 bold inlineblock js-item-last pid-1010785-last_nColor"></p>
<p id="TSB__summary_change_1010785" class="change js-item-change Font">
<span id="TSB__summary_change_value_1010785" class="js-change-value pid-1010785-pc"></span> <span id="TSB__summary_change_percent_1010785" class="js-change-percent pid-1010785-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_1010785" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_1010785"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_1010785" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_1010785" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_1010785" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_1010785" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_252" data-pairid="252" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_252" class="arrowBox">
<span id="TSB__arrowBoxSpan_252" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_252" class="arial_14 bold" title="Microsoft Corporation (NASDAQ)"><a href="/equities/microsoft-corp" class="js-item-name">Microsoft</a></p>
<p id="TSB__summary_last_252" class="arial_24 bold inlineblock js-item-last pid-252-last_nColor"></p>
<p id="TSB__summary_change_252" class="change js-item-change Font">
<span id="TSB__summary_change_value_252" class="js-change-value pid-252-pc"></span> <span id="TSB__summary_change_percent_252" class="js-change-percent pid-252-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_252" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_252"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_252" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_252" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_252" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_252" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_6408" data-pairid="6408" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_6408" class="arrowBox">
<span id="TSB__arrowBoxSpan_6408" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_6408" class="arial_14 bold" title="Apple Inc (NASDAQ)"><a href="/equities/apple-computer-inc" class="js-item-name">Apple</a></p>
<p id="TSB__summary_last_6408" class="arial_24 bold inlineblock js-item-last pid-6408-last_nColor"></p>
<p id="TSB__summary_change_6408" class="change js-item-change Font">
<span id="TSB__summary_change_value_6408" class="js-change-value pid-6408-pc"></span> <span id="TSB__summary_change_percent_6408" class="js-change-percent pid-6408-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_6408" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_6408"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_6408" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_6408" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_6408" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_6408" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_6369" data-pairid="6369" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_6369" class="arrowBox">
<span id="TSB__arrowBoxSpan_6369" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_6369" class="arial_14 bold" title="Alphabet Inc Class A (NASDAQ)"><a href="/equities/google-inc" class="js-item-name">Alphabet A</a></p>
<p id="TSB__summary_last_6369" class="arial_24 bold inlineblock js-item-last pid-6369-last_nColor"></p>
<p id="TSB__summary_change_6369" class="change js-item-change Font">
<span id="TSB__summary_change_value_6369" class="js-change-value pid-6369-pc"></span> <span id="TSB__summary_change_percent_6369" class="js-change-percent pid-6369-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_6369" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_6369"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_6369" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_6369" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_6369" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_6369" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_7888" data-pairid="7888" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_7888" class="arrowBox">
<span id="TSB__arrowBoxSpan_7888" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_7888" class="arial_14 bold" title="Exxon Mobil Corp (Нью-Йорк)"><a href="/equities/exxon-mobil" class="js-item-name">Exxon Mobil</a></p>
<p id="TSB__summary_last_7888" class="arial_24 bold inlineblock js-item-last pid-7888-last_nColor"></p>
<p id="TSB__summary_change_7888" class="change js-item-change Font">
<span id="TSB__summary_change_value_7888" class="js-change-value pid-7888-pc"></span> <span id="TSB__summary_change_percent_7888" class="js-change-percent pid-7888-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_7888" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_7888"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_7888" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_7888" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_7888" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_7888" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_284" data-pairid="284" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_284" class="arrowBox">
<span id="TSB__arrowBoxSpan_284" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_284" class="arial_14 bold" title="BP PLC (Лондон)"><a href="/equities/bp" class="js-item-name">BP</a></p>
<p id="TSB__summary_last_284" class="arial_24 bold inlineblock js-item-last pid-284-last_nColor"></p>
<p id="TSB__summary_change_284" class="change js-item-change Font">
<span id="TSB__summary_change_value_284" class="js-change-value pid-284-pc"></span> <span id="TSB__summary_change_percent_284" class="js-change-percent pid-284-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_284" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_284"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_284" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_284" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_284" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_284" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_9251" data-pairid="9251" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_9251" class="arrowBox">
<span id="TSB__arrowBoxSpan_9251" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_9251" class="arial_14 bold" title="E.ON AG (Xetra)"><a href="/equities/e.on" class="js-item-name">E.ON</a></p>
<p id="TSB__summary_last_9251" class="arial_24 bold inlineblock js-item-last pid-9251-last_nColor"></p>
<p id="TSB__summary_change_9251" class="change js-item-change Font">
<span id="TSB__summary_change_value_9251" class="js-change-value pid-9251-pc"></span> <span id="TSB__summary_change_percent_9251" class="js-change-percent pid-9251-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_9251" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_9251"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_9251" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_9251" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_9251" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_9251" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_396" data-pairid="396" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_396" class="arrowBox">
<span id="TSB__arrowBoxSpan_396" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_396" class="arial_14 bold" title="BNP Paribas SA (Париж)"><a href="/equities/bnp-paribas" class="js-item-name">BNP Paribas</a></p>
<p id="TSB__summary_last_396" class="arial_24 bold inlineblock js-item-last pid-396-last_nColor"></p>
<p id="TSB__summary_change_396" class="change js-item-change Font">
<span id="TSB__summary_change_value_396" class="js-change-value pid-396-pc"></span> <span id="TSB__summary_change_percent_396" class="js-change-percent pid-396-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_396" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_396"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_396" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_396" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_396" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_396" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_23701" data-pairid="23701" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_23701" class="arrowBox">
<span id="TSB__arrowBoxSpan_23701" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_23701" class="arial_14 bold" title="США 2-летние (Нью-Йорк)"><a href="/rates-bonds/u.s.-2-year-bond-yield" class="js-item-name">США 2-летние</a></p>
<p id="TSB__summary_last_23701" class="arial_24 bold inlineblock js-item-last pid-23701-last_nColor"></p>
<p id="TSB__summary_change_23701" class="change js-item-change Font">
<span id="TSB__summary_change_value_23701" class="js-change-value pid-23701-pc"></span> <span id="TSB__summary_change_percent_23701" class="js-change-percent pid-23701-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_23701" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_23701"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_23701" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_23701" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_23701" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_23701" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_23703" data-pairid="23703" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_23703" class="arrowBox">
<span id="TSB__arrowBoxSpan_23703" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_23703" class="arial_14 bold" title="США 5-летние (Нью-Йорк)"><a href="/rates-bonds/u.s.-5-year-bond-yield" class="js-item-name">США 5-летние</a></p>
<p id="TSB__summary_last_23703" class="arial_24 bold inlineblock js-item-last pid-23703-last_nColor"></p>
<p id="TSB__summary_change_23703" class="change js-item-change Font">
<span id="TSB__summary_change_value_23703" class="js-change-value pid-23703-pc"></span> <span id="TSB__summary_change_percent_23703" class="js-change-percent pid-23703-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_23703" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_23703"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_23703" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_23703" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_23703" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_23703" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_23705" data-pairid="23705" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_23705" class="arrowBox">
<span id="TSB__arrowBoxSpan_23705" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_23705" class="arial_14 bold" title="США 10-летние (Нью-Йорк)"><a href="/rates-bonds/u.s.-10-year-bond-yield" class="js-item-name">США 10-летние</a></p>
<p id="TSB__summary_last_23705" class="arial_24 bold inlineblock js-item-last pid-23705-last_nColor"></p>
<p id="TSB__summary_change_23705" class="change js-item-change Font">
<span id="TSB__summary_change_value_23705" class="js-change-value pid-23705-pc"></span> <span id="TSB__summary_change_percent_23705" class="js-change-percent pid-23705-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_23705" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_23705"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_23705" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_23705" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_23705" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_23705" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_23706" data-pairid="23706" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_23706" class="arrowBox">
<span id="TSB__arrowBoxSpan_23706" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_23706" class="arial_14 bold" title="США 30-летние (Нью-Йорк)"><a href="/rates-bonds/u.s.-30-year-bond-yield" class="js-item-name">США 30-летние</a></p>
<p id="TSB__summary_last_23706" class="arial_24 bold inlineblock js-item-last pid-23706-last_nColor"></p>
<p id="TSB__summary_change_23706" class="change js-item-change Font">
<span id="TSB__summary_change_value_23706" class="js-change-value pid-23706-pc"></span> <span id="TSB__summary_change_percent_23706" class="js-change-percent pid-23706-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_23706" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_23706"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_23706" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_23706" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_23706" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_23706" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8895" data-pairid="8895" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8895" class="arrowBox">
<span id="TSB__arrowBoxSpan_8895" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8895" class="arial_14 bold" title="Фьючерс на Euro Bund"><a href="/rates-bonds/euro-bund" class="js-item-name">Euro Bund</a></p>
<p id="TSB__summary_last_8895" class="arial_24 bold inlineblock js-item-last pid-8895-last_nColor"></p>
<p id="TSB__summary_change_8895" class="change js-item-change Font">
<span id="TSB__summary_change_value_8895" class="js-change-value pid-8895-pc"></span> <span id="TSB__summary_change_percent_8895" class="js-change-percent pid-8895-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8895" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8895"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8895" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8895" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8895" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8895" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8880" data-pairid="8880" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8880" class="arrowBox">
<span id="TSB__arrowBoxSpan_8880" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8880" class="arial_14 bold" title="Фьючерс на US 10 YR T-Note"><a href="/rates-bonds/us-10-yr-t-note" class="js-item-name">US 10 YR T-Note</a></p>
<p id="TSB__summary_last_8880" class="arial_24 bold inlineblock js-item-last pid-8880-last_nColor"></p>
<p id="TSB__summary_change_8880" class="change js-item-change Font">
<span id="TSB__summary_change_value_8880" class="js-change-value pid-8880-pc"></span> <span id="TSB__summary_change_percent_8880" class="js-change-percent pid-8880-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8880" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8880"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8880" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8880" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8880" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8880" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8986" data-pairid="8986" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8986" class="arrowBox">
<span id="TSB__arrowBoxSpan_8986" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8986" class="arial_14 bold" title="Фьючерс на Japan Govt. Bond"><a href="/rates-bonds/japan-govt.-bond" class="js-item-name">Japan Govt. Bond</a></p>
<p id="TSB__summary_last_8986" class="arial_24 bold inlineblock js-item-last pid-8986-last_nColor"></p>
<p id="TSB__summary_change_8986" class="change js-item-change Font">
<span id="TSB__summary_change_value_8986" class="js-change-value pid-8986-pc"></span> <span id="TSB__summary_change_percent_8986" class="js-change-percent pid-8986-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8986" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8986"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8986" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8986" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8986" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8986" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8886" data-pairid="8886" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8886" class="arrowBox">
<span id="TSB__arrowBoxSpan_8886" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8886" class="arial_14 bold" title="Фьючерс на UK Gilt"><a href="/rates-bonds/uk-gilt" class="js-item-name">UK Gilt</a></p>
<p id="TSB__summary_last_8886" class="arial_24 bold inlineblock js-item-last pid-8886-last_nColor"></p>
<p id="TSB__summary_change_8886" class="change js-item-change Font">
<span id="TSB__summary_change_value_8886" class="js-change-value pid-8886-pc"></span> <span id="TSB__summary_change_percent_8886" class="js-change-percent pid-8886-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8886" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8886"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8886" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8886" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8886" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8886" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_504" data-pairid="504" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_504" class="arrowBox">
<span id="TSB__arrowBoxSpan_504" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_504" class="arial_14 bold" title="SPDR Dow Jones Industrial Average (Нью-Йорк)"><a href="/etfs/diamonds-trust" class="js-item-name">SPDR DJIA</a></p>
<p id="TSB__summary_last_504" class="arial_24 bold inlineblock js-item-last pid-504-last_nColor"></p>
<p id="TSB__summary_change_504" class="change js-item-change Font">
<span id="TSB__summary_change_value_504" class="js-change-value pid-504-pc"></span> <span id="TSB__summary_change_percent_504" class="js-change-percent pid-504-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_504" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_504"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_504" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_504" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_504" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_504" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_44554" data-pairid="44554" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_44554" class="arrowBox">
<span id="TSB__arrowBoxSpan_44554" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_44554" class="arial_14 bold" title="iShares Russell 1000 Growth (Нью-Йорк)"><a href="/etfs/ishares-russell-1000-growth" class="js-item-name">iShares Russell 1000 Growth</a></p>
<p id="TSB__summary_last_44554" class="arial_24 bold inlineblock js-item-last pid-44554-last_nColor"></p>
<p id="TSB__summary_change_44554" class="change js-item-change Font">
<span id="TSB__summary_change_value_44554" class="js-change-value pid-44554-pc"></span> <span id="TSB__summary_change_percent_44554" class="js-change-percent pid-44554-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_44554" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_44554"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_44554" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_44554" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_44554" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_44554" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_14202" data-pairid="14202" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_14202" class="arrowBox">
<span id="TSB__arrowBoxSpan_14202" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_14202" class="arial_14 bold" title="iShares Russell 2000 (Нью-Йорк)"><a href="/etfs/ishares-russell-2000-index-etf" class="js-item-name">iShares Russell 2000</a></p>
<p id="TSB__summary_last_14202" class="arial_24 bold inlineblock js-item-last pid-14202-last_nColor"></p>
<p id="TSB__summary_change_14202" class="change js-item-change Font">
<span id="TSB__summary_change_value_14202" class="js-change-value pid-14202-pc"></span> <span id="TSB__summary_change_percent_14202" class="js-change-percent pid-14202-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_14202" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_14202"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_14202" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_14202" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_14202" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_14202" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_651" data-pairid="651" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_651" class="arrowBox">
<span id="TSB__arrowBoxSpan_651" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_651" class="arial_14 bold" title="Invesco QQQ Trust Series 1 (NASDAQ)"><a href="/etfs/powershares-qqqq" class="js-item-name">Invesco QQQ Trust Series 1</a></p>
<p id="TSB__summary_last_651" class="arial_24 bold inlineblock js-item-last pid-651-last_nColor"></p>
<p id="TSB__summary_change_651" class="change js-item-change Font">
<span id="TSB__summary_change_value_651" class="js-change-value pid-651-pc"></span> <span id="TSB__summary_change_percent_651" class="js-change-percent pid-651-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_651" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_651"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_651" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_651" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_651" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_651" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_525" data-pairid="525" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_525" class="arrowBox">
<span id="TSB__arrowBoxSpan_525" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_525" class="arial_14 bold" title="SPDR S&P 500 (Нью-Йорк)"><a href="/etfs/spdr-s-p-500" class="js-item-name">SPDR S&P 500</a></p>
<p id="TSB__summary_last_525" class="arial_24 bold inlineblock js-item-last pid-525-last_nColor"></p>
<p id="TSB__summary_change_525" class="change js-item-change Font">
<span id="TSB__summary_change_value_525" class="js-change-value pid-525-pc"></span> <span id="TSB__summary_change_percent_525" class="js-change-percent pid-525-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_525" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_525"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_525" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_525" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_525" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_525" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_14210" data-pairid="14210" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_14210" class="arrowBox">
<span id="TSB__arrowBoxSpan_14210" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_14210" class="arial_14 bold" title="ProShares UltraShort S&P500 (Нью-Йорк)"><a href="/etfs/proshares-ultrashort-s-p500" class="js-item-name">ProShares UltraShort S&P500</a></p>
<p id="TSB__summary_last_14210" class="arial_24 bold inlineblock js-item-last pid-14210-last_nColor"></p>
<p id="TSB__summary_change_14210" class="change js-item-change Font">
<span id="TSB__summary_change_value_14210" class="js-change-value pid-14210-pc"></span> <span id="TSB__summary_change_percent_14210" class="js-change-percent pid-14210-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_14210" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_14210"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_14210" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_14210" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_14210" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_14210" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_14206" data-pairid="14206" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_14206" class="arrowBox">
<span id="TSB__arrowBoxSpan_14206" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_14206" class="arial_14 bold" title="ProShares UltraShort QQQ (Нью-Йорк)"><a href="/etfs/proshares-ultrashort-qqq-etf" class="js-item-name">ProShares UltraShort QQQ</a></p>
<p id="TSB__summary_last_14206" class="arial_24 bold inlineblock js-item-last pid-14206-last_nColor"></p>
<p id="TSB__summary_change_14206" class="change js-item-change Font">
<span id="TSB__summary_change_value_14206" class="js-change-value pid-14206-pc"></span> <span id="TSB__summary_change_percent_14206" class="js-change-percent pid-14206-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_14206" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_14206"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_14206" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_14206" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_14206" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_14206" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div class=" js-inner-table" data-tab-id="1"  id="TSB_1_inner" pair_ID="1" rel="inner"  data-gae='tsb_fx' ><table rel="pairTable" class="genTbl openTbl quotesSideBlockTbl collapsedTbl">
<tbody>
<tr id="TSB_pair_1" data-pairid="1" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="EUR/USD - Евро Доллар США (CFD)"><a href="/currencies/eur-usd" data-gae='tsb_fx-eur-usd' >EUR/USD</a></td>
<td class="lastNum js-item-last pid-1-last">1,1593</td>
<td class="chg bold js-item-summary left textNum redFont">Продавать</td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_1691" data-pairid="1691" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite redClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="EUR/RUB - Евро Российский рубль (CFD)"><a href="/currencies/eur-rub" data-gae='tsb_fx-eur-rub' >EUR/RUB</a></td>
<td class="lastNum js-item-last pid-1691-last">76,0865</td>
<td class="chg bold js-item-summary left textNum greenFont">Активно покупать</td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_2186" data-pairid="2186" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite redClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="USD/RUB - Доллар США Российский рубль (CFD)"><a href="/currencies/usd-rub" data-gae='tsb_fx-usd-rub' >USD/RUB</a></td>
<td class="lastNum js-item-last pid-2186-last">65,5503</td>
<td class="chg bold js-item-summary left textNum greenFont">Активно покупать</td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_2" data-pairid="2" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="GBP/USD - Британский фунт Доллар США (CFD)"><a href="/currencies/gbp-usd" data-gae='tsb_fx-gbp-usd' >GBP/USD</a></td>
<td class="lastNum js-item-last pid-2-last">1,3036</td>
<td class="chg bold js-item-summary left textNum greenFont">Активно покупать</td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_3" data-pairid="3" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="USD/JPY - Доллар США Японская иена (CFD)"><a href="/currencies/usd-jpy" data-gae='tsb_fx-usd-jpy' >USD/JPY</a></td>
<td class="lastNum js-item-last pid-3-last">113,94</td>
<td class="chg bold js-item-summary left textNum greenFont">Активно покупать</td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_9" data-pairid="9" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="EUR/JPY - Евро Японская иена (CFD)"><a href="/currencies/eur-jpy" data-gae='tsb_fx-eur-jpy' >EUR/JPY</a></td>
<td class="lastNum js-item-last pid-9-last">132,09</td>
<td class="chg bold js-item-summary left textNum neutralFont">Нейтрально</td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_5" data-pairid="5" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="AUD/USD - Австралийский доллар Доллар США (CFD)"><a href="/currencies/aud-usd" data-gae='tsb_fx-aud-usd' >AUD/USD</a></td>
<td class="lastNum js-item-last pid-5-last">0,7213</td>
<td class="chg bold js-item-summary left textNum neutralFont">Нейтрально</td>
<td class="sideColumn">&nbsp;</td>
</tr>
</tbody>
</table>
</div><div class="displayNone js-inner-table" data-tab-id="2"  id="TSB_2_inner" pair_ID="8830" rel="inner"  data-gae='tsb_commodities' ><table rel="pairTable" class="genTbl openTbl quotesSideBlockTbl collapsedTbl">
<tbody>
<tr id="TSB_pair_8830" data-pairid="8830" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="Фьючерс на золото (CFD)"><a href="/commodities/gold" data-gae='tsb_commodities-gold' >Золото</a></td>
<td class="lastNum js-item-last pid-8830-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_8836" data-pairid="8836" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="Фьючерс на серебро (CFD)"><a href="/commodities/silver" data-gae='tsb_commodities-silver' >Серебро</a></td>
<td class="lastNum js-item-last pid-8836-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_8833" data-pairid="8833" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="Фьючерс на нефть Brent (CFD)"><a href="/commodities/brent-oil" data-gae='tsb_commodities-brent-oil' >Нефть Brent</a></td>
<td class="lastNum js-item-last pid-8833-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_8849" data-pairid="8849" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="Фьючерсы на нефть WTI (CFD)"><a href="/commodities/crude-oil" data-gae='tsb_commodities-crude-oil' >Нефть WTI</a></td>
<td class="lastNum js-item-last pid-8849-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_8883" data-pairid="8883" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="Фьючерс на палладий (CFD)"><a href="/commodities/palladium" data-gae='tsb_commodities-palladium' >Палладий</a></td>
<td class="lastNum js-item-last pid-8883-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_8831" data-pairid="8831" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="Фьючерс на медь (CFD)"><a href="/commodities/copper" data-gae='tsb_commodities-copper' >Медь</a></td>
<td class="lastNum js-item-last pid-8831-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_8910" data-pairid="8910" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="Фьючерс на платину (CFD)"><a href="/commodities/platinum" data-gae='tsb_commodities-platinum' >Платина</a></td>
<td class="lastNum js-item-last pid-8910-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
</tbody>
</table>
</div><div class="displayNone js-inner-table" data-tab-id="3"  id="TSB_3_inner" pair_ID="13665" rel="inner"  data-gae='tsb_indices' ><table rel="pairTable" class="genTbl openTbl quotesSideBlockTbl collapsedTbl">
<tbody>
<tr id="TSB_pair_13665" data-pairid="13665" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite redClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="РТС (Москва)"><a href="/indices/rtsi" data-gae='tsb_indices-rtsi' >РТС</a></td>
<td class="lastNum js-item-last pid-13665-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_13666" data-pairid="13666" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite redClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="Индекс МосБиржи (Москва)"><a href="/indices/mcx" data-gae='tsb_indices-mcx' >Индекс МосБиржи</a></td>
<td class="lastNum js-item-last pid-13666-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_172" data-pairid="172" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite redClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="DAX (Xetra) (CFD)"><a href="/indices/germany-30" data-gae='tsb_indices-germany-30' >DAX</a></td>
<td class="lastNum js-item-last pid-172-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_27" data-pairid="27" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite redClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="FTSE 100 (Лондон) (CFD)"><a href="/indices/uk-100" data-gae='tsb_indices-uk-100' >FTSE 100</a></td>
<td class="lastNum js-item-last pid-27-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_166" data-pairid="166" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite redClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="S&P 500 (Нью-Йорк) (CFD)"><a href="/indices/us-spx-500" data-gae='tsb_indices-us-spx-500' >S&P 500</a></td>
<td class="lastNum js-item-last pid-166-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_169" data-pairid="169" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite redClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="Dow Jones Industrial Average (Нью-Йорк)"><a href="/indices/us-30" data-gae='tsb_indices-us-30' >Dow 30</a></td>
<td class="lastNum js-item-last pid-169-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_178" data-pairid="178" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite redClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="Nikkei 225 (Токио) (CFD)"><a href="/indices/japan-ni225" data-gae='tsb_indices-japan-ni225' >Nikkei 225</a></td>
<td class="lastNum js-item-last pid-178-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
</tbody>
</table>
</div><div class="displayNone js-inner-table" data-tab-id="4"  id="TSB_4_inner" pair_ID="945629" rel="inner"  data-gae='tsb_stocks' ><table rel="pairTable" class="genTbl openTbl quotesSideBlockTbl collapsedTbl">
<tbody>
<tr id="TSB_pair_945629" data-pairid="945629" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="BTC/USD - Биткойн Доллар США (CFD)"><a href="/crypto/bitcoin/btc-usd" data-gae='tsb_stocks-btc-usd' >BTC/USD</a></td>
<td class="lastNum js-item-last pid-945629-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_997650" data-pairid="997650" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="ETH/USD - Эфириум Доллар США (CFD)"><a href="/crypto/ethereum/eth-usd" data-gae='tsb_stocks-eth-usd' >ETH/USD</a></td>
<td class="lastNum js-item-last pid-997650-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_1031333" data-pairid="1031333" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="BCH/USD - Bitcoin Cash Доллар США (CFD)"><a href="/crypto/bitcoin-cash/bch-usd" data-gae='tsb_stocks-bch-usd' >BCH/USD</a></td>
<td class="lastNum js-item-last pid-1031333-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_1010798" data-pairid="1010798" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="LTC/USD - Лайткоин Доллар США (CFD)"><a href="/crypto/litecoin/ltc-usd" data-gae='tsb_stocks-ltc-usd' >LTC/USD</a></td>
<td class="lastNum js-item-last pid-1010798-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_1031068" data-pairid="1031068" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="IOTA/USD - IOTA Доллар США (CFD)"><a href="/crypto/iota/iota-usd" data-gae='tsb_stocks-iota-usd' >IOTA/USD</a></td>
<td class="lastNum js-item-last pid-1031068-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_1031678" data-pairid="1031678" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="BTC/RUB - Биткойн Российский рубль (CFD)"><a href="/crypto/bitcoin/btc-rub" data-gae='tsb_stocks-btc-rub' >BTC/RUB</a></td>
<td class="lastNum js-item-last pid-1031678-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_1010782" data-pairid="1010782" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="XRP/USD - Рипл Доллар США (CFD)"><a href="/crypto/ripple/xrp-usd" data-gae='tsb_stocks-xrp-usd' >XRP/USD</a></td>
<td class="lastNum js-item-last pid-1010782-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_1010785" data-pairid="1010785" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="DASH/USD - Дэш Доллар США (CFD)"><a href="/crypto/dash/dash-usd" data-gae='tsb_stocks-dash-usd' >DASH/USD</a></td>
<td class="lastNum js-item-last pid-1010785-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
</tbody>
</table>
</div>		</div>
</div>
<script>
$(window).trigger('appendNewData',[["pid-eu-8833:","pid-eu-8849:","pid-eu-8830:","pid-eu-8836:","pid-eu-8910:","pid-eu-8883:","pid-eu-8862:","pid-eu-49768:","pid-eu-1:","pid-eu-1691:","pid-eu-2186:","pid-eu-2:","pid-eu-3:","pid-eu-9:","pid-eu-5:","pidTechSumm-1:","pidTechSumm-1691:","pidTechSumm-2186:","pidTechSumm-2:","pidTechSumm-3:","pidTechSumm-9:","pidTechSumm-5:","isOpenPair-8833:","isOpenPair-8849:","isOpenPair-8830:","isOpenPair-8836:","isOpenPair-8910:","isOpenPair-8883:","isOpenPair-8862:","isOpenPair-49768:"]]);
</script><div class="clear " id="user_last_quotes"><div class="sideColumnBox js-recent-quotes"><div class="sideColumnBoxTitle">Просмотренные котировки</div><div id="tab_1_inner"><div id="recent" class="recent"><div class="emptyText lightgrayFont">Котировки, которые вы просматриваете чаще всего, будут автоматически отображаться здесь</div></div></div></div><script>var uri = ''; var sid = '1b850a2f8000122c4ecbfb775fd864e5'; </script></div> <!-- /sideBanner --><div class="clear rightColumnAd scrollingRightBox" id="ad3"><div id='div-gpt-ad-1370187203350-sb3'></div></div> <!-- /sideBanner -->    	            <!-- js -->
<script type="text/javascript">
$(window).bind("load", function() {
scrollng_ad('scrollingRightBox', 'footerWrapper');
});
</script>
</aside><!-- /rightColumn -->

<div class="clear"></div>
<footer id="footerWrapper">
<div>
<div id="findABroker" class="findABrokerWrapper">
<p class="h2LikeTitle"><a href="/brokers/forex-brokers">Найдите брокера</a></p>
<div id="findABrokerTabs">
<div id="findABrokerTabsMenu" class="findABrokerTabsMenu">
<div class="TabContainer noOverflow js-tab-container">
<span class="newSiteIconsSprite arrow"></span>
<ul class="tabsForBox oneLvlTabsWidget js-one-level-tabs " id="findTheBroker">

<li id=fabID_1 class="first selected" ><a href="javascript:void(0);">Форекс</a></li>

<li id=fabID_9 ><a href="javascript:void(0);">Крипто</a></li>

<li id=fabID_2 ><a href="javascript:void(0);">CFDs</a></li>

<li id=fabID_6 class="last" ><a href="javascript:void(0);">Акции</a></li>

</ul>
<span id="lastDummyLI" data-more-cat="Другие категории" class="moreTabOneLvlDummy displayNone"><a href="javascript:void();">Другие категории</a><span class="showMoreDownArrow"></span></span>
</div>

<script>
$(document).ready(function(){
tabsWidgetOnelevel('', '', 'findTheBroker', +'');
});
</script>		</div>
<div id="findABroker_html" class="findABroker">

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=YyM1a2cwZD0yZ2hsNWUyNzdiNWszNzE7NSJmNDU_ZxtkE2VBZnY2cWM5aDZuZWRtPzw-NmY3NXE0dGJjOj40YWNnNWZnM2QuMiRoaQ==')"><img data-rowid="227387" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/logo_120x60_Capital_Level.png"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=NnZhPzNkP2Y3YmxoZDQ5PDNmYjxhZTQ-YnVgMmNpZBg5TjURZXVlImE7bDI1OzY9NzdhYD5vZyNnJ2JjY2duOzYyYTIzZz91NyFsbQ==')"><img data-rowid="275017" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/IFX_Investing_logo_120x60.png"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=YCBiPDZhPmczZjw4NWUxNGM2MW9jZzI4YHcyYDE7ZRkxRmRANSVkIzVvO2VibGFqMjA_OjdnOn43d2JjZmJgNWBkYjE2Yj50MyU8PQ==')"><img data-rowid="275256" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/fab_65653538.png"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=YiI3aWA3Yzo2Y2hsZzc5PDdiNGphZWZsMCdvPTowZBhkEz4aZ3c_eDBqbDIyPDY9MTUzMTBmZiIwcDMyNzNvOmJmN2RgNGMpNiBoaQ==')"><img data-rowid="275420" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/fab_34089116.jpg"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=ZSU1a2YxN25kMT46ZTU3MjVgZDphZTM5NCNkNmNpZho1QmJGNCQwdzVvPWMzPTM4PzsyMGEwNXEycmVkMjZgNWVhNWZmMjd9ZHI-Pw==')"><img data-rowid="275427" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/libertex_logo_120x60.png"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=YCAxbzZhMGkxZG9rNGRiZ2QxNWtiZjA6MSZnNTE7ZBgyRTQQMSE-eTNpajQzPTA7YGRhaTVkN3M2dmFgMDQ1YGBkMWI2YjB6MSdvbg==')"><img data-rowid="275487" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/fab_7127653.png"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=N3c3aW45MGljNmBkMGAzNjFkNmgzNzE7NCNjMTU_Yh4xRmFFZHQ3cGM5PWM0OmVuNzI2MjFiNXEzc2JjNjIzZjczN2RuOjB6Y3VgYQ==')"><img data-rowid="275545" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/fab_81337736.jpg"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=YCAzbW84MWhkMTw4ZjY3MmM2M21mYmBqNyAyYDA6YR0xRjIWYHA2cTFrajRkbmZrMjphYDRjMnYycjMyYWVjNmBkM2BvOzF7ZHI8PQ==')"><img data-rowid="233811" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/fab_69621100.jpg"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=NHQ-YG84Nm9lMGhsMGA3Mj5rYz02MjY8YXZhMzY8bhJgFz4aNSU3cGQ-bDJlaDI5Nj41MTNgNXE1dTQ1Z2NkMTQwPm1vOzZ8ZXNoaQ==')"><img data-rowid="245845" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/fab_44227365.gif"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=ZSVjPWYxM2oxZG1pYDBiZzFkNGphZWBqYXZlN2RuNUkxRjcTYnJlImQ-bjBibmRpZ2M2ND5oMXUycjU0YWViN2VhYzBmMjN5MSdtbA==')"><img data-rowid="253420" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/fab_7127653.png"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=NnZiPDFmMWhkMWpuYjI5PDJnYjw0MDowNiFjMWdtYh4wRzMXNCQwdzBqazVmaWRuMDIzOjJjNnJmJmNiMjYwZTYyYjExZTF7ZHJqaw==')"><img data-rowid="264297" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/fab_54534147.png"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=M3NjPWYxZTxkMT05MmJkYTRhMG5lYTY8PCtgMmZsZBhgFz4aYnIxdjVvOWdmaGZsPjdiZTdkNHAycjQ1NjJnMjM3YzBmMmUvZHI9PA==')"><img data-rowid="274975" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/fab_44629481.jpg"></a>
</div>
</div>
</div>
<script>
window.FABTabsInitObject = {
dataStr: '{"1":[{"id":"227387","link":"\/jp.php?v2=YyM1a2cwZD0yZ2hsNWUyNzdiNWszNzE7NSJmNDU_ZxtkE2VBZnY2cWM5aDZuZWRtPzw-NmY3NXE0dGJjOj40YWNnNWZnM2QuMiRoaQ==","img120":"Fab120\/logo_120x60_Capital_Level.png"},{"id":"275017","link":"\/jp.php?v2=NnZhPzNkP2Y3YmxoZDQ5PDNmYjxhZTQ-YnVgMmNpZBg5TjURZXVlImE7bDI1OzY9NzdhYD5vZyNnJ2JjY2duOzYyYTIzZz91NyFsbQ==","img120":"Fab120\/IFX_Investing_logo_120x60.png"},{"id":"275256","link":"\/jp.php?v2=YCBiPDZhPmczZjw4NWUxNGM2MW9jZzI4YHcyYDE7ZRkxRmRANSVkIzVvO2VibGFqMjA_OjdnOn43d2JjZmJgNWBkYjE2Yj50MyU8PQ==","img120":"Fab120\/fab_65653538.png"},{"id":"275420","link":"\/jp.php?v2=YiI3aWA3Yzo2Y2hsZzc5PDdiNGphZWZsMCdvPTowZBhkEz4aZ3c_eDBqbDIyPDY9MTUzMTBmZiIwcDMyNzNvOmJmN2RgNGMpNiBoaQ==","img120":"Fab120\/fab_34089116.jpg"},{"id":"275427","link":"\/jp.php?v2=ZSU1a2YxN25kMT46ZTU3MjVgZDphZTM5NCNkNmNpZho1QmJGNCQwdzVvPWMzPTM4PzsyMGEwNXEycmVkMjZgNWVhNWZmMjd9ZHI-Pw==","img120":"Fab120\/libertex_logo_120x60.png"},{"id":"275487","link":"\/jp.php?v2=YCAxbzZhMGkxZG9rNGRiZ2QxNWtiZjA6MSZnNTE7ZBgyRTQQMSE-eTNpajQzPTA7YGRhaTVkN3M2dmFgMDQ1YGBkMWI2YjB6MSdvbg==","img120":"Fab120\/fab_7127653.png"},{"id":"275545","link":"\/jp.php?v2=N3c3aW45MGljNmBkMGAzNjFkNmgzNzE7NCNjMTU_Yh4xRmFFZHQ3cGM5PWM0OmVuNzI2MjFiNXEzc2JjNjIzZjczN2RuOjB6Y3VgYQ==","img120":"Fab120\/fab_81337736.jpg"},{"id":"233811","link":"\/jp.php?v2=YCAzbW84MWhkMTw4ZjY3MmM2M21mYmBqNyAyYDA6YR0xRjIWYHA2cTFrajRkbmZrMjphYDRjMnYycjMyYWVjNmBkM2BvOzF7ZHI8PQ==","img120":"Fab120\/fab_69621100.jpg"},{"id":"245845","link":"\/jp.php?v2=NHQ-YG84Nm9lMGhsMGA3Mj5rYz02MjY8YXZhMzY8bhJgFz4aNSU3cGQ-bDJlaDI5Nj41MTNgNXE1dTQ1Z2NkMTQwPm1vOzZ8ZXNoaQ==","img120":"Fab120\/fab_44227365.gif"},{"id":"253420","link":"\/jp.php?v2=ZSVjPWYxM2oxZG1pYDBiZzFkNGphZWBqYXZlN2RuNUkxRjcTYnJlImQ-bjBibmRpZ2M2ND5oMXUycjU0YWViN2VhYzBmMjN5MSdtbA==","img120":"Fab120\/fab_7127653.png"},{"id":"264297","link":"\/jp.php?v2=NnZiPDFmMWhkMWpuYjI5PDJnYjw0MDowNiFjMWdtYh4wRzMXNCQwdzBqazVmaWRuMDIzOjJjNnJmJmNiMjYwZTYyYjExZTF7ZHJqaw==","img120":"Fab120\/fab_54534147.png"},{"id":"274975","link":"\/jp.php?v2=M3NjPWYxZTxkMT05MmJkYTRhMG5lYTY8PCtgMmZsZBhgFz4aYnIxdjVvOWdmaGZsPjdiZTdkNHAycjQ1NjJnMjM3YzBmMmUvZHI9PA==","img120":"Fab120\/fab_44629481.jpg"}],"9":[{"id":"275045","link":"\/jp.php?v2=ZCQybGE2Nm8xZGltZzdmY2cyN2lkYGBqYnViMGNpZBhiFT8bb38xdjBqOWduYDI5YGA_OzRnO382dm5vMzdgNWRgMmFhNTZ8MSdpaA==","img120":"Fab120\/IFX_Investing_logo_120x60.png"},{"id":"274994","link":"\/jp.php?v2=MHAybGQzM2plMG9rYDBkYTVgNWsyNmJoPCswYjc9bxMwR2VBYnI1cmM5azVmaDI4Y2owOTZkO381dWVkMTVnMjA0MmFkMDN5ZXNvbg==","img120":"Fab120\/fab_44629481.jpg"}],"2":[{"id":"275257","link":"\/jp.php?v2=NnYxb245ZD00YTo-NWVlYDBlMmw2MmBqYXZjMWRuM09gF2ZCZ3c2cWQ-PWNla2JpMTMyNz5vO38ycmJjMDRmMzYyMWJuOmQuNCI6Ow==","img120":"Fab120\/fab_65653538.png"},{"id":"275486","link":"\/jp.php?v2=OXkxbzJlZD1hNDs_NGRjZjdiYjw-OmVvNSI3ZWdtYx9kE2JGZXVjJDBqaTdvYTE6NjI_NzBgNHBgIDc2MTVgNTk9MWIyZmQuYXc7Og==","img120":"Fab120\/fab_7127653.png"},{"id":"275546","link":"\/jp.php?v2=MHAxbzNkNWxhNGhsZDRhZD5rZTtiZjM5ZnFnNTI4Yh5kEzEVbn5lIjFrPWNnaTU-NTAzNzJiNXE2dmVkYWUwZTA0MWIzZzV_YXdoaQ==","img120":"Fab120\/fab_81337736.jpg"},{"id":"233812","link":"\/jp.php?v2=NXU-YDViP2ZlMDk9bj5mYz9qYjwyNmFrMyQ0ZjA6YBwxRmZCZHQ3cDFrO2VkbjU4NT0-PzRgYiYwcGdmOj5nMjUxPm01YT91ZXM5OA==","img120":"Fab120\/fab_69621100.jpg"},{"id":"245846","link":"\/jp.php?v2=MXE_YW84NWw0YT05YjI0MTRhN2k3M2ZsMCcyYDY8M082QTYSYXEwdzNpPWNuYzE6Nj4zN2IyZSFiImFgZmIyZzE1P2xvOzV_NCI9PA==","img120":"Fab120\/fab_44227365.gif"},{"id":"253421","link":"\/jp.php?v2=OHgzbWQzZTwyZz46MmIxNGM2YT8xNTM5YHdlN2NpYh5lEmVBYnI0czBqOWcwPDQ5Z2MzMWM0M3dnJ25vMjZkMTg8M2BkMGUvMiQ-Pw==","img120":"Fab120\/fab_7127653.png"},{"id":"264298","link":"\/jp.php?v2=YCA-YDZhZTwwZWltbz83MmI3P2FlYTQ-NSJlNzQ-ZRk0QzURZnY0czNpPGI0OzY8MDIyOzVrYCRgIGZnMTViN2BkPm02YmUvMCZpaA==","img120":"Fab120\/fab_54534147.png"},{"id":"274976","link":"\/jp.php?v2=ZCRhP2A3YzplMG9rYzNlYDNmMW9iZmVvMyRvPTU_ZBhgF2FFYHA3cGQ-azVvYTA6Nj9jZDVlMnYzc2JjZGA3YmRgYTJgNGMpZXNvbg==","img120":"Fab120\/fab_44629481.jpg"}],"6":[{"id":"275488","link":"\/jp.php?v2=NXUzbTZhMWgzZjs_ZTU3MmM2NWtkYDY8Z3BnNTI4M09nEDEVMSExdmQ-bDJjbTc8MzcyOmY4ZSE9fW5vMDRkMTUxM2A2YjF7MyU7Og==","img120":"Fab120\/fab_7127653.png"},{"id":"275547","link":"\/jp.php?v2=NnYxbzJlNWw-azk9bz9mYzRhYz0_OzM5NyBhMzY8ZBg4T2RAZHQxdjdtOmQyPGVuYGVkYGQ1YiY0dDAxNzNhNDYyMWIyZjV_Pig5OA==","img120":"Fab120\/fab_81337736.jpg"}]}',
columnsNumber: 6	};
</script>

</div>

<section class="top">
<div class="float_lang_base_1 links">
<div class="title">Investing.com</div>
<ul class="first inlineblock">
<li><a title="Что нового" href="https://ru.investing.com/about-us/whats-new"  >Что нового</a></li>
<li><a title="Моб. приложение" href="https://ru.investing.com/mobile/"  >Моб. приложение</a></li>
<li><a title="Инвестпортфель" href="/portfolio/"  >Инвестпортфель</a></li>
<li><a title="Информеры" href="/webmaster-tools/"  >Информеры</a></li>
</ul>
<ul class="inlineblock">
<li><a title="О нас" href="/about-us/"  >О нас</a></li>
<li><a title="Реклама" href="https://ru.investing.com/about-us/advertise"  >Реклама</a></li>
<li><a title="Партнёрская программа" href="/affiliates"  >Партнёрская программа</a></li>
<li><a title="Свяжитесь с нами" href="/about-us/contact-us" target='_blank' >Свяжитесь с нами</a></li>
</ul>
<div class="clear"></div>
</div>
<div class="float_lang_base_1 mobile">
<img src="https://i-invdn-com.akamaized.net/footer/footerMobile2018.png" alt="" class="mobileCells">
<div class="title">Мобильное приложение</div>
<a href="https://play.google.com/store/apps/details?id=com.fusionmedia.investing&hl=ru-RU&referrer=utm_source%3Dru_footer%26utm_medium%3Dru_footer%26utm_term%3Dru_footer%26utm_content%3Dru_footer%26utm_campaign%3Dru_footer" target="_blank"><img src="https://i-invdn-com.akamaized.net/footer/img4/googleplay_ru.png" alt="Скачать"></a><a href="https://itunes.apple.com/ru/app/financial-markets-stocks-forex/id909998122" target="_blank"><img src="https://i-invdn-com.akamaized.net/footer/img4/appstore_ru.png" alt="App store"></a>        </div>
<div class="float_lang_base_1 social">
<div class="title">Мы в соцсетях</div>
<a target="_blank" href="https://vk.com/investingcom" title='vkontakte' class="vKontakt"></a><a target="_blank" href="https://www.facebook.com/pages/Investingcom-Россия/137592209761994" title='Facebook' class="facebook"></a><a target="_blank" href="https://twitter.com/investingru" title='Twitter' class="twitter"></a>        </div>
<div class="clear"></div>
</section>
<section class="bottom">
<img src="https://i-invdn-com.akamaized.net/logos/footerLogo2.png" alt="Investing.com">
<ul class="float_lang_base_2 clearfix">
<li><a href="/about-us/terms-and-conditions">Правила использования</a></li>
<li><a href="/about-us/privacy-policy">Политика конфиденциальности</a></li>
<li><a href="/about-us/risk-warning">Предупреждение</a></li>
</ul>
<div>&copy; 2007-2018 Fusion Media Limited. Все права зарегистрированы. 18+        </div>
<div class="disclaimer">
<span class="bold">Предупреждение о риске:</span> <b>Fusion Media</b> не несет никакой ответственности за утрату ваших денег в результате того, что вы положились на информацию, содержащуюся на этом сайте, включая данные, котировки, графики и сигналы форекс. Учитывайте высочайший уровень риска, связанный с инвестированием в финансовые рынки. Операции на международном валютном рынке Форекс содержат в себе высокий уровень риска и подходят не всем инвесторам. Торговля или инвестиции в криптовалюты связаны с потенциальными рисками. Цены на криптовалюты чрезвычайно волатильны и могут изменяться под действием разнообразных финансовых новостей, законодательных решений или политических событий. Торговля криптовалютами подходит не всем инвесторам. Прежде чем начать торговать на международной бирже или любом другом финансовом инструменте, включая криптовалюты, вы должны правильно оценить цели инвестирования, уровень своей экспертизы и допустимый уровень риска. Спекулируйте только теми деньгами, которые Вы можете позволить себе потерять.<br>
<b>Fusion Media</b> напоминает вам, что данные, предоставленные на данном сайте, не обязательно даны в режиме реального времени и могут не являться точными. Все цены на акции, индексы, фьючерсы и криптовалюты носят ориентировочный характер и на них нельзя полагаться при торговле. Таким образом, Fusion Media не несет никакой ответственности за любые убытки, которые вы можете понести в результате использования этих данных. </br>
<b>Fusion Media</b> может получать компенсацию от рекламодателей, упоминаемых на страницах издания, основываясь на вашем взаимодействии с рекламой или рекламодателями.<br/>
Версия этого документа на английском языке является определяющей и имеет преимущественную силу в том случае, если возникают разночтения между версиями на английском и русском языках.        </div>
</section>
<div class="ruSocialBtns">
<span>
<script id="top100Counter" type="text/javascript" src="http://counter.rambler.ru/top100.jcn?2907049"></script><noscript><a href="http://top100.rambler.ru/navi/2907049/"><img src="http://counter.rambler.ru/top100.cnt?2907049" alt="Rambler's Top100" border="0" /></a></noscript>			</span>
<span>
<script type="text/javascript">//<![CDATA[
var _tmr = _tmr || [];
_tmr.push({id: "2339218", type: "pageView", start: (new Date()).getTime()});
(function (d, w) {
var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true;
ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }})
(document, window); //]]></script><noscript><div style="position:absolute;left:-10000px;">
<img src="http://top-fwz1.mail.ru/counter?id=2339218;js=na" style="border:0;" height="1" width="1" alt="Рейтинг@Mail.ru" />
</div></noscript>

<!-- Rating@Mail.ru logo -->
<a href="http://top.mail.ru/jump?from=2339218" target=_blank>
<img src="http://top-fwz1.mail.ru/counter?id=2339218;t=395;l=1"
style="border:0;" height="31" width="88" alt="Рейтинг@Mail.ru" /></a>			</span>
</div>
</footer>

<script type="text/javascript" src="https://i-invdn-com.akamaized.net/js/technical-1.0.07.min.js"></script>
<script type="text/javascript" src="https://i-invdn-com.akamaized.net/js/n/search-bar-2.0-1.16.min.js"></script>
<script type="text/javascript" src="https://i-invdn-com.akamaized.net/js/core-highcharts-1.02.min.js"></script>
<script type="text/javascript" src="https://i-invdn-com.akamaized.net/js/jscharts-8.1.12.min.js"></script>

<script type="text/javascript" src="https://i-invdn-com.akamaized.net/js/ads.js"></script>
<script type="text/javascript" src="https://static.doubleclick.net/instream/ad_status.js"></script>

<script type="text/javascript">

if(!Modernizr.csstransitions && ie !== 7) {
// Bind MouseOver effect to Navigation list nodes
$('.navMenuUL > li').bind('mouseenter', function() {
clearTimeout($(this).data('hideTimeout'));
var someelement = this;
var timeoutId = setTimeout(function(){ $('.subTopMenuTwoColumnContainer', someelement).show();$('.arrow', someelement).show();$('.arrow2', someelement).show(); }, 120);
$(someelement).data('showTimeout', timeoutId);
// Bind MouseOut effect to Navigation list nodes
}).bind('mouseleave', function() {
clearTimeout($(this).data('showTimeout'));
var someelement = this;
var timeoutId = setTimeout(function(){ $('.subTopMenuTwoColumnContainer', someelement).hide();$('.arrow', someelement).hide();$('.arrow2', someelement).hide(); }, 120);
$(someelement).data('hideTimeout', timeoutId);
});

// Bind MouseOver effect to Real Time Quotes list nodes
$('#navRealTime').bind('mouseenter', function() {
clearTimeout($(this).data('hideTimeout'));
var someelement = this;
var timeoutId = setTimeout(function(){ $('.SubmenuNav', someelement).show(); }, 120);
$(someelement).data('showTimeout', timeoutId);
// Bind MouseOut effect to Navigation list nodes
}).bind('mouseleave', function() {
clearTimeout($(this).data('showTimeout'));
var someelement = this;
var timeoutId = setTimeout(function(){ $('.SubmenuNav', someelement).hide(); }, 120);
$(someelement).data('hideTimeout', timeoutId);
});

// Bind MouseOver effect to Real Time Quotes list nodes
$('.navRealTimeSubmenu .row').bind('mouseenter', function() {

clearTimeout($(this).data('hideTimeout'));
var someelement = this;
var timeoutId = setTimeout(function(){ $('.navRealTimeSubMenuContainer', someelement).show(); }, 120);
$(someelement).data('showTimeout', timeoutId);
// Bind MouseOut effect to Navigation list nodes
}).bind('mouseleave', function() {
clearTimeout($(this).data('showTimeout'));
var someelement = this;
var timeoutId = setTimeout(function(){ $('.navRealTimeSubMenuContainer', someelement).hide(); }, 120);
$(someelement).data('hideTimeout', timeoutId);
});
}
$('#navRealTime').bind('click', function() {
clearTimeout($(this).data('hideTimeout'));
var someelement = this;
var timeoutId = setTimeout(function(){ $('.SubmenuNav', someelement).show(); }, 120);
$(someelement).data('showTimeout', timeoutId);
});

var _gaq = _gaq || [];
//new
ga('create', 'UA-2555300-10', 'investing.com');
ga('create', 'UA-2555300-55', 'auto', {'name': 'allSitesTracker'});  // ALL sites tracker.

ga('require', 'displayfeatures');
ga('allSitesTracker.set', 'dimension17', getCookie('_ga').substr(6));
ga('set','&uid', 203902335);
ga('set', 'dimension1', 203902335);
ga('send', 'pageview');
ga('allSitesTracker.set','&uid', 203902335);
<!-- Crypto Dimension LD -->
<!--Adblock Check-->

jQuery(document).ready(checkAds());

function checkAds(){

function isIE() {
var ua = window.navigator.userAgent;
var trident = ua.indexOf('Trident/');
var edge = ua.indexOf('Edge/');
var msie = ua.indexOf('MSIE ');
return Math.max(msie,edge,trident)>0;

}

var isIE = isIE();
var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor) && !isIE;
var isFireFox = /Firefox/.test(navigator.userAgent);
var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
var isOneOfThoseBrowsers = [isIE,isChrome,isFireFox,isSafari].indexOf(true)>-1;

var b = 2;

//var hasAdsenseBlock = !!$('#adsense').length;
//var hasShadowDom = (!!$("html")[0].shadowRoot);
//var headerIsShown = ($(".midHeader").height() > 0 && $(".midHeader").css('display') !== 'none');


if (window.google_ad_status == 1){

var adbBlk = getCookie('adbBLk');
if(isOneOfThoseBrowsers && adbBlk>0){
ga('allSitesTracker.set', 'dimension22', '7_Blk_No');
setCookie('adbBLk',7,-1000);
}  

}else{

b = 1;
$('#TopTextAd').css('display','flex');

if(isOneOfThoseBrowsers) {
setCookie('adbBLk',7,1000,'/');
ga('allSitesTracker.set', 'dimension21', '7_Blk_Yes');

}
}

$.ajax({
url: "/jp.php?b="+b+"&k=Y29lPm4wZW1kNG5mZj05O2I7MGwyZmBiZTZnMWFgYWkyMWUxZ2FmZmVnazAyaWE8PjZjYD9tOmxgZ2VhZDIzN2N8ZSxuf2VsZGBuYGYxOTliLTA9MmpgIGUxZyxhJmEpMmtlcGd_ZjBlbWtq&d=1&i=1538374538&e=86131153&lip=web242.forexpros.com&ti=1538374538",
type: 'get',
cache: false,
type: 'GET',
dataType: 'html',
success: function (data) {
console.log('server jp b1 web242.forexpros.com');
}
});

}


ga('allSitesTracker.send', 'pageview');
<!--End Adblock Check-->


</script>


<!-- Yandex.Metrika counter -->
<script type="text/javascript">
var yaParams = {/*Visit parameters here*/};
</script>

<script type="text/javascript">
(function (d, w, c) {
(w[c] = w[c] || []).push(function() {
try {
w.yaCounter23341048 = new Ya.Metrika({id:23341048,
webvisor:true,
clickmap:true,
trackLinks:true,
accurateTrackBounce:true,params:window.yaParams||{ }});
} catch(e) { }
});

var n = d.getElementsByTagName("script")[0],
s = d.createElement("script"),
f = function () { n.parentNode.insertBefore(s, n); };
s.type = "text/javascript";
s.async = true;
s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

if (w.opera == "[object Opera]") {
d.addEventListener("DOMContentLoaded", f, false);
} else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<div><img src="//mc.yandex.ru/watch/23341048" style="position:absolute; left:-9999px;" alt="" /></div>
<!-- /Yandex.Metrika counter -->




<script>
//disabled is a temporary argument for testing purposes 
var BoxesEvents = function($,ga,disabled)
{
var e_stack = []; //each event is pushed to this stack
//this objcet links urls to event types
var boxes = {"side_central_banks":"centralbanks","webmaster_tools":"wmt","Most_Popular_News":"mostpop","broker_promotions":"sb_promotions","marketMoversBoxWrapper":"sb_marketmovers","QBS_2_inner":"sb_indices","QBS_3_inner":"sb_commodities","QBS_1_inner":"sb_forex","QBS_4_inner":"sb_bonds","TSB_1_inner":"tsb_fx","TSB_2_inner":"tsb_commodities","TSB_3_inner":"tsb_indices","TSB_4_inner":"tsb_stocks"};

var resume_click_event = function(ev)
{
if (ev['link']==='') {return;}
window.location.href = ev['link'];
}


var register_events = function()
{
for (var id in boxes)
{
(function(id){
var _$b = $('#'+id);
if (_$b.length < 1) {return;}

//box click event
_$b.mouseup(function(e){
if (e.button === 2) {return true;} //right mouse button
var middleMouse = e.button === 1;
if (!middleMouse) {e.preventDefault();}
var box_event = boxes[id];
e_stack.push({'event':box_event,'link':''});
var ce = null; //current event 
//function creator
var callbackCreate = function(data) {return function() {resume_click_event(data);}}
var hitCallback = null;

while(ce = e_stack.pop())
{
if (ce['link'].substr(0,10) === "javascript") {continue;}//if the link is javascript call ignore it
hitCallback = middleMouse ? function(){} : callbackCreate(ce);
//if a link with no event do nothing
if (ce['event'] == '') {hitCallback(); return true;}

window.ga('t0.send',{
'hitType': 'event',
'eventCategory': 'side-box-events',
'eventAction': ce['event'],
'eventValue' : 1,
'hitCallback': hitCallback //resume click event
}
);
//fallback if ga doesn't work
window.setTimeout(hitCallback,1500);
}
return middleMouse;
});

//link event 
_$b.on('mouseup','a',function(e){
if (e.button === 2) {return true;} //right mouse button
e.preventDefault();
var url = this.getAttribute('href');
var event = this.getAttribute('data-gae');
e_stack.push({'event':'','link':url});
if (event)
{
e_stack.pop();
e_stack.push({'event':event,'link':url});
} 
});

})(id);
}		   	
};

var init = function()
{
if (disabled) {return;}
if (typeof $ !== 'function') {return;}
if (typeof ga !== 'function') {return;}
register_events();
}();
}(window.jQuery,window.ga,false);
</script>



</div><!-- /wrapper -->

<script type='text/javascript'>
if(!window.googletag) {
window.googletag = {cmd: []};
}
googletag.cmd.push(function() {googletag.defineSlot('/6938/FP_RU_site/FP_RU_Sideblock_1_Default', [300, 600], 'div-gpt-ad-1370187203350-sb1').setTargeting('ad_group',Adomik.randomAdGroup()).addService(googletag.pubads());googletag.pubads().setTargeting("Section","technical");googletag.pubads().setTargeting("Charts_Page","no");googletag.pubads().setTargeting("CID",getCookie('_ga').substr(6));googletag.pubads().setTargeting("ad_h",(new Date).getUTCHours());googletag.pubads().setTargeting("Page_URL","/technical/candlestick-patterns");googletag.pubads().setTargeting("Traffic_Type","OG");googletag.pubads().setTargeting("User_type","Logged_in");       if(window.outerWidth >= 1630) {
googletag.cmd.push(function() {
googletag.defineSlot('/6938/FP_RU_site/FP_RU_Takeover_Default', [[300, 600], [120, 600]], 'div-gpt-ad-1478019486943-0').setTargeting('ad_group', Adomik.randomAdGroup()).addService(googletag.pubads());
});
}
if(window.outerWidth >= 1340 && window.outerWidth < 1630) {
googletag.cmd.push(function() {
googletag.defineSlot('/6938/FP_RU_site/FP_RU_Takeover_Default', [[160, 600], [120, 600]], 'div-gpt-ad-1478019486943-0').setTargeting('ad_group', Adomik.randomAdGroup()).addService(googletag.pubads());
});
}

googletag.enableServices();
});    if (sponsoredArticle) {
googletag.cmd.push(function() { googletag.display(sponsoredArticle); });
}
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1370187203350-sb1'); });</script><script type='text/javascript'>
$(document).ready(function () {
googletag.cmd.push(function() {
googletag.enableServices();
});
window.loadSideBlockAd3 = function () { var sb3 = googletag.defineSlot('/6938/FP_RU_site/FP_RU_Sideblock_3_viewable',  [[300, 250],[300, 600]], 'div-gpt-ad-1370187203350-sb3').setTargeting('ad_group',Adomik.randomAdGroup()).addService(googletag.pubads());
googletag.cmd.push(function() {
if(typeof(refreshSlot)=="function"){ refreshSlot(sb3); } else{ googletag.display('div-gpt-ad-1370187203350-sb3'); }
}); }});
if(window.outerWidth >= 1340) {
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1478019486943-0'); });
}
</script><script type='text/javascript'>

if (typeof(checkBigAd) === 'function') {
checkBigAd();
}
if (typeof(addSakindo) === 'function') {
addSakindo();
}

(function () {
$(window).scroll($.throttle(function () {
if (window.loadSideBlockAd3 && $.isInView('#ad3', true, 20)) {
if (typeof(googletag.defineSlot) === 'function') {
window.loadSideBlockAd3();
window.loadSideBlockAd3 = null;
}
}
}, 16))
})()
</script>



</body>

<script>var TimeZoneID = +"18";window.timezoneOffset = +"10800";window.stream = "https://stream32.forexpros.com:443";</script><script>window.uid = 203902335</script><script>

$(function(){
$(window).trigger("socketRetry",[["pid-eu-8833:","pid-eu-8849:","pid-eu-8830:","pid-eu-8836:","pid-eu-8910:","pid-eu-8883:","pid-eu-8862:","pid-eu-49768:","pid-eu-1:","pid-eu-1691:","pid-eu-2186:","pid-eu-2:","pid-eu-3:","pid-eu-9:","pid-eu-5:","pidTechSumm-1:","pidTechSumm-1691:","pidTechSumm-2186:","pidTechSumm-2:","pidTechSumm-3:","pidTechSumm-9:","pidTechSumm-5:","isOpenPair-8833:","isOpenPair-8849:","isOpenPair-8830:","isOpenPair-8836:","isOpenPair-8910:","isOpenPair-8883:","isOpenPair-8862:","isOpenPair-49768:","domain-7:"]]);
});
</script><script>function refresher() { void (0);}</script><script>
var _comscore = _comscore || [];
_comscore.push(
{ c1: "2", c2: "16896267" }

);
(function()
{ var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true; s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js"; el.parentNode.insertBefore(s, el); }

)();
</script>		<script type="text/javascript">
var google_conversion_id = 1000940071;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
</script><script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
<noscript><div style="display:inline;"><img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1000940071/?value=0&amp;guid=ON&amp;script=0"/></div></noscript>

<!-- tns-counter.ru -->
<script type="text/javascript">
(function(win, doc, cb){
(win[cb] = win[cb] || []).push(function() {
try {
tnsCounterRuinvesting_com = new TNS.TnsCounter({
'account':'ruinvesting_com',
'tmsec': 'ruinvesting_total'
    				});
} catch(e){}
});

var tnsscript = doc.createElement('script');
tnsscript.type = 'text/javascript';
tnsscript.async = true;
tnsscript.src = ('https:' == doc.location.protocol ? 'https:' : 'http:') +
'//www.tns-counter.ru/tcounter.js';
var s = doc.getElementsByTagName('script')[0];
s.parentNode.insertBefore(tnsscript, s);
})(window, this.document,'tnscounter_callback');
</script>

<script type="text/javascript">
$(document).ready(function () {
window._qevents = window._qevents || [];

(function() {
var elem = document.createElement('script');
elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
elem.async = true;
elem.type = "text/javascript";
var scpt = document.getElementsByTagName('script')[0];
scpt.parentNode.insertBefore(elem, scpt);
}());

window._qevents.push({
qacct:"p-WSPjmcyyuDvN0"
});
});
</script>
<noscript>
<div style="display:none;">
<img src="//pixel.quantserve.com/pixel/p-WSPjmcyyuDvN0.gif" border="0" height="1" width="1" alt="Quantcast"/>
</div>
</noscript>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function()
{n.callMethod? n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '751110881643258');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=751110881643258&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

<script type="text/javascript">
fbq = window.fbq || $.noop;
</script>

</html>

2018/10/01 12:15:38.568 [E]  http: read on closed response body
2018/10/01 12:15:39.219 [D]  <!DOCTYPE HTML>
<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" xmlns:schema="http://schema.org/" class="ru" lang="ru" geo="KZ">
<head>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
</script>

<link rel="dns-prefetch" href="https://i-invdn-com.akamaized.net" />
<link rel="dns-prefetch" href="https://a-invdn-com.akamaized.net" />

<title>Список котировок для теханализа - Investing.com</title>

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<link rel="shortcut icon" href="https://i-invdn-com.akamaized.net/logos/favicon.ico">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta http-equiv="content-language" content="ru">
<meta name="description" content="Создайте собственный список котировок для технического анализа с помощью функции «Мой теханализ»">
<meta http-equiv="pragma" content="no-cache">
<meta name="wmail-verification" content="1e519b53c38953db">
<meta name="twitter:url" content="https://ru.investing.com/technical/personalized-quotes-technical-summary"/>
<meta name="twitter:site" content="@investingru">
<meta name="twitter:title" content="Список котировок для теханализа - Investing.com">
<meta name="twitter:description" content="Создайте собственный список котировок для технического анализа с помощью функции «Мой теханализ»">
<meta name="twitter:card" content="summary">
<meta property="og:title" content="Список котировок для теханализа - Investing.com"/>
<meta property="og:description" content="Создайте собственный список котировок для технического анализа с помощью функции «Мой теханализ»"/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="https://ru.investing.com/technical/personalized-quotes-technical-summary"/>
<meta property="og:site_name" content="investing.com Россия"/>
<meta property="og:locale" content="ru_RU"/>
<meta property="og:image" content="https://i-invdn-com.akamaized.net/investing_300X300.png"/>
<meta name="google-site-verification" content="DTRxWXB3vjNUsTCPICWo9yzZmIllylXYkRevEXo7szg">




<!-- css -->
<link rel="stylesheet" href="https://i-invdn-com.akamaized.net/css/video-js_v3.css" type="text/css">
<link rel="stylesheet" href="https://i-invdn-com.akamaized.net/css/mainOldMin_v3b.css" type="text/css">
<link rel="stylesheet" href="https://i-invdn-com.akamaized.net/css/newMainCssMin_v57g.css" type="text/css">


<link href="https://i-invdn-com.akamaized.net/css/printContent_v10.css" media="print" rel="stylesheet" type="text/css" />


<!-- css -->

<!-- js -->
<script src="https://i-invdn-com.akamaized.net/js/jquery-6.4.9.04.min.js"></script>
<script type="text/javascript">
var domainId = "7";
window.CDN_URL = "https://i-invdn-com.akamaized.net";
</script>
<script>
(function() {
var po = document.createElement('script');
po.type = 'text/javascript';
po.async = true;
po.src = 'https://apis.google.com/js/client:plusone.js?onload=loadAfterGApiReady';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(po, s);
})();

</script>

<script type="text/javascript" src="https://i-invdn-com.akamaized.net/js/n/utils-0.16.min.js"></script>
<script type="text/javascript" src="https://i-invdn-com.akamaized.net/js/main-1.17.229.min.js"></script>
<!-- Forces IE6,IE7 to understand PSEUDO CLASSES :after and :before (add before: " "; after: " "; to CSS) -->
<!--[if (gte IE 6)&(lte IE 7)]>
<script src="https://i-invdn-com.akamaized.net/js/jquery.pseudo-1.1.min.js"></script>
<![endif]-->
<!--Global variables for JavaScript-->
<script type="text/javascript">
window.login_url = '/members-admin/login';
window.close_word = 'Закрыть';
window.lightbox_image = 'Картинка';
window.lightbox_from = 'из';
var FP = {
global  : {
_defaultDomain  : 'ru.investing.com',
_textAlign      : 'left'
        }
};
var userType;
var userSupportedType ;
</script>
<!--Gloval variables for JavaScript-->
<!-- js -->


<script type="text/javascript" src="http://vk.com/js/api/openapi.js?34"></script>
<script type="text/javascript"> VK.init({apiId: 3564591 , onlyWidgets: true});</script><script type="text/javascript">
/* Modernizr 2.0.6 (Custom Build) | MIT & BSD
     * Build: http://www.modernizr.com/download/#-csstransitions-iepp-cssclasses-testprop-testallprops-domprefixes
     */
window.Modernizr=function(a,b,c){function A(a,b){var c=a.charAt(0).toUpperCase()+a.substr(1),d=(a+" "+n.join(c+" ")+c).split(" ");return z(d,b)}function z(a,b){for(var d in a)if(k[a[d]]!==c)return b=="pfx"?a[d]:!0;return!1}function y(a,b){return!!~(""+a).indexOf(b)}function x(a,b){return typeof a===b}function w(a,b){return v(prefixes.join(a+";")+(b||""))}function v(a){k.cssText=a}var d="2.0.6",e={},f=!0,g=b.documentElement,h=b.head||b.getElementsByTagName("head")[0],i="modernizr",j=b.createElement(i),k=j.style,l,m=Object.prototype.toString,n="Webkit Moz O ms Khtml".split(" "),o={},p={},q={},r=[],s,t={}.hasOwnProperty,u;!x(t,c)&&!x(t.call,c)?u=function(a,b){return t.call(a,b)}:u=function(a,b){return b in a&&x(a.constructor.prototype[b],c)},o.csstransitions=function(){return A("transitionProperty")};for(var B in o)u(o,B)&&(s=B.toLowerCase(),e[s]=o[B](),r.push((e[s]?"":"no-")+s));v(""),j=l=null,a.attachEvent&&function(){var a=b.createElement("div");a.innerHTML="<elem></elem>";return a.childNodes.length!==1}()&&function(a,b){function s(a){var b=-1;while(++b<g)a.createElement(f[b])}a.iepp=a.iepp||{};var d=a.iepp,e=d.html5elements||"abbr|article|aside|audio|canvas|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",f=e.split("|"),g=f.length,h=new RegExp("(^|\\s)("+e+")","gi"),i=new RegExp("<(/*)("+e+")","gi"),j=/^\s*[\{\}]\s*$/,k=new RegExp("(^|[^\\n]*?\\s)("+e+")([^\\n]*)({[\\n\\w\\W]*?})","gi"),l=b.createDocumentFragment(),m=b.documentElement,n=m.firstChild,o=b.createElement("body"),p=b.createElement("style"),q=/print|all/,r;d.getCSS=function(a,b){if(a+""===c)return"";var e=-1,f=a.length,g,h=[];while(++e<f){g=a[e];if(g.disabled)continue;b=g.media||b,q.test(b)&&h.push(d.getCSS(g.imports,b),g.cssText),b="all"}return h.join("")},d.parseCSS=function(a){var b=[],c;while((c=k.exec(a))!=null)b.push(((j.exec(c[1])?"\n":c[1])+c[2]+c[3]).replace(h,"$1.iepp_$2")+c[4]);return b.join("\n")},d.writeHTML=function(){var a=-1;r=r||b.body;while(++a<g){var c=b.getElementsByTagName(f[a]),d=c.length,e=-1;while(++e<d)c[e].className.indexOf("iepp_")<0&&(c[e].className+=" iepp_"+f[a])}l.appendChild(r),m.appendChild(o),o.className=r.className,o.id=r.id,o.innerHTML=r.innerHTML.replace(i,"<$1font")},d._beforePrint=function(){p.styleSheet.cssText=d.parseCSS(d.getCSS(b.styleSheets,"all")),d.writeHTML()},d.restoreHTML=function(){o.innerHTML="",m.removeChild(o),m.appendChild(r)},d._afterPrint=function(){d.restoreHTML(),p.styleSheet.cssText=""},s(b),s(l);d.disablePP||(n.insertBefore(p,n.firstChild),p.media="print",p.className="iepp-printshim",a.attachEvent("onbeforeprint",d._beforePrint),a.attachEvent("onafterprint",d._afterPrint))}(a,b),e._version=d,e._domPrefixes=n,e.testProp=function(a){return z([a])},e.testAllProps=A,g.className=g.className.replace(/\bno-js\b/,"")+(f?" js "+r.join(" "):"");return e}(this,this.document);
window.ie = (function(){var undef,v = 3,div = document.createElement('div'),all = div.getElementsByTagName('i');while (div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',all[0]);return v > 4 ? v : undef;}());
</script>

<!-- TEMP -->

<style>
.googleContentRec {display:inline-block;width:650px;height:300px;}

</style>

<style>
/* INPUTS OVERWRITES */
.combineSearchBox {border:1px solid #737373;} /* overwrite main css */
.combineSearchBox.newInput.inputTextBox, .portfolioSearch .newInput.inputTextBox  {padding:0;}
</style>

<!--[if (IE 8)|(IE 9)]>
<link rel="stylesheet" type="text/css" href="https://i-invdn-com.akamaized.net/css/ie8-9main_v2b.css">
<![endif]-->
<!--[if IE 8]>
<link rel="stylesheet" type="text/css" href="https://i-invdn-com.akamaized.net/css/ie8main_v2a.css">
<![endif]-->
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="https://i-invdn-com.akamaized.net/css/ie7main_v1i.css">
<![endif]-->


<style>
.MainTableDirectory .HeadTitlesRow TH {text-align:center; font-weight:bold;}
</style>

<style>
.openTbl TH.icon {text-align:center;}     .openTbl TH.icon SPAN {position:relative; top:2px;}
</style>
<script>
var oldIE = false, explorerEight = false ,explorerNine = false, sponsoredArticle;
</script>
<!--[if lte IE 7]>
<script>
oldIE = true;
</script>
<![endif]-->
<!--[if (IE 8)]>
<script>
explorerEight = true;
</script>
<![endif]-->
<!--[if (IE 9)]>
<script>
explorerNine = true;
</script>
<![endif]-->
<script type='text/javascript'>
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
var gads = document.createElement('script');
gads.async = true;
gads.type = 'text/javascript';
var useSSL = 'https:' == document.location.protocol;
gads.src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
var node = document.getElementsByTagName('script')[0];
node.parentNode.insertBefore(gads, node);
})();
</script>
<script>var f3a85cc4b4be9e620977f3d0987c0f4="aedcd1d1f93a1f1a1549ebac0ca91400";</script><script src="https://cdn.optimizely.com/js/4229603524.js"></script><script>window.newsAndAnalysisAlertSettings = JSON.parse('null');</script>
</head>
<body class="takeover dfpTakeovers" >
<div id="sideNotificationZone" class="sideNotificationZone">
<div class="floatingAlertWrapper alertWrapper js-notification-item displayNone">
<div class="alertNotifIcon"><span class="js-icon"></span></div>
<div class="alertNotifData">
<a class="alertDataTitle js-title"></a>
<span class="alertDataDetails js-small-text"></span>
</div>
<a data-tooltip="Настройка уведомлений" class="js-manage-alerts gearIconSmall genToolTip oneliner reverseToolTip"></a>
<span class="alertCloseIconSmall js-close"></span>
</div>
</div>

<div class="breakingNews">
<div class="floatingAlertWrapper">
<span class="breakingNewsTitle bold">Главные новости</span>
<span class="breakingNewsText js-breaking-news-content"></span>
</div>
<span class="closeIcon js-close"></span>
</div>

<div class="generalOverlay js-general-overlay displayNone"></div>

<script>
$('.js-general-overlay').on('click', $.stopProp);
</script>

<div class="earAdv left">
<div id='div-gpt-ad-1478019486943-0'>
</div>
</div>


<div class="wrapper">
<header>
<!-- topBar -->
<div class="topBar">
<div class="topBarInnerWrapper">
<a href="/" class="topBarLogo">
<img src="https://i-invdn-com.akamaized.net/logos/investing-com-logo.png" alt="Investing.com - Ведущий Финансовый Портал" class="investingLogo">
</a>




<div id="topBarPopup" load="topBarSpinner"></div>

<div class="js-search-overlay topSearchOverlay displayNone"></div>
<div class="searchDiv newSearchDiv js-main-search-wrapper">
<div class="searchBoxContainer topBarSearch topBarInputSelected" >
<input autocomplete='off' type="text" class="searchText arial_12 lightgrayFont js-main-search-bar" value="" placeholder="Поиск на сайте...">
<label class="searchGlassIcon js-magnifying-glass-icon">&nbsp;</label>
<i class="cssSpinner"></i>
</div>

<div class="js-results-dialog displayNone">
<div class="js-query-results newSearch_topBar newSearch_topBarResults displayNone">
<div class="searchMain">
<div class="js-query-quotes-header textBox groupHeader searchPopup_results">
<a class="js-quote-header-link">Котировки</a>
<div class="js-quote-types-dropdown newSearchSelect selectWrap">
<a class="newBtnDropdown noHover">
<span class="js-dropdown-text-input inputDropDown">Все типы инструментов</span>
<i class="bottunImageDoubleArrow buttonWhiteImageDownArrow"></i>
</a>

<ul class="dropdownBtnList displayNone js-quote-types-dropdown-ul">
<li data-value='all'>Все типы инструментов</li><li data-value='indice'>Индексы</li><li data-value='equities'>Акции</li><li data-value='etf'>ETF</li><li data-value='fund'>Фонды</li><li data-value='commodity'>Товары</li><li data-value='currency'>Валюты</li><li data-value='crypto'>Крипто</li><li data-value='bond'>Облигации</li>			            </ul>
</div>
</div>

<div class="js-scrollable-results-wrapper newResultsContainer">
<div class="js-table-results tableWrapper"></div>
</div>

<!-- NO RESULTS -->
<div class="js-query-no-results noResults displayNone">
<i class="searchNoResultsNew"></i>
<p class="lighterGrayFont">Ваш поиск не дал результатов</p>
</div>

<div class="searchResultsFooter">
<i class="blueSearchGlassIcon middle"></i>
<a class="js-footer-link" href="/">Найти на сайте:&nbsp;<span class="js-footer-link-text bold"></span></a>
</div>
</div>
<div class="js-right-side-results searchAside"></div>
</div>

<div class="js-default-search-results newSearch_topBar">
<div class="searchMain js-main-group-results"></div>
<div class="searchAside">
<div class="searchAside_item">

<a class="asideTitle" href="/news/most-popular-news">
<div class="js-search-ga-header groupHeader" ga-label="Popular News">Популярные новости</div>
<span class="js-search-ga-more" ga-label="Popular News - More">Еще</span>
</a>
<div class="js-search-ga-items articles mediumTitle1" ga-label="Popular News - Article"><article class="articleItem  ">
<a href="/news/economy-news/article-576305" class="img" ><img src="https://i-invdn-com.akamaized.net/news/indicatornews_2_150x108_S_1416306967.jpg" alt="Усманов хочет помочь своему другу - владельцу "Эвертона"" onerror="javascript:this.src='https://i-invdn-com.akamaized.net/news/news-paper_108x81.png';" ></a>	<div class="textDiv">
<a href="/news/economy-news/article-576305" title="Усманов хочет помочь своему другу - владельцу "Эвертона"" class="title" >Усманов хочет помочь своему другу - владельцу "Эвертона"</a>
</div>
<div class="clear"></div>
</article><article class="articleItem  ">
<a href="/news/economy-news/article-576297" class="img" ><img src="https://i-invdn-com.akamaized.net/news/LYNXMPED951YE_S.jpg" alt="Илон Маск заплатит штраф и выйдет из совета директоров Tesla" onerror="javascript:this.src='https://i-invdn-com.akamaized.net/news/news-paper_108x81.png';" ></a>	<div class="textDiv">
<a href="/news/economy-news/article-576297" title="Илон Маск заплатит штраф и выйдет из совета директоров Tesla" class="title" >Илон Маск заплатит штраф и выйдет из совета директоров Tesla</a>
</div>
<div class="clear"></div>
</article><article class="articleItem  ">
<a href="/news/economy-news/article-576303" class="img" ><img src="https://i-invdn-com.akamaized.net/news/indicatornews_5_150x108_S_1416303181.jpg" alt="Воробьев: Подмосковью "нужны новые рабочие места на новых заводах", а "лишнее жилье не нужно"" onerror="javascript:this.src='https://i-invdn-com.akamaized.net/news/news-paper_108x81.png';" ></a>	<div class="textDiv">
<a href="/news/economy-news/article-576303" title="Воробьев: Подмосковью "нужны новые рабочие места на новых заводах", а "лишнее жилье не нужно"" class="title" >Воробьев: Подмосковью "нужны новые рабочие места на новых заводах", а "лишнее жилье не нужно"</a>
</div>
<div class="clear"></div>
</article></div>
</div>
<div class="searchAside_item js-popular-analysis-wrapper">

<a class="asideTitle" href="/analysis/most-popular-analysis">
<div class="js-search-ga-header groupHeader" ga-label="Popular Analysis">Популярная аналитика</div>
<span class="js-search-ga-more" ga-label="Popular Analysis - More">Еще</span>
</a>
<div class="js-search-ga-items articles smallTitle1 analysisImg js-popular-analysis-items" ga-label="Popular Analysis - Article"><article class="articleItem  ">
<a href="/analysis/article-200241863" class="img" ><img src="https://d1-invdn-com.akamaized.net/company_logo/8a041e87308c804e01b487506cdf1197.jpg"  onerror="javascript:this.src='https://i-invdn-com.akamaized.net/anonym.gif';" ></a>	<div class="textDiv">
<a href="/analysis/article-200241863" title="Акции JD.com потеряли 40%, и это неспроста" class="title" >Акции JD.com потеряли 40%, и это неспроста</a>
</div>
<div class="clear"></div>
</article><article class="articleItem  ">
<a href="/analysis/article-200241809" class="img" ><img src="https://d7-invdn-com.akamaized.net/company_logo/200244477_1441640812.png"  onerror="javascript:this.src='https://i-invdn-com.akamaized.net/anonym.gif';" ></a>	<div class="textDiv">
<a href="/analysis/article-200241809" title="Долларовые "быки" вернули себе контроль над рынком" class="title" >Долларовые "быки" вернули себе контроль над рынком</a>
</div>
<div class="clear"></div>
</article><article class="articleItem  ">
<a href="/analysis/article-200241890" class="img" ><img src="https://d7-invdn-com.akamaized.net/company_logo/43fb9bbc0f27aba60f7dd6bb210541aa.jpg"  onerror="javascript:this.src='https://i-invdn-com.akamaized.net/anonym.gif';" ></a>	<div class="textDiv">
<a href="/analysis/article-200241890" title="Прогноз форекс на неделю: 01.10.2018 - 05.10.2018" class="title" >Прогноз форекс на неделю: 01.10.2018 - 05.10.2018 <span class="middle videoIconNew">&nbsp;</span> </a>
</div>
<div class="clear"></div>
</article></div>
</div>
</div>

</div>



</div>

<!--Templates-->

<!--
Group Templates
-->
<!--Quote group template-->
<div class="js-group-template textBox displayNone">
<div class="js-group-title groupHeader"></div>
<div class="js-group-results newResultsContainer">
</div>
</div>

<!--News items group template-->
<div class="js-news-items-group-template searchAside_item displayNone">
<a class="asideTitle js-group-more-link">
<div class="js-group-title groupHeader"></div>
<span>Еще</span>
</a>
<div class="js-group-results articles smallTitle1 analysisImg"></div>
</div>

<!--
Row Templates
-->
<!--Quote row template-->
<a class="row js-quote-row-template js-quote-item displayNone">
<span class="first flag"><i class="ceFlags middle js-quote-item-flag"></i></span>
<span class="second js-quote-item-symbol symbolName"></span>
<span class="third js-quote-item-name"></span>
<span class="fourth typeExchange js-quote-item-type"></span>
</a>

<!--News row template-->
<article class="js-news-item-template articleItem displayNone">
<a href="/" class="js-news-item-link">
<img class="js-news-item-img" src="https://d1-invdn-com.akamaized.net/company_logo/d01b563261bcf223986f7ac222680343.jpg">
<div class="js-news-item-name textDiv"></div>
</a>
</article>

<div class="js-tool-item-template displayNone">
<a class="js-tool-item-link eventsAndTools">
<span class="js-tool-item-name"></span>
</a>
</div>
</div>

<script type="text/javascript">
window.topBarSearchData = {
texts: {
recentSearchText: 'История поиска',
popularSearchText: 'Популярные запросы',
newsSearchText: 'Новости',
analysisSearchText: 'Аналитика',
economicEventsSearchText: 'Экономические события',
toolsSearchText: 'Инструменты и разделы',
authorsSearchText: 'Авторы',
webinarsSearchText: 'Вебинары'
		},
popularSearches: [{"pairId":"13994","link":"\/equities\/tesla-motors","symbol":"TSLA","name":"Tesla Inc","flag":"USA","type":"\u0410\u043a\u0446\u0438\u0438 - NASDAQ","ci":"5"},{"pairId":"8833","link":"\/commodities\/brent-oil","symbol":"LCO","name":"\u0424\u044c\u044e\u0447\u0435\u0440\u0441 \u043d\u0430 \u043d\u0435\u0444\u0442\u044c Brent","flag":"UK","type":"\u0422\u043e\u0432\u0430\u0440 - ICE","ci":"4"},{"pairId":"2186","link":"\/currencies\/usd-rub","symbol":"USD\/RUB","name":"\u0414\u043e\u043b\u043b\u0430\u0440 \u0421\u0428\u0410 \u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u0438\u0439 \u0440\u0443\u0431\u043b\u044c","flag":"Russian_Federation","type":"\u0412\u0430\u043b\u044e\u0442\u0430","ci":"56"},{"pairId":"13711","link":"\/equities\/sberbank_rts","symbol":"SBER","name":"\u0421\u0431\u0435\u0440\u0431\u0430\u043d\u043a \u041f\u0410\u041e","flag":"Russian_Federation","type":"\u0410\u043a\u0446\u0438\u0438 - \u041c\u043e\u0441\u043a\u0432\u0430","ci":"56"},{"pairId":"6374","link":"\/equities\/research-in-motion-ltd","symbol":"BB","name":"BlackBerry Ltd","flag":"USA","type":"\u0410\u043a\u0446\u0438\u0438 - \u041d\u044c\u044e-\u0419\u043e\u0440\u043a","ci":"5"}]	};
</script>
<div class="topBarTools">
<span id="userAccount" class="topBarUserAvatar js-open-auth-trigger-inside" data-page-type="topBar">
<div class="loggedIn inlineblock"><span onmouseover="_myAccountDropDown.render();" onmouseout="_myAccountDropDown.render();" class="topBarText bold pointer name"><img src="https://i-invdn-com.akamaized.net/defaultUserMaleTmp.png" class="userAvatar middle inlineblock" id="userImg" ><span class="myAccount topBarText">Владимир</span></span></div><div id="myAccountHeaderPop" class="tooltipPopup bigArrowTopbar noHeader displayNone myAccountSelPopup" onmouseover="_myAccountDropDown.render();" onmouseout="_myAccountDropDown.render();"><div class="content"><ul class="bold "><li style="width:px;"><span class="smallGrayArrow middle"></span><a href="/portfolio" class="arial_12 bold">Инвестпортфель</a></li><li style="width:px;"><span class="smallGrayArrow middle"></span><a href="/members-admin/alert-center" class="arial_12 bold">Мои уведомления</a></li><li style="width:px;"><span class="smallGrayArrow middle"></span><a href="/members-admin/alerts-feed" class="arial_12 bold">Лента уведомлений</a></li><li style="width:px;"><span class="smallGrayArrow middle"></span><a href="/members-admin/saved-items" class="arial_12 bold">Закладки</a></li><li style="width:px;"><span class="smallGrayArrow middle"></span><a href="/members-admin/content-analysis-list" class="arial_12 bold">Публикации</a></li><li style="width:px;"><span class="smallGrayArrow middle"></span><a href="/members-admin/content-analysis-add" class="arial_12 bold">Добавить статью</a></li></ul><div class="dottedBorder">&nbsp;</div><ul class="noBold "><li style="width:px;"><span class="smallGrayEmptyArrow middle"></span><a href="/members-admin/sentiments" class="arial_12 bold">Мой прогноз</a></li><li style="width:px;"><span class="smallGrayEmptyArrow middle"></span><a href="/members-admin/trading-accounts-list" class="arial_12 bold">Счета</a></li></ul><div class="dottedBorder">&nbsp;</div><ul class="noBold "><li style="width:px;"><span class="smallGrayEmptyArrow"></span><a href="/members-admin/ad-free" class="arial_12 bold">Версия без рекламы</a></li><li style="width:px;"><span class="smallGrayEmptyArrow"></span><a href="/members-admin/config-edit-user-details" class="arial_12 bold">Настройки</a></li><li style="width:px;"><span class="smallGrayEmptyArrow"></span><a href="/members-admin/logout?logoutToken=1730017812725d05ae84b31b283edd6d" class="arial_12 bold">Выйти</a></li></ul></div><!-- /subMenuOneColumn --></div><!-- /myAccountHeaderPop --><div id="settingsDropDown" class="subMenuOneColumnWrap hoverboxWithShadow settingsPopup" onmouseover="settingsDropDown.render();" onmouseout="settingsDropDown.render();"><div class="subMenuOneColumn"><ul class="bold "></ul><div class="dottedBorder">&nbsp;</div><ul class="noBold "></ul></div><!-- /subMenuOneColumn --><span class="arrow">&nbsp;</span></div><!-- /myAccountHeaderPop -->			</span>
<span id="topBarAlertCenterBtn" class="topBarIconWrap">
<i class="topBarAlertsIcon"></i><i class="topBarAlertBadge js-badge arial_10 displayNone">0</i></span>
<span id="portfolioTopBarBtn" class="topBarIconWrap">
<i class="topBarPortfolioIcon"></i></span>
<span id="topBarMarketBtn" class="topBarIconWrap">
<i class="topBarWorldMarketsIcon"></i></span>

<div class="langSelect inlineblock">
<div id="langSelect" class="inlineblock pointer" onmouseover="$('#editionContainer').fadeOut('fast'); flagsDropDown.render();" onmouseout="flagsDropDown.render();"><span class="ceFlags Russian_Federation middle inlineblock"></span></div>
<div id="TopFlagsContainer" class="tooltipPopup countrySelPopup bigArrowTopbar noHeader displayNone" onmouseover="flagsDropDown.render();" onmouseout="flagsDropDown.render();">
<div class="content">
<ul><li><a href="https://www.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to www.investing.com'); setTimeout('document.location = \'https://www.investing.com\'', 500);return false;"><span class="ceFlags USA"></span>English (USA)</a></li><li><a href="https://tr.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to tr.investing.com'); setTimeout('document.location = \'https://tr.investing.com\'', 500);return false;"><span class="ceFlags Turkey"></span>Türkçe</a></li><li><a href="https://uk.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to uk.investing.com'); setTimeout('document.location = \'https://uk.investing.com\'', 500);return false;"><span class="ceFlags UK"></span>English (UK)</a></li><li><a href="https://sa.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to sa.investing.com'); setTimeout('document.location = \'https://sa.investing.com\'', 500);return false;"><span class="ceFlags Saudi_Arabia"></span>‏العربية‏</a></li><li><a href="https://in.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to in.investing.com'); setTimeout('document.location = \'https://in.investing.com\'', 500);return false;"><span class="ceFlags India"></span>English (India)</a></li><li><a href="https://gr.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to gr.investing.com'); setTimeout('document.location = \'https://gr.investing.com\'', 500);return false;"><span class="ceFlags Greece"></span>Ελληνικά</a></li><li><a href="https://ca.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to ca.investing.com'); setTimeout('document.location = \'https://ca.investing.com\'', 500);return false;"><span class="ceFlags Canada"></span>English (Canada)</a></li><li><a href="https://se.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to se.investing.com'); setTimeout('document.location = \'https://se.investing.com\'', 500);return false;"><span class="ceFlags Sweden"></span>Svenska</a></li><li><a href="https://au.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to au.investing.com'); setTimeout('document.location = \'https://au.investing.com\'', 500);return false;"><span class="ceFlags Australia"></span>English (Australia)</a></li><li><a href="https://fi.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to fi.investing.com'); setTimeout('document.location = \'https://fi.investing.com\'', 500);return false;"><span class="ceFlags Finland"></span>Suomi</a></li><li><a href="https://za.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to za.investing.com'); setTimeout('document.location = \'https://za.investing.com\'', 500);return false;"><span class="ceFlags South_Africa"></span>English (South Africa)</a></li><li><a href="https://il.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to il.investing.com'); setTimeout('document.location = \'https://il.investing.com\'', 500);return false;"><span class="ceFlags Israel"></span>עברית</a></li><li><a href="https://de.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to de.investing.com'); setTimeout('document.location = \'https://de.investing.com\'', 500);return false;"><span class="ceFlags Germany"></span>Deutsch</a></li><li><a href="https://jp.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to jp.investing.com'); setTimeout('document.location = \'https://jp.investing.com\'', 500);return false;"><span class="ceFlags Japan"></span>日本語</a></li><li><a href="https://es.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to es.investing.com'); setTimeout('document.location = \'https://es.investing.com\'', 500);return false;"><span class="ceFlags Spain"></span>Español (España)</a></li><li><a href="https://kr.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to kr.investing.com'); setTimeout('document.location = \'https://kr.investing.com\'', 500);return false;"><span class="ceFlags South_Korea"></span>한국어</a></li><li><a href="https://mx.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to mx.investing.com'); setTimeout('document.location = \'https://mx.investing.com\'', 500);return false;"><span class="ceFlags Mexico"></span>Español (México)</a></li><li><a href="https://cn.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to cn.investing.com'); setTimeout('document.location = \'https://cn.investing.com\'', 500);return false;"><span class="ceFlags China"></span>中文</a></li><li><a href="https://fr.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to fr.investing.com'); setTimeout('document.location = \'https://fr.investing.com\'', 500);return false;"><span class="ceFlags France"></span>Français</a></li><li><a href="https://hk.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to hk.investing.com'); setTimeout('document.location = \'https://hk.investing.com\'', 500);return false;"><span class="ceFlags Hong_Kong"></span>香港</a></li><li><a href="https://it.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to it.investing.com'); setTimeout('document.location = \'https://it.investing.com\'', 500);return false;"><span class="ceFlags Italy"></span>Italiano</a></li><li><a href="https://id.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to id.investing.com'); setTimeout('document.location = \'https://id.investing.com\'', 500);return false;"><span class="ceFlags Indonesia"></span>Bahasa Indonesia</a></li><li><a href="https://nl.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to nl.investing.com'); setTimeout('document.location = \'https://nl.investing.com\'', 500);return false;"><span class="ceFlags Netherlands"></span>Nederlands</a></li><li><a href="https://ms.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to ms.investing.com'); setTimeout('document.location = \'https://ms.investing.com\'', 500);return false;"><span class="ceFlags Malaysia"></span>Bahasa Melayu</a></li><li><a href="https://pt.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to pt.investing.com'); setTimeout('document.location = \'https://pt.investing.com\'', 500);return false;"><span class="ceFlags Portugal"></span>Português (Portugal)</a></li><li><a href="https://th.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to th.investing.com'); setTimeout('document.location = \'https://th.investing.com\'', 500);return false;"><span class="ceFlags Thailand"></span>ไทย</a></li><li><a href="https://pl.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to pl.investing.com'); setTimeout('document.location = \'https://pl.investing.com\'', 500);return false;"><span class="ceFlags Poland"></span>Polski</a></li><li><a href="https://vn.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to vn.investing.com'); setTimeout('document.location = \'https://vn.investing.com\'', 500);return false;"><span class="ceFlags Vietnam"></span>Tiếng Việt</a></li><li><a href="https://br.investing.com" onclick="ga('allSitesTracker.send', 'event', 'Edition Redirect Popup', 'Users Choice', 'Redirect to br.investing.com'); setTimeout('document.location = \'https://br.investing.com\'', 500);return false;"><span class="ceFlags Brazil"></span>Português (Brasil)</a></li></ul>					</div>
</div>
</div>
<!-- GEO POPUP -->
<!-- /GEO POPUP -->
</div>
</div>
<!-- /topBarInnerWrapper -->
</div><!-- /topBar -->

<script type="text/javascript">


loader([{
type: 'component', value: 'Translate'
	}]).ready(function(Translate) {
Translate
.setDictionary('TopBarAlertsSignInPopup', {
'popupHeader': 'Последние уведомления',
'notLoggedInText': "\u0417\u0430\u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0438\u0440\u0443\u0439\u0442\u0435\u0441\u044c, \u0447\u0442\u043e\u0431\u044b \u0441\u043e\u0437\u0434\u0430\u0432\u0430\u0442\u044c \u0443\u0432\u0435\u0434\u043e\u043c\u043b\u0435\u043d\u0438\u044f \u043f\u043e \u0438\u043d\u0441\u0442\u0440\u0443\u043c\u0435\u043d\u0442\u0430\u043c, \n\u044d\u043a\u043e\u043d\u043e\u043c\u0438\u0447\u0435\u0441\u043a\u0438\u043c \u0441\u043e\u0431\u044b\u0442\u0438\u044f\u043c \u0438 \u0430\u043d\u0430\u043b\u0438\u0442\u0438\u043a\u0435.",
'iconClasses': 'signinIcon',
'popupClasses': 'topBarAlertsSignInPopup'
			})
.setDictionary('TopBarPortfolioSignInPopup', {
'popupHeader': 'Портфель',
'notLoggedInText': "\u0417\u0430\u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0438\u0440\u0443\u0439\u0442\u0435\u0441\u044c, \u0447\u0442\u043e\u0431\u044b \u0441\u043e\u0437\u0434\u0430\u0442\u044c \u0441\u043e\u0431\u0441\u0442\u0432\u0435\u043d\u043d\u044b\u0439 \u0438\u043d\u0432\u0435\u0441\u0442\u0438\u0446\u0438\u043e\u043d\u043d\u044b\u0439 \u043f\u043e\u0440\u0442\u0444\u0435\u043b\u044c \u0438 \u0441\u043f\u0438\u0441\u043e\u043a \u043d\u0430\u0431\u043b\u044e\u0434\u0435\u043d\u0438\u044f",
'iconClasses': 'signinIconPortfolio',
'popupClasses': 'topBarPortfolioBox'
			});
})
</script>
<div id="navBar" class="navBar">
<nav id="navMenu" class="navMenuWrapper">
<ul class="navMenuUL">
<li>
<a href="/markets/" class="nav">Котировки</a>
<ul class="subMenuNav">
<li class="row"><!--firstRow-->
<a href="/currencies/">Форекс</a>
<div class="navBarDropDown"  >
<ul class="main"  >

<li><a href="/currencies/single-currency-crosses">Кросс-курсы валют</a></li>

<li><a href="/currencies/streaming-forex-rates-majors">Основные валюты</a></li>

<li><a href="/currencies/live-currency-cross-rates">Валютные кроссы</a></li>

<li><a href="/currencies/exchange-rates-table">Таблица кросс-курсов</a></li>

<li><a href="/quotes/us-dollar-index">Индекс USD</a></li>

<li><a href="/currencies/fx-futures">Фьючерсы Форекс</a></li>

<li><a href="/currencies/forex-options">Форекс-опционы</a></li>
</ul>
<ul class="popular"  >
<li><a href="/currencies/eur-usd">EUR/USD</a></li>
<li><a href="/currencies/gbp-usd">GBP/USD</a></li>
<li><a href="/currencies/usd-rub">USD/RUB</a></li>
<li><a href="/currencies/eur-rub">EUR/RUB</a></li>
<li><a href="/currencies/usd-jpy">USD/JPY</a></li>
<li><a href="/currencies/usd-chf">USD/CHF</a></li>
<li><a href="/crypto/bitcoin/btc-usd">BTC/USD</a></li>
</ul>
</div>        </li>
<li class="row"><!--firstRow-->
<a href="/commodities/">Товары</a>
<div class="navBarDropDown"  >
<ul class="main"  >

<li><a href="/commodities/real-time-futures">Биржевые товары</a></li>

<li><a href="/commodities/metals">Металлы</a></li>

<li><a href="/commodities/softs">Товары softs</a></li>

<li><a href="/commodities/meats">Мясо</a></li>

<li><a href="/commodities/energies">Энергоносители</a></li>

<li><a href="/commodities/grains">Зерно</a></li>

<li><a href="/indices/commodities-indices">Сырьевые индексы</a></li>
</ul>
<ul class="popular"  >
<li><a href="/commodities/gold">Золото</a></li>
<li><a href="/commodities/brent-oil">Нефть Brent</a></li>
<li><a href="/commodities/crude-oil">Нефть WTI</a></li>
<li><a href="/commodities/silver">Серебро</a></li>
<li><a href="/commodities/natural-gas">Природный газ</a></li>
<li><a href="/commodities/copper">Медь</a></li>
</ul>
</div>        </li>
<li class="row"><!--firstRow-->
<a href="/indices/">Индексы</a>
<div class="navBarDropDown"  >
<ul class="main"  >

<li><a href="/indices/russia-indices">Индексы РФ</a></li>

<li><a href="/indices/major-indices">Основные индексы</a></li>

<li><a href="/indices/world-indices">Мировые индексы</a></li>

<li><a href="/indices/global-indices">Глобальные индексы</a></li>

<li><a href="/indices/indices-futures">Фьючерсы</a></li>

<li><a href="/indices/indices-cfds">CFD на индексы</a></li>
</ul>
<ul class="popular"  >
<li><a href="/indices/rtsi">РТС</a></li>
<li><a href="/indices/mcx">Индекс МосБиржи</a></li>
<li><a href="/indices/us-30">Dow 30</a></li>
<li><a href="/indices/us-spx-500">S&P 500</a></li>
<li><a href="/indices/germany-30">DAX</a></li>
<li><a href="/indices/nasdaq-composite">Nasdaq</a></li>
</ul>
</div>        </li>
<li class="row"><!--firstRow-->
<a href="/equities/">Акции</a>
<div class="navBarDropDown"  >
<ul class="main"  >

<li><a href="/stock-screener/">Фильтр акций</a></li>

<li><a href="/equities/trending-stocks">Трендовые акции</a></li>

<li><a href="/equities/russia">Россия</a></li>

<li><a href="/equities/pre-market">Премаркет США</a></li>

<li><a href="/equities/europe">Европа</a></li>

<li><a href="/equities/americas">Америка</a></li>

<li><a href="/equities/52-week-high">52-нед. максимум</a></li>

<li><a href="/equities/52-week-low">52-нед. минимум</a></li>

<li><a href="/equities/most-active-stocks">Активные</a></li>

<li><a href="/equities/top-stock-gainers">Лидеры роста</a></li>

<li><a href="/equities/top-stock-losers">Лидеры падения</a></li>

<li><a href="/equities/world-adrs">Мировые АДР</a></li>

<li><a href="/equities/russia-adrs">Россия - АДР</a></li>
</ul>
<ul class="popular"  >
<li><a href="/equities/sberbank_rts">Сбербанк</a></li>
<li><a href="/equities/severstal_rts">Северсталь</a></li>
<li><a href="/equities/gazprom_rts">Газпром</a></li>
<li><a href="/equities/rosneft_rts">Роснефть</a></li>
<li><a href="/equities/lukoil_rts">ЛУКОЙЛ</a></li>
<li><a href="/equities/rostelecom">Ростелеком</a></li>
</ul>
</div>        </li>
<li class="row"><!--firstRow-->
<a href="/etfs/">ETF</a>
<div class="navBarDropDown"  >
<ul class="main"  >

<li><a href="/etfs/major-etfs">Основные ETF</a></li>

<li><a href="/etfs/world-etfs">Мировые ETF</a></li>

<li><a href="/etfs/usa-etfs">США - ETF</a></li>
</ul>
<ul class="popular"  >
<li><a href="/etfs/spdr-s-p-500">SPDR S&P 500</a></li>
<li><a href="/etfs/ishares-msci-emg-markets">iShares MSCI Emerging Markets</a></li>
<li><a href="/etfs/spdr-gold-trust">SPDR Gold Shares</a></li>
<li><a href="/etfs/powershares-qqqq">Invesco QQQ Trust Series 1</a></li>
</ul>
</div>        </li>
<li class="row"><!--firstRow-->
<a href="/funds/">Фонды</a>
<div class="navBarDropDown"  >
<ul class="main"  >

<li><a href="/funds/world-funds">Мировые инвестфонды</a></li>

<li><a href="/funds/major-funds">Основные инвестфонды</a></li>
</ul>
<ul class="popular"  >
<li><a href="/funds/vanguard-total-stock-mkt-idx-instl">Vanguard Total Stock Mkt Idx Instl Sel</a></li>
<li><a href="/funds/pimco-commodity-real-ret-strat-c">PIMCO Commodity Real Ret Strat C</a></li>
<li><a href="/funds/vanguard-total-bond-market-index-ad">Vanguard Total Bond Market Index Adm</a></li>
<li><a href="/funds/vanguard-500-index-inv">Vanguard 500 Index Investor</a></li>
</ul>
</div>        </li>
<li class="row"><!--firstRow-->
<a href="/rates-bonds/">Облигации</a>
<div class="navBarDropDown"  >
<ul class="main"  >

<li><a href="/rates-bonds/financial-futures">Финансовые фьючерсы</a></li>

<li><a href="/rates-bonds/world-government-bonds">Гособлигации</a></li>

<li><a href="/rates-bonds/government-bond-spreads">Спреды доходности</a></li>

<li><a href="/rates-bonds/forward-rates">Форвардные курсы</a></li>

<li><a href="/indices/bond-indices">Облигационные индексы</a></li>
</ul>
<ul class="popular"  >
<li><a href="/rates-bonds/us-2-yr-t-note">US 2 YR T-Note</a></li>
<li><a href="/rates-bonds/us-10-yr-t-note">US 10 YR T-Note</a></li>
<li><a href="/rates-bonds/us-30-yr-t-bond">US 30Y T-Bond</a></li>
<li><a href="/rates-bonds/uk-gilt">UK Gilt</a></li>
<li><a href="/rates-bonds/euro-bund">Euro Bund</a></li>
<li><a href="/rates-bonds/euro-schatz">Euro SCHATZ</a></li>
</ul>
</div>        </li>
<li class="row"><!--firstRow-->
<a href="/crypto/">Криптовалюты</a>
<div class="navBarDropDown"  >
<ul class="main"  >

<li><a href="/crypto/currency-pairs">Пары с криптовалютами</a></li>

<li><a href="/crypto/currencies">Все криптовалюты</a></li>

<li><a href="/crypto/bitcoin">Bitcoin</a></li>

<li><a href="/crypto/ethereum">Ethereum</a></li>

<li><a href="/currency-converter/?tag=Cryptocurrency">Конвертер валют</a></li>
</ul>
<ul class="popular"  >
<li><a href="/crypto/bitcoin/btc-usd">BTC/USD</a></li>
<li><a href="/crypto/ethereum/eth-usd">ETH/USD</a></li>
<li><a href="/crypto/litecoin/ltc-usd">LTC/USD</a></li>
<li><a href="/crypto/ethereum-classic/etc-usd">ETC/USD</a></li>
<li><a href="/crypto/ethereum/eth-btc">ETH/BTC</a></li>
</ul>
</div>        </li>
</ul>            </li>
<li >
<a class="nav" href="//ru.investing.com/crypto/">Крипто</a>
<div class="navBarDropDown" style="width: 387px" >
<ul class="main" style="width: 200px" >

<li class="navTitle">Криптовалюты</li>

<li><a href="/crypto/currencies">Все криптовалюты</a></li>

<li><a href="/crypto/currency-pairs">Пары с криптовалютами</a></li>

<li><a href="/crypto/ico-calendar">Календарь ICO</a></li>

<li><a href="/brokers/cryptocurrency-brokers">Брокеры криптовалют</a></li>

<li><a href="/crypto/bitcoin">Bitcoin</a></li>

<li><a href="/crypto/ethereum">Ethereum</a></li>

<li><a href="/crypto/ripple">Ripple</a></li>

<li><a href="/crypto/litecoin">Litecoin</a></li>

<li><a href="/currency-converter/?tag=Cryptocurrency">Конвертер валют</a></li>
</ul>
<ul class="popular" style="width: 187px" >
<li class="navTitle">Другие криптовалюты</li>
<li><a href="/crypto/bitcoin/btc-usd">BTC/USD</a></li>
<li><a href="/crypto/ethereum/eth-usd">ETH/USD</a></li>
<li><a href="/crypto/litecoin/ltc-usd">LTC/USD</a></li>
<li><a href="/crypto/ethereum-classic/etc-usd">ETC/USD</a></li>
<li><a href="/crypto/ethereum/eth-btc">ETH/BTC</a></li>
<li><a href="/crypto/iota/iota-usd">IOTA/USD</a></li>
<li><a href="/crypto/ripple/xrp-usd">XRP/USD</a></li>
<li><a href="/crypto/bitcoin/bitcoin-futures">Фьючерс CME на Bitcoin</a></li>
<li><a href="/crypto/bitcoin/cboe-bitcoin-futures">Фьючерс CBOE на Bitcoin</a></li>
</ul>
</div>                </li>
<li >
<a class="nav" href="//ru.investing.com/news/">Новости</a>
<div class="navBarDropDown" style="width: 367px" >
<ul class="main" style="width: 180px" >

<li class="navTitle">Новости рынка</li>

<li><a href="/news/forex-news">Форекс</a></li>

<li><a href="/news/commodities-news">Сырьевые товары</a></li>

<li><a href="/news/stock-market-news">Фондовый рынок</a></li>

<li><a href="/news/economic-indicators">Экономпоказатели</a></li>

<li><a href="/news/economy-news">Экономика</a></li>

<li><a href="/news/cryptocurrency-news">Криптовалюты</a></li>
</ul>
<ul class="popular" style="width: 187px" >
<li class="navTitle">Дополнительно</li>
<li><a href="/news/most-popular-news">Самое популярное</a></li>
<li><a href="/news/politics-news">Политика</a></li>
<li><a href="/economic-calendar/">Экономический календарь</a></li>
</ul>
</div>                </li>
<li >
<a class="nav" href="//ru.investing.com/analysis/">Аналитика</a>
<div class="navBarDropDown" style="width: 346px" >
<ul class="main" style="width: 159px" >

<li class="navTitle">Мнения</li>

<li><a href="/analysis/market-overview">Обзор рынка</a></li>

<li><a href="/analysis/forex">Форекс</a></li>

<li><a href="/analysis/stock-markets">Рынки акций</a></li>

<li><a href="/analysis/commodities">Сырьевые товары</a></li>

<li><a href="/analysis/bonds">Облигации</a></li>

<li><a href="/analysis/cryptocurrency">Криптовалюты</a></li>
</ul>
<ul class="popular" style="width: 187px" >
<li class="navTitle">Дополнительно</li>
<li><a href="/analysis/most-popular-analysis">Самое популярное</a></li>
<li><a href="/analysis/editors-picks">Выбор редакции</a></li>
<li><a href="/analysis/comics">Карикатуры</a></li>
</ul>
</div>                </li>
<li class="selected">
<a class="nav" href="//ru.investing.com/technical/">Теханализ</a>
<div class="navBarDropDown" style="width: 439px" >
<ul class="main" style="width: 219px" >

<li class="navTitle">Инструменты</li>

<li><a href="/technical/personalized-quotes-technical-summary">Технический обзор</a></li>

<li><a href="/technical/technical-analysis">Технический анализ</a></li>

<li><a href="/technical/pivot-points">Точки разворота</a></li>

<li><a href="/technical/moving-averages">Скользящие средние</a></li>

<li><a href="/technical/indicators">Индикаторы</a></li>

<li><a href="/technical/candlestick-patterns">Свечные модели</a></li>
</ul>
<ul class="popular" style="width: 220px" >
<li class="navTitle">Дополнительно</li>
<li><a href="/technical/candlestick-patterns">Свечные модели</a></li>
<li><a href="/tools/fibonacci-calculator">Калькулятор Фибоначчи</a></li>
<li><a href="/tools/pivot-point-calculator">Калькулятор точки разворота</a></li>
</ul>
</div>                </li>
<li >
<a class="nav" href="//ru.investing.com/charts/">Графики</a>
<div class="navBarDropDown" style="width: 430px" >
<ul class="main" style="width: 210px" >

<li class="navTitle">Графики в реальном времени</li>

<li><a href="/charts/live-charts">Интерактивные графики</a></li>

<li><a href="/charts/forex-charts">Форекс</a></li>

<li><a href="/charts/futures-charts">Фьючерсы</a></li>

<li><a href="/charts/stocks-charts">Акции</a></li>

<li><a href="/charts/indices-charts">Индексы</a></li>

<li><a href="/charts/cryptocurrency-charts">Графики криптовалют</a></li>
</ul>
<ul class="popular" style="width: 220px" >
<li><a href="/charts/real-time-forex-charts">Интерактивный график Форекс</a></li>
<li><a href="/charts/real-time-futures-charts">График фьючерсов</a></li>
<li><a href="/charts/real-time-indices-charts">Интерактивный график индексов</a></li>
<li><a href="/charts/real-time-stocks-charts">Интерактивный график акций</a></li>
<li><a href="/charts/multiple-forex-streaming-charts">Потоковые графики Форекс</a></li>
<li><a href="/charts/multiple-indices-streaming-charts">Потоковые графики индексов</a></li>
</ul>
</div>                </li>
<li >
<a class="nav" href="//ru.investing.com/brokers/">Брокеры</a>
<div class="navBarDropDown" style="width: 360px" >
<ul class="main" style="width: 180px" >

<li class="navTitle">Брокеры</li>

<li><a href="/brokers/forex-brokers">Форекс</a></li>

<li><a href="/brokers/cryptocurrency-brokers">Брокеры криптовалют</a></li>

<li><a href="/brokers/cfd-brokers">CFD</a></li>

<li><a href="/brokers/stock-brokers">Акции</a></li>

<li><a href="/brokers/promotions">Промоакции</a></li>

<li><a href="/brokers/compare-spreads-eur-usd">Сравнить спреды</a></li>

<li><a href="/brokers/compare-quotes-eur-usd">Сравнить котировки</a></li>
</ul>
<ul class="popular" style="width: 180px" >
<li class="navTitle">Дополнительно</li>
<li><a href="/brokers/forex-demo-accounts">Демо-счета Форекс</a></li>
<li><a href="/brokers/forex-live-accounts">Счета Forex Live</a></li>
<li><a href="/brokers/press-releases">Пресс-релизы</a></li>
<li><a href="/brokers/interviews">Интервью</a></li>
<li><a href="/brokers/options-brokers">Опционы</a></li>
<li><a href="/brokers/spread-betting-brokers">Спред-беттинг</a></li>
<li><a href="/brokers/regulation">Регуляторы</a></li>
</ul>
</div>                </li>
<li >
<a class="nav" href="//ru.investing.com/tools/">Инструменты</a>
<div class="navBarDropDown" style="width: 360px" >
<ul class="main" style="width: 180px" >

<li class="navTitle">Календари</li>

<li><a href="/economic-calendar/">Экономический календарь</a></li>

<li><a href="/holiday-calendar/">Календарь праздников</a></li>

<li><a href="/earnings-calendar/">Календарь отчетности</a></li>

<li><a href="/dividends-calendar/">Календарь дивидендов</a></li>

<li><a href="/stock-split-calendar/">Календарь сплитов</a></li>

<li><a href="/ipo-calendar/">Календарь IPO</a></li>

<li><a href="/futures-expiration-calendar/">Календарь экспирации фьючерсов</a></li>

<li class="navTitle">Инструменты</li>

<li><a href="/stock-screener/">Фильтр акций</a></li>

<li><a href="/central-banks/fed-rate-monitor">Прогноз по ставке ФРС</a></li>

<li><a href="/currency-converter/">Конвертер валют</a></li>

<li><a href="/tools/fibonacci-calculator">Калькулятор Фибоначчи</a></li>
</ul>
<ul class="popular" style="width: 180px" >
<li class="navTitle">Дополнительно</li>
<li><a href="/tools/correlation-calculator">Корреляция Форекс</a></li>
<li><a href="/tools/pivot-point-calculator">Калькулятор точки разворота</a></li>
<li><a href="/tools/profit-calculator">Калькулятор доходности</a></li>
<li><a href="/tools/margin-calculator">Калькулятор маржи</a></li>
<li><a href="/tools/currency-heatmap">Тепловая карта валют</a></li>
<li><a href="/tools/forex-volatility-calculator">Калькулятор волатильности</a></li>
<li><a href="/tools/forward-rates-calculator">Калькулятор форвардных курсов</a></li>
<li><a href="/tools/mortgage-calculator">Ипотечный калькулятор</a></li>
</ul>
</div>                </li>
<li >
<a class="nav" href="//ru.investing.com/portfolio/">Инвестпортфель</a>
</li>
<li class="last">
<a class="nav pointer" >Еще</a>
<div class="navBarDropDown" style="width: 327px" >
<ul class="main" style="width: 159px" >

<li><a href="/directory/traders/">Рейтинг торговых счетов</a></li>

<li><a href="/education/">Обучение</a></li>

<li><a href="/education/webinars">Вебинары</a></li>

<li><a href="/education/forex-trading-kit">Руководство трейдера</a></li>

<li><a href="/central-banks/">Банки</a></li>

<li><a href="/webmaster-tools/">Информеры</a></li>

<li><a href="/broker-blacklist/">Черный список</a></li>

<li><a href="/members-admin/ad-free">Версия без рекламы</a></li>
</ul>
<ul class="popular" style="width: 168px" >
<li class="navTitle">Программы</li>
<li><a href="/directory/Торговые-Платформы">Торговые платформы</a></li>
<li><a href="/directory/Приложения-для-Графиков">Приложения для графиков</a></li>
<li><a href="/directory/Сигналы-Системы">Сигналы/Системы</a></li>
</ul>
</div>                </li>
</ul>
</nav>

<!-- /grey sub menu -->
<nav id="subNav" class="subMenuWrapper">
<ul class="subNavUL">
<li class="selected" >
<a href="/technical/personalized-quotes-technical-summary" >                Технический обзор            </a>            </li>
<li  >
<a href="/technical/technical-analysis" >                Технический анализ            </a>            </li>
<li  >
<a href="/technical/pivot-points" >                Точки разворота            </a>            </li>
<li  >
<a href="/technical/moving-averages" >                Скользящие средние            </a>            </li>
<li  >
<a href="/technical/indicators" >                Индикаторы            </a>            </li>
<li class="last " >
<a href="/technical/candlestick-patterns" >                Свечные модели            </a>            </li>
</ul>
</nav>
</div>

</header>

<script type='text/javascript'>
window.Adomik = window.Adomik || {};
Adomik.randomAdGroup = function() {
var rand = Math.random();
switch (false) {
case !(rand < 0.09): return "ad_ex" + (Math.floor(100 * rand));
case !(rand < 0.10): return "ad_bc";
default: return "ad_opt";
}
};
</script>

<div class="midHeader">
<div id="sln-hbanner" class="wideTopBanner"><div id='div-gpt-ad-1441189641022-23' style='height:250px; display: none;'></div><div id='div-gpt-ad-1370187203350-head1' style='height:90px;'></div></div>
</div>


<div id="theBigX" class="largeBannerCloser" onClick="javascript:onBigX();"></div>

<script type='text/javascript'>

function onBigX() {
var slotLeaderboard;
$('#theBigX').fadeOut('fast');
$('.midHeader').animate({
height: '90px'
		},200 );
googletag.cmd.push(function() {
slotLeaderboard = googletag.defineSlot('/6938/FP_RU_site/FP_RU_Leaderboard_Default', [[728, 90], [970, 90]], 'div-gpt-ad-1370187203350-head1').setTargeting('ad_group', Adomik.randomAdGroup()).addService(googletag.pubads());
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1370187203350-head1'); });
});
$('#div-gpt-ad-1441189641022-23').hide();
$('#div-gpt-ad-1370187203350-head1').show();
}

function getMaxLeaderBoard() {
return (getCookie("geoC") || '').toLowerCase() === 'us' ? 5 : 3;
}

function checkBigAd() {
var counter = +getCookie("billboardCounter_7") || 0;
if( true && counter === 0 && true) {
$('.midHeader').height('250px');
$('#div-gpt-ad-1441189641022-23').show();
$('#div-gpt-ad-1370187203350-head1').hide();
$('#theBigX').show();
googletag.cmd.push(function() {
googletag.defineSlot('/6938/FP_RU_site/FP_RU_Billboard_Default',  [970, 250] , 'div-gpt-ad-1441189641022-23').setTargeting('ad_group', Adomik.randomAdGroup()).addService(googletag.pubads());
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1441189641022-23'); });
});
} else {
googletag.cmd.push(function() {
googletag.defineSlot('/6938/FP_RU_site/FP_RU_Leaderboard_Default', [[728, 90], [970, 90]], 'div-gpt-ad-1370187203350-head1').setTargeting('ad_group', Adomik.randomAdGroup()).addService(googletag.pubads());
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1370187203350-head1'); });
});
}

counter++;
if(counter === getMaxLeaderBoard()) {
counter = 0;
}
setCookie("billboardCounter_7", counter);





}

</script>



<!-- #2 columns template -->
<section id="leftColumn" >


<h1 class="float_lang_base_1 relativeAttr"
dir="ltr" >Список котировок для теханализа	</h1>



<div class="headBtnWrapper float_lang_base_2">
<a class="newBtn infoBox LightGray noText" onmouseover="_infoBox.render();" onmouseout="_infoBox.render();">&nbsp;
<div id="infoBoxToolTipWrap" class="tooltipPopup noTriangle">
<div id="infoBoxToolTip">
<span class="noBold arial_12">Технический анализ по собственному списку финансовых инструментов в режиме реального времени. Создайте собственную таблицу с помощью нашей интерактивной платформы, созданной специально для мониторинга интересующих вас финансовых инструментов. Чтобы создать список, пройдите в «Настройки» подраздела «Мой теханализ» и выберите интересующие вас инструменты и временные периоды. Вся информация будет обновляться автоматически в реальном времени.</span>
</div>
</div>
</a>
</div>
<div class="headBtnWrapper float_lang_base_2">
</div>






<div class="clear"></div>
<div class="innerHeaderSeperatorBottom"></div>
<!-- Page Content -->
<script type="text/javascript">
window.siteData = {
htmlDirection: 'ltr',
decimalPoint: ',' || '.',
thousandSep: '.' || ',',
isEu : true,
userLoggedIn: true,
userHasPhoneRegistered: false,
currencyPosition: 'right',
datepicker: {
applyButton: 'Применить',
format: 'd/m/Y',
formatShort: 'd/m/y',
formatLong: 'd/m/Y',
formatSend: 'yy-mm-dd',
firstDay: '1',
dayNames: ["Вс","Пн","Вт","Ср","Чт","Пт","Сб"],
monthNamesShort: ["Янв.", "Фев.", "Март", "Апр.", "Май", "Июнь", "Июль", "Авг.", "Сент.", "Окт.", "Нояб.", "Дек."],
monthNames: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
translations: {
custom: 'Выбрать даты',
start: 'Начало',
end: 'До окончания'
                        }
},
techTranslations: {"_Currencies_Strong_Buy":"\u0410\u043a\u0442\u0438\u0432\u043d\u043e \u043f\u043e\u043a\u0443\u043f\u0430\u0442\u044c","_Currencies_Strong_Sell":"\u0410\u043a\u0442\u0438\u0432\u043d\u043e \u043f\u0440\u043e\u0434\u0430\u0432\u0430\u0442\u044c","_specialBuy":"\u041f\u043e\u043a\u0443\u043f\u0430\u0442\u044c","_specialSell":"\u041f\u0440\u043e\u0434\u0430\u0432\u0430\u0442\u044c","_moving_avarge_tool_sell":"\u041f\u0440\u043e\u0434\u0430\u0432\u0430\u0442\u044c","_moving_avarge_tool_buy":"\u041f\u043e\u043a\u0443\u043f\u0430\u0442\u044c","_Currencies_Neutral":"\u041d\u0435\u0439\u0442\u0440\u0430\u043b\u044c\u043d\u043e","_not_available":"\u0414\u0430\u043d\u043d\u044b\u0435 \u043e\u0442\u0441\u0443\u0442\u0441\u0442\u0432\u0443\u044e\u0442","_Currencies_Less_Volatility":"\u041c\u0435\u043d\u0435\u0435 \u0432\u043e\u043b\u0430\u0442\u0438\u043b\u0435\u043d","_Currencies_More_Volatility":"\u0412\u044b\u0441\u043e\u043a\u0430\u044f \u0432\u043e\u043b\u0430\u0442-\u0442\u044c","_Currencies_Overbought":"\u041f\u0435\u0440\u0435\u043a\u0443\u043f\u043b\u0435\u043d\u043d\u043e\u0441\u0442\u044c","_Currencies_Oversold":"\u041f\u0435\u0440\u0435\u043f\u0440\u043e\u0434\u0430\u043d\u043d\u043e\u0441\u0442\u044c"},
edition: $('html').attr('lang')
};
window.siteData.blocked_users = [];

(function (pattern) {
window.siteData.elapsedTimePattern = pattern.is('false') ? null : $.extend(JSON.parse(pattern), { justNow: 'Сейчас' });
})('{"minutes":{"X \u043c\u0438\u043d\u0443\u0442\u0443 \u043d\u0430\u0437\u0430\u0434":[1,21,31,41,51],"X \u043c\u0438\u043d\u0443\u0442\u044b \u043d\u0430\u0437\u0430\u0434":["2-4","22-24","32-34","42-44","52-54"],"X \u043c\u0438\u043d\u0443\u0442 \u043d\u0430\u0437\u0430\u0434":["5-20","25-30","35-40","45-50","55-59"]},"hours":{"X \u0447\u0430\u0441 \u043d\u0430\u0437\u0430\u0434":[1,21],"X \u0447\u0430\u0441\u0430 \u043d\u0430\u0437\u0430\u0434":["2-4","22-24"],"X \u0447\u0430\u0441\u043e\u0432 \u043d\u0430\u0437\u0430\u0434":["5-20"]}}');

window.siteData.smlID = 10093;
window.siteData.mmID = 25;

window.truncatedCommentShowMoreDefine = 'Показать еще';
</script>
<ul class="arial_14 bold newBigTabs" id="pairSublinksLevel1">
<li>
<a href="/technical/technical-summary">Форекс</a>
<span class="selected newSiteIconsSprite smallTriangleArrowUp displayNone">&nbsp;</span>
<span class="newSiteIconsSprite smallTriangleDarkGrayArrowUp displayNone selected">&nbsp;</span>
</li>
<li>
<a href="/technical/indices-technical-summary">Индексы</a>
<span class="selected newSiteIconsSprite smallTriangleArrowUp displayNone">&nbsp;</span>
<span class="newSiteIconsSprite smallTriangleDarkGrayArrowUp displayNone selected">&nbsp;</span>
<div class="subMenuOneColumn displayNone">
<ul class="bold">
<li><a href="/technical/Индексы-России---технический-обзор" class="arial_12 bold">Россия</a></li>
</ul>
</div>
</li>
<li>
<a href="/technical/stocks-technical-summary">Акции</a>
<span class="selected newSiteIconsSprite smallTriangleArrowUp displayNone">&nbsp;</span>
<span class="newSiteIconsSprite smallTriangleDarkGrayArrowUp displayNone selected">&nbsp;</span>
<div class="subMenuOneColumn displayNone">
<ul class="bold">
<li><a href="/technical/Технический-обзор-акций-России" class="arial_12 bold">Россия</a></li>
</ul>
</div>
</li>
<li>
<a href="/technical/futures-technical-summary">Фьючерсы</a>
<span class="selected newSiteIconsSprite smallTriangleArrowUp displayNone">&nbsp;</span>
<span class="newSiteIconsSprite smallTriangleDarkGrayArrowUp displayNone selected">&nbsp;</span>
</li>
<li>
<a href="/technical/commodities-technical-summary">Товары</a>
<span class="selected newSiteIconsSprite smallTriangleArrowUp displayNone">&nbsp;</span>
<span class="newSiteIconsSprite smallTriangleDarkGrayArrowUp displayNone selected">&nbsp;</span>
</li>
<li class="genBadge" data-badgetext="НОВОЕ" >
Мой теханализ                <span class="selected newSiteIconsSprite smallTriangleArrowUp inlineblock">&nbsp;</span>
<span class="newSiteIconsSprite smallTriangleDarkGrayArrowUp displayNone selected1">&nbsp;</span>
</li>
</ul>

<div class="clear"></div>


<div class="technicalSummaryHeader">
<a href="javascript:void(0);" id="filterStateAnchor" class="newBtn filter LightGray float_lang_base_1 js-toggleFiltersVisibility">Настроить</a>	 <span class="inlineblock float_lang_base_2" style="padding-left: 7px; text-align:left;vertical-align:top;">

<div class="newSocialButtons small">
<a href="http://vkontakte.ru/share.php?url=https%3A%2F%2Fru.investing.com%2Ftechnical%2Fpersonalized-quotes-technical-summary" class="vk button genToolTip oneliner" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=615,width=600');return false;"  data-tooltip="Поделиться в ВКонтакте"><i class="vkLogo"></i></a><a href="https://www.facebook.com/sharer/sharer.php?app_id=179222828858436&u=https%3A%2F%2Fru.investing.com%2Ftechnical%2Fpersonalized-quotes-technical-summary&display=popup&ref=plugin" class="facebook button genToolTip oneliner" target="_blank" onclick="javascript:window.open(this.href, '', 'height=320,width=725');return false;" data-tooltip="Поделиться в Facebook"><i class="facebookLogo"></i></a><a href="javascript:void(0);" class="twitter button genToolTip oneliner" data-tooltip="Поделиться в Twitter"
onclick="openTwitPopup('https://invst.ly/36gml', 10093,'commonPagesBySML','%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA+%D0%BA%D0%BE%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BE%D0%BA+%D0%B4%D0%BB%D1%8F+%D1%82%D0%B5%D1%85%D0%B0%D0%BD%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0+%D0%BD%D0%B0+%40InvestingRU','twitter',true);">
<i class="twitterLogo"></i></a><a href="javascript:void(0);" class="shareWFriend button genToolTip oneliner" data-tooltip="Отправить по электронной почте"
onclick="openSharePopup('https://invst.ly/36gml', 10093,'commonPagesBySML', '%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA%20%D0%BA%D0%BE%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BE%D0%BA%20%D0%B4%D0%BB%D1%8F%20%D1%82%D0%B5%D1%85%D0%B0%D0%BD%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%20%D0%BD%D0%B0%20%40InvestingRU%0A%0A','%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA%20%D0%BA%D0%BE%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BE%D0%BA%20%D0%B4%D0%BB%D1%8F%20%D1%82%D0%B5%D1%85%D0%B0%D0%BD%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%20%D0%BD%D0%B0%20%40InvestingRU','shareLink',true,'1');">
<i class="newEnvelopeIcon"></i><span class="actionName"></span></a></div>
<script>
$(document).ready(function () {
$('.js-scrollto-comment').unbind('click').click(function (e) {
e.preventDefault();
var href = $(this).attr('href');
if(href !=='#comments'){
window.location = href;
return false;
}
$.scrollTo($('#addAComment').offset().top-135,1000);
setTimeout(function (e) {
$('#addAComment textarea').focus();
}, 1000);
});
});
</script></span>
<span id="pair_updatetime" class="float_lang_base_2 arial_11 lightgrayFont bold noWrap" style="padding-top:5px;">
Все данные активны и обновляются автоматически.	 </span>
<div class="clear"></div>
</div>
<form name='pairs_form' id='pairs_form'>
<INPUT TYPE='hidden' NAME='pairs_panel' VALUE="hide">
</form>


<script type="text/javascript">
function submit_technical_summary() {
var config = get_user_settings();
$("#pairs_table").toggle();
return config;
};

function get_user_settings() {
var currencies = '';
var intervals = '';
count=0;
$("#ul_container,#more_pairs_table").find("input:checkbox:checked").each(function(i,e){
currencies += "," + $(e).val();
});
$("[id^=select_interval_]","#ecoFiltersIntervals").find("select").each(function(i,e){
name=$(e).val();
intervals += "," + $(e).val();
});
return currencies.substring(1) + "|" + intervals.substring(1);
}
</script>

<div id="pairs_table" class="displayNone">
<div id="filtersArrowIndicator" class="newSiteIconsSprite arrowIndicator filtersArrowIndicator"></div>
<div id="ecoFiltersIntervals" class="ecoFilterBox majors js-periods">
<span class="float_lang_base_2 bugCloseIcon js-toggleFiltersVisibility">&nbsp;</span>
<p class="arial_14 bold" style="margin-bottom:15px;">Выбрать временные интервалы</p>
<div class="arial_13 bold inlineblock" id="select_interval_1">
Столбец 1	<select class="newInput selectBox">
<option value="60"  >1 минута</option>
<option value="300"  >5 минут</option>
<option value="900" selected="selected" >15 минут</option>
<option value="1800"  >30 минут</option>
<option value="3600"  >1 час</option>
<option value="18000"  >5 часов</option>
<option value="86400"  >1 день</option>
<option value="week"  >На неделю</option>
<option value="month"  >Месяц</option>
</select>
</div><div class="arial_13 bold inlineblock" id="select_interval_2">
Столбец 2	<select class="newInput selectBox">
<option value="60"  >1 минута</option>
<option value="300"  >5 минут</option>
<option value="900"  >15 минут</option>
<option value="1800" selected="selected" >30 минут</option>
<option value="3600"  >1 час</option>
<option value="18000"  >5 часов</option>
<option value="86400"  >1 день</option>
<option value="week"  >На неделю</option>
<option value="month"  >Месяц</option>
</select>
</div><div class="arial_13 bold inlineblock" id="select_interval_3">
Столбец 3	<select class="newInput selectBox">
<option value="60"  >1 минута</option>
<option value="300"  >5 минут</option>
<option value="900"  >15 минут</option>
<option value="1800"  >30 минут</option>
<option value="3600" selected="selected" >1 час</option>
<option value="18000"  >5 часов</option>
<option value="86400"  >1 день</option>
<option value="week"  >На неделю</option>
<option value="month"  >Месяц</option>
</select>
</div><div class="arial_13 bold inlineblock" id="select_interval_4">
Столбец 4	<select class="newInput selectBox">
<option value="60"  >1 минута</option>
<option value="300"  >5 минут</option>
<option value="900"  >15 минут</option>
<option value="1800"  >30 минут</option>
<option value="3600"  >1 час</option>
<option value="18000" selected="selected" >5 часов</option>
<option value="86400"  >1 день</option>
<option value="week"  >На неделю</option>
<option value="month"  >Месяц</option>
</select>
</div>	</div>
<div class="ecoFilterBox customTechSummary">
<div id="techSelectBox">
<p class="arial_14 bold">Найти дополнительные активы для отображения</p>
<!--  widgetsearch FROM   -->
<div class="searchDiv newInput inputTextBox float_lang_base_1" id="searchDiv_techSearch">
<div class="inlineblock searchBoxContainer topBarSearch " id="searchTextTopContainer_techSearch">
<form onsubmit="return search_techSearch.allowdForm;" id="combineSearchForm_techSearch" action="/" method="post">
<div class="inlineblock" id="searchBox_techSearch">
<input type="text" autocomplete="off" class="searchText arial_12 lightgrayFont middle " id="searchText_techSearch" name="quotes_search_text"
placeholder="USD/RUB или Газпром" tabindex="1"></div><label for="searchText_techSearch" class="searchGlassIcon">&nbsp;</label>
<i class="cssSpinner"></i>
</form>
</div>
<div class="clear"></div>

<div class="searchPopupResults displayNone" id="subContainer_techSearch">
<div class="searchTabs" id="search_tabs_techSearch">
<div id="searchBoxTabsTopTabsMenu">
<ul id="searchBoxTabsTopTabs_techSearch" class="tabsForBox">

</ul>
</div>
</div>
<div  id="countries_wrapper_techSearch" class="displayNone">
<div class="newSiteIconsSprite smallTriangleArrowUp"></div>
<div class="countriesDiv">
<table id="countries_table_techSearch">
<tbody>

</tbody>
</table>
</div>
</div>
<div identifier="searchThing" class="textBox" id="searchTopResults_techSearch"></div>
<!-- searchPopupResults -->
</div>
</div>

<div id="search_loading_techSearch" style="display:none">
<span class="inlineblock loading">&nbsp;</span>
</div>
<div class="clear"></div>
<!--  END SECTION -->
<script>
window.searchParams = window.searchParams || {};
window.searches = window.searches || {};

window.searchParams['techSearch'] = {
$newSearchWidget : true,
$prefix:'techSearch',
$AllCountries:'Все страны',
$tabsinfo:'[{"id":"search_box_top_techSearchAll","name":"\u0412\u0441\u0435","select":true},{"id":"search_box_top_techSearchIndices","name":"\u0418\u043d\u0434\u0435\u043a\u0441\u044b","select":false},{"id":"search_box_top_techSearchStocks","name":"\u0410\u043a\u0446\u0438\u0438","select":false},{"id":"search_box_top_techSearchETFs","name":"ETF","select":false},{"id":"search_box_top_techSearchFunds","name":"\u0424\u043e\u043d\u0434\u044b","select":false},{"id":"search_box_top_techSearchCommodities","name":"\u0422\u043e\u0432\u0430\u0440\u044b","select":false},{"id":"search_box_top_techSearchForex","name":"\u0412\u0430\u043b\u044e\u0442\u044b","select":false},{"id":"search_box_top_techSearchBonds","name":"\u041e\u0431\u043b\u0438\u0433\u0430\u0446\u0438\u0438","select":false},{"id":"search_box_top_techSearchCountries","name":"\u0412\u0441\u0435 \u0441\u0442\u0440\u0430\u043d\u044b","select":false}]',
$searchtype:'',
$resultType:'userQuotes',
$functionName:'addInstrumentToBox',
$paramList:null,
$url:'/instruments/Service/Search/',
$no_search_results:'Ваш поиск не дал результатов'
    };

window.searches['techSearch'] = new FXautoComplete(window.searchParams['techSearch']);

$( document ).ready(function() {
$('#searchText_techSearch').attr('tabindex', 1);
});
</script>	</div>
<div class="innerContainer" data-min="1" data-max="18">
<span>Выбрано:
<span class="js-techInstrumentCount">0</span> из <span class="js-techInstrumentCountTotal">0</span>
</span>
<ul id="techInstrumentsContainer">
<li class="favItem js-techInstrumentLabel"><span></span><i class="bugCloseIcon"></i></li>
</ul>
</div>
</div>

<script>
function addInstrumentToBox(pairObject) {
Technical.addTechSearchBlock(pairObject);
}
</script>		<div class="ecoFilterBox" id="emailSettingsContainer">
<div class="flexBetween ">
<label class="allowEmails pointer" for="usr_allow_emails"> <input class="checkbox" id="usr_allow_emails" type="checkbox" > Получать ежедневный теханализ по почте</label>
<label class="js-edit-details blueFont pointer displayNone" for="gmtTechnicalOpen">Редакт.<span class="showMorerightArrow"></span></label>
</div>
<input type="checkbox" class="displayNone gmtTechnicalOpen" id="gmtTechnicalOpen">
<div class="gmtTechnicalSelect overflowHidden">
<div class="ecoFilterBoxRow">
<select>
<option value="0" >00:00</option><option value="1" >01:00</option><option value="2" >02:00</option><option value="3" >03:00</option><option value="4" >04:00</option><option value="5" >05:00</option><option value="6" >06:00</option><option value="7" >07:00</option><option value="8" >08:00</option><option value="9" >09:00</option><option value="10" >10:00</option><option value="11" >11:00</option><option value="12" >12:00</option><option value="13" >13:00</option><option value="14" >14:00</option><option value="15" >15:00</option><option value="16" >16:00</option><option value="17" >17:00</option><option value="18" >18:00</option><option value="19" >19:00</option><option value="20" selected>20:00</option><option value="21" >21:00</option><option value="22" >22:00</option><option value="23" >23:00</option>				 </select>
<span class="lightgrayFont">Временная зона: (GMT +3:00) Moscow, St. Petersburg, Volgograd</span>
</div>
<div class="ecoFilterBoxRow"> 					 <input id="day1" name="options[technical_days][]" type="checkbox" class="daysInput js-email-days" value="1" />
<label for="day1">Понедельник</label>
<input id="day2" name="options[technical_days][]" type="checkbox" class="daysInput js-email-days" value="2" />
<label for="day2">Вторник</label>
<input id="day3" name="options[technical_days][]" type="checkbox" class="daysInput js-email-days" value="3" />
<label for="day3">Среда</label>
<input id="day4" name="options[technical_days][]" type="checkbox" class="daysInput js-email-days" value="4" />
<label for="day4">Четверг</label>
<input id="day5" name="options[technical_days][]" type="checkbox" class="daysInput js-email-days" value="5" />
<label for="day5">Пятница</label>
<input id="day6" name="options[technical_days][]" type="checkbox" class="daysInput js-email-days" value="6" />
<label for="day6">Суббота</label>
<input id="day0" name="options[technical_days][]" type="checkbox" class="daysInput js-email-days" value="0" />
<label for="day0">Воскресенье</label>
</div>
<div class="ecoFilterBoxRow">
Для дополнительных настроек <a href="http://ru.investing.com/members-admin/config-notifications">Нажмите здесь</a>.				 <input id="timezone_offset" type="hidden" value="10800">
</div>
</div>
</div>
<script>
(function () {
var $hiddenCheckbox = $('#gmtTechnicalOpen'),
$editCaption = $('.js-edit-details');
$('#usr_allow_emails').change(function () {
var isChecked = $(this).is(':checked');
$hiddenCheckbox.prop('checked', isChecked);
$editCaption._toggleShow(isChecked);
})
})()
</script>
<div class="ecoFilterBox submitBox align_center">
<a href="javascript:void(0);" onclick="Technical.submitTechForm('userQuotes');" class="newBtn Arrow LightGray">Применить</a>    </div>
</div><div class="align_center displayNone" id="loadingSpinner">
<img src="https://i-invdn-com.akamaized.net/images/ajax-loader-big.gif">
</div>
<div id="technical_summary_container">
<div class="clear"></div>
<table class="genTbl closedTbl technicalSummaryTbl noHover">
<thead>
<tr>
<th class="center symbol noWrap">Название</th>
<th class="left type noWrap" >Тип</th>
<th class="left noWrap" data-period="900">15 минут</th>
<th class="left noWrap" data-period="1800">30 минут</th>
<th class="left noWrap" data-period="3600">1 час</th>
<th class="left noWrap" data-period="18000">5 часов</th>
</tr>
</thead>
<tbody>
<tr data-pairid="2186" data-row-type="movingAverages">
<td rowspan="3" class="symbol middle center">
<a href="/currencies/usd-rub" title="USD/RUB - Доллар США Российский рубль" class="arial_14 bold middle center">USD/RUB</a>
<p class="redFont bold pid-2186-last-noblink">65,5503</p>
</td>
<td class="bold left type">Скол. средние:</td>
<td class="left js-socket-elem">Нейтрально</td>
<td class="left js-socket-elem">Продавать</td>
<td class="left js-socket-elem">Продавать</td>
<td class="left js-socket-elem">Активно продавать</td>
</tr>
<tr data-pairid="2186" data-row-type="indicators">
<td class="bold left type">Индикаторы:</td>
<td class="left js-socket-elem">Активно покупать</td>
<td class="left js-socket-elem">Продавать</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Активно продавать</td>
</tr>
<tr data-pairid="2186" data-row-type="summary">
<td class="bold left type arial_14">Резюме:</td>
<td class="greenFont bold left arial_14 js-socket-elem">Покупать</td>
<td class="redFont bold left arial_14 js-socket-elem">Продавать</td>
<td class="redFont bold left arial_14 js-socket-elem">Активно продавать</td>
<td class="redFont bold left arial_14 js-socket-elem">Активно продавать</td>
</tr>
<tr data-pairid="945629" data-row-type="movingAverages">
<td rowspan="3" class="symbol middle center">
<a href="/crypto/bitcoin/btc-usd" title="BTC/USD - Биткойн Доллар США" class="arial_14 bold middle center">BTC/USD</a>
<p class="greenFont bold pid-945629-last-noblink">6.612,1</p>
</td>
<td class="bold left type">Скол. средние:</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Продавать</td>
<td class="left js-socket-elem">Нейтрально</td>
<td class="left js-socket-elem">Покупать</td>
</tr>
<tr data-pairid="945629" data-row-type="indicators">
<td class="bold left type">Индикаторы:</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Продавать</td>
<td class="left js-socket-elem">Активно покупать</td>
</tr>
<tr data-pairid="945629" data-row-type="summary">
<td class="bold left type arial_14">Резюме:</td>
<td class="redFont bold left arial_14 js-socket-elem">Активно продавать</td>
<td class="redFont bold left arial_14 js-socket-elem">Активно продавать</td>
<td class="lightgrayFont bold left arial_14 js-socket-elem">Нейтрально</td>
<td class="greenFont bold left arial_14 js-socket-elem">Активно покупать</td>
</tr>
<tr data-pairid="997650" data-row-type="movingAverages">
<td rowspan="3" class="symbol middle center">
<a href="/crypto/ethereum/eth-usd" title="ETH/USD - Эфириум Доллар США" class="arial_14 bold middle center">ETH/USD</a>
<p class="redFont bold pid-997650-last-noblink">229,68</p>
</td>
<td class="bold left type">Скол. средние:</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Продавать</td>
<td class="left js-socket-elem">Нейтрально</td>
</tr>
<tr data-pairid="997650" data-row-type="indicators">
<td class="bold left type">Индикаторы:</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Покупать</td>
</tr>
<tr data-pairid="997650" data-row-type="summary">
<td class="bold left type arial_14">Резюме:</td>
<td class="redFont bold left arial_14 js-socket-elem">Активно продавать</td>
<td class="redFont bold left arial_14 js-socket-elem">Активно продавать</td>
<td class="redFont bold left arial_14 js-socket-elem">Активно продавать</td>
<td class="lightgrayFont bold left arial_14 js-socket-elem">Нейтрально</td>
</tr>
<tr data-pairid="1010782" data-row-type="movingAverages">
<td rowspan="3" class="symbol middle center">
<a href="/crypto/ripple/xrp-usd" title="XRP/USD - Рипл Доллар США" class="arial_14 bold middle center">XRP/USD</a>
<p class="greenFont bold pid-1010782-last-noblink">0,57800</p>
</td>
<td class="bold left type">Скол. средние:</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Продавать</td>
<td class="left js-socket-elem">Нейтрально</td>
<td class="left js-socket-elem">Покупать</td>
</tr>
<tr data-pairid="1010782" data-row-type="indicators">
<td class="bold left type">Индикаторы:</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Активно покупать</td>
</tr>
<tr data-pairid="1010782" data-row-type="summary">
<td class="bold left type arial_14">Резюме:</td>
<td class="redFont bold left arial_14 js-socket-elem">Активно продавать</td>
<td class="redFont bold left arial_14 js-socket-elem">Активно продавать</td>
<td class="redFont bold left arial_14 js-socket-elem">Продавать</td>
<td class="greenFont bold left arial_14 js-socket-elem">Активно покупать</td>
</tr>
<tr data-pairid="1031333" data-row-type="movingAverages">
<td rowspan="3" class="symbol middle center">
<a href="/crypto/bitcoin-cash/bch-usd" title="BCH/USD - Bitcoin Cash Доллар США" class="arial_14 bold middle center">BCH/USD</a>
<p class="redFont bold pid-1031333-last-noblink">534,19</p>
</td>
<td class="bold left type">Скол. средние:</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Продавать</td>
<td class="left js-socket-elem">Покупать</td>
</tr>
<tr data-pairid="1031333" data-row-type="indicators">
<td class="bold left type">Индикаторы:</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Продавать</td>
<td class="left js-socket-elem">Покупать</td>
<td class="left js-socket-elem">Нейтрально</td>
</tr>
<tr data-pairid="1031333" data-row-type="summary">
<td class="bold left type arial_14">Резюме:</td>
<td class="redFont bold left arial_14 js-socket-elem">Активно продавать</td>
<td class="redFont bold left arial_14 js-socket-elem">Активно продавать</td>
<td class="lightgrayFont bold left arial_14 js-socket-elem">Нейтрально</td>
<td class="lightgrayFont bold left arial_14 js-socket-elem">Нейтрально</td>
</tr>
<tr data-pairid="1024821" data-row-type="movingAverages">
<td rowspan="3" class="symbol middle center">
<a href="/crypto/eos/eos-usd" title="EOS/USD - EOS Доллар США" class="arial_14 bold middle center">EOS/USD</a>
<p class="redFont bold pid-1024821-last-noblink">5,6844</p>
</td>
<td class="bold left type">Скол. средние:</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Продавать</td>
</tr>
<tr data-pairid="1024821" data-row-type="indicators">
<td class="bold left type">Индикаторы:</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Продавать</td>
<td class="left js-socket-elem">Покупать</td>
</tr>
<tr data-pairid="1024821" data-row-type="summary">
<td class="bold left type arial_14">Резюме:</td>
<td class="redFont bold left arial_14 js-socket-elem">Активно продавать</td>
<td class="redFont bold left arial_14 js-socket-elem">Активно продавать</td>
<td class="redFont bold left arial_14 js-socket-elem">Активно продавать</td>
<td class="lightgrayFont bold left arial_14 js-socket-elem">Нейтрально</td>
</tr>
<tr data-pairid="2148" data-row-type="movingAverages">
<td rowspan="3" class="symbol middle center">
<a href="/currencies/usd-kzt" title="USD/KZT - Доллар США Казахстанский тенге" class="arial_14 bold middle center">USD/KZT</a>
<p class="redFont bold pid-2148-last-noblink">362,395</p>
</td>
<td class="bold left type">Скол. средние:</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Продавать</td>
<td class="left js-socket-elem">Продавать</td>
<td class="left js-socket-elem">Продавать</td>
</tr>
<tr data-pairid="2148" data-row-type="indicators">
<td class="bold left type">Индикаторы:</td>
<td class="left js-socket-elem">Нейтрально</td>
<td class="left js-socket-elem">Продавать</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Активно покупать</td>
</tr>
<tr data-pairid="2148" data-row-type="summary">
<td class="bold left type arial_14">Резюме:</td>
<td class="redFont bold left arial_14 js-socket-elem">Продавать</td>
<td class="redFont bold left arial_14 js-socket-elem">Продавать</td>
<td class="redFont bold left arial_14 js-socket-elem">Активно продавать</td>
<td class="lightgrayFont bold left arial_14 js-socket-elem">Нейтрально</td>
</tr>
<tr data-pairid="1656" data-row-type="movingAverages">
<td rowspan="3" class="symbol middle center">
<a href="/currencies/eur-kzt" title="EUR/KZT - Евро Казахстанский тенге" class="arial_14 bold middle center">EUR/KZT</a>
<p class="redFont bold pid-1656-last-noblink">420,580</p>
</td>
<td class="bold left type">Скол. средние:</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Активно продавать</td>
<td class="left js-socket-elem">Активно продавать</td>
</tr>
<tr data-pairid="1656" data-row-type="indicators">
<td class="bold left type">Индикаторы:</td>
<td class="left js-socket-elem">Нейтрально</td>
<td class="left js-socket-elem">Нейтрально</td>
<td class="left js-socket-elem">Нейтрально</td>
<td class="left js-socket-elem">Активно продавать</td>
</tr>
<tr data-pairid="1656" data-row-type="summary">
<td class="bold left type arial_14">Резюме:</td>
<td class="redFont bold left arial_14 js-socket-elem">Продавать</td>
<td class="redFont bold left arial_14 js-socket-elem">Продавать</td>
<td class="redFont bold left arial_14 js-socket-elem">Продавать</td>
<td class="redFont bold left arial_14 js-socket-elem">Активно продавать</td>
</tr>
</tbody>
</table>
<script>
window.techSummaryPairs = [{"pair_ID":"2186","name":"USD\/RUB","trans_name":"USD\/RUB"},{"pair_ID":"945629","name":"BTC\/USD","trans_name":"BTC\/USD"},{"pair_ID":"997650","name":"ETH\/USD","trans_name":"ETH\/USD"},{"pair_ID":"1010782","name":"XRP\/USD","trans_name":"XRP\/USD"},{"pair_ID":"1031333","name":"BCH\/USD","trans_name":"BCH\/USD"},{"pair_ID":"1024821","name":"EOS\/USD","trans_name":"EOS\/USD"},{"pair_ID":"2148","name":"USD\/KZT","trans_name":"USD\/KZT"},{"pair_ID":"1656","name":"EUR\/KZT","trans_name":"EUR\/KZT"}];
</script>
</div>

<div class="clear"></div>
<div id="theDisclaimer" class="disclaimerBox">
<b>Дисклеймер:</b> Fusion Media не несет никакой ответственности за утрату ваших денег в результате того, что вы положились на информацию, содержащуюся на этом сайте, включая данные, котировки, графики и сигналы форекс. Операции на международном валютном рынке Форекс содержат в себе высокий уровень риска. Торговля на рынке Форекс может не подходить для всех инвесторов. Спекулируйте только теми деньгами, которые Вы можете позволить себе потерять.
</br>
Fusion Media напоминает вам, что данные, предоставленные на данном сайте, не обязательно даны в режиме реального времени и могут не являться точными. Все цены на акции, индексы, фьючерсы носят ориентировочный характер и на них нельзя полагаться при торговле. Версия этого документа на английском языке является определяющей и имеет преимущественную силу в том случае, если возникают разночтения между версиями на английском и русском языках. Таким образом, Fusion Media не несет никакой ответственности за любые убытки, которые вы можете понести в результате использования этих данных. Вы ищете акции, котировки или графики форекс? Взгляните на портал Investing.com - лучший технический анализ и современный экономический календарь к вашим услугам!
</br>
Английская версия данного соглашения является основной версией в случае, если информация на русском и английском языке не совпадают.</div>
<script src="https://i-invdn-com.akamaized.net/js/jquery.scroll.0.js"></script>
<script type="text/javascript">
$('#scrollToDisc').live('click', function()
{
$.scrollTo( $('#theDisclaimer'), 1000);
scroll_up_disclaimer();
});
</script>        <!-- /Page Content -->


</section>  <!-- /leftColumn -->
<aside id="rightColumn">
<div class="tradeNowRightColumn"></div><script type="text/javascript">
$(function() {
// Initialize chart block
var chart = Object.create(FP.Modules.js_sideblock_chart());
// Set containers
chart.set_wrapper('quotesBoxChartWrp');
chart.set_chart_container('quotesBoxChart');
chart.set_loading_container('quotesBoxChartLoading');
chart.nav();
// Trigger chart's rendering
$('#quotesBoxChart').trigger('chartTabChanged');

});
</script> <div class="clear  firstSideBox js-quotes rightBoxes" id="Quotes"><div class="TabContainer noOverflow js-tab-container">
<span class="newSiteIconsSprite arrow"></span>
<ul class="tabsForBox oneLvlTabsWidget js-one-level-tabs " id="quotesBoxWithTabsTop">

<li id=QBS_3 class="first selected" ><a href="javascript:void(0);">Товары</a></li>

<li id=QBS_1 ><a href="javascript:void(0);">Форекс</a></li>

<li id=QBS_2 ><a href="javascript:void(0);">Индексы</a></li>

<li id=QBS_7 class="last" ><a href="javascript:void(0);">Крипто</a></li>

</ul>
<span id="lastDummyLI" data-more-cat="Другие категории" class="moreTabOneLvlDummy displayNone"><a href="javascript:void();">Другие категории</a><span class="showMoreDownArrow"></span></span>
</div>

<script>
$(document).ready(function(){
tabsWidgetOnelevel('', 'storedSiteBlockQuotesBoxTabObject', 'quotesBoxWithTabsTop', +'');
});
</script><div class="quotesBox quotesBoxWithTabsBottom"><span class="threeDotsIconSmall genToolTip oneliner js-three-dots" data-tooltip="Настройка вкладок"></span><div class="tooltipPopup quotesTabsSelection displayNone js-popup-wrapper"><div class="header">Выбор вкладок</div><div class="content ui-sortable rowsWrap js-sideblock-tab-selection"><label class="addRow js-ts-label" data-tab-id="3"><span class="checkers"></span><input type="checkbox" class="checkbox"checked >Товары</label><label class="addRow js-ts-label" data-tab-id="1"><span class="checkers"></span><input type="checkbox" class="checkbox"checked >Форекс</label><label class="addRow js-ts-label" data-tab-id="2"><span class="checkers"></span><input type="checkbox" class="checkbox"checked >Индексы</label><label class="addRow js-ts-label" data-tab-id="7"><span class="checkers"></span><input type="checkbox" class="checkbox"checked >Крипто</label><label class="addRow js-ts-label" data-tab-id="4"><span class="checkers"></span><input type="checkbox" class="checkbox">Облигации</label><label class="addRow js-ts-label" data-tab-id="6"><span class="checkers"></span><input type="checkbox" class="checkbox">ETF</label><label class="addRow js-ts-label" data-tab-id="5"><span class="checkers"></span><input type="checkbox" class="checkbox">Акции</label></div><div class="footer"><span class="js-tab-selection-counter">Выбрано: %COUNT%/4 </span><a href="javascript:void(0);" class="newBtn Orange2 noIcon js-tab-selection-apply">Обновить</a></div></div><div class="tabsBoxBottom" id="quotesBoxChartWrp" data-chart_type="area" data-config_fontSize="9px" data-config_last_value_line="1" data-config_last_close_value_line="1" data-config_decimal_point="," data-config_thousands_sep="."><div class="chartFrame"><div id="quotesBoxChartTimeFrames" class="timePeriods"><span data-time-frame="1day">1д</span><span data-time-frame="1week">1н</span><span data-time-frame="1month">1м</span><span data-time-frame="6months">6м</span><span data-time-frame="1year">1г</span><span data-time-frame="5years">5л</span><span data-time-frame="max">Макс.</span><a href="#" class="js-chart-link quotesboxLinkIcon"></a></div><div id="quotesBoxChart" class="quotesChartWrapper"></div><div id="quotesBoxChartLoading" class="loading-responsive medium-circle"></div></div><table id="QBS_3_inner" data-gae='sb_commodities'  class=" genTbl openTbl quotesSideBlockTbl collapsedTbl"><tbody><tr class="LeftLiContainer Selected" pair="8833" data-pair_id="8833" data-external="sbcharts.investing.com" data-pair_url="/commodities/brent-oil-streaming-chart" id="sb_pair_8833" chart_link="/commodities/brent-oil-streaming-chart" data-chart-hash="ecab723bd0c0050290d8fcb3b6ecc974"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/commodities/brent-oil" title="Фьючерс на нефть Brent (F) (CFD)"  data-gae='sb_commodities-brent-oil' >Нефть Brent</a></td><td class="lastNum pid-8833-last" id="sb_last_8833">83,31</td><td class="chg greenFont pid-8833-pc" id="sb_change_8833">+0,58</td><td class="chgPer greenFont pid-8833-pcp" id="sb_changepc_8833">+0,70%</td><td class="icon"><span class="greenClockIcon isOpenPair-8833" id="sb_clock_8833"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="8849" data-pair_id="8849" data-external="sbcharts.investing.com" data-pair_url="/commodities/crude-oil-streaming-chart" id="sb_pair_8849" chart_link="/commodities/crude-oil-streaming-chart" data-chart-hash="c4caa3c525726eb96b6ee67ddfa57896"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/commodities/crude-oil" title="Фьючерсы на нефть WTI (F) (CFD)"  data-gae='sb_commodities-crude-oil' >Нефть WTI</a></td><td class="lastNum pid-8849-last" id="sb_last_8849">73,58</td><td class="chg greenFont pid-8849-pc" id="sb_change_8849">+0,33</td><td class="chgPer greenFont pid-8849-pcp" id="sb_changepc_8849">+0,45%</td><td class="icon"><span class="greenClockIcon isOpenPair-8849" id="sb_clock_8849"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="8830" data-pair_id="8830" data-external="sbcharts.investing.com" data-pair_url="/commodities/gold-streaming-chart" id="sb_pair_8830" chart_link="/commodities/gold-streaming-chart" data-chart-hash="d565a7612f2d571d32033540114babb3"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/commodities/gold" title="Фьючерс на золото (F) (CFD)"  data-gae='sb_commodities-gold' >Золото</a></td><td class="lastNum pid-8830-last" id="sb_last_8830">1.192,90</td><td class="chg redFont pid-8830-pc" id="sb_change_8830">-3,30</td><td class="chgPer redFont pid-8830-pcp" id="sb_changepc_8830">-0,28%</td><td class="icon"><span class="greenClockIcon isOpenPair-8830" id="sb_clock_8830"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="8836" data-pair_id="8836" data-external="sbcharts.investing.com" data-pair_url="/commodities/silver-streaming-chart" id="sb_pair_8836" chart_link="/commodities/silver-streaming-chart" data-chart-hash="c4651fb6e12fa8166f3d0af875aede53"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/commodities/silver" title="Фьючерс на серебро (F) (CFD)"  data-gae='sb_commodities-silver' >Серебро</a></td><td class="lastNum pid-8836-last" id="sb_last_8836">14,655</td><td class="chg redFont pid-8836-pc" id="sb_change_8836">-0,057</td><td class="chgPer redFont pid-8836-pcp" id="sb_changepc_8836">-0,39%</td><td class="icon"><span class="greenClockIcon isOpenPair-8836" id="sb_clock_8836"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="8910" data-pair_id="8910" data-external="sbcharts.investing.com" data-pair_url="/commodities/platinum-streaming-chart" id="sb_pair_8910" chart_link="/commodities/platinum-streaming-chart" data-chart-hash="ecc18f69640cd8c23af34a7a8bdda156"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/commodities/platinum" title="Фьючерс на платину (F) (CFD)"  data-gae='sb_commodities-platinum' >Платина</a></td><td class="lastNum pid-8910-last" id="sb_last_8910">817,20</td><td class="chg redFont pid-8910-pc" id="sb_change_8910">-1,30</td><td class="chgPer redFont pid-8910-pcp" id="sb_changepc_8910">-0,16%</td><td class="icon"><span class="greenClockIcon isOpenPair-8910" id="sb_clock_8910"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="8883" data-pair_id="8883" data-external="sbcharts.investing.com" data-pair_url="/commodities/palladium-streaming-chart" id="sb_pair_8883" chart_link="/commodities/palladium-streaming-chart" data-chart-hash="720874c49751ddb4fb54bb546174ec63"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/commodities/palladium" title="Фьючерс на палладий (F) (CFD)"  data-gae='sb_commodities-palladium' >Палладий</a></td><td class="lastNum pid-8883-last" id="sb_last_8883">1.057,10</td><td class="chg redFont pid-8883-pc" id="sb_change_8883">-7,50</td><td class="chgPer redFont pid-8883-pcp" id="sb_changepc_8883">-0,70%</td><td class="icon"><span class="greenClockIcon isOpenPair-8883" id="sb_clock_8883"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="8862" data-pair_id="8862" data-external="sbcharts.investing.com" data-pair_url="/commodities/natural-gas-streaming-chart" id="sb_pair_8862" chart_link="/commodities/natural-gas-streaming-chart" data-chart-hash="2862b1367008cea06e03f8cbaf7f40ed"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/commodities/natural-gas" title="Фьючерс на природный газ (F) (CFD)"  data-gae='sb_commodities-natural-gas' >Природный газ</a></td><td class="lastNum pid-8862-last" id="sb_last_8862">3,022</td><td class="chg greenFont pid-8862-pc" id="sb_change_8862">+0,020</td><td class="chgPer greenFont pid-8862-pcp" id="sb_changepc_8862">+0,67%</td><td class="icon"><span class="greenClockIcon isOpenPair-8862" id="sb_clock_8862"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer last" pair="49768" data-pair_id="49768" data-external="sbcharts.investing.com" data-pair_url="/commodities/aluminum-streaming-chart" id="sb_pair_49768" chart_link="/commodities/aluminum-streaming-chart" data-chart-hash="badc0afca3b1bf2883f1eb94670663a2"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/commodities/aluminum" title="Фьючерс на алюминий (F) (CFD)"  data-gae='sb_commodities-aluminum' >Алюминий</a></td><td class="lastNum pid-49768-last" id="sb_last_49768">2.052,50</td><td class="chg redFont pid-49768-pc" id="sb_change_49768">-0,50</td><td class="chgPer redFont pid-49768-pcp" id="sb_changepc_49768">-0,02%</td><td class="icon"><span class="greenClockIcon isOpenPair-49768" id="sb_clock_49768"></span></td><td class="sideColumn">&nbsp;</td></tr></tbody></table><table id="QBS_1_inner" data-gae='sb_forex'  class="displayNone genTbl openTbl quotesSideBlockTbl collapsedTbl"><tbody><tr class="LeftLiContainer Selected" pair="2186" data-pair_id="2186" data-external="sbcharts.investing.com" data-pair_url="/currencies/usd-rub-chart" id="sb_pair_2186" chart_link="/currencies/usd-rub-chart" data-chart-hash="1706480cd1261085a67b0e31f93d7e89"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/currencies/usd-rub" title="USD/RUB - Доллар США Российский рубль"  data-gae='sb_forex-usd-rub' >USD/RUB</a></td><td class="lastNum pid-2186-last" id="sb_last_2186">65,5503</td><td class="chg redFont pid-2186-pc" id="sb_change_2186">0,0000</td><td class="chgPer redFont pid-2186-pcp" id="sb_changepc_2186">0,00%</td><td class="icon"><span class="redClockIcon isOpenPair-2186" id="sb_clock_2186"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="1" data-pair_id="1" data-external="sbcharts.investing.com" data-pair_url="/currencies/eur-usd-chart" id="sb_pair_1" chart_link="/currencies/eur-usd-chart" data-chart-hash="429d74b767b97c5148ee1b2fd4151ba2"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/currencies/eur-usd" title="EUR/USD - Евро Доллар США"  data-gae='sb_forex-eur-usd' >EUR/USD</a></td><td class="lastNum pid-1-last" id="sb_last_1">1,1593</td><td class="chg redFont pid-1-pc" id="sb_change_1">-0,0011</td><td class="chgPer redFont pid-1-pcp" id="sb_changepc_1">-0,09%</td><td class="icon"><span class="greenClockIcon isOpenExch-1002" id="sb_clock_1"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="2" data-pair_id="2" data-external="sbcharts.investing.com" data-pair_url="/currencies/gbp-usd-chart" id="sb_pair_2" chart_link="/currencies/gbp-usd-chart" data-chart-hash="d8560659b81ddf1ba317896bd144332a"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/currencies/gbp-usd" title="GBP/USD - Британский фунт Доллар США"  data-gae='sb_forex-gbp-usd' >GBP/USD</a></td><td class="lastNum pid-2-last" id="sb_last_2">1,3036</td><td class="chg greenFont pid-2-pc" id="sb_change_2">+0,0005</td><td class="chgPer greenFont pid-2-pcp" id="sb_changepc_2">+0,04%</td><td class="icon"><span class="greenClockIcon isOpenExch-1002" id="sb_clock_2"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="7" data-pair_id="7" data-external="sbcharts.investing.com" data-pair_url="/currencies/usd-cad-chart" id="sb_pair_7" chart_link="/currencies/usd-cad-chart" data-chart-hash="68ca4930a11af002d4d0a2eff6864866"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/currencies/usd-cad" title="USD/CAD - Доллар США Канадский доллар"  data-gae='sb_forex-usd-cad' >USD/CAD</a></td><td class="lastNum pid-7-last" id="sb_last_7">1,2820</td><td class="chg redFont pid-7-pc" id="sb_change_7">-0,0090</td><td class="chgPer redFont pid-7-pcp" id="sb_changepc_7">-0,70%</td><td class="icon"><span class="greenClockIcon isOpenExch-1002" id="sb_clock_7"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="3" data-pair_id="3" data-external="sbcharts.investing.com" data-pair_url="/currencies/usd-jpy-chart" id="sb_pair_3" chart_link="/currencies/usd-jpy-chart" data-chart-hash="0fdacd3571a3a6ef8960f7e765071282"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/currencies/usd-jpy" title="USD/JPY - Доллар США Японская иена"  data-gae='sb_forex-usd-jpy' >USD/JPY</a></td><td class="lastNum pid-3-last" id="sb_last_3">113,94</td><td class="chg greenFont pid-3-pc" id="sb_change_3">+0,24</td><td class="chgPer greenFont pid-3-pcp" id="sb_changepc_3">+0,21%</td><td class="icon"><span class="greenClockIcon isOpenExch-1002" id="sb_clock_3"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="5" data-pair_id="5" data-external="sbcharts.investing.com" data-pair_url="/currencies/aud-usd-chart" id="sb_pair_5" chart_link="/currencies/aud-usd-chart" data-chart-hash="64644f156f6ac47c39d47a9f2ceb89dc"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/currencies/aud-usd" title="AUD/USD - Австралийский доллар Доллар США"  data-gae='sb_forex-aud-usd' >AUD/USD</a></td><td class="lastNum pid-5-last" id="sb_last_5">0,7213</td><td class="chg redFont pid-5-pc" id="sb_change_5">-0,0009</td><td class="chgPer redFont pid-5-pcp" id="sb_changepc_5">-0,12%</td><td class="icon"><span class="greenClockIcon isOpenExch-1002" id="sb_clock_5"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="11" data-pair_id="11" data-external="sbcharts.investing.com" data-pair_url="/currencies/gbp-jpy-chart" id="sb_pair_11" chart_link="/currencies/gbp-jpy-chart" data-chart-hash="1824abedd1ffc8fc66f66cf1089224c6"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/currencies/gbp-jpy" title="GBP/JPY - Британский фунт Японская иена"  data-gae='sb_forex-gbp-jpy' >GBP/JPY</a></td><td class="lastNum pid-11-last" id="sb_last_11">148,52</td><td class="chg greenFont pid-11-pc" id="sb_change_11">+0,35</td><td class="chgPer greenFont pid-11-pcp" id="sb_changepc_11">+0,24%</td><td class="icon"><span class="greenClockIcon isOpenExch-1002" id="sb_clock_11"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer last" pair="1691" data-pair_id="1691" data-external="sbcharts.investing.com" data-pair_url="/currencies/eur-rub-chart" id="sb_pair_1691" chart_link="/currencies/eur-rub-chart" data-chart-hash="f5d340c78a9833562bce290f9f21c36a"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/currencies/eur-rub" title="EUR/RUB - Евро Российский рубль"  data-gae='sb_forex-eur-rub' >EUR/RUB</a></td><td class="lastNum pid-1691-last" id="sb_last_1691">76,0865</td><td class="chg redFont pid-1691-pc" id="sb_change_1691">0,0000</td><td class="chgPer redFont pid-1691-pcp" id="sb_changepc_1691">0,00%</td><td class="icon"><span class="redClockIcon isOpenPair-1691" id="sb_clock_1691"></span></td><td class="sideColumn">&nbsp;</td></tr></tbody></table><table id="QBS_2_inner" data-gae='sb_indices'  class="displayNone genTbl openTbl quotesSideBlockTbl collapsedTbl"><tbody><tr class="LeftLiContainer Selected" pair="13665" data-pair_id="13665" data-external="sbcharts.investing.com" data-pair_url="/indices/rtsi-chart" id="sb_pair_13665" chart_link="/indices/rtsi-chart" data-chart-hash="c351250a76c38a7f8c64430699c9f172"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/indices/rtsi" title="РТС"  data-gae='sb_indices-rtsi' >РТС</a></td><td class="lastNum pid-13665-last" id="sb_last_13665">1.192,04</td><td class="chg greenFont pid-13665-pc" id="sb_change_13665">+4,16</td><td class="chgPer greenFont pid-13665-pcp" id="sb_changepc_13665">+0,35%</td><td class="icon"><span class="redClockIcon isOpenExch-40" id="sb_clock_13665"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="13666" data-pair_id="13666" data-external="sbcharts.investing.com" data-pair_url="/indices/mcx-chart" id="sb_pair_13666" chart_link="/indices/mcx-chart" data-chart-hash="771bf1dd1d32e90d63d06c49583f68b4"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/indices/mcx" title="Индекс МосБиржи"  data-gae='sb_indices-mcx' >Индекс МосБиржи</a></td><td class="lastNum pid-13666-last" id="sb_last_13666">2.475,36</td><td class="chg greenFont pid-13666-pc" id="sb_change_13666">+0,79</td><td class="chgPer greenFont pid-13666-pcp" id="sb_changepc_13666">+0,03%</td><td class="icon"><span class="redClockIcon isOpenExch-40" id="sb_clock_13666"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="8839" data-pair_id="8839" data-external="sbcharts.investing.com" data-pair_url="/indices/us-spx-500-futures-streaming-chart" id="sb_pair_8839" chart_link="/indices/us-spx-500-futures-streaming-chart" data-chart-hash="1a5f95b308e1d982eec66b97f9895bac"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/indices/us-spx-500-futures" title="Фьючерс на S&P 500 (F) (CFD)"  data-gae='sb_indices-us-spx-500-futures' >S&P 500 (F)</a></td><td class="lastNum pid-8839-last" id="sb_last_8839">2.934,75</td><td class="chg greenFont pid-8839-pc" id="sb_change_8839">+15,75</td><td class="chgPer greenFont pid-8839-pcp" id="sb_changepc_8839">+0,54%</td><td class="icon"><span class="greenClockIcon isOpenPair-8839" id="sb_clock_8839"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="169" data-pair_id="169" data-external="sbcharts.investing.com" data-pair_url="/indices/us-30-chart" id="sb_pair_169" chart_link="/indices/us-30-chart" data-chart-hash="d5f35207ae3dfc721a517a541dbba7d8"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/indices/us-30" title="Dow Jones Industrial Average"  data-gae='sb_indices-us-30' >Dow 30</a></td><td class="lastNum pid-169-last" id="sb_last_169">26.458,31</td><td class="chg greenFont pid-169-pc" id="sb_change_169">+18,38</td><td class="chgPer greenFont pid-169-pcp" id="sb_changepc_169">+0,07%</td><td class="icon"><span class="redClockIcon isOpenExch-1" id="sb_clock_169"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="172" data-pair_id="172" data-external="sbcharts.investing.com" data-pair_url="/indices/germany-30-chart" id="sb_pair_172" chart_link="/indices/germany-30-chart" data-chart-hash="20b3a6a97fa3088df002fbc8bbcbecbd"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/indices/germany-30" title="DAX (CFD)"  data-gae='sb_indices-germany-30' >DAX</a></td><td class="lastNum pid-172-last" id="sb_last_172">12.246,73</td><td class="chg redFont pid-172-pc" id="sb_change_172">-188,86</td><td class="chgPer redFont pid-172-pcp" id="sb_changepc_172">-1,52%</td><td class="icon"><span class="redClockIcon isOpenExch-4" id="sb_clock_172"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="27" data-pair_id="27" data-external="sbcharts.investing.com" data-pair_url="/indices/uk-100-chart" id="sb_pair_27" chart_link="/indices/uk-100-chart" data-chart-hash="1d2984fb05d560efab6535929b9749b6"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/indices/uk-100" title="FTSE 100 (CFD)"  data-gae='sb_indices-uk-100' >FTSE 100</a></td><td class="lastNum pid-27-last" id="sb_last_27">7.510,20</td><td class="chg redFont pid-27-pc" id="sb_change_27">-35,24</td><td class="chgPer redFont pid-27-pcp" id="sb_changepc_27">-0,47%</td><td class="icon"><span class="redClockIcon isOpenExch-3" id="sb_clock_27"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="8827" data-pair_id="8827" data-external="sbcharts.investing.com" data-pair_url="/quotes/us-dollar-index-streaming-chart" id="sb_pair_8827" chart_link="/quotes/us-dollar-index-streaming-chart" data-chart-hash="06620ff894868f010dafb51493836e71"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/quotes/us-dollar-index" title="Фьючерс на индекс USD (F) (CFD)"  data-gae='sb_indices-us-dollar-index' >Индекс USD</a></td><td class="lastNum pid-8827-last" id="sb_last_8827">94,82</td><td class="chg greenFont pid-8827-pc" id="sb_change_8827">+0,02</td><td class="chgPer greenFont pid-8827-pcp" id="sb_changepc_8827">+0,02%</td><td class="icon"><span class="greenClockIcon isOpenPair-8827" id="sb_clock_8827"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer last" pair="1055949" data-pair_id="1055949" data-external="sbcharts.investing.com" data-pair_url="/crypto/bitcoin/bitcoin-futures-streaming-chart" id="sb_pair_1055949" chart_link="/crypto/bitcoin/bitcoin-futures-streaming-chart" data-chart-hash="725b028dd90d739e762616e8d32a06ba"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/crypto/bitcoin/bitcoin-futures" title="Фьючерс CME на Bitcoin (F) (CFD)"  data-gae='sb_indices-bitcoin-futures' >Фьючерс на Bitcoin</a></td><td class="lastNum pid-1055949-last" id="sb_last_1055949">6.570,0</td><td class="chg redFont pid-1055949-pc" id="sb_change_1055949">-45,0</td><td class="chgPer redFont pid-1055949-pcp" id="sb_changepc_1055949">-0,68%</td><td class="icon"><span class="greenClockIcon isOpenExch-1004" id="sb_clock_1055949"></span></td><td class="sideColumn">&nbsp;</td></tr></tbody></table><table id="QBS_7_inner" class="displayNone genTbl openTbl quotesSideBlockTbl collapsedTbl"><tbody><tr class="LeftLiContainer Selected" pair="945629" data-pair_id="945629" data-external="sbcharts.investing.com" data-pair_url="/crypto/bitcoin/btc-usd-chart" id="sb_pair_945629" chart_link="/crypto/bitcoin/btc-usd-chart" data-chart-hash="980ef075ac6b87dd9f940f398b9bcbf8"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/crypto/bitcoin/btc-usd" title="BTC/USD - Биткойн Доллар США"  data-gae='-btc-usd' >BTC/USD</a></td><td class="lastNum pid-945629-last" id="sb_last_945629">6.612,1</td><td class="chg greenFont pid-945629-pc" id="sb_change_945629">+36,0</td><td class="chgPer greenFont pid-945629-pcp" id="sb_changepc_945629">+0,55%</td><td class="icon"><span class="greenClockIcon isOpenExch-1014" id="sb_clock_945629"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="997650" data-pair_id="997650" data-external="sbcharts.investing.com" data-pair_url="/crypto/ethereum/eth-usd-chart" id="sb_pair_997650" chart_link="/crypto/ethereum/eth-usd-chart" data-chart-hash="f1d1f100714c475a9df5f6ee74338791"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/crypto/ethereum/eth-usd" title="ETH/USD - Эфириум Доллар США"  data-gae='-eth-usd' >ETH/USD</a></td><td class="lastNum pid-997650-last" id="sb_last_997650">229,68</td><td class="chg redFont pid-997650-pc" id="sb_change_997650">-2,53</td><td class="chgPer redFont pid-997650-pcp" id="sb_changepc_997650">-1,09%</td><td class="icon"><span class="greenClockIcon isOpenExch-1014" id="sb_clock_997650"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="1031333" data-pair_id="1031333" data-external="sbcharts.investing.com" data-pair_url="/crypto/bitcoin-cash/bch-usd-chart" id="sb_pair_1031333" chart_link="/crypto/bitcoin-cash/bch-usd-chart" data-chart-hash="9b37b549c305bd9c6240e4fc8f477f5d"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/crypto/bitcoin-cash/bch-usd" title="BCH/USD - Bitcoin Cash Доллар США"  data-gae='-bch-usd' >BCH/USD</a></td><td class="lastNum pid-1031333-last" id="sb_last_1031333">534,19</td><td class="chg redFont pid-1031333-pc" id="sb_change_1031333">-1,71</td><td class="chgPer redFont pid-1031333-pcp" id="sb_changepc_1031333">-0,32%</td><td class="icon"><span class="greenClockIcon isOpenExch-1014" id="sb_clock_1031333"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="1010798" data-pair_id="1010798" data-external="sbcharts.investing.com" data-pair_url="/crypto/litecoin/ltc-usd-chart" id="sb_pair_1010798" chart_link="/crypto/litecoin/ltc-usd-chart" data-chart-hash="898e0191a596d14e3ac23c545ba0aa90"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/crypto/litecoin/ltc-usd" title="LTC/USD - Лайткоин Доллар США"  data-gae='-ltc-usd' >LTC/USD</a></td><td class="lastNum pid-1010798-last" id="sb_last_1010798">61,040</td><td class="chg greenFont pid-1010798-pc" id="sb_change_1010798">+0,380</td><td class="chgPer greenFont pid-1010798-pcp" id="sb_changepc_1010798">+0,63%</td><td class="icon"><span class="greenClockIcon isOpenExch-1016" id="sb_clock_1010798"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="1031068" data-pair_id="1031068" data-external="sbcharts.investing.com" data-pair_url="/crypto/iota/iota-usd-chart" id="sb_pair_1031068" chart_link="/crypto/iota/iota-usd-chart" data-chart-hash="547fd5173f147c0a19966be83152910c"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/crypto/iota/iota-usd" title="IOTA/USD - IOTA Доллар США"  data-gae='-iota-usd' >IOT/USD</a></td><td class="lastNum pid-1031068-last" id="sb_last_1031068">0,56830</td><td class="chg greenFont pid-1031068-pc" id="sb_change_1031068">+0,00453</td><td class="chgPer greenFont pid-1031068-pcp" id="sb_changepc_1031068">+0,80%</td><td class="icon"><span class="greenClockIcon isOpenExch-1014" id="sb_clock_1031068"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="1031678" data-pair_id="1031678" data-external="sbcharts.investing.com" data-pair_url="/crypto/bitcoin/btc-rub-chart" id="sb_pair_1031678" chart_link="/crypto/bitcoin/btc-rub-chart" data-chart-hash="8acda036134b4c8e0c6dc4b4afcb5021"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/crypto/bitcoin/btc-rub" title="BTC/RUB - Биткойн Российский рубль"  data-gae='-btc-rub' >BTC/RUB</a></td><td class="lastNum pid-1031678-last" id="sb_last_1031678">441.535</td><td class="chg greenFont pid-1031678-pc" id="sb_change_1031678">+4.075</td><td class="chgPer greenFont pid-1031678-pcp" id="sb_changepc_1031678">+0,93%</td><td class="icon"><span class="greenClockIcon isOpenExch-1029" id="sb_clock_1031678"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer" pair="1010782" data-pair_id="1010782" data-external="sbcharts.investing.com" data-pair_url="/crypto/ripple/xrp-usd-chart" id="sb_pair_1010782" chart_link="/crypto/ripple/xrp-usd-chart" data-chart-hash="3a1c1287203a98d5ff2e5514d3b2126a"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/crypto/ripple/xrp-usd" title="XRP/USD - Рипл Доллар США"  data-gae='-xrp-usd' >XRP/USD</a></td><td class="lastNum pid-1010782-last" id="sb_last_1010782">0,57800</td><td class="chg greenFont pid-1010782-pc" id="sb_change_1010782">+0,00510</td><td class="chgPer greenFont pid-1010782-pcp" id="sb_changepc_1010782">+0,89%</td><td class="icon"><span class="greenClockIcon isOpenExch-1015" id="sb_clock_1010782"></span></td><td class="sideColumn">&nbsp;</td></tr><tr class="LeftLiContainer last" pair="1010785" data-pair_id="1010785" data-external="sbcharts.investing.com" data-pair_url="/crypto/dash/dash-usd-chart" id="sb_pair_1010785" chart_link="/crypto/dash/dash-usd-chart" data-chart-hash="ea3c96e6853fe63956763735b8f09dcd"><td class="sideColumn">&nbsp;</td><td class="left bold first noWrap"><a href="/crypto/dash/dash-usd" title="DASH/USD - Дэш Доллар США"  data-gae='-dash-usd' >DASH/USD</a></td><td class="lastNum pid-1010785-last" id="sb_last_1010785">187,43</td><td class="chg greenFont pid-1010785-pc" id="sb_change_1010785">+1,22</td><td class="chgPer greenFont pid-1010785-pcp" id="sb_changepc_1010785">+0,66%</td><td class="icon"><span class="greenClockIcon isOpenExch-1015" id="sb_clock_1010785"></span></td><td class="sideColumn">&nbsp;</td></tr></tbody></table></div></div></div> <!-- /sideBanner --><div class="tradeNowRightColumn"><a href="javascript:void(0);" data-link="/jp.php?v2=OHg3aWE2YTg2Y2hsZzc5PDFkMmxmYjI4MCdnNTU_YS9jJzUyZTdkNGVsb2xmKDcvZyUwPTFlYDA2PWduMzUyYDgrNydhOmE8NmBoZGc9OTgxdjIpZmw=" data-section="sideblock_bottom" data-tradenowid="228537" title="&#1053;&#1072;&#1095;&#1085;&#1080;&#1090;&#1077; &#1090;&#1086;&#1088;&#1075;&#1086;&#1074;&#1072;&#1090;&#1100; &#1089; Forex Club" data-color="Orange2" data-text="&#1058;&#1086;&#1088;&#1075;&#1086;&#1074;&#1072;&#1090;&#1100; &#1089;&#1077;&#1081;&#1095;&#1072;&#1089;" class="newBtn noIcon tradenowBtn js-tradenow-btn Orange2">Торговать сейчас</a>    <div class="clear"></div>
</div><div class="clear " id="ad1"><div id='div-gpt-ad-1370187203350-sb1' style='margin-top:18px;' ></div></div> <!-- /sideBanner -->	<div class="toolIframeBox rightBoxes techSummaryTool js-techsum-main-wrapper" id="TechSummary">
<div id="TSB_tabsMenu">
<div class="TabContainer noOverflow js-tab-container">
<span class="newSiteIconsSprite arrow"></span>
<ul class="tabsForBox oneLvlTabsWidget js-one-level-tabs tabsForBox js-tabs-container" id="TSB_topTabs">

<li id=TSB_1 class="first selected" data-tab-id=1><a href="javascript:void(0);">Форекс</a></li>

<li id=TSB_2 data-tab-id=2><a href="javascript:void(0);">Товары</a></li>

<li id=TSB_3 data-tab-id=3><a href="javascript:void(0);">Индексы</a></li>

<li id=TSB_4 class="last" data-tab-id=4><a href="javascript:void(0);">Крипто</a></li>

</ul>
<span id="lastDummyLI" data-more-cat="Другие категории" class="moreTabOneLvlDummy displayNone"><a href="javascript:void();">Другие категории</a><span class="showMoreDownArrow"></span></span>
</div>

<script>
$(document).ready(function(){
tabsWidgetOnelevel('', 'storedSiteBlockTechnicalBoxTabObject', 'TSB_topTabs', +'');
});
</script>		</div>
<div id="TSB_ChartWrp" rel="" data-chart_type="area" data-config_fontsize="9px" data-config_ymaxmultiplier="0" class="tabsBoxBottom techSummaryWrap js-box-content">
<span class="pointer threeDotsIconSmall genToolTip oneliner js-three-dots" data-tooltip="Настройка вкладок"></span>
<div id="techsum" class="tooltipPopup quotesTabsSelection js-popup-wrapper displayNone">
<div class="header"> Выбор вкладок </div>
<div class="content ui-sortable rowsWrap js-sideblock-tab-selection">
<label class="addRow js-ts-label" data-tab-id="1"><span class="checkers"></span><input type="checkbox" class="checkbox"checked >Форекс</label><label class="addRow js-ts-label" data-tab-id="2"><span class="checkers"></span><input type="checkbox" class="checkbox"checked >Товары</label><label class="addRow js-ts-label" data-tab-id="3"><span class="checkers"></span><input type="checkbox" class="checkbox"checked >Индексы</label><label class="addRow js-ts-label" data-tab-id="4"><span class="checkers"></span><input type="checkbox" class="checkbox"checked >Крипто</label><label class="addRow js-ts-label" data-tab-id="5"><span class="checkers"></span><input type="checkbox" class="checkbox">Акции</label><label class="addRow js-ts-label" data-tab-id="6"><span class="checkers"></span><input type="checkbox" class="checkbox">Облигации</label><label class="addRow js-ts-label" data-tab-id="7"><span class="checkers"></span><input type="checkbox" class="checkbox">ETF</label>				</div>
<div class="footer"><span class="js-tab-selection-counter"> Выбрано: %COUNT%/4</span><a href="javascript:void(0);" id="applyTechSumOrder" class="newBtn Orange2 noIcon js-tab-selection-apply"> Обновить</a></div>
</div>
<div class="dataBox timeframe js-timeframe">
<div class="float_lang_base_1" >Интервалы                    <select id="TSB_timeframe">


<option  value="60">1 мин</option>

<option selected="selected" value="300">5 мин</option>

<option  value="900">15 мин</option>

<option  value="1800">30 мин</option>

<option  value="3600">1 час</option>

<option  value="18000">5 часов</option>

<option  value="86400">1 день</option>

<option  value="week">1 неделя</option>

</select>
</div>
<div class="float_lang_base_2 dateFont" id="TSB_updateTime"></div>
<div class="clear"></div>
</div>
<div class="topBox">
<div class="loading-responsive medium-circle displayNone" id="TSB__loader"></div>
<div id="TSB__mainSummary">
<div id="TSB_arrowBox" class="arrowBox">
<span id="TSB__arrowBoxSpan" class="newSiteIconsSprite downArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name" class="arial_14 bold" title="EUR/USD - Евро Доллар США"><a href="/currencies/eur-usd" class="js-item-name">EUR/USD</a></p>
<p id="TSB__summary_last" class="arial_24 bold inlineblock js-item-last pid--last_nColor">1,1593</p>
<p id="TSB__summary_change" class="change js-item-change redFont">
<span id="TSB__summary_change_value" class="js-change-value pid--pc">-0,0011</span> <span id="TSB__summary_change_percent" class="js-change-percent pid--pcp parentheses">-0,09%</span>
</p>
</div>

<div id="TSB__data_summary" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval sell arial_12 bold float_lang_base_2" id="TSB__technical_summary">Активно продавать</span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy" data-counter-type="buy" class="parentheses">2</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell" data-counter-type="sell" class="parentheses">10</span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy" data-counter-type="buy" class="parentheses">2</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell" data-counter-type="sell" class="parentheses">6</span></p>
<div class="clear"></div>
</div>                </div>
</div>
<div id="TSB_main_summary_1" data-pairid="1" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_1" class="arrowBox">
<span id="TSB__arrowBoxSpan_1" class="newSiteIconsSprite downArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_1" class="arial_14 bold" title="EUR/USD - Евро Доллар США"><a href="/currencies/eur-usd" class="js-item-name">EUR/USD</a></p>
<p id="TSB__summary_last_1" class="arial_24 bold inlineblock js-item-last pid-1-last_nColor">1,1593</p>
<p id="TSB__summary_change_1" class="change js-item-change redFont">
<span id="TSB__summary_change_value_1" class="js-change-value pid-1-pc">-0,0011</span> <span id="TSB__summary_change_percent_1" class="js-change-percent pid-1-pcp parentheses">-0,09%</span>
</p>
</div>

<div id="TSB__data_summary_1" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval sell arial_12 bold float_lang_base_2" id="TSB__technical_summary_1">Активно продавать</span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_1" data-counter-type="buy" class="parentheses">2</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_1" data-counter-type="sell" class="parentheses">10</span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_1" data-counter-type="buy" class="parentheses">2</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_1" data-counter-type="sell" class="parentheses">6</span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_1691" data-pairid="1691" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_1691" class="arrowBox">
<span id="TSB__arrowBoxSpan_1691" class="newSiteIconsSprite downArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_1691" class="arial_14 bold" title="EUR/RUB - Евро Российский рубль"><a href="/currencies/eur-rub" class="js-item-name">EUR/RUB</a></p>
<p id="TSB__summary_last_1691" class="arial_24 bold inlineblock js-item-last pid-1691-last_nColor">76,0865</p>
<p id="TSB__summary_change_1691" class="change js-item-change redFont">
<span id="TSB__summary_change_value_1691" class="js-change-value pid-1691-pc">0,0000</span> <span id="TSB__summary_change_percent_1691" class="js-change-percent pid-1691-pcp parentheses">0,00%</span>
</p>
</div>

<div id="TSB__data_summary_1691" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval buy arial_12 bold float_lang_base_2" id="TSB__technical_summary_1691">Активно покупать</span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_1691" data-counter-type="buy" class="parentheses">10</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_1691" data-counter-type="sell" class="parentheses">2</span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_1691" data-counter-type="buy" class="parentheses">7</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_1691" data-counter-type="sell" class="parentheses">1</span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_2186" data-pairid="2186" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_2186" class="arrowBox">
<span id="TSB__arrowBoxSpan_2186" class="newSiteIconsSprite downArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_2186" class="arial_14 bold" title="USD/RUB - Доллар США Российский рубль"><a href="/currencies/usd-rub" class="js-item-name">USD/RUB</a></p>
<p id="TSB__summary_last_2186" class="arial_24 bold inlineblock js-item-last pid-2186-last_nColor">65,5503</p>
<p id="TSB__summary_change_2186" class="change js-item-change redFont">
<span id="TSB__summary_change_value_2186" class="js-change-value pid-2186-pc">0,0000</span> <span id="TSB__summary_change_percent_2186" class="js-change-percent pid-2186-pcp parentheses">0,00%</span>
</p>
</div>

<div id="TSB__data_summary_2186" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval buy arial_12 bold float_lang_base_2" id="TSB__technical_summary_2186">Активно покупать</span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_2186" data-counter-type="buy" class="parentheses">9</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_2186" data-counter-type="sell" class="parentheses">3</span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_2186" data-counter-type="buy" class="parentheses">9</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_2186" data-counter-type="sell" class="parentheses">0</span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_2" data-pairid="2" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_2" class="arrowBox">
<span id="TSB__arrowBoxSpan_2" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_2" class="arial_14 bold" title="GBP/USD - Британский фунт Доллар США"><a href="/currencies/gbp-usd" class="js-item-name">GBP/USD</a></p>
<p id="TSB__summary_last_2" class="arial_24 bold inlineblock js-item-last pid-2-last_nColor">1,3035</p>
<p id="TSB__summary_change_2" class="change js-item-change greenFont">
<span id="TSB__summary_change_value_2" class="js-change-value pid-2-pc">+0,0004</span> <span id="TSB__summary_change_percent_2" class="js-change-percent pid-2-pcp parentheses">+0,03%</span>
</p>
</div>

<div id="TSB__data_summary_2" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval buy arial_12 bold float_lang_base_2" id="TSB__technical_summary_2">Активно покупать</span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_2" data-counter-type="buy" class="parentheses">11</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_2" data-counter-type="sell" class="parentheses">1</span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_2" data-counter-type="buy" class="parentheses">5</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_2" data-counter-type="sell" class="parentheses">0</span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_3" data-pairid="3" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_3" class="arrowBox">
<span id="TSB__arrowBoxSpan_3" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_3" class="arial_14 bold" title="USD/JPY - Доллар США Японская иена"><a href="/currencies/usd-jpy" class="js-item-name">USD/JPY</a></p>
<p id="TSB__summary_last_3" class="arial_24 bold inlineblock js-item-last pid-3-last_nColor">113,94</p>
<p id="TSB__summary_change_3" class="change js-item-change greenFont">
<span id="TSB__summary_change_value_3" class="js-change-value pid-3-pc">+0,24</span> <span id="TSB__summary_change_percent_3" class="js-change-percent pid-3-pcp parentheses">+0,21%</span>
</p>
</div>

<div id="TSB__data_summary_3" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval buy arial_12 bold float_lang_base_2" id="TSB__technical_summary_3">Активно покупать</span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_3" data-counter-type="buy" class="parentheses">12</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_3" data-counter-type="sell" class="parentheses">0</span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_3" data-counter-type="buy" class="parentheses">6</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_3" data-counter-type="sell" class="parentheses">0</span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_9" data-pairid="9" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_9" class="arrowBox">
<span id="TSB__arrowBoxSpan_9" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_9" class="arial_14 bold" title="EUR/JPY - Евро Японская иена"><a href="/currencies/eur-jpy" class="js-item-name">EUR/JPY</a></p>
<p id="TSB__summary_last_9" class="arial_24 bold inlineblock js-item-last pid-9-last_nColor">132,09</p>
<p id="TSB__summary_change_9" class="change js-item-change greenFont">
<span id="TSB__summary_change_value_9" class="js-change-value pid-9-pc">+0,15</span> <span id="TSB__summary_change_percent_9" class="js-change-percent pid-9-pcp parentheses">+0,11%</span>
</p>
</div>

<div id="TSB__data_summary_9" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_9">Нейтрально</span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_9" data-counter-type="buy" class="parentheses">9</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_9" data-counter-type="sell" class="parentheses">3</span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_9" data-counter-type="buy" class="parentheses">3</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_9" data-counter-type="sell" class="parentheses">3</span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_5" data-pairid="5" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_5" class="arrowBox">
<span id="TSB__arrowBoxSpan_5" class="newSiteIconsSprite downArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_5" class="arial_14 bold" title="AUD/USD - Австралийский доллар Доллар США"><a href="/currencies/aud-usd" class="js-item-name">AUD/USD</a></p>
<p id="TSB__summary_last_5" class="arial_24 bold inlineblock js-item-last pid-5-last_nColor">0,7213</p>
<p id="TSB__summary_change_5" class="change js-item-change redFont">
<span id="TSB__summary_change_value_5" class="js-change-value pid-5-pc">-0,0009</span> <span id="TSB__summary_change_percent_5" class="js-change-percent pid-5-pcp parentheses">-0,12%</span>
</p>
</div>

<div id="TSB__data_summary_5" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_5">Нейтрально</span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_5" data-counter-type="buy" class="parentheses">6</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_5" data-counter-type="sell" class="parentheses">6</span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_5" data-counter-type="buy" class="parentheses">5</span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_5" data-counter-type="sell" class="parentheses">2</span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8830" data-pairid="8830" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8830" class="arrowBox">
<span id="TSB__arrowBoxSpan_8830" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8830" class="arial_14 bold" title="Фьючерс на золото"><a href="/commodities/gold" class="js-item-name">Золото</a></p>
<p id="TSB__summary_last_8830" class="arial_24 bold inlineblock js-item-last pid-8830-last_nColor"></p>
<p id="TSB__summary_change_8830" class="change js-item-change Font">
<span id="TSB__summary_change_value_8830" class="js-change-value pid-8830-pc"></span> <span id="TSB__summary_change_percent_8830" class="js-change-percent pid-8830-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8830" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8830"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8830" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8830" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8830" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8830" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8836" data-pairid="8836" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8836" class="arrowBox">
<span id="TSB__arrowBoxSpan_8836" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8836" class="arial_14 bold" title="Фьючерс на серебро"><a href="/commodities/silver" class="js-item-name">Серебро</a></p>
<p id="TSB__summary_last_8836" class="arial_24 bold inlineblock js-item-last pid-8836-last_nColor"></p>
<p id="TSB__summary_change_8836" class="change js-item-change Font">
<span id="TSB__summary_change_value_8836" class="js-change-value pid-8836-pc"></span> <span id="TSB__summary_change_percent_8836" class="js-change-percent pid-8836-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8836" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8836"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8836" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8836" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8836" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8836" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8833" data-pairid="8833" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8833" class="arrowBox">
<span id="TSB__arrowBoxSpan_8833" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8833" class="arial_14 bold" title="Фьючерс на нефть Brent"><a href="/commodities/brent-oil" class="js-item-name">Нефть Brent</a></p>
<p id="TSB__summary_last_8833" class="arial_24 bold inlineblock js-item-last pid-8833-last_nColor"></p>
<p id="TSB__summary_change_8833" class="change js-item-change Font">
<span id="TSB__summary_change_value_8833" class="js-change-value pid-8833-pc"></span> <span id="TSB__summary_change_percent_8833" class="js-change-percent pid-8833-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8833" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8833"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8833" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8833" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8833" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8833" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8849" data-pairid="8849" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8849" class="arrowBox">
<span id="TSB__arrowBoxSpan_8849" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8849" class="arial_14 bold" title="Фьючерсы на нефть WTI"><a href="/commodities/crude-oil" class="js-item-name">Нефть WTI</a></p>
<p id="TSB__summary_last_8849" class="arial_24 bold inlineblock js-item-last pid-8849-last_nColor"></p>
<p id="TSB__summary_change_8849" class="change js-item-change Font">
<span id="TSB__summary_change_value_8849" class="js-change-value pid-8849-pc"></span> <span id="TSB__summary_change_percent_8849" class="js-change-percent pid-8849-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8849" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8849"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8849" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8849" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8849" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8849" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8883" data-pairid="8883" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8883" class="arrowBox">
<span id="TSB__arrowBoxSpan_8883" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8883" class="arial_14 bold" title="Фьючерс на палладий"><a href="/commodities/palladium" class="js-item-name">Палладий</a></p>
<p id="TSB__summary_last_8883" class="arial_24 bold inlineblock js-item-last pid-8883-last_nColor"></p>
<p id="TSB__summary_change_8883" class="change js-item-change Font">
<span id="TSB__summary_change_value_8883" class="js-change-value pid-8883-pc"></span> <span id="TSB__summary_change_percent_8883" class="js-change-percent pid-8883-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8883" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8883"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8883" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8883" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8883" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8883" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8831" data-pairid="8831" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8831" class="arrowBox">
<span id="TSB__arrowBoxSpan_8831" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8831" class="arial_14 bold" title="Фьючерс на медь"><a href="/commodities/copper" class="js-item-name">Медь</a></p>
<p id="TSB__summary_last_8831" class="arial_24 bold inlineblock js-item-last pid-8831-last_nColor"></p>
<p id="TSB__summary_change_8831" class="change js-item-change Font">
<span id="TSB__summary_change_value_8831" class="js-change-value pid-8831-pc"></span> <span id="TSB__summary_change_percent_8831" class="js-change-percent pid-8831-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8831" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8831"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8831" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8831" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8831" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8831" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8910" data-pairid="8910" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8910" class="arrowBox">
<span id="TSB__arrowBoxSpan_8910" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8910" class="arial_14 bold" title="Фьючерс на платину"><a href="/commodities/platinum" class="js-item-name">Платина</a></p>
<p id="TSB__summary_last_8910" class="arial_24 bold inlineblock js-item-last pid-8910-last_nColor"></p>
<p id="TSB__summary_change_8910" class="change js-item-change Font">
<span id="TSB__summary_change_value_8910" class="js-change-value pid-8910-pc"></span> <span id="TSB__summary_change_percent_8910" class="js-change-percent pid-8910-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8910" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8910"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8910" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8910" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8910" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8910" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_13665" data-pairid="13665" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_13665" class="arrowBox">
<span id="TSB__arrowBoxSpan_13665" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_13665" class="arial_14 bold" title="РТС (Москва)"><a href="/indices/rtsi" class="js-item-name">РТС</a></p>
<p id="TSB__summary_last_13665" class="arial_24 bold inlineblock js-item-last pid-13665-last_nColor"></p>
<p id="TSB__summary_change_13665" class="change js-item-change Font">
<span id="TSB__summary_change_value_13665" class="js-change-value pid-13665-pc"></span> <span id="TSB__summary_change_percent_13665" class="js-change-percent pid-13665-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_13665" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_13665"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_13665" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_13665" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_13665" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_13665" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_13666" data-pairid="13666" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_13666" class="arrowBox">
<span id="TSB__arrowBoxSpan_13666" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_13666" class="arial_14 bold" title="Индекс МосБиржи (Москва)"><a href="/indices/mcx" class="js-item-name">Индекс МосБиржи</a></p>
<p id="TSB__summary_last_13666" class="arial_24 bold inlineblock js-item-last pid-13666-last_nColor"></p>
<p id="TSB__summary_change_13666" class="change js-item-change Font">
<span id="TSB__summary_change_value_13666" class="js-change-value pid-13666-pc"></span> <span id="TSB__summary_change_percent_13666" class="js-change-percent pid-13666-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_13666" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_13666"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_13666" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_13666" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_13666" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_13666" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_172" data-pairid="172" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_172" class="arrowBox">
<span id="TSB__arrowBoxSpan_172" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_172" class="arial_14 bold" title="DAX (Xetra)"><a href="/indices/germany-30" class="js-item-name">DAX</a></p>
<p id="TSB__summary_last_172" class="arial_24 bold inlineblock js-item-last pid-172-last_nColor"></p>
<p id="TSB__summary_change_172" class="change js-item-change Font">
<span id="TSB__summary_change_value_172" class="js-change-value pid-172-pc"></span> <span id="TSB__summary_change_percent_172" class="js-change-percent pid-172-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_172" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_172"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_172" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_172" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_172" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_172" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_27" data-pairid="27" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_27" class="arrowBox">
<span id="TSB__arrowBoxSpan_27" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_27" class="arial_14 bold" title="FTSE 100 (Лондон)"><a href="/indices/uk-100" class="js-item-name">FTSE 100</a></p>
<p id="TSB__summary_last_27" class="arial_24 bold inlineblock js-item-last pid-27-last_nColor"></p>
<p id="TSB__summary_change_27" class="change js-item-change Font">
<span id="TSB__summary_change_value_27" class="js-change-value pid-27-pc"></span> <span id="TSB__summary_change_percent_27" class="js-change-percent pid-27-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_27" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_27"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_27" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_27" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_27" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_27" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_166" data-pairid="166" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_166" class="arrowBox">
<span id="TSB__arrowBoxSpan_166" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_166" class="arial_14 bold" title="S&P 500 (Нью-Йорк)"><a href="/indices/us-spx-500" class="js-item-name">S&P 500</a></p>
<p id="TSB__summary_last_166" class="arial_24 bold inlineblock js-item-last pid-166-last_nColor"></p>
<p id="TSB__summary_change_166" class="change js-item-change Font">
<span id="TSB__summary_change_value_166" class="js-change-value pid-166-pc"></span> <span id="TSB__summary_change_percent_166" class="js-change-percent pid-166-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_166" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_166"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_166" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_166" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_166" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_166" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_169" data-pairid="169" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_169" class="arrowBox">
<span id="TSB__arrowBoxSpan_169" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_169" class="arial_14 bold" title="Dow Jones Industrial Average (Нью-Йорк)"><a href="/indices/us-30" class="js-item-name">Dow 30</a></p>
<p id="TSB__summary_last_169" class="arial_24 bold inlineblock js-item-last pid-169-last_nColor"></p>
<p id="TSB__summary_change_169" class="change js-item-change Font">
<span id="TSB__summary_change_value_169" class="js-change-value pid-169-pc"></span> <span id="TSB__summary_change_percent_169" class="js-change-percent pid-169-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_169" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_169"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_169" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_169" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_169" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_169" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_178" data-pairid="178" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_178" class="arrowBox">
<span id="TSB__arrowBoxSpan_178" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_178" class="arial_14 bold" title="Nikkei 225 (Токио)"><a href="/indices/japan-ni225" class="js-item-name">Nikkei 225</a></p>
<p id="TSB__summary_last_178" class="arial_24 bold inlineblock js-item-last pid-178-last_nColor"></p>
<p id="TSB__summary_change_178" class="change js-item-change Font">
<span id="TSB__summary_change_value_178" class="js-change-value pid-178-pc"></span> <span id="TSB__summary_change_percent_178" class="js-change-percent pid-178-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_178" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_178"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_178" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_178" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_178" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_178" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_945629" data-pairid="945629" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_945629" class="arrowBox">
<span id="TSB__arrowBoxSpan_945629" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_945629" class="arial_14 bold" title="BTC/USD - Биткойн Доллар США"><a href="/crypto/bitcoin/btc-usd" class="js-item-name">BTC/USD</a></p>
<p id="TSB__summary_last_945629" class="arial_24 bold inlineblock js-item-last pid-945629-last_nColor"></p>
<p id="TSB__summary_change_945629" class="change js-item-change Font">
<span id="TSB__summary_change_value_945629" class="js-change-value pid-945629-pc"></span> <span id="TSB__summary_change_percent_945629" class="js-change-percent pid-945629-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_945629" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_945629"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_945629" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_945629" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_945629" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_945629" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_997650" data-pairid="997650" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_997650" class="arrowBox">
<span id="TSB__arrowBoxSpan_997650" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_997650" class="arial_14 bold" title="ETH/USD - Эфириум Доллар США"><a href="/crypto/ethereum/eth-usd" class="js-item-name">ETH/USD</a></p>
<p id="TSB__summary_last_997650" class="arial_24 bold inlineblock js-item-last pid-997650-last_nColor"></p>
<p id="TSB__summary_change_997650" class="change js-item-change Font">
<span id="TSB__summary_change_value_997650" class="js-change-value pid-997650-pc"></span> <span id="TSB__summary_change_percent_997650" class="js-change-percent pid-997650-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_997650" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_997650"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_997650" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_997650" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_997650" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_997650" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_1031333" data-pairid="1031333" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_1031333" class="arrowBox">
<span id="TSB__arrowBoxSpan_1031333" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_1031333" class="arial_14 bold" title="BCH/USD - Bitcoin Cash Доллар США"><a href="/crypto/bitcoin-cash/bch-usd" class="js-item-name">BCH/USD</a></p>
<p id="TSB__summary_last_1031333" class="arial_24 bold inlineblock js-item-last pid-1031333-last_nColor"></p>
<p id="TSB__summary_change_1031333" class="change js-item-change Font">
<span id="TSB__summary_change_value_1031333" class="js-change-value pid-1031333-pc"></span> <span id="TSB__summary_change_percent_1031333" class="js-change-percent pid-1031333-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_1031333" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_1031333"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_1031333" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_1031333" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_1031333" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_1031333" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_1010798" data-pairid="1010798" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_1010798" class="arrowBox">
<span id="TSB__arrowBoxSpan_1010798" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_1010798" class="arial_14 bold" title="LTC/USD - Лайткоин Доллар США"><a href="/crypto/litecoin/ltc-usd" class="js-item-name">LTC/USD</a></p>
<p id="TSB__summary_last_1010798" class="arial_24 bold inlineblock js-item-last pid-1010798-last_nColor"></p>
<p id="TSB__summary_change_1010798" class="change js-item-change Font">
<span id="TSB__summary_change_value_1010798" class="js-change-value pid-1010798-pc"></span> <span id="TSB__summary_change_percent_1010798" class="js-change-percent pid-1010798-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_1010798" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_1010798"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_1010798" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_1010798" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_1010798" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_1010798" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_1031068" data-pairid="1031068" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_1031068" class="arrowBox">
<span id="TSB__arrowBoxSpan_1031068" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_1031068" class="arial_14 bold" title="IOTA/USD - IOTA Доллар США"><a href="/crypto/iota/iota-usd" class="js-item-name">IOTA/USD</a></p>
<p id="TSB__summary_last_1031068" class="arial_24 bold inlineblock js-item-last pid-1031068-last_nColor"></p>
<p id="TSB__summary_change_1031068" class="change js-item-change Font">
<span id="TSB__summary_change_value_1031068" class="js-change-value pid-1031068-pc"></span> <span id="TSB__summary_change_percent_1031068" class="js-change-percent pid-1031068-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_1031068" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_1031068"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_1031068" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_1031068" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_1031068" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_1031068" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_1031678" data-pairid="1031678" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_1031678" class="arrowBox">
<span id="TSB__arrowBoxSpan_1031678" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_1031678" class="arial_14 bold" title="BTC/RUB - Биткойн Российский рубль"><a href="/crypto/bitcoin/btc-rub" class="js-item-name">BTC/RUB</a></p>
<p id="TSB__summary_last_1031678" class="arial_24 bold inlineblock js-item-last pid-1031678-last_nColor"></p>
<p id="TSB__summary_change_1031678" class="change js-item-change Font">
<span id="TSB__summary_change_value_1031678" class="js-change-value pid-1031678-pc"></span> <span id="TSB__summary_change_percent_1031678" class="js-change-percent pid-1031678-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_1031678" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_1031678"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_1031678" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_1031678" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_1031678" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_1031678" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_1010782" data-pairid="1010782" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_1010782" class="arrowBox">
<span id="TSB__arrowBoxSpan_1010782" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_1010782" class="arial_14 bold" title="XRP/USD - Рипл Доллар США"><a href="/crypto/ripple/xrp-usd" class="js-item-name">XRP/USD</a></p>
<p id="TSB__summary_last_1010782" class="arial_24 bold inlineblock js-item-last pid-1010782-last_nColor"></p>
<p id="TSB__summary_change_1010782" class="change js-item-change Font">
<span id="TSB__summary_change_value_1010782" class="js-change-value pid-1010782-pc"></span> <span id="TSB__summary_change_percent_1010782" class="js-change-percent pid-1010782-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_1010782" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_1010782"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_1010782" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_1010782" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_1010782" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_1010782" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_1010785" data-pairid="1010785" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_1010785" class="arrowBox">
<span id="TSB__arrowBoxSpan_1010785" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_1010785" class="arial_14 bold" title="DASH/USD - Дэш Доллар США"><a href="/crypto/dash/dash-usd" class="js-item-name">DASH/USD</a></p>
<p id="TSB__summary_last_1010785" class="arial_24 bold inlineblock js-item-last pid-1010785-last_nColor"></p>
<p id="TSB__summary_change_1010785" class="change js-item-change Font">
<span id="TSB__summary_change_value_1010785" class="js-change-value pid-1010785-pc"></span> <span id="TSB__summary_change_percent_1010785" class="js-change-percent pid-1010785-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_1010785" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_1010785"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_1010785" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_1010785" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_1010785" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_1010785" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_252" data-pairid="252" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_252" class="arrowBox">
<span id="TSB__arrowBoxSpan_252" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_252" class="arial_14 bold" title="Microsoft Corporation (NASDAQ)"><a href="/equities/microsoft-corp" class="js-item-name">Microsoft</a></p>
<p id="TSB__summary_last_252" class="arial_24 bold inlineblock js-item-last pid-252-last_nColor"></p>
<p id="TSB__summary_change_252" class="change js-item-change Font">
<span id="TSB__summary_change_value_252" class="js-change-value pid-252-pc"></span> <span id="TSB__summary_change_percent_252" class="js-change-percent pid-252-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_252" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_252"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_252" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_252" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_252" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_252" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_6408" data-pairid="6408" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_6408" class="arrowBox">
<span id="TSB__arrowBoxSpan_6408" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_6408" class="arial_14 bold" title="Apple Inc (NASDAQ)"><a href="/equities/apple-computer-inc" class="js-item-name">Apple</a></p>
<p id="TSB__summary_last_6408" class="arial_24 bold inlineblock js-item-last pid-6408-last_nColor"></p>
<p id="TSB__summary_change_6408" class="change js-item-change Font">
<span id="TSB__summary_change_value_6408" class="js-change-value pid-6408-pc"></span> <span id="TSB__summary_change_percent_6408" class="js-change-percent pid-6408-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_6408" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_6408"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_6408" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_6408" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_6408" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_6408" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_6369" data-pairid="6369" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_6369" class="arrowBox">
<span id="TSB__arrowBoxSpan_6369" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_6369" class="arial_14 bold" title="Alphabet Inc Class A (NASDAQ)"><a href="/equities/google-inc" class="js-item-name">Alphabet A</a></p>
<p id="TSB__summary_last_6369" class="arial_24 bold inlineblock js-item-last pid-6369-last_nColor"></p>
<p id="TSB__summary_change_6369" class="change js-item-change Font">
<span id="TSB__summary_change_value_6369" class="js-change-value pid-6369-pc"></span> <span id="TSB__summary_change_percent_6369" class="js-change-percent pid-6369-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_6369" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_6369"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_6369" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_6369" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_6369" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_6369" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_7888" data-pairid="7888" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_7888" class="arrowBox">
<span id="TSB__arrowBoxSpan_7888" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_7888" class="arial_14 bold" title="Exxon Mobil Corp (Нью-Йорк)"><a href="/equities/exxon-mobil" class="js-item-name">Exxon Mobil</a></p>
<p id="TSB__summary_last_7888" class="arial_24 bold inlineblock js-item-last pid-7888-last_nColor"></p>
<p id="TSB__summary_change_7888" class="change js-item-change Font">
<span id="TSB__summary_change_value_7888" class="js-change-value pid-7888-pc"></span> <span id="TSB__summary_change_percent_7888" class="js-change-percent pid-7888-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_7888" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_7888"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_7888" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_7888" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_7888" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_7888" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_284" data-pairid="284" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_284" class="arrowBox">
<span id="TSB__arrowBoxSpan_284" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_284" class="arial_14 bold" title="BP PLC (Лондон)"><a href="/equities/bp" class="js-item-name">BP</a></p>
<p id="TSB__summary_last_284" class="arial_24 bold inlineblock js-item-last pid-284-last_nColor"></p>
<p id="TSB__summary_change_284" class="change js-item-change Font">
<span id="TSB__summary_change_value_284" class="js-change-value pid-284-pc"></span> <span id="TSB__summary_change_percent_284" class="js-change-percent pid-284-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_284" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_284"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_284" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_284" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_284" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_284" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_9251" data-pairid="9251" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_9251" class="arrowBox">
<span id="TSB__arrowBoxSpan_9251" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_9251" class="arial_14 bold" title="E.ON AG (Xetra)"><a href="/equities/e.on" class="js-item-name">E.ON</a></p>
<p id="TSB__summary_last_9251" class="arial_24 bold inlineblock js-item-last pid-9251-last_nColor"></p>
<p id="TSB__summary_change_9251" class="change js-item-change Font">
<span id="TSB__summary_change_value_9251" class="js-change-value pid-9251-pc"></span> <span id="TSB__summary_change_percent_9251" class="js-change-percent pid-9251-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_9251" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_9251"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_9251" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_9251" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_9251" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_9251" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_396" data-pairid="396" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_396" class="arrowBox">
<span id="TSB__arrowBoxSpan_396" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_396" class="arial_14 bold" title="BNP Paribas SA (Париж)"><a href="/equities/bnp-paribas" class="js-item-name">BNP Paribas</a></p>
<p id="TSB__summary_last_396" class="arial_24 bold inlineblock js-item-last pid-396-last_nColor"></p>
<p id="TSB__summary_change_396" class="change js-item-change Font">
<span id="TSB__summary_change_value_396" class="js-change-value pid-396-pc"></span> <span id="TSB__summary_change_percent_396" class="js-change-percent pid-396-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_396" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_396"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_396" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_396" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_396" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_396" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_23701" data-pairid="23701" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_23701" class="arrowBox">
<span id="TSB__arrowBoxSpan_23701" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_23701" class="arial_14 bold" title="США 2-летние (Нью-Йорк)"><a href="/rates-bonds/u.s.-2-year-bond-yield" class="js-item-name">США 2-летние</a></p>
<p id="TSB__summary_last_23701" class="arial_24 bold inlineblock js-item-last pid-23701-last_nColor"></p>
<p id="TSB__summary_change_23701" class="change js-item-change Font">
<span id="TSB__summary_change_value_23701" class="js-change-value pid-23701-pc"></span> <span id="TSB__summary_change_percent_23701" class="js-change-percent pid-23701-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_23701" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_23701"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_23701" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_23701" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_23701" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_23701" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_23703" data-pairid="23703" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_23703" class="arrowBox">
<span id="TSB__arrowBoxSpan_23703" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_23703" class="arial_14 bold" title="США 5-летние (Нью-Йорк)"><a href="/rates-bonds/u.s.-5-year-bond-yield" class="js-item-name">США 5-летние</a></p>
<p id="TSB__summary_last_23703" class="arial_24 bold inlineblock js-item-last pid-23703-last_nColor"></p>
<p id="TSB__summary_change_23703" class="change js-item-change Font">
<span id="TSB__summary_change_value_23703" class="js-change-value pid-23703-pc"></span> <span id="TSB__summary_change_percent_23703" class="js-change-percent pid-23703-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_23703" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_23703"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_23703" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_23703" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_23703" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_23703" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_23705" data-pairid="23705" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_23705" class="arrowBox">
<span id="TSB__arrowBoxSpan_23705" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_23705" class="arial_14 bold" title="США 10-летние (Нью-Йорк)"><a href="/rates-bonds/u.s.-10-year-bond-yield" class="js-item-name">США 10-летние</a></p>
<p id="TSB__summary_last_23705" class="arial_24 bold inlineblock js-item-last pid-23705-last_nColor"></p>
<p id="TSB__summary_change_23705" class="change js-item-change Font">
<span id="TSB__summary_change_value_23705" class="js-change-value pid-23705-pc"></span> <span id="TSB__summary_change_percent_23705" class="js-change-percent pid-23705-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_23705" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_23705"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_23705" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_23705" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_23705" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_23705" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_23706" data-pairid="23706" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_23706" class="arrowBox">
<span id="TSB__arrowBoxSpan_23706" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_23706" class="arial_14 bold" title="США 30-летние (Нью-Йорк)"><a href="/rates-bonds/u.s.-30-year-bond-yield" class="js-item-name">США 30-летние</a></p>
<p id="TSB__summary_last_23706" class="arial_24 bold inlineblock js-item-last pid-23706-last_nColor"></p>
<p id="TSB__summary_change_23706" class="change js-item-change Font">
<span id="TSB__summary_change_value_23706" class="js-change-value pid-23706-pc"></span> <span id="TSB__summary_change_percent_23706" class="js-change-percent pid-23706-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_23706" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_23706"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_23706" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_23706" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_23706" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_23706" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8895" data-pairid="8895" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8895" class="arrowBox">
<span id="TSB__arrowBoxSpan_8895" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8895" class="arial_14 bold" title="Фьючерс на Euro Bund"><a href="/rates-bonds/euro-bund" class="js-item-name">Euro Bund</a></p>
<p id="TSB__summary_last_8895" class="arial_24 bold inlineblock js-item-last pid-8895-last_nColor"></p>
<p id="TSB__summary_change_8895" class="change js-item-change Font">
<span id="TSB__summary_change_value_8895" class="js-change-value pid-8895-pc"></span> <span id="TSB__summary_change_percent_8895" class="js-change-percent pid-8895-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8895" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8895"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8895" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8895" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8895" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8895" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8880" data-pairid="8880" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8880" class="arrowBox">
<span id="TSB__arrowBoxSpan_8880" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8880" class="arial_14 bold" title="Фьючерс на US 10 YR T-Note"><a href="/rates-bonds/us-10-yr-t-note" class="js-item-name">US 10 YR T-Note</a></p>
<p id="TSB__summary_last_8880" class="arial_24 bold inlineblock js-item-last pid-8880-last_nColor"></p>
<p id="TSB__summary_change_8880" class="change js-item-change Font">
<span id="TSB__summary_change_value_8880" class="js-change-value pid-8880-pc"></span> <span id="TSB__summary_change_percent_8880" class="js-change-percent pid-8880-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8880" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8880"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8880" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8880" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8880" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8880" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8986" data-pairid="8986" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8986" class="arrowBox">
<span id="TSB__arrowBoxSpan_8986" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8986" class="arial_14 bold" title="Фьючерс на Japan Govt. Bond"><a href="/rates-bonds/japan-govt.-bond" class="js-item-name">Japan Govt. Bond</a></p>
<p id="TSB__summary_last_8986" class="arial_24 bold inlineblock js-item-last pid-8986-last_nColor"></p>
<p id="TSB__summary_change_8986" class="change js-item-change Font">
<span id="TSB__summary_change_value_8986" class="js-change-value pid-8986-pc"></span> <span id="TSB__summary_change_percent_8986" class="js-change-percent pid-8986-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8986" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8986"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8986" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8986" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8986" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8986" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_8886" data-pairid="8886" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_8886" class="arrowBox">
<span id="TSB__arrowBoxSpan_8886" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_8886" class="arial_14 bold" title="Фьючерс на UK Gilt"><a href="/rates-bonds/uk-gilt" class="js-item-name">UK Gilt</a></p>
<p id="TSB__summary_last_8886" class="arial_24 bold inlineblock js-item-last pid-8886-last_nColor"></p>
<p id="TSB__summary_change_8886" class="change js-item-change Font">
<span id="TSB__summary_change_value_8886" class="js-change-value pid-8886-pc"></span> <span id="TSB__summary_change_percent_8886" class="js-change-percent pid-8886-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_8886" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_8886"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_8886" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_8886" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_8886" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_8886" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_504" data-pairid="504" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_504" class="arrowBox">
<span id="TSB__arrowBoxSpan_504" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_504" class="arial_14 bold" title="SPDR Dow Jones Industrial Average (Нью-Йорк)"><a href="/etfs/diamonds-trust" class="js-item-name">SPDR DJIA</a></p>
<p id="TSB__summary_last_504" class="arial_24 bold inlineblock js-item-last pid-504-last_nColor"></p>
<p id="TSB__summary_change_504" class="change js-item-change Font">
<span id="TSB__summary_change_value_504" class="js-change-value pid-504-pc"></span> <span id="TSB__summary_change_percent_504" class="js-change-percent pid-504-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_504" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_504"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_504" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_504" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_504" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_504" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_44554" data-pairid="44554" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_44554" class="arrowBox">
<span id="TSB__arrowBoxSpan_44554" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_44554" class="arial_14 bold" title="iShares Russell 1000 Growth (Нью-Йорк)"><a href="/etfs/ishares-russell-1000-growth" class="js-item-name">iShares Russell 1000 Growth</a></p>
<p id="TSB__summary_last_44554" class="arial_24 bold inlineblock js-item-last pid-44554-last_nColor"></p>
<p id="TSB__summary_change_44554" class="change js-item-change Font">
<span id="TSB__summary_change_value_44554" class="js-change-value pid-44554-pc"></span> <span id="TSB__summary_change_percent_44554" class="js-change-percent pid-44554-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_44554" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_44554"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_44554" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_44554" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_44554" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_44554" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_14202" data-pairid="14202" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_14202" class="arrowBox">
<span id="TSB__arrowBoxSpan_14202" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_14202" class="arial_14 bold" title="iShares Russell 2000 (Нью-Йорк)"><a href="/etfs/ishares-russell-2000-index-etf" class="js-item-name">iShares Russell 2000</a></p>
<p id="TSB__summary_last_14202" class="arial_24 bold inlineblock js-item-last pid-14202-last_nColor"></p>
<p id="TSB__summary_change_14202" class="change js-item-change Font">
<span id="TSB__summary_change_value_14202" class="js-change-value pid-14202-pc"></span> <span id="TSB__summary_change_percent_14202" class="js-change-percent pid-14202-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_14202" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_14202"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_14202" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_14202" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_14202" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_14202" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_651" data-pairid="651" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_651" class="arrowBox">
<span id="TSB__arrowBoxSpan_651" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_651" class="arial_14 bold" title="Invesco QQQ Trust Series 1 (NASDAQ)"><a href="/etfs/powershares-qqqq" class="js-item-name">Invesco QQQ Trust Series 1</a></p>
<p id="TSB__summary_last_651" class="arial_24 bold inlineblock js-item-last pid-651-last_nColor"></p>
<p id="TSB__summary_change_651" class="change js-item-change Font">
<span id="TSB__summary_change_value_651" class="js-change-value pid-651-pc"></span> <span id="TSB__summary_change_percent_651" class="js-change-percent pid-651-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_651" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_651"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_651" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_651" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_651" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_651" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_525" data-pairid="525" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_525" class="arrowBox">
<span id="TSB__arrowBoxSpan_525" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_525" class="arial_14 bold" title="SPDR S&P 500 (Нью-Йорк)"><a href="/etfs/spdr-s-p-500" class="js-item-name">SPDR S&P 500</a></p>
<p id="TSB__summary_last_525" class="arial_24 bold inlineblock js-item-last pid-525-last_nColor"></p>
<p id="TSB__summary_change_525" class="change js-item-change Font">
<span id="TSB__summary_change_value_525" class="js-change-value pid-525-pc"></span> <span id="TSB__summary_change_percent_525" class="js-change-percent pid-525-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_525" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_525"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_525" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_525" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_525" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_525" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_14210" data-pairid="14210" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_14210" class="arrowBox">
<span id="TSB__arrowBoxSpan_14210" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_14210" class="arial_14 bold" title="ProShares UltraShort S&P500 (Нью-Йорк)"><a href="/etfs/proshares-ultrashort-s-p500" class="js-item-name">ProShares UltraShort S&P500</a></p>
<p id="TSB__summary_last_14210" class="arial_24 bold inlineblock js-item-last pid-14210-last_nColor"></p>
<p id="TSB__summary_change_14210" class="change js-item-change Font">
<span id="TSB__summary_change_value_14210" class="js-change-value pid-14210-pc"></span> <span id="TSB__summary_change_percent_14210" class="js-change-percent pid-14210-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_14210" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_14210"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_14210" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_14210" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_14210" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_14210" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div id="TSB_main_summary_14206" data-pairid="14206" class="displayNone js-pair-summary-box" rel=""><div id="TSB_arrowBox_14206" class="arrowBox">
<span id="TSB__arrowBoxSpan_14206" class="newSiteIconsSprite upArrow float_lang_base_1 js-item-arrow"></span>
<p id="TSB__summary_name_14206" class="arial_14 bold" title="ProShares UltraShort QQQ (Нью-Йорк)"><a href="/etfs/proshares-ultrashort-qqq-etf" class="js-item-name">ProShares UltraShort QQQ</a></p>
<p id="TSB__summary_last_14206" class="arial_24 bold inlineblock js-item-last pid-14206-last_nColor"></p>
<p id="TSB__summary_change_14206" class="change js-item-change Font">
<span id="TSB__summary_change_value_14206" class="js-change-value pid-14206-pc"></span> <span id="TSB__summary_change_percent_14206" class="js-change-percent pid-14206-pcp parentheses">%</span>
</p>
</div>

<div id="TSB__data_summary_14206" class="dataBox summary" data-section-type="summary">
<p class="float_lang_base_1 bold">Резюме</p>
<span class="js-item-summary studySummaryOval neutral arial_12 bold float_lang_base_2" id="TSB__technical_summary_14206"></span>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="movingAverages">
<p class="float_lang_base_1 nameText">Скол. средние:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ma_buy_14206" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ma_sell_14206" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div>
<div class="dataBox" data-section-type="indicators">
<p class="float_lang_base_1 nameText">Индикаторы:</p>
<p class="float_lang_base_1 buyText">Покупать <span id="TSB___ti_buy_14206" data-counter-type="buy" class="parentheses"></span></p>
<p class="float_lang_base_2 sellText">Продавать <span id="TSB___ti_sell_14206" data-counter-type="sell" class="parentheses"></span></p>
<div class="clear"></div>
</div></div><div class=" js-inner-table" data-tab-id="1"  id="TSB_1_inner" pair_ID="1" rel="inner"  data-gae='tsb_fx' ><table rel="pairTable" class="genTbl openTbl quotesSideBlockTbl collapsedTbl">
<tbody>
<tr id="TSB_pair_1" data-pairid="1" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="EUR/USD - Евро Доллар США (CFD)"><a href="/currencies/eur-usd" data-gae='tsb_fx-eur-usd' >EUR/USD</a></td>
<td class="lastNum js-item-last pid-1-last">1,1593</td>
<td class="chg bold js-item-summary left textNum redFont">Активно продавать</td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_1691" data-pairid="1691" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite redClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="EUR/RUB - Евро Российский рубль (CFD)"><a href="/currencies/eur-rub" data-gae='tsb_fx-eur-rub' >EUR/RUB</a></td>
<td class="lastNum js-item-last pid-1691-last">76,0865</td>
<td class="chg bold js-item-summary left textNum greenFont">Активно покупать</td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_2186" data-pairid="2186" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite redClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="USD/RUB - Доллар США Российский рубль (CFD)"><a href="/currencies/usd-rub" data-gae='tsb_fx-usd-rub' >USD/RUB</a></td>
<td class="lastNum js-item-last pid-2186-last">65,5503</td>
<td class="chg bold js-item-summary left textNum greenFont">Активно покупать</td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_2" data-pairid="2" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="GBP/USD - Британский фунт Доллар США (CFD)"><a href="/currencies/gbp-usd" data-gae='tsb_fx-gbp-usd' >GBP/USD</a></td>
<td class="lastNum js-item-last pid-2-last">1,3035</td>
<td class="chg bold js-item-summary left textNum greenFont">Активно покупать</td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_3" data-pairid="3" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="USD/JPY - Доллар США Японская иена (CFD)"><a href="/currencies/usd-jpy" data-gae='tsb_fx-usd-jpy' >USD/JPY</a></td>
<td class="lastNum js-item-last pid-3-last">113,94</td>
<td class="chg bold js-item-summary left textNum greenFont">Активно покупать</td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_9" data-pairid="9" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="EUR/JPY - Евро Японская иена (CFD)"><a href="/currencies/eur-jpy" data-gae='tsb_fx-eur-jpy' >EUR/JPY</a></td>
<td class="lastNum js-item-last pid-9-last">132,09</td>
<td class="chg bold js-item-summary left textNum neutralFont">Нейтрально</td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_5" data-pairid="5" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="AUD/USD - Австралийский доллар Доллар США (CFD)"><a href="/currencies/aud-usd" data-gae='tsb_fx-aud-usd' >AUD/USD</a></td>
<td class="lastNum js-item-last pid-5-last">0,7213</td>
<td class="chg bold js-item-summary left textNum neutralFont">Нейтрально</td>
<td class="sideColumn">&nbsp;</td>
</tr>
</tbody>
</table>
</div><div class="displayNone js-inner-table" data-tab-id="2"  id="TSB_2_inner" pair_ID="8830" rel="inner"  data-gae='tsb_commodities' ><table rel="pairTable" class="genTbl openTbl quotesSideBlockTbl collapsedTbl">
<tbody>
<tr id="TSB_pair_8830" data-pairid="8830" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="Фьючерс на золото (CFD)"><a href="/commodities/gold" data-gae='tsb_commodities-gold' >Золото</a></td>
<td class="lastNum js-item-last pid-8830-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_8836" data-pairid="8836" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="Фьючерс на серебро (CFD)"><a href="/commodities/silver" data-gae='tsb_commodities-silver' >Серебро</a></td>
<td class="lastNum js-item-last pid-8836-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_8833" data-pairid="8833" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="Фьючерс на нефть Brent (CFD)"><a href="/commodities/brent-oil" data-gae='tsb_commodities-brent-oil' >Нефть Brent</a></td>
<td class="lastNum js-item-last pid-8833-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_8849" data-pairid="8849" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="Фьючерсы на нефть WTI (CFD)"><a href="/commodities/crude-oil" data-gae='tsb_commodities-crude-oil' >Нефть WTI</a></td>
<td class="lastNum js-item-last pid-8849-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_8883" data-pairid="8883" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="Фьючерс на палладий (CFD)"><a href="/commodities/palladium" data-gae='tsb_commodities-palladium' >Палладий</a></td>
<td class="lastNum js-item-last pid-8883-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_8831" data-pairid="8831" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="Фьючерс на медь (CFD)"><a href="/commodities/copper" data-gae='tsb_commodities-copper' >Медь</a></td>
<td class="lastNum js-item-last pid-8831-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_8910" data-pairid="8910" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="Фьючерс на платину (CFD)"><a href="/commodities/platinum" data-gae='tsb_commodities-platinum' >Платина</a></td>
<td class="lastNum js-item-last pid-8910-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
</tbody>
</table>
</div><div class="displayNone js-inner-table" data-tab-id="3"  id="TSB_3_inner" pair_ID="13665" rel="inner"  data-gae='tsb_indices' ><table rel="pairTable" class="genTbl openTbl quotesSideBlockTbl collapsedTbl">
<tbody>
<tr id="TSB_pair_13665" data-pairid="13665" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite redClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="РТС (Москва)"><a href="/indices/rtsi" data-gae='tsb_indices-rtsi' >РТС</a></td>
<td class="lastNum js-item-last pid-13665-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_13666" data-pairid="13666" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite redClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="Индекс МосБиржи (Москва)"><a href="/indices/mcx" data-gae='tsb_indices-mcx' >Индекс МосБиржи</a></td>
<td class="lastNum js-item-last pid-13666-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_172" data-pairid="172" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite redClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="DAX (Xetra) (CFD)"><a href="/indices/germany-30" data-gae='tsb_indices-germany-30' >DAX</a></td>
<td class="lastNum js-item-last pid-172-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_27" data-pairid="27" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite redClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="FTSE 100 (Лондон) (CFD)"><a href="/indices/uk-100" data-gae='tsb_indices-uk-100' >FTSE 100</a></td>
<td class="lastNum js-item-last pid-27-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_166" data-pairid="166" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite redClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="S&P 500 (Нью-Йорк) (CFD)"><a href="/indices/us-spx-500" data-gae='tsb_indices-us-spx-500' >S&P 500</a></td>
<td class="lastNum js-item-last pid-166-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_169" data-pairid="169" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite redClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="Dow Jones Industrial Average (Нью-Йорк)"><a href="/indices/us-30" data-gae='tsb_indices-us-30' >Dow 30</a></td>
<td class="lastNum js-item-last pid-169-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_178" data-pairid="178" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite redClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="Nikkei 225 (Токио) (CFD)"><a href="/indices/japan-ni225" data-gae='tsb_indices-japan-ni225' >Nikkei 225</a></td>
<td class="lastNum js-item-last pid-178-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
</tbody>
</table>
</div><div class="displayNone js-inner-table" data-tab-id="4"  id="TSB_4_inner" pair_ID="945629" rel="inner"  data-gae='tsb_stocks' ><table rel="pairTable" class="genTbl openTbl quotesSideBlockTbl collapsedTbl">
<tbody>
<tr id="TSB_pair_945629" data-pairid="945629" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="BTC/USD - Биткойн Доллар США (CFD)"><a href="/crypto/bitcoin/btc-usd" data-gae='tsb_stocks-btc-usd' >BTC/USD</a></td>
<td class="lastNum js-item-last pid-945629-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_997650" data-pairid="997650" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="ETH/USD - Эфириум Доллар США (CFD)"><a href="/crypto/ethereum/eth-usd" data-gae='tsb_stocks-eth-usd' >ETH/USD</a></td>
<td class="lastNum js-item-last pid-997650-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_1031333" data-pairid="1031333" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="BCH/USD - Bitcoin Cash Доллар США (CFD)"><a href="/crypto/bitcoin-cash/bch-usd" data-gae='tsb_stocks-bch-usd' >BCH/USD</a></td>
<td class="lastNum js-item-last pid-1031333-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_1010798" data-pairid="1010798" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="LTC/USD - Лайткоин Доллар США (CFD)"><a href="/crypto/litecoin/ltc-usd" data-gae='tsb_stocks-ltc-usd' >LTC/USD</a></td>
<td class="lastNum js-item-last pid-1010798-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_1031068" data-pairid="1031068" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="IOTA/USD - IOTA Доллар США (CFD)"><a href="/crypto/iota/iota-usd" data-gae='tsb_stocks-iota-usd' >IOTA/USD</a></td>
<td class="lastNum js-item-last pid-1031068-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_1031678" data-pairid="1031678" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="BTC/RUB - Биткойн Российский рубль (CFD)"><a href="/crypto/bitcoin/btc-rub" data-gae='tsb_stocks-btc-rub' >BTC/RUB</a></td>
<td class="lastNum js-item-last pid-1031678-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_1010782" data-pairid="1010782" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="XRP/USD - Рипл Доллар США (CFD)"><a href="/crypto/ripple/xrp-usd" data-gae='tsb_stocks-xrp-usd' >XRP/USD</a></td>
<td class="lastNum js-item-last pid-1010782-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
<tr id="TSB_pair_1010785" data-pairid="1010785" class="LeftLiContainer js-item-row">
<td class="sideColumn">&nbsp;</td>
<td class="icon js-item-icon"><span class="newSiteIconsSprite greenClockIcon">&nbsp;</span></td>
<td class="left first noWrap" title="DASH/USD - Дэш Доллар США (CFD)"><a href="/crypto/dash/dash-usd" data-gae='tsb_stocks-dash-usd' >DASH/USD</a></td>
<td class="lastNum js-item-last pid-1010785-last"></td>
<td class="chg bold js-item-summary left textNum neutralFont"></td>
<td class="sideColumn">&nbsp;</td>
</tr>
</tbody>
</table>
</div>		</div>
</div>
<script>
$(window).trigger('appendNewData',[["pid-eu-2186:","pid-eu-945629:","pid-eu-997650:","pid-eu-1010782:","pid-eu-1031333:","pid-eu-1024821:","pid-eu-2148:","pid-eu-1656:","pid-eu-8833:","pid-eu-8849:","pid-eu-8830:","pid-eu-8836:","pid-eu-8910:","pid-eu-8883:","pid-eu-8862:","pid-eu-49768:","pid-eu-1:","pid-eu-1691:","pid-eu-2:","pid-eu-3:","pid-eu-9:","pid-eu-5:","pidTechSumm-2186:","pidTechSumm-945629:","pidTechSumm-997650:","pidTechSumm-1010782:","pidTechSumm-1031333:","pidTechSumm-1024821:","pidTechSumm-2148:","pidTechSumm-1656:","pidTechSumm-1:","pidTechSumm-1691:","pidTechSumm-2:","pidTechSumm-3:","pidTechSumm-9:","pidTechSumm-5:","isOpenPair-8833:","isOpenPair-8849:","isOpenPair-8830:","isOpenPair-8836:","isOpenPair-8910:","isOpenPair-8883:","isOpenPair-8862:","isOpenPair-49768:"]]);
</script><div class="clear " id="user_last_quotes"><div class="sideColumnBox js-recent-quotes"><div class="sideColumnBoxTitle">Просмотренные котировки</div><div id="tab_1_inner"><div id="recent" class="recent"><div class="emptyText lightgrayFont">Котировки, которые вы просматриваете чаще всего, будут автоматически отображаться здесь</div></div></div></div><script>var uri = ''; var sid = '2bdac06a211b2e3feb084a67fe79deb4'; </script></div> <!-- /sideBanner --><div class="clear rightColumnAd scrollingRightBox" id="ad3"><div id='div-gpt-ad-1370187203350-sb3'></div></div> <!-- /sideBanner -->    	            <!-- js -->
<script type="text/javascript">
$(window).bind("load", function() {
scrollng_ad('scrollingRightBox', 'footerWrapper');
});
</script>
</aside><!-- /rightColumn -->

<div class="clear"></div>
<footer id="footerWrapper">
<div>
<div id="findABroker" class="findABrokerWrapper">
<p class="h2LikeTitle"><a href="/brokers/forex-brokers">Найдите брокера</a></p>
<div id="findABrokerTabs">
<div id="findABrokerTabsMenu" class="findABrokerTabsMenu">
<div class="TabContainer noOverflow js-tab-container">
<span class="newSiteIconsSprite arrow"></span>
<ul class="tabsForBox oneLvlTabsWidget js-one-level-tabs " id="findTheBroker">

<li id=fabID_1 class="first selected" ><a href="javascript:void(0);">Форекс</a></li>

<li id=fabID_9 ><a href="javascript:void(0);">Крипто</a></li>

<li id=fabID_2 ><a href="javascript:void(0);">CFDs</a></li>

<li id=fabID_6 class="last" ><a href="javascript:void(0);">Акции</a></li>

</ul>
<span id="lastDummyLI" data-more-cat="Другие категории" class="moreTabOneLvlDummy displayNone"><a href="javascript:void();">Другие категории</a><span class="showMoreDownArrow"></span></span>
</div>

<script>
$(document).ready(function(){
tabsWidgetOnelevel('', '', 'findTheBroker', +'');
});
</script>		</div>
<div id="findABroker_html" class="findABroker">

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=ZSU_YWE2PmdkMWtvbj5kYTdiZTsxNWZsNCMzYTc9ZhpgFzYSMSEwdz9lYT8yOTY_ZGdja2EwMHQ9fWBhYGQ1YGVgP2VhND50ZHJrag==')"><img data-rowid="227387" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/logo_120x60_Capital_Level.png"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=ZSUybGcwZTw1YDw4ZTVhZGA1YjwyNmZsZ3BmNDE7ZRllEjIWYXE3cGM5PGJnaTM4Nzc_PmU0MXUzc2ZnNTE0YWVgMmhnMmUvNSM8PQ==')"><img data-rowid="275017" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/IFX_Investing_logo_120x60.png"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=ZCRlO2I1NG03Yjw4bz81MDdiMW8-OjY8NSJnNTA6NUlnED8bZ3dhJmE7bjBjbTE6NTcwNTZmYCRhITQ1MTVuO2RhZT9iNzR-NyE8PQ==')"><img data-rowid="275256" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/fab_65653538.png"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=NnYxb2I1P2Y2Y2pubj4zNjVgMmw2MjY8YnU1Z2dtZho3QGRAbn4wdzZsbjBkajQ_YGQ_PTJkMHQ9fWdmZmI3YjYzMWtiNz91NiBqaw==')"><img data-rowid="275420" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/fab_34089116.jpg"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=NXU0am45MGkyZzo-NWUyN2I3YT9kYDsxMCdgMjY8ZxtgFzURNiY_eGQ-bDIyPDM4YGRiYDFgNnI3d2dmNTFgNTUwNG5uOzB6MiQ6Ow==')"><img data-rowid="275427" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/libertex_logo_120x60.png"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=NXUxbzViP2Y-azk9bj5hZGA1NmhhZTI4NyBnNWBqYR1iFWFFbn41cmI4PmBnaWVuMTU_N2U0ZiJiIjU0YGQwZTUwMWs1YD91Pig5OA==')"><img data-rowid="275487" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/fab_7127653.png"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=YyM1a245MWhhNDk9N2dkYTVgZjg2MjowPSowYmRuZxtkEzURNCQydTJoaDZhbzU-Y2Y2MmEyNnIwcGJjZmJkMWNmNW9uOzF7YXc5OA==')"><img data-rowid="275545" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/fab_81337736.jpg"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=MHBhP284ZD0zZjo-YDBhZDZjNmg2MjY8ZXJiMDQ-Zxs2QWJGZXU_eDJoOmRiaDQ5PzczMjZhMXUycjAxYGQwZTA1YTtvOmQuMyU6Ow==')"><img data-rowid="233811" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/fab_69621100.jpg"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=YiJlOzRjNG0-azw4YDBlYGcyN2k0MGVvNSJhMzM5MEwwRzQQY3M-eWQ-PWNgbTQ_YmpkYDdkNnI2dm9uYWVvOmJnZT80YTR-Pig8PQ==')"><img data-rowid="245845" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/fab_44227365.gif"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=OHhiPG45Yjs2Yzo-YTFlYGcyM21iZjE7MSZmNGNpMEw5TmFFMiI2cWE7aTduYjY7MzcxMzJkM3dgIGJjMDQ0YTg9YjhuO2IoNiA6Ow==')"><img data-rowid="253420" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/fab_7127653.png"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=OXljPWM0Zj9hNG1pNWU4PWQxPmBlYTc9NCNkNjI4YBwxRjMXYXFjJDNpPmBka2NpPjw_Nj5vZiJnJzMyZ2MyZzk8YzljNmYsYXdtbA==')"><img data-rowid="264297" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/fab_54534147.png"></a>

<a href="javascript:void(0);" onclick="window.open('/jp.php?v2=YiJkOjViZj9iNz46NWVjZjJnP2FkYGdtMSYwYjowM09iFT4aZHQ3cD5kaTdgbjU_Y2pmYTFiOn4wcDMyYGRkMWJnZD41YGYsYnQ-Pw==')"><img data-rowid="274975" src="https://a-invdn-com.akamaized.net/Broker_Button_New/Fab120/fab_44629481.jpg"></a>
</div>
</div>
</div>
<script>
window.FABTabsInitObject = {
dataStr: '{"1":[{"id":"227387","link":"\/jp.php?v2=ZSU_YWE2PmdkMWtvbj5kYTdiZTsxNWZsNCMzYTc9ZhpgFzYSMSEwdz9lYT8yOTY_ZGdja2EwMHQ9fWBhYGQ1YGVgP2VhND50ZHJrag==","img120":"Fab120\/logo_120x60_Capital_Level.png"},{"id":"275017","link":"\/jp.php?v2=ZSUybGcwZTw1YDw4ZTVhZGA1YjwyNmZsZ3BmNDE7ZRllEjIWYXE3cGM5PGJnaTM4Nzc_PmU0MXUzc2ZnNTE0YWVgMmhnMmUvNSM8PQ==","img120":"Fab120\/IFX_Investing_logo_120x60.png"},{"id":"275256","link":"\/jp.php?v2=ZCRlO2I1NG03Yjw4bz81MDdiMW8-OjY8NSJnNTA6NUlnED8bZ3dhJmE7bjBjbTE6NTcwNTZmYCRhITQ1MTVuO2RhZT9iNzR-NyE8PQ==","img120":"Fab120\/fab_65653538.png"},{"id":"275420","link":"\/jp.php?v2=NnYxb2I1P2Y2Y2pubj4zNjVgMmw2MjY8YnU1Z2dtZho3QGRAbn4wdzZsbjBkajQ_YGQ_PTJkMHQ9fWdmZmI3YjYzMWtiNz91NiBqaw==","img120":"Fab120\/fab_34089116.jpg"},{"id":"275427","link":"\/jp.php?v2=NXU0am45MGkyZzo-NWUyN2I3YT9kYDsxMCdgMjY8ZxtgFzURNiY_eGQ-bDIyPDM4YGRiYDFgNnI3d2dmNTFgNTUwNG5uOzB6MiQ6Ow==","img120":"Fab120\/libertex_logo_120x60.png"},{"id":"275487","link":"\/jp.php?v2=NXUxbzViP2Y-azk9bj5hZGA1NmhhZTI4NyBnNWBqYR1iFWFFbn41cmI4PmBnaWVuMTU_N2U0ZiJiIjU0YGQwZTUwMWs1YD91Pig5OA==","img120":"Fab120\/fab_7127653.png"},{"id":"275545","link":"\/jp.php?v2=YyM1a245MWhhNDk9N2dkYTVgZjg2MjowPSowYmRuZxtkEzURNCQydTJoaDZhbzU-Y2Y2MmEyNnIwcGJjZmJkMWNmNW9uOzF7YXc5OA==","img120":"Fab120\/fab_81337736.jpg"},{"id":"233811","link":"\/jp.php?v2=MHBhP284ZD0zZjo-YDBhZDZjNmg2MjY8ZXJiMDQ-Zxs2QWJGZXU_eDJoOmRiaDQ5PzczMjZhMXUycjAxYGQwZTA1YTtvOmQuMyU6Ow==","img120":"Fab120\/fab_69621100.jpg"},{"id":"245845","link":"\/jp.php?v2=YiJlOzRjNG0-azw4YDBlYGcyN2k0MGVvNSJhMzM5MEwwRzQQY3M-eWQ-PWNgbTQ_YmpkYDdkNnI2dm9uYWVvOmJnZT80YTR-Pig8PQ==","img120":"Fab120\/fab_44227365.gif"},{"id":"253420","link":"\/jp.php?v2=OHhiPG45Yjs2Yzo-YTFlYGcyM21iZjE7MSZmNGNpMEw5TmFFMiI2cWE7aTduYjY7MzcxMzJkM3dgIGJjMDQ0YTg9YjhuO2IoNiA6Ow==","img120":"Fab120\/fab_7127653.png"},{"id":"264297","link":"\/jp.php?v2=OXljPWM0Zj9hNG1pNWU4PWQxPmBlYTc9NCNkNjI4YBwxRjMXYXFjJDNpPmBka2NpPjw_Nj5vZiJnJzMyZ2MyZzk8YzljNmYsYXdtbA==","img120":"Fab120\/fab_54534147.png"},{"id":"274975","link":"\/jp.php?v2=YiJkOjViZj9iNz46NWVjZjJnP2FkYGdtMSYwYjowM09iFT4aZHQ3cD5kaTdgbjU_Y2pmYTFiOn4wcDMyYGRkMWJnZD41YGYsYnQ-Pw==","img120":"Fab120\/fab_44629481.jpg"}],"9":[{"id":"275045","link":"\/jp.php?v2=YyM1a2cwMGk3Ym5qbz8zNjNmYz1iZjA6MSY0ZmBqM083QDMXZHQwdzRuazUwPjgzMzMxNWMwOn5hIWNiZmJgNWNmNW9nMjB6NyFubw==","img120":"Fab120\/IFX_Investing_logo_120x60.png"},{"id":"274994","link":"\/jp.php?v2=MnI3aTNkN25jNjk9ZTUxNDNmYz0-OjsxPCtuPGBqYR1lEjAUMyNhJjFrPGIwPmNpZG0yOz5sZyMwcGZnZ2NuOzI3N20zZjd9Y3U5OA==","img120":"Fab120\/fab_44629481.jpg"}],"2":[{"id":"275257","link":"\/jp.php?v2=OHgybDRjP2ZlMDk9YjI0MTVgM21iZjowYHc0ZjA6NEhnEDQQMiI1cjdtO2VgbjM4Pz1lYDdmN3NnJ2NiNDAwZTg9Mmg0YT91ZXM5OA==","img120":"Fab120\/fab_65653538.png"},{"id":"275486","link":"\/jp.php?v2=ZSVlO284Yjs2Yzk9MGAwNWUwZTs0MGJoZ3A3ZTM5N0tkEzURMyM3cDdtaTduYDY9NDAzOzVlM3cwcGNiMDRvOmVgZT9vOmIoNiA5OA==","img120":"Fab120\/fab_7127653.png"},{"id":"275546","link":"\/jp.php?v2=YiJiPGQzYTgxZGxoZDQyNzdiZjgzN2VvZnE1ZzsxM09lEjQQZ3djJDdtaDZnaWNoMzYwNDVlYSVnJ2NiY2dlMGJnYjhkMWErMSdsbQ==","img120":"Fab120\/fab_81337736.jpg"},{"id":"233812","link":"\/jp.php?v2=MnIybGI1N25lMDo-ZjZjZmA1Yz1mYmZsZXJkNjowYR1iFTEVMyMydTBqO2ViaDE8YmphYDZiZSFlJWdmYGRiNzI3MmhiNzd9ZXM6Ow==","img120":"Fab120\/fab_69621100.jpg"},{"id":"245846","link":"\/jp.php?v2=MnIxbzRjNWw2Yzo-YDBkYWQxNGoxNWVvNiE1ZzU_MEw2QTAUbn42cTRubzFlaGNoPzc0MGQ0YiY8fDIzMDRmMzI3MWs0YTV_NiA6Ow==","img120":"Fab120\/fab_44227365.gif"},{"id":"253421","link":"\/jp.php?v2=YiIzbTFmP2ZmM2puMmJiZzFkYT8_OzQ-NCNiMGBqZhpjFGNHNiZmITVvbjBgbGRpMDQ2NGI1ZSEwcG5vYWVlMGJnM2kxZD91ZnBqaw==","img120":"Fab120\/fab_7127653.png"},{"id":"264298","link":"\/jp.php?v2=MnIybG45NG0xZG1pZjY0MWcyNGowNGdtMSZlN2dtZhoyRWZCNSVjJGE7OmQ1Ojc9NTc2Pz9hZyM9fTMyYWVhNDI3MmhuOzR-MSdtbA==","img120":"Fab120\/fab_54534147.png"},{"id":"274976","link":"\/jp.php?v2=OXljPTViPmc1YG5qYjIyNzRhP2FjZ2BqMCduPGRuNEhnEDMXYHAydTRubTMzPTgyPjc_OGQ0M3c2djc2Z2NmMzk8Yzk1YD50NSNubw==","img120":"Fab120\/fab_44629481.jpg"}],"6":[{"id":"275488","link":"\/jp.php?v2=MXFhPzRjZTxkMW5qZzdhZGUwYz03Mzc9MyRvPWZsNUk2QTAUMiI0c2Q-O2U1O2NoNjJhaTJsZSFiIjU0Y2dhNDE0YTs0YWUvZHJubw==","img120":"Fab120\/fab_7127653.png"},{"id":"275547","link":"\/jp.php?v2=YCAzbW45P2Y1YDs_NGRhZD5rZTszN2BqZnEzYWBqZRkzRGZCY3M0cz5kPmBmaDM4MDUwNDVkNHA3dzAxYWVuO2BlM2luOz91NSM7Og==","img120":"Fab120\/fab_81337736.jpg"}]}',
columnsNumber: 6	};
</script>

</div>

<section class="top">
<div class="float_lang_base_1 links">
<div class="title">Investing.com</div>
<ul class="first inlineblock">
<li><a title="Что нового" href="https://ru.investing.com/about-us/whats-new"  >Что нового</a></li>
<li><a title="Моб. приложение" href="https://ru.investing.com/mobile/"  >Моб. приложение</a></li>
<li><a title="Инвестпортфель" href="/portfolio/"  >Инвестпортфель</a></li>
<li><a title="Информеры" href="/webmaster-tools/"  >Информеры</a></li>
</ul>
<ul class="inlineblock">
<li><a title="О нас" href="/about-us/"  >О нас</a></li>
<li><a title="Реклама" href="https://ru.investing.com/about-us/advertise"  >Реклама</a></li>
<li><a title="Партнёрская программа" href="/affiliates"  >Партнёрская программа</a></li>
<li><a title="Свяжитесь с нами" href="/about-us/contact-us" target='_blank' >Свяжитесь с нами</a></li>
</ul>
<div class="clear"></div>
</div>
<div class="float_lang_base_1 mobile">
<img src="https://i-invdn-com.akamaized.net/footer/footerMobile2018.png" alt="" class="mobileCells">
<div class="title">Мобильное приложение</div>
<a href="https://play.google.com/store/apps/details?id=com.fusionmedia.investing&hl=ru-RU&referrer=utm_source%3Dru_footer%26utm_medium%3Dru_footer%26utm_term%3Dru_footer%26utm_content%3Dru_footer%26utm_campaign%3Dru_footer" target="_blank"><img src="https://i-invdn-com.akamaized.net/footer/img4/googleplay_ru.png" alt="Скачать"></a><a href="https://itunes.apple.com/ru/app/financial-markets-stocks-forex/id909998122" target="_blank"><img src="https://i-invdn-com.akamaized.net/footer/img4/appstore_ru.png" alt="App store"></a>        </div>
<div class="float_lang_base_1 social">
<div class="title">Мы в соцсетях</div>
<a target="_blank" href="https://vk.com/investingcom" title='vkontakte' class="vKontakt"></a><a target="_blank" href="https://www.facebook.com/pages/Investingcom-Россия/137592209761994" title='Facebook' class="facebook"></a><a target="_blank" href="https://twitter.com/investingru" title='Twitter' class="twitter"></a>        </div>
<div class="clear"></div>
</section>
<section class="bottom">
<img src="https://i-invdn-com.akamaized.net/logos/footerLogo2.png" alt="Investing.com">
<ul class="float_lang_base_2 clearfix">
<li><a href="/about-us/terms-and-conditions">Правила использования</a></li>
<li><a href="/about-us/privacy-policy">Политика конфиденциальности</a></li>
<li><a href="/about-us/risk-warning">Предупреждение</a></li>
</ul>
<div>&copy; 2007-2018 Fusion Media Limited. Все права зарегистрированы. 18+        </div>
<div class="disclaimer">
<span class="bold">Предупреждение о риске:</span> <b>Fusion Media</b> не несет никакой ответственности за утрату ваших денег в результате того, что вы положились на информацию, содержащуюся на этом сайте, включая данные, котировки, графики и сигналы форекс. Учитывайте высочайший уровень риска, связанный с инвестированием в финансовые рынки. Операции на международном валютном рынке Форекс содержат в себе высокий уровень риска и подходят не всем инвесторам. Торговля или инвестиции в криптовалюты связаны с потенциальными рисками. Цены на криптовалюты чрезвычайно волатильны и могут изменяться под действием разнообразных финансовых новостей, законодательных решений или политических событий. Торговля криптовалютами подходит не всем инвесторам. Прежде чем начать торговать на международной бирже или любом другом финансовом инструменте, включая криптовалюты, вы должны правильно оценить цели инвестирования, уровень своей экспертизы и допустимый уровень риска. Спекулируйте только теми деньгами, которые Вы можете позволить себе потерять.<br>
<b>Fusion Media</b> напоминает вам, что данные, предоставленные на данном сайте, не обязательно даны в режиме реального времени и могут не являться точными. Все цены на акции, индексы, фьючерсы и криптовалюты носят ориентировочный характер и на них нельзя полагаться при торговле. Таким образом, Fusion Media не несет никакой ответственности за любые убытки, которые вы можете понести в результате использования этих данных. </br>
<b>Fusion Media</b> может получать компенсацию от рекламодателей, упоминаемых на страницах издания, основываясь на вашем взаимодействии с рекламой или рекламодателями.<br/>
Версия этого документа на английском языке является определяющей и имеет преимущественную силу в том случае, если возникают разночтения между версиями на английском и русском языках.        </div>
</section>
<div class="ruSocialBtns">
<span>
<script id="top100Counter" type="text/javascript" src="http://counter.rambler.ru/top100.jcn?2907049"></script><noscript><a href="http://top100.rambler.ru/navi/2907049/"><img src="http://counter.rambler.ru/top100.cnt?2907049" alt="Rambler's Top100" border="0" /></a></noscript>			</span>
<span>
<script type="text/javascript">//<![CDATA[
var _tmr = _tmr || [];
_tmr.push({id: "2339218", type: "pageView", start: (new Date()).getTime()});
(function (d, w) {
var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true;
ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }})
(document, window); //]]></script><noscript><div style="position:absolute;left:-10000px;">
<img src="http://top-fwz1.mail.ru/counter?id=2339218;js=na" style="border:0;" height="1" width="1" alt="Рейтинг@Mail.ru" />
</div></noscript>

<!-- Rating@Mail.ru logo -->
<a href="http://top.mail.ru/jump?from=2339218" target=_blank>
<img src="http://top-fwz1.mail.ru/counter?id=2339218;t=395;l=1"
style="border:0;" height="31" width="88" alt="Рейтинг@Mail.ru" /></a>			</span>
</div>
</footer>

<script type="text/javascript" src="https://i-invdn-com.akamaized.net/js/technical-1.0.07.min.js"></script>
<script type="text/javascript" src="https://i-invdn-com.akamaized.net/js/n/search-bar-2.0-1.16.min.js"></script>
<script type="text/javascript" src="https://i-invdn-com.akamaized.net/js/core-highcharts-1.02.min.js"></script>
<script type="text/javascript" src="https://i-invdn-com.akamaized.net/js/jscharts-8.1.12.min.js"></script>

<script type="text/javascript" src="https://i-invdn-com.akamaized.net/js/ads.js"></script>
<script type="text/javascript" src="https://static.doubleclick.net/instream/ad_status.js"></script>

<script type="text/javascript">

if(!Modernizr.csstransitions && ie !== 7) {
// Bind MouseOver effect to Navigation list nodes
$('.navMenuUL > li').bind('mouseenter', function() {
clearTimeout($(this).data('hideTimeout'));
var someelement = this;
var timeoutId = setTimeout(function(){ $('.subTopMenuTwoColumnContainer', someelement).show();$('.arrow', someelement).show();$('.arrow2', someelement).show(); }, 120);
$(someelement).data('showTimeout', timeoutId);
// Bind MouseOut effect to Navigation list nodes
}).bind('mouseleave', function() {
clearTimeout($(this).data('showTimeout'));
var someelement = this;
var timeoutId = setTimeout(function(){ $('.subTopMenuTwoColumnContainer', someelement).hide();$('.arrow', someelement).hide();$('.arrow2', someelement).hide(); }, 120);
$(someelement).data('hideTimeout', timeoutId);
});

// Bind MouseOver effect to Real Time Quotes list nodes
$('#navRealTime').bind('mouseenter', function() {
clearTimeout($(this).data('hideTimeout'));
var someelement = this;
var timeoutId = setTimeout(function(){ $('.SubmenuNav', someelement).show(); }, 120);
$(someelement).data('showTimeout', timeoutId);
// Bind MouseOut effect to Navigation list nodes
}).bind('mouseleave', function() {
clearTimeout($(this).data('showTimeout'));
var someelement = this;
var timeoutId = setTimeout(function(){ $('.SubmenuNav', someelement).hide(); }, 120);
$(someelement).data('hideTimeout', timeoutId);
});

// Bind MouseOver effect to Real Time Quotes list nodes
$('.navRealTimeSubmenu .row').bind('mouseenter', function() {

clearTimeout($(this).data('hideTimeout'));
var someelement = this;
var timeoutId = setTimeout(function(){ $('.navRealTimeSubMenuContainer', someelement).show(); }, 120);
$(someelement).data('showTimeout', timeoutId);
// Bind MouseOut effect to Navigation list nodes
}).bind('mouseleave', function() {
clearTimeout($(this).data('showTimeout'));
var someelement = this;
var timeoutId = setTimeout(function(){ $('.navRealTimeSubMenuContainer', someelement).hide(); }, 120);
$(someelement).data('hideTimeout', timeoutId);
});
}
$('#navRealTime').bind('click', function() {
clearTimeout($(this).data('hideTimeout'));
var someelement = this;
var timeoutId = setTimeout(function(){ $('.SubmenuNav', someelement).show(); }, 120);
$(someelement).data('showTimeout', timeoutId);
});

var _gaq = _gaq || [];
//new
ga('create', 'UA-2555300-10', 'investing.com');
ga('create', 'UA-2555300-55', 'auto', {'name': 'allSitesTracker'});  // ALL sites tracker.

ga('require', 'displayfeatures');
ga('allSitesTracker.set', 'dimension17', getCookie('_ga').substr(6));
ga('set','&uid', 203902335);
ga('set', 'dimension1', 203902335);
ga('send', 'pageview');
ga('allSitesTracker.set','&uid', 203902335);
<!-- Crypto Dimension LD -->
<!--Adblock Check-->

jQuery(document).ready(checkAds());

function checkAds(){

function isIE() {
var ua = window.navigator.userAgent;
var trident = ua.indexOf('Trident/');
var edge = ua.indexOf('Edge/');
var msie = ua.indexOf('MSIE ');
return Math.max(msie,edge,trident)>0;

}

var isIE = isIE();
var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor) && !isIE;
var isFireFox = /Firefox/.test(navigator.userAgent);
var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
var isOneOfThoseBrowsers = [isIE,isChrome,isFireFox,isSafari].indexOf(true)>-1;

var b = 2;

//var hasAdsenseBlock = !!$('#adsense').length;
//var hasShadowDom = (!!$("html")[0].shadowRoot);
//var headerIsShown = ($(".midHeader").height() > 0 && $(".midHeader").css('display') !== 'none');


if (window.google_ad_status == 1){

var adbBlk = getCookie('adbBLk');
if(isOneOfThoseBrowsers && adbBlk>0){
ga('allSitesTracker.set', 'dimension22', '7_Blk_No');
setCookie('adbBLk',7,-1000);
}  

}else{

b = 1;
$('#TopTextAd').css('display','flex');

if(isOneOfThoseBrowsers) {
setCookie('adbBLk',7,1000,'/');
ga('allSitesTracker.set', 'dimension21', '7_Blk_Yes');

}
}

$.ajax({
url: "/jp.php?b="+b+"&k=YGxhZzExNW1iNjs1M2k1PjVhYThlZjA7MztkM2c1ZmpgbjNsNDNlaGJmbTdgPGNqMmAwMj5tOmA1M25rYTdiMmB_YSgxIDU8YmY7MzNlNSk1MmFlZSAwZzN6ZCdnImYzYCUzezRhZT9iaA==&d=1&i=1538374538&e=86131153&lip=web45.forexpros.com&ti=1538374538",
type: 'get',
cache: false,
type: 'GET',
dataType: 'html',
success: function (data) {
console.log('server jp b1 web45.forexpros.com');
}
});

}


ga('allSitesTracker.send', 'pageview');
<!--End Adblock Check-->


</script>


<!-- Yandex.Metrika counter -->
<script type="text/javascript">
var yaParams = {/*Visit parameters here*/};
</script>

<script type="text/javascript">
(function (d, w, c) {
(w[c] = w[c] || []).push(function() {
try {
w.yaCounter23341048 = new Ya.Metrika({id:23341048,
webvisor:true,
clickmap:true,
trackLinks:true,
accurateTrackBounce:true,params:window.yaParams||{ }});
} catch(e) { }
});

var n = d.getElementsByTagName("script")[0],
s = d.createElement("script"),
f = function () { n.parentNode.insertBefore(s, n); };
s.type = "text/javascript";
s.async = true;
s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

if (w.opera == "[object Opera]") {
d.addEventListener("DOMContentLoaded", f, false);
} else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<div><img src="//mc.yandex.ru/watch/23341048" style="position:absolute; left:-9999px;" alt="" /></div>
<!-- /Yandex.Metrika counter -->




<script>
//disabled is a temporary argument for testing purposes 
var BoxesEvents = function($,ga,disabled)
{
var e_stack = []; //each event is pushed to this stack
//this objcet links urls to event types
var boxes = {"side_central_banks":"centralbanks","webmaster_tools":"wmt","Most_Popular_News":"mostpop","broker_promotions":"sb_promotions","marketMoversBoxWrapper":"sb_marketmovers","QBS_2_inner":"sb_indices","QBS_3_inner":"sb_commodities","QBS_1_inner":"sb_forex","QBS_4_inner":"sb_bonds","TSB_1_inner":"tsb_fx","TSB_2_inner":"tsb_commodities","TSB_3_inner":"tsb_indices","TSB_4_inner":"tsb_stocks"};

var resume_click_event = function(ev)
{
if (ev['link']==='') {return;}
window.location.href = ev['link'];
}


var register_events = function()
{
for (var id in boxes)
{
(function(id){
var _$b = $('#'+id);
if (_$b.length < 1) {return;}

//box click event
_$b.mouseup(function(e){
if (e.button === 2) {return true;} //right mouse button
var middleMouse = e.button === 1;
if (!middleMouse) {e.preventDefault();}
var box_event = boxes[id];
e_stack.push({'event':box_event,'link':''});
var ce = null; //current event 
//function creator
var callbackCreate = function(data) {return function() {resume_click_event(data);}}
var hitCallback = null;

while(ce = e_stack.pop())
{
if (ce['link'].substr(0,10) === "javascript") {continue;}//if the link is javascript call ignore it
hitCallback = middleMouse ? function(){} : callbackCreate(ce);
//if a link with no event do nothing
if (ce['event'] == '') {hitCallback(); return true;}

window.ga('t0.send',{
'hitType': 'event',
'eventCategory': 'side-box-events',
'eventAction': ce['event'],
'eventValue' : 1,
'hitCallback': hitCallback //resume click event
}
);
//fallback if ga doesn't work
window.setTimeout(hitCallback,1500);
}
return middleMouse;
});

//link event 
_$b.on('mouseup','a',function(e){
if (e.button === 2) {return true;} //right mouse button
e.preventDefault();
var url = this.getAttribute('href');
var event = this.getAttribute('data-gae');
e_stack.push({'event':'','link':url});
if (event)
{
e_stack.pop();
e_stack.push({'event':event,'link':url});
} 
});

})(id);
}		   	
};

var init = function()
{
if (disabled) {return;}
if (typeof $ !== 'function') {return;}
if (typeof ga !== 'function') {return;}
register_events();
}();
}(window.jQuery,window.ga,false);
</script>



</div><!-- /wrapper -->

<script type='text/javascript'>
if(!window.googletag) {
window.googletag = {cmd: []};
}
googletag.cmd.push(function() {googletag.defineSlot('/6938/FP_RU_site/FP_RU_Sideblock_1_Default', [300, 600], 'div-gpt-ad-1370187203350-sb1').setTargeting('ad_group',Adomik.randomAdGroup()).addService(googletag.pubads());googletag.pubads().setTargeting("Section","technical");googletag.pubads().setTargeting("Charts_Page","no");googletag.pubads().setTargeting("CID",getCookie('_ga').substr(6));googletag.pubads().setTargeting("ad_h",(new Date).getUTCHours());googletag.pubads().setTargeting("Page_URL","/technical/personalized-quotes-technical-summary");googletag.pubads().setTargeting("Traffic_Type","OG");googletag.pubads().setTargeting("User_type","Logged_in");       if(window.outerWidth >= 1630) {
googletag.cmd.push(function() {
googletag.defineSlot('/6938/FP_RU_site/FP_RU_Takeover_Default', [[300, 600], [120, 600]], 'div-gpt-ad-1478019486943-0').setTargeting('ad_group', Adomik.randomAdGroup()).addService(googletag.pubads());
});
}
if(window.outerWidth >= 1340 && window.outerWidth < 1630) {
googletag.cmd.push(function() {
googletag.defineSlot('/6938/FP_RU_site/FP_RU_Takeover_Default', [[160, 600], [120, 600]], 'div-gpt-ad-1478019486943-0').setTargeting('ad_group', Adomik.randomAdGroup()).addService(googletag.pubads());
});
}

googletag.enableServices();
});    if (sponsoredArticle) {
googletag.cmd.push(function() { googletag.display(sponsoredArticle); });
}
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1370187203350-sb1'); });</script><script type='text/javascript'>
$(document).ready(function () {
googletag.cmd.push(function() {
googletag.enableServices();
});
window.loadSideBlockAd3 = function () { var sb3 = googletag.defineSlot('/6938/FP_RU_site/FP_RU_Sideblock_3_viewable',  [[300, 250],[300, 600]], 'div-gpt-ad-1370187203350-sb3').setTargeting('ad_group',Adomik.randomAdGroup()).addService(googletag.pubads());
googletag.cmd.push(function() {
if(typeof(refreshSlot)=="function"){ refreshSlot(sb3); } else{ googletag.display('div-gpt-ad-1370187203350-sb3'); }
}); }});
if(window.outerWidth >= 1340) {
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1478019486943-0'); });
}
</script><script type='text/javascript'>

if (typeof(checkBigAd) === 'function') {
checkBigAd();
}
if (typeof(addSakindo) === 'function') {
addSakindo();
}

(function () {
$(window).scroll($.throttle(function () {
if (window.loadSideBlockAd3 && $.isInView('#ad3', true, 20)) {
if (typeof(googletag.defineSlot) === 'function') {
window.loadSideBlockAd3();
window.loadSideBlockAd3 = null;
}
}
}, 16))
})()
</script>



</body>

<script>var TimeZoneID = +"18";window.timezoneOffset = +"10800";window.stream = "https://stream21.forexpros.com:443";</script><script>window.uid = 203902335</script><script>

$(function(){
$(window).trigger("socketRetry",[["pid-eu-2186:","pid-eu-945629:","pid-eu-997650:","pid-eu-1010782:","pid-eu-1031333:","pid-eu-1024821:","pid-eu-2148:","pid-eu-1656:","pid-eu-8833:","pid-eu-8849:","pid-eu-8830:","pid-eu-8836:","pid-eu-8910:","pid-eu-8883:","pid-eu-8862:","pid-eu-49768:","pid-eu-1:","pid-eu-1691:","pid-eu-2:","pid-eu-3:","pid-eu-9:","pid-eu-5:","pidTechSumm-2186:","pidTechSumm-945629:","pidTechSumm-997650:","pidTechSumm-1010782:","pidTechSumm-1031333:","pidTechSumm-1024821:","pidTechSumm-2148:","pidTechSumm-1656:","pidTechSumm-1:","pidTechSumm-1691:","pidTechSumm-2:","pidTechSumm-3:","pidTechSumm-9:","pidTechSumm-5:","isOpenPair-8833:","isOpenPair-8849:","isOpenPair-8830:","isOpenPair-8836:","isOpenPair-8910:","isOpenPair-8883:","isOpenPair-8862:","isOpenPair-49768:","domain-7:"]]);
});
</script><script>function refresher() { void (0);}</script><script>
var _comscore = _comscore || [];
_comscore.push(
{ c1: "2", c2: "16896267" }

);
(function()
{ var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true; s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js"; el.parentNode.insertBefore(s, el); }

)();
</script>		<script type="text/javascript">
var google_conversion_id = 1000940071;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
</script><script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
<noscript><div style="display:inline;"><img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1000940071/?value=0&amp;guid=ON&amp;script=0"/></div></noscript>

<!-- tns-counter.ru -->
<script type="text/javascript">
(function(win, doc, cb){
(win[cb] = win[cb] || []).push(function() {
try {
tnsCounterRuinvesting_com = new TNS.TnsCounter({
'account':'ruinvesting_com',
'tmsec': 'ruinvesting_total'
    				});
} catch(e){}
});

var tnsscript = doc.createElement('script');
tnsscript.type = 'text/javascript';
tnsscript.async = true;
tnsscript.src = ('https:' == doc.location.protocol ? 'https:' : 'http:') +
'//www.tns-counter.ru/tcounter.js';
var s = doc.getElementsByTagName('script')[0];
s.parentNode.insertBefore(tnsscript, s);
})(window, this.document,'tnscounter_callback');
</script>

<script type="text/javascript">
$(document).ready(function () {
window._qevents = window._qevents || [];

(function() {
var elem = document.createElement('script');
elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
elem.async = true;
elem.type = "text/javascript";
var scpt = document.getElementsByTagName('script')[0];
scpt.parentNode.insertBefore(elem, scpt);
}());

window._qevents.push({
qacct:"p-WSPjmcyyuDvN0"
});
});
</script>
<noscript>
<div style="display:none;">
<img src="//pixel.quantserve.com/pixel/p-WSPjmcyyuDvN0.gif" border="0" height="1" width="1" alt="Quantcast"/>
</div>
</noscript>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function()
{n.callMethod? n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '751110881643258');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=751110881643258&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

<script type="text/javascript">
fbq = window.fbq || $.noop;
</script>

</html>`
