package database

const (
	//NOTIFY_CANDLE_MODEL_CHAN - канал, по которому база данных уведомляет стратега,
	//когда в базе появляется/изменяется свечная модель
	NOTIFY_CANDLE_MODEL_CHAN = `candle_models_chan`
)
