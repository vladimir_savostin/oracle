package main

import (
	"github.com/astaxie/beego/logs"
	"github.com/go-pg/migrations"
)

func init() {
	migrations.Register(func(db migrations.DB) error {
		logs.Warn("create table analysed signals...")
		_, err := db.Exec(create_table_analysed_signals)
		return err
	}, func(db migrations.DB) error {
		logs.Warn("drop table analysed signals...")
		_, err := db.Exec(drop_table_analysed_signals)
		return err
	})
}

const create_table_analysed_signals = `
CREATE TABLE "analysed_signals" (
	"id" serial NOT NULL,
	"instrument" varchar NOT NULL,
	"price" varchar NOT NULL,
	"time_frame" varchar NOT NULL,
	"reliability" integer NOT NULL,
	"model_name" varchar NOT NULL,
	"candle_number" varchar NOT NULL,
	"description" varchar NOT NULL,
	"date" varchar NOT NULL,
	"indicators" varchar NOT NULL,
	"moving_averages" varchar NOT NULL,
	"indicators_summary" varchar NOT NULL,
	"strategy" varchar NOT NULL,
	"current_candle" integer,
	"candle1" jsonb,
	"candle2" jsonb,
	"candle3" jsonb,
	"candle4" jsonb,
	"candle5" jsonb,
	"candle6" jsonb,
	"candle7" jsonb,
	"candle8" jsonb,
	"candle9" jsonb,
	"candle10" jsonb,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	"is_done" boolean,
	CONSTRAINT analysed_signals_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);
`

const drop_table_analysed_signals = `
DROP TABLE IF EXISTS "analysed_signals";
`
