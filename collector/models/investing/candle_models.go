package investing

import "strings"

//CandleModel - структура, содержащая всю информацию о конкрентной свечной модели для конкретного инструмента,
//содержащейся на сайте-источнике
type CandleModel struct {
	Instrument   string //Название инструмента
	TimeFrame    string //Временной интервал свечи (н-р 1H, 1D и т.д.)
	Reliability  int    //Надежность сигнала (от 1 до 3)
	ModelName    string //Имя свечной модели (н-р "Shooting star")
	CandleNumber string //Номер свечи
	Description  string //Словесное описание иконки тренда (н-р "Медвежий разворот")
	Date         string `json:"Date,omitempty"` //дата формирования завершенной модели
}

//CheckInstrumentField - приводит поле инструмент к стандартному виду, при необходимости;
//Т.к. бывает что инструмент на разнах страницах называется по разному
//(например: цена на серебро в свечных моделях отображдается как "серебро/USD", а в таблице индикаторов как XAG/USD)
func (m *CandleModel) CheckInstrumentField() {
	if strings.HasPrefix(m.Instrument, "Серебро") { //если это Серебро - меняем на XAG
		m.Instrument = strings.Replace(m.Instrument, "Серебро", "XAG", 1)
	}
	if strings.HasPrefix(m.Instrument, "American Airlines.") { //если это American Airlines. (с точкой) - меняем на American Airlines (без точки)
		m.Instrument = strings.Replace(m.Instrument, "American Airlines.", "American Airlines", 1)
	}
}

//ClearData - приводит все значения к исходному для каждого типа
func (m *CandleModel) ClearData() {
	m.Instrument = ""
	m.TimeFrame = ""
	m.Reliability = 0
	m.ModelName = ""
	m.CandleNumber = ""
	m.Description = ""
	m.Date = ""
}
