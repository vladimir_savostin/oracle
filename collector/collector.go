package collector

import (
	"github.com/jinzhu/copier"
	"oracle/collector/models/investing"
	"oracle/database"
	"sync"
	"time"
)

//Work - основной цикл работы коллектора, сбор информации
//завершается при получении от телеграм-бота сигнала из канала shutdown
func (collector *Collector) Work(shutdown chan bool) {
	defer collector.CloseDB()
	go func() { //запуск основного цикла работы в отдельной горутине
		for {
			collector.wg = &sync.WaitGroup{}
			collector.wg.Add(3) //увеличиваем дельту на число горутин
			go collector.SearchPrices()
			go collector.SearchIndicators()
			go collector.SearchCandleModels()
			collector.wg.Wait()              //как только все горутины закончат
			collector.Ready <- true          //отправляем сигнал стратегу о том, что коллектор обновил БД
			time.Sleep(collector.workPeriod) //засыпаем до следующего обновления БД
		}
	}()
	<-shutdown //ожидание сигнала на остановку сервиса
}

//SearchCandleModels в цикле ищет свечные модели на investing.com
//находит среди них новые (те, которых еще нет в базе)
//сохраняет совокупную информацию о свечной модели в базу данных
//удаляет устаревшие свечные модели
func (collector *Collector) SearchCandleModels() {
	var allCandleModels []investing.CandleModel
	for _, parser := range collector.Parsers { //пробегаем по всем парсерам
		candleModels, err := parser.ParseCandleModels() //и ищем свенчые модели
		collector.checkError(err)
		for _, data := range candleModels {
			data.CheckInstrumentField() //приводим поле instrument к стандартному виду, при необходимости
			err := collector.proceedCandleModel(data)
			collector.checkError(err)
			allCandleModels = append(allCandleModels, data) //добавляем модели по одной, вместо append(allCandleModels, candleModels...)
			//т.к. мы можем изменить data в методе data.CheckInstrumentField() и это не коснется моделей в срезе candleModels
		}
	}
	collector.clearOldCandleModels(allCandleModels)
	collector.wg.Done()
}

//proceedCandleModel - производит всю необходимую работу со свечной моделью:
//Проверка, сохранена ли она уже в базу
//Сохранение - если не сохранена
//Обновление данных (в частности номер свечи) при необходимости
func (collector *Collector) proceedCandleModel(candleModel investing.CandleModel) error {
	var m database.CandleModel
	err := copier.Copy(&m, candleModel)
	collector.checkError(err)
	found, err := m.Find(collector.DB)
	collector.checkError(err)
	if found { //если модель уже есть в базе обновим при необходимости
		if m.GetCandleNumber() != candleModel.CandleNumber {
			err := m.SetCandleNumber(collector.DB, candleModel.CandleNumber)
			collector.checkError(err)
		}
	} else { //если модели нет в базе - запишем ее в базу
		err := m.Save(collector.DB)
		collector.checkError(err)
	}
	return nil
}

//SearchPrices в цикле ищет текущие цены на инструменты и динамически обновляет их в базе
func (collector *Collector) SearchPrices() {
	for _, parser := range collector.Parsers { //пробегаем по всем парсерам
		prices, err := parser.ParsePrices()
		collector.checkError(err)
		for _, data := range prices {
			err := collector.proceedPrice(data)
			collector.checkError(err)
		}
	}
	collector.wg.Done()
}

//proceedPrice - производит всю необходимую работу c текущими ценами:
//сохранение/обновление данных
func (collector *Collector) proceedPrice(price investing.Price) error {
	var p database.Price
	err := copier.Copy(&p, price)
	collector.checkError(err)
	err = p.SaveOrUpdate(collector.DB)
	collector.checkError(err)
	return nil
}

//SearchIndicators в цикле ищет индикаторы на investing.com
//обновляет все индикаторы, которые в базе
func (collector *Collector) SearchIndicators() {
	for _, parser := range collector.Parsers { //пробегаем по всем парсерам
		indicators, err := parser.ParseIndicators()
		collector.checkError(err)
		err = collector.proceedIndicators(indicators)
		collector.checkError(err)
	}

	collector.wg.Done()
}

//proceedIndicators - производит всю необходимую работу c индикаторами:
//Сохранение либо обновление таблиц с индикаторами
func (collector *Collector) proceedIndicators(indicators investing.Indicators) error {
	var indicator database.Indicator
	var indicatorMA database.MovingAverage
	var indicatorSum database.IndicatorSummary
	for instrument, innerMap := range indicators { //пробегаем по всем инструментам
		//присваиваем имена инструментов моделям базы данных
		indicator.Instrument = instrument
		indicatorMA.Instrument = instrument
		indicatorSum.Instrument = instrument
		for timeFrame, data := range innerMap { //пробегаем по внутреннему отображению таймфреймов на значения индикаторов
			//заполняем модели
			switch timeFrame { //проверяем таймфрейм и присваиваем опрделенному полю модели значение соответсвующего индикатора
			case database.Minute15:
				indicator.Minute15 = data.Indicators
				indicatorMA.Minute15 = data.MovingAverages
				indicatorSum.Minute15 = data.Summary
			case database.Minute30:
				indicator.Minute30 = data.Indicators
				indicatorMA.Minute30 = data.MovingAverages
				indicatorSum.Minute30 = data.Summary
			case database.Hour1:
				indicator.Hour1 = data.Indicators
				indicatorMA.Hour1 = data.MovingAverages
				indicatorSum.Hour1 = data.Summary
			case database.Hour5:
				indicator.Hour5 = data.Indicators
				indicatorMA.Hour5 = data.MovingAverages
				indicatorSum.Hour5 = data.Summary
			case database.Day:
				indicator.Day = data.Indicators
				indicatorMA.Day = data.MovingAverages
				indicatorSum.Day = data.Summary
			case database.Week:
				indicator.Week = data.Indicators
				indicatorMA.Week = data.MovingAverages
				indicatorSum.Week = data.Summary
			case database.Month:
				indicator.Month = data.Indicators
				indicatorMA.Month = data.MovingAverages
				indicatorSum.Month = data.Summary
			}
		}
		//сохраняем модели в базу
		err := indicator.SaveOrUpdate(collector.DB)
		collector.checkError(err)
		err = indicatorMA.SaveOrUpdate(collector.DB)
		collector.checkError(err)
		err = indicatorSum.SaveOrUpdate(collector.DB)
		collector.checkError(err)
	}
	return nil
}

//clearOldCandleModels - удаляет из базы неактуальные свечные модели (те, которых уже нет на сайте)
//в качестве аргумента принимает список моделей, которые только что были получены с сайта всеми парсерами
func (collector Collector) clearOldCandleModels(models []investing.CandleModel) {
	var dbModels database.CandleModels
	dbModels.FindAll(collector.DB)
	for _, d := range dbModels {
		if !belongsTo(d, models) {
			err := d.Delete(collector.DB)
			collector.checkError(err)
		}
	}
}

//belongsTo - возвращает true если экземпляр d представлен в срезе m
func belongsTo(d database.CandleModel, m []investing.CandleModel) bool {
	for _, data := range m { //сравниваем d с каждым экземпляром m на полную идентичность
		if d.Instrument == data.Instrument &&
			d.TimeFrame == data.TimeFrame &&
			d.ModelName == data.ModelName &&
			d.Date == data.Date &&
			d.Description == data.Description &&
			d.Reliability == data.Reliability &&
			d.CandleNumber == data.CandleNumber {
			return true
		}
	}
	return false
}
