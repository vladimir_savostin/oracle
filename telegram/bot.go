package telegram

import (
	"encoding/json"
	"github.com/astaxie/beego/logs"
	api "gopkg.in/telegram-bot-api.v4"
	"oracle/collector/models"
	"oracle/config"
)

type TlgBot struct {
	bot    *api.BotAPI //сам бот
	chatID int64       //чат в котрый шлются сигналы
}

func NewBot(settings *config.Settings) (b *TlgBot, err error) {
	b = new(TlgBot)
	b.bot, err = api.NewBotAPI(TOKEN)
	b.chatID = settings.Telegram.ChatId
	return b, err
}

//Work - основная функция телеграм-бота
//В ней он принимает сигналы от перформера по каналу signalsChanel
//и отправляет их в телеграм-чат
//Если он принимает команду из чата на выключение сервиса, он отсылает этот сигнал всем сервисам по каналу shutdown
func (b *TlgBot) Work(signalsChanel chan models.Signal, shutdown chan bool) {
	logs.Warn("Started the oracle chat-bot")
	defer logs.Warn("Stopped the oracle chat-bot")
	b.bot.Send(api.NewMessage(b.chatID, "********** oracle is started *************"))
	defer b.bot.Send(api.NewMessage(b.chatID, "*********** oracle is stopped ***********"))
	updatesChanel, err := b.bot.GetUpdatesChan(api.UpdateConfig{})
	if err != nil {
		logs.Error(err)
	}
	for {
		select {
		case signal := <-signalsChanel: //если пришел новый сигнал, отправляем его в чат
			logs.Warn("BOT: new signal: %v", signal)
			msg, err := b.prepareSignalMSG(signal)
			if err != nil {
				logs.Error(err)
				continue
			}
			b.bot.Send(api.NewMessage(b.chatID, msg))
		case update := <-updatesChanel: //если пришло новое сообщение из чата, то выполним одно из действий
			if update.Message.IsCommand() {
				//todo check user id
				switch update.Message.Command() {
				case "help":
					b.bot.Send(api.NewMessage(b.chatID, help_msg))
				case "stop":
				case "start":
				case "shutdown": //выключить все сервисы
					b.bot.Send(api.NewMessage(b.chatID, "shutting down the oracle... please wait..."))
					shutdown <- true
					return
				}
			}
		}

	}
}

//prepareSignalMSG - готовит сигнал к отправке в телеграм-чат
func (b *TlgBot) prepareSignalMSG(signal models.Signal) (msg string, err error) {
	payload, err := json.MarshalIndent(signal, "", "")
	if err != nil {
		return "", err
	}
	return signal.FormatSignalString(string(payload)), nil

}
