package config

import (
	"github.com/tkanos/gonfig"
)

func GetSettings(fileName string) (settings *Settings, err error) {
	settings = new(Settings)
	err = gonfig.GetConf(fileName, settings)
	return
}

//Настройки для оракула
type Settings struct {
	DB        database  `json:"database"`
	Collector collector `json:"collector"`
	Analyst   analyst   `json:"analyst"`
	Telegram  telegram  `json:"telegram"`
}

//настройки базы
type database struct {
	User     string `json:"user"`
	Password string `json:"password"`
	Database string `json:"database"`
}

//настройки коллектора
type collector struct {
	Parsers    []*Parser `json:"parsers"`
	WorkPeriod int       `json:"work_period"` //период работы коллектора в секундах
}

//настройки парсера
type Parser struct {
	MainEmail     string `json:"main_email"`     //основной аккаунт
	MainPassword  string `json:"main_password"`  //пароль от основного аккаунта
	ExtraEmail    string `json:"extra_email"`    //дополнительный аккаунт
	ExtraPassword string `json:"extra_password"` //пароль от дополнительного аккаунта
}

type analyst struct {
	WorkPeriod int `json:"work_period"` //период работы аналитика в секундах
}

//настройки телеграм-бота
type telegram struct {
	ChatId int64 `json:"chat_id"` //id чата, куда шлются сигналы
}

//еуые
