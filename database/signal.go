package database

import (
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"time"
)

//Signals - срез сформированных сигналов
type Signals []Signal

//Signal таблица свормированных сигналов
type Signal struct {
	ID                int64
	Instrument        string //имя биржевого инструмента (н-р "RUB/USD")
	Price             string //текущая цена
	TimeFrame         string //Временной интервал свечи (н-р 1H, 1D и т.д.)
	Reliability       int    //Надежность сигнала (от 1 до 3)
	ModelName         string //Имя свечной модели (н-р "Shooting star")
	CandleNumber      string //Номер свечи
	Description       string //Словесное описание иконки тренда (н-р "Медвежий разворот")
	Date              string //дата формирования завершенной модели
	Indicators        string //Индикаторы
	MovingAverages    string //Скользящие средние
	IndicatorsSummary string //Резюмэ по индикаторам и скользящим средним
	Strategy          string //стратегия которая сформировала сигнал
	CreatedAt         time.Time
}

//Save - сохраняет модель в базу
func (s *Signal) Save(db *pg.DB) error {
	_, err := db.Model(s).Insert()
	if err != nil {
		return err
	}
	return nil
}

//FindAll - возвращает срез всех сигналов из таблицы БД
func (s *Signals) FindAll(db *pg.DB) error {
	return db.Model(s).Select()
}

//Delete - удаляет модель из базы
func (s *Signal) Delete(db *pg.DB) error {
	_, err := db.Model(s).WherePK().Delete()
	if err != nil {
		return err
	}
	return nil
}

//BeforeInsert - действия, проводимые с s перед сохранением в базу
func (s *Signal) BeforeInsert(db orm.DB) error {
	if s.CreatedAt.IsZero() {
		s.CreatedAt = time.Now()
	}
	return nil
}

//Clear - очищает все поля
func (s *Signal) Clear() {
	s.ID = 0
	s.Instrument = ""
	s.Price = ""
	s.TimeFrame = ""
	s.Reliability = 0
	s.ModelName = ""
	s.CandleNumber = ""
	s.Description = ""
	s.Date = ""
	s.Indicators = ""
	s.MovingAverages = ""
	s.IndicatorsSummary = ""
	s.Strategy = ""
	s.CreatedAt = time.Time{}
}
