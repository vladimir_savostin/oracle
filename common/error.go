package common

import "github.com/astaxie/beego/logs"

func CheckError(err error, data ...interface{}) {
	if err != nil {
		logs.Error(err, data)
	}
}
